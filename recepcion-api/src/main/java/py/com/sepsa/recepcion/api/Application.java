/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.api;

import javax.ws.rs.ApplicationPath;

/**
 *
 * @author Fabio A. González Sosa
 */
@ApplicationPath("/rest/api")
public class Application extends javax.ws.rs.core.Application {

    /**
     * Constructor.
     */
    public Application() {
        ;
    }

}
