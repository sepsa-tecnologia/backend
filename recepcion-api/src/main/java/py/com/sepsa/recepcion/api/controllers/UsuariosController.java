/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.api.controllers;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.com.sepsa.recepcion.api.BaseController;
import py.com.sepsa.recepcion.api.controllers.actions.LoginAppAction;

/**
 *
 * @author Fabio A. González Sosa
 */
@Path("/")
@RequestScoped
public class UsuariosController extends BaseController {

    @Inject
    private LoginAppAction loginAppAction;

    @POST
    @Path("/v1/usuarios.loginApp")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response loginApp(String json) {
        return executeControllerAction(loginAppAction, json);
    }

}
