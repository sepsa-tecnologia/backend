/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.api.controllers.actions;

import com.google.gson.JsonObject;
import fa.gs.utils.misc.Type;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import javax.enterprise.context.Dependent;
import javax.ws.rs.core.Response;
import py.com.sepsa.recepcion.api.controllers.DocumentosController;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;
import py.com.sepsa.recepcion.core.logic.Errnos;
import py.com.sepsa.recepcion.core.logic.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
@Dependent
public class DocumentoEstaValidadoAction extends RestControllerActionWithJsonObjectParam<DocumentosController> {

    @Override
    public Response doAction(DocumentosController ctx, JsonObject json) throws Throwable {
        // Obtener parametros.
        Long idDocumento = JsonResolver.get(json, "documento$id", Type.LONG);
        TipoEntidadEnum tipoDocumento = TipoEntidadEnum.adapter().getEnumerable(JsonResolver.get(json, "documento$tipo", Type.STRING));

        // Verificar que documento esté validado.
        py.com.sepsa.recepcion.core.logic.actions.DocumentoEstaValidadoAction documentoEstaValidadoAction = Injection.bean(py.com.sepsa.recepcion.core.logic.actions.DocumentoEstaValidadoAction.class);
        boolean estaValidado = documentoEstaValidadoAction.check(idDocumento, tipoDocumento);
        if (!estaValidado) {
            return ServiceResponse.ko()
                    .cause("El documento no está validado para el proceso de recepción")
                    .errno(Errnos.DOCUMENTO_NO_VALIDADO)
                    .build();
        }

        return ServiceResponse.ok().build();
    }

}
