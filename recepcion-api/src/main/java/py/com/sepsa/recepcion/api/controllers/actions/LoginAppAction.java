/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.api.controllers.actions;

import com.google.gson.JsonObject;
import fa.gs.utils.authentication.user.AuthenticationInfo;
import fa.gs.utils.misc.Type;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import fa.gs.utils.result.simple.Result;
import javax.enterprise.context.Dependent;
import javax.ws.rs.core.Response;
import py.com.sepsa.recepcion.api.authentication.UserCredentialAuthenticator;
import py.com.sepsa.recepcion.api.authentication.UserTokenAuthenticator;
import py.com.sepsa.recepcion.api.controllers.UsuariosController;

/**
 *
 * @author Fabio A. González Sosa
 */
@Dependent
public class LoginAppAction extends RestControllerActionWithJsonObjectParam<UsuariosController> {

    @Override
    public Response doAction(UsuariosController ctx, JsonObject json) throws Throwable {
        // Obtener parametros.
        String username = JsonResolver.get(json, "usuario$username", Type.STRING);
        String password = JsonResolver.get(json, "usuario$password", Type.STRING);

        // Authenticar usuario.
        UserCredentialAuthenticator authenticator = new UserCredentialAuthenticator();
        Result<AuthenticationInfo> resAuth = authenticator.authenticateUserCredentials(username, password);
        if (resAuth.isFailure()) {
            return ServiceResponse.ko()
                    .cause(resAuth)
                    .build();
        } else {
            // Generar token con informacion de autenticacion.
            AuthenticationInfo info = resAuth.value();
            String token = UserTokenAuthenticator.issueToken(info);

            return ServiceResponse.ok()
                    .payload(token)
                    .build();
        }
    }

}
