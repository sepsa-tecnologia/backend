/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.api.controllers.actions;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import fa.gs.utils.adapters.Adapters;
import fa.gs.utils.adapters.Construction;
import fa.gs.utils.adapters.impl.Adapter0;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.Type;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import fa.gs.utils.result.simple.Result;
import java.util.Collection;
import javax.enterprise.context.Dependent;
import javax.ws.rs.core.Response;
import py.com.sepsa.recepcion.api.controllers.DocumentosController;
import py.com.sepsa.recepcion.core.data.adapters.FacturaInfo2JsonAdapter;
import py.com.sepsa.recepcion.core.data.adapters.NotaRemisionInfo2JsonAdapter;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;
import py.com.sepsa.recepcion.core.data.pojos.FacturaInfo;
import py.com.sepsa.recepcion.core.data.pojos.NotaRemisionInfo;
import py.com.sepsa.recepcion.core.logic.Errnos;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerFacturasAction;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerNotasRemisionAction;

/**
 *
 * @author Fabio A. González Sosa
 */
@Dependent
public class DocumentoObtenerAction extends RestControllerActionWithJsonObjectParam<DocumentosController> {

    @Override
    public Response doAction(DocumentosController ctx, JsonObject json) throws Throwable {
        // Obtener parametros.
        Params params = Params.instance(json);

        // Obtener documento, si hubiere.
        switch (params.documentoTipo) {
            case FACTURA:
                return obtenerFactura(params);
            case NOTA_REMISION:
                return obtenerNotaRemision(params);
            default:
                return documentoNoExiste();
        }
    }

    private Response obtenerFactura(Params params) throws Throwable {
        // Obtener facturas.
        ObtenerFacturasAction action = Injection.bean(ObtenerFacturasAction.class);
        Result<Collection<FacturaInfo>> resFacturas = action.obtenerFacturas(params.proveedorRazonSocialORuc, params.documentoNumero);
        resFacturas.raise();
        Collection<FacturaInfo> facturas = resFacturas.value(Lists.empty());

        // Obtener primer elemento.
        FacturaInfo factura = Lists.first(facturas);
        if (factura == null) {
            return documentoNoExiste();
        }

        JsonElement json = Adapters.adapt(FacturaInfo2JsonAdapter.class, factura);
        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

    private Response obtenerNotaRemision(Params params) throws Throwable {
        // Obtener facturas.
        ObtenerNotasRemisionAction action = Injection.bean(ObtenerNotasRemisionAction.class);
        Result<Collection<NotaRemisionInfo>> resNotasRemision = action.obtenerNotasRemision(params.proveedorRazonSocialORuc, params.documentoNumero);
        resNotasRemision.raise();
        Collection<NotaRemisionInfo> notasRemision = resNotasRemision.value(Lists.empty());

        // Obtener primer elemento.
        NotaRemisionInfo notaRemision = Lists.first(notasRemision);
        if (notaRemision == null) {
            return documentoNoExiste();
        }

        JsonElement json = Adapters.adapt(NotaRemisionInfo2JsonAdapter.class, notaRemision);
        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

    private Response documentoNoExiste() {
        return ServiceResponse.ko()
                .cause("Documento no existe")
                .errno(Errnos.DOCUMENTO_NO_EXISTE)
                .build();
    }

    @Construction(strategy = Construction.Strategy.UNSAFE)
    private static class Params extends Adapter0<JsonObject, Params> {

        private String proveedorRazonSocialORuc;
        private TipoEntidadEnum documentoTipo;
        private String documentoNumero;

        static Params instance(JsonObject obj) {
            return Adapters.adapt(Params.class, obj);
        }

        @Override
        public Params adapt(JsonObject obj) {
            String proveedorRazonSocialORuc0 = JsonResolver.get(obj, "proveedor$razonSocialORuc", Type.STRING);
            TipoEntidadEnum documentoTipo0 = TipoEntidadEnum.adapter().getEnumerable(JsonResolver.get(obj, "documento$tipo", Type.STRING));
            String documentoNumero0 = JsonResolver.get(obj, "documento$nro", Type.STRING);

            // TODO: VALIDAR ELEMENTOS.
            Params params = new Params();
            params.proveedorRazonSocialORuc = proveedorRazonSocialORuc0;
            params.documentoTipo = documentoTipo0;
            params.documentoNumero = documentoNumero0;
            return params;
        }

    }

}
