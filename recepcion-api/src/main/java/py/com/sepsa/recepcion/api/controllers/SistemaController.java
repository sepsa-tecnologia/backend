/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.api.controllers;

import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.rest.responses.ServiceResponse;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.com.sepsa.recepcion.api.BaseController;

/**
 *
 * @author Fabio A. González Sosa
 */
@Path("/")
@RequestScoped
public class SistemaController extends BaseController {

    @GET
    @Path("/v1/sistema.ping")
    @Produces(MediaType.APPLICATION_JSON)
    public Response loginApp(String json) {
        JsonObjectBuilder builder = JsonObjectBuilder.instance();
        builder.add("ping", "pong");

        return ServiceResponse.ok()
                .payload(builder.build())
                .build();
    }

}
