/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.api.controllers;

import fa.gs.utils.rest.responses.ServiceResponse;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.com.sepsa.recepcion.api.BaseController;
import py.com.sepsa.recepcion.core.logic.BackgroundWorkers;
import py.com.sepsa.recepcion.core.logic.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
@Path("/")
@RequestScoped
public class BackgroundWorkersController extends BaseController {

    private final BackgroundWorkers backgroundWorkers = Injection.bean(BackgroundWorkers.class);

    @POST
    @Path("/v1/backgroundWorkers.iniciar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response backgroundWorkersIniciar(String json) {
        boolean ok = backgroundWorkers.start();
        if (ok) {
            return ServiceResponse.ok().build();
        } else {
            return ServiceResponse.ko().build();
        }
    }

    @POST
    @Path("/v1/backgroundWorkers.detener")
    @Produces(MediaType.APPLICATION_JSON)
    public Response backgroundWorkersDetener(String json) {
        boolean ok = backgroundWorkers.stop();
        if (ok) {
            return ServiceResponse.ok().build();
        } else {
            return ServiceResponse.ko().build();
        }
    }
}
