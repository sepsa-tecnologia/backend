/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.api;

import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.rest.controllers.RestController;
import py.com.sepsa.recepcion.core.logging.AppLoggerFactory;

/**
 *
 * @author Fabio A. González Sosa
 */
public class BaseController extends RestController {

    private final AppLogger log = AppLoggerFactory.api();

    @Override
    public AppLogger getLog() {
        return log;
    }

}
