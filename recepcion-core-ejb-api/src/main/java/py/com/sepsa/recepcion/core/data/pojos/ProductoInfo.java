/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import fa.gs.utils.collections.maps.ResultSetMap;
import fa.gs.utils.database.criteria.column.NativeColumn;
import java.io.Serializable;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ProductoInfo implements Serializable {

    private Long id;
    private String descripcionCorta;
    private String descripcionLarga;
    private String codigoGtin;
    private String codigoInterno;
    private String marca;
    private String paisProcedencia;

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public Long getId() {
        return this.id;
    }

    public void setId(Long value) {
        this.id = value;
    }

    public String getDescripcionCorta() {
        return this.descripcionCorta;
    }

    public void setDescripcionCorta(String value) {
        this.descripcionCorta = value;
    }

    public String getDescripcionLarga() {
        return this.descripcionLarga;
    }

    public void setDescripcionLarga(String value) {
        this.descripcionLarga = value;
    }

    public String getCodigoGtin() {
        return this.codigoGtin;
    }

    public void setCodigoGtin(String value) {
        this.codigoGtin = value;
    }

    public String getCodigoInterno() {
        return this.codigoInterno;
    }

    public void setCodigoInterno(String value) {
        this.codigoInterno = value;
    }

    public String getMarca() {
        return this.marca;
    }

    public void setMarca(String value) {
        this.marca = value;
    }

    public String getPaisProcedencia() {
        return this.paisProcedencia;
    }

    public void setPaisProcedencia(String value) {
        this.paisProcedencia = value;
    }
    //</editor-fold>

    public static class COLUMNS {

        public static final NativeColumn<Long> id = NativeColumn.instance("id", Long.class);
        public static final NativeColumn<String> descripcionCorta = NativeColumn.instance("descripcionCorta", String.class);
        public static final NativeColumn<String> descripcionLarga = NativeColumn.instance("descripcionLarga", String.class);
        public static final NativeColumn<String> codigoGtin = NativeColumn.instance("codigoGtin", String.class);
        public static final NativeColumn<String> codigoInterno = NativeColumn.instance("codigoInterno", String.class);
        public static final NativeColumn<String> marca = NativeColumn.instance("marca", String.class);
        public static final NativeColumn<String> paisProcedencia = NativeColumn.instance("paisProcedencia", String.class);
    }

    public static class Mapper extends fa.gs.utils.database.utils.ResultSetMapper<ProductoInfo> {

        @Override
        protected ProductoInfo getEmptyAdaptee() {
            return new ProductoInfo();
        }

        @Override
        public ProductoInfo adapt(final ProductoInfo obj, ResultSetMap row) {
            Long id0 = row.long0(COLUMNS.id);
            String descripcionCorta0 = row.string(COLUMNS.descripcionCorta);
            String descripcionLarga0 = row.string(COLUMNS.descripcionLarga);
            String codigoGtin0 = row.string(COLUMNS.codigoGtin);
            String codigoInterno0 = row.string(COLUMNS.codigoInterno);
            String marca0 = row.string(COLUMNS.marca);
            String paisProcedencia0 = row.string(COLUMNS.paisProcedencia);

            obj.setId(id0);
            obj.setDescripcionCorta(descripcionCorta0);
            obj.setDescripcionLarga(descripcionLarga0);
            obj.setCodigoGtin(codigoGtin0);
            obj.setCodigoInterno(codigoInterno0);
            obj.setMarca(marca0);
            obj.setPaisProcedencia(paisProcedencia0);
            return obj;
        }
    }
}
