/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.criteria.column.NativeColumn;
import fa.gs.utils.database.mapping.Mapping;
import fa.gs.utils.database.mapping.mappers.DateMapper;
import fa.gs.utils.database.mapping.mappers.EnumMapper;
import fa.gs.utils.database.mapping.mappers.LongMapper;
import fa.gs.utils.database.mapping.mappers.PojoMapper;
import fa.gs.utils.database.mapping.mappers.StringMapper;
import fa.gs.utils.database.mapping.mappings.Mappings;
import fa.gs.utils.misc.Assertions;
import java.util.Collection;
import java.util.Date;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;

/**
 *
 * @author Fabio A. González Sosa
 */
public class FacturaInfo implements DocumentoInfo, DocumentoConEstado, DocumentoConNro {

    private Long id;
    private EstadoEnum estado;
    private String nroTimbrado;
    private Date vtoTimbrado;
    private Long idDocumento;
    private String nroDocumento;
    private Long idOrdenCompra;
    private String glnComprador;
    private String rucComprador;
    private String razonSocialComprador;
    private String glnProveedor;
    private String rucProveedor;
    private String razonSocialProveedor;
    private Date fechaRegistro;
    private Date fechaEmision;
    private final Collection<FacturaDetalleInfo> detalles;

    public FacturaInfo() {
        this.detalles = Lists.empty();
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }

    public String getNroTimbrado() {
        return nroTimbrado;
    }

    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    public Date getVtoTimbrado() {
        return vtoTimbrado;
    }

    public void setVtoTimbrado(Date vtoTimbrado) {
        this.vtoTimbrado = vtoTimbrado;
    }

    public Long getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Long idDocumento) {
        this.idDocumento = idDocumento;
    }

    @Override
    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public Long getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Long idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public String getGlnComprador() {
        return glnComprador;
    }

    public void setGlnComprador(String glnComprador) {
        this.glnComprador = glnComprador;
    }

    public String getRucComprador() {
        return rucComprador;
    }

    public void setRucComprador(String rucComprador) {
        this.rucComprador = rucComprador;
    }

    public String getRazonSocialComprador() {
        return razonSocialComprador;
    }

    public void setRazonSocialComprador(String razonSocialComprador) {
        this.razonSocialComprador = razonSocialComprador;
    }

    public String getGlnProveedor() {
        return glnProveedor;
    }

    public void setGlnProveedor(String glnProveedor) {
        this.glnProveedor = glnProveedor;
    }

    public String getRucProveedor() {
        return rucProveedor;
    }

    public void setRucProveedor(String rucProveedor) {
        this.rucProveedor = rucProveedor;
    }

    public String getRazonSocialProveedor() {
        return razonSocialProveedor;
    }

    public void setRazonSocialProveedor(String razonSocialProveedor) {
        this.razonSocialProveedor = razonSocialProveedor;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Collection<FacturaDetalleInfo> getDetalles() {
        return detalles;
    }

    public void setDetalles(Collection<FacturaDetalleInfo> detalles) {
        if (!Assertions.isNullOrEmpty(detalles)) {
            this.detalles.addAll(detalles);
        }
    }

    @Override
    public TipoEntidadEnum getTipoDocumento() {
        return TipoEntidadEnum.FACTURA;
    }
    //</editor-fold>    

    public static class MAPPINGS {

        public static final Mapping<Long> id = Mappings.instance("id", Long.class);
        public static final Mapping<String> estadoCodigo = Mappings.instance("estado", String.class);
        public static final Mapping<Long> idDocumento = Mappings.instance("idDocumento", Long.class);
        public static final Mapping<String> nroTimbrado = Mappings.instance("nroTimbrado", String.class);
        public static final Mapping<Date> vtoTimbrado = Mappings.instance("vtoTimbrado", Date.class);
        public static final Mapping<String> nroDocumento = Mappings.instance("nroDocumento", String.class);
        public static final Mapping<Long> idOrdenCompra = Mappings.instance("idOrdenCompra", Long.class);
        public static final Mapping<String> glnComprador = Mappings.instance("glnComprador", String.class);
        public static final Mapping<String> rucComprador = Mappings.instance("rucComprador", String.class);
        public static final Mapping<String> razonSocialComprador = Mappings.instance("razonSocialComprador", String.class);
        public static final Mapping<String> glnProveedor = Mappings.instance("glnProveedor", String.class);
        public static final Mapping<String> rucProveedor = Mappings.instance("rucProveedor", String.class);
        public static final Mapping<String> razonSocialProveedor = Mappings.instance("razonSocialProveedor", String.class);
        public static final Mapping<Date> fechaRegistro = Mappings.instance("fechaRegistro", Date.class);
        public static final Mapping<Date> fechaEmision = Mappings.instance("fechaEmision", Date.class);
    }

    public static class COLUMNS {

        public static final NativeColumn<Long> f_id = NativeColumn.instance("f", "id", Long.class);
        public static final NativeColumn<String> e_estado_codigo = NativeColumn.instance("e", "codigo", String.class);
        public static final NativeColumn<String> f_nro_timbrado = NativeColumn.instance("f", "nro_timbrado", String.class);
        public static final NativeColumn<Date> f_vto_timbrado = NativeColumn.instance("f", "vencimiento_timbrado", Date.class);
        public static final NativeColumn<Long> f_id_documento = NativeColumn.instance("f", "id_documento", Long.class);
        public static final NativeColumn<String> f_nro_documento = NativeColumn.instance("f", "nro_documento", String.class);
        public static final NativeColumn<Long> f_id_orden_compra = NativeColumn.instance("f", "id_orden_compra", Long.class);
        public static final NativeColumn<Long> oc_gln_comprador = NativeColumn.instance("oc", "gln_origen", Long.class);
        public static final NativeColumn<Long> oc_ruc_comprador = NativeColumn.instance("oc", "ruc_origen", Long.class);
        public static final NativeColumn<Long> oc_razon_social_comprador = NativeColumn.instance("oc", "razon_social_origen", Long.class);
        public static final NativeColumn<Long> oc_gln_proveedor = NativeColumn.instance("oc", "gln_destino", Long.class);
        public static final NativeColumn<Long> oc_ruc_proveedor = NativeColumn.instance("oc", "ruc_destino", Long.class);
        public static final NativeColumn<Long> oc_razon_social_proveedor = NativeColumn.instance("oc", "razon_social_destino", Long.class);
        public static final NativeColumn<Date> f_fecha_registro = NativeColumn.instance("f", "fecha_registro", Date.class);
        public static final NativeColumn<Date> f_fecha_emision = NativeColumn.instance("f", "fecha_emision", Date.class);
    }

    public static class MAPPER extends PojoMapper<MAPPER, FacturaInfo> {

        public static MAPPER INSTANCE;

        static {
            MAPPER.INSTANCE = new MAPPER();
            MAPPER.INSTANCE.with(LongMapper.instance(MAPPINGS.id));
            MAPPER.INSTANCE.with(EnumMapper.instance(MAPPINGS.estadoCodigo, EstadoEnum.adapter()));
            MAPPER.INSTANCE.with(StringMapper.instance(MAPPINGS.nroTimbrado));
            MAPPER.INSTANCE.with(DateMapper.instance(MAPPINGS.vtoTimbrado));
            MAPPER.INSTANCE.with(LongMapper.instance(MAPPINGS.idDocumento));
            MAPPER.INSTANCE.with(StringMapper.instance(MAPPINGS.nroDocumento));
            MAPPER.INSTANCE.with(LongMapper.instance(MAPPINGS.idOrdenCompra));
            MAPPER.INSTANCE.with(StringMapper.instance(MAPPINGS.glnComprador));
            MAPPER.INSTANCE.with(StringMapper.instance(MAPPINGS.rucComprador));
            MAPPER.INSTANCE.with(StringMapper.instance(MAPPINGS.razonSocialComprador));
            MAPPER.INSTANCE.with(StringMapper.instance(MAPPINGS.glnProveedor));
            MAPPER.INSTANCE.with(StringMapper.instance(MAPPINGS.rucProveedor));
            MAPPER.INSTANCE.with(StringMapper.instance(MAPPINGS.razonSocialProveedor));
            MAPPER.INSTANCE.with(DateMapper.instance(MAPPINGS.fechaRegistro));
            MAPPER.INSTANCE.with(DateMapper.instance(MAPPINGS.fechaEmision));
        }

        @Override
        protected FacturaInfo getEmptyAdaptee() {
            return new FacturaInfo();
        }

    }

}
