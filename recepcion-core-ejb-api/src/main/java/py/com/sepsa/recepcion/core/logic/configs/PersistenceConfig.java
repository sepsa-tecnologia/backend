/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.configs;

import java.io.Serializable;

/**
 *
 * @author Fabio A. González Sosa
 */
public interface PersistenceConfig extends Serializable {

    String getDatasourceName();

}
