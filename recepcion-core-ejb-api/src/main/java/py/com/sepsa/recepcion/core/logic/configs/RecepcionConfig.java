/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.configs;

import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author Fabio A. González Sosa
 */
public interface RecepcionConfig extends Serializable {

    /**
     * Permite determinar si la plataforma es administrada por Sepsa o si es
     * mantenida de manera externa por un cliente arbitrario.
     *
     * @return {@code true} si la plataforma se encuentra administrada por un
     * cliente externo a Sepsa, caso contrario {@code false}.
     */
    boolean isStandaloneApp();

    /**
     * Permite determinar el valor de shard asignado a un cliente independiente
     * a Sepsa que administra la plataforma.
     *
     * @return Codigo shard asignado.
     */
    UUID getStandaloneShard();

}
