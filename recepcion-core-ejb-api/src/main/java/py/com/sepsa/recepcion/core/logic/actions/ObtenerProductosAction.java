/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.result.simple.Result;
import java.util.Collection;
import javax.ejb.Remote;
import py.com.sepsa.recepcion.core.data.pojos.ProductoInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Remote
public interface ObtenerProductosAction extends BusinessLogicAction {

    Result<ProductoInfo> obtenerProducto(String codigoGtinOInterno);

    Result<ProductoInfo> obtenerProducto(Long idProducto);

    Result<Collection<ProductoInfo>> obtenerProductos(Long[] idProductos);

}
