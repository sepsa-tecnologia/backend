package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.Facade;
import javax.ejb.Remote;
import py.com.sepsa.recepcion.core.database.entities.recepcion.InformacionEntrega;

@Remote
public interface InformacionEntregaFacade extends Facade<InformacionEntrega> {
}
