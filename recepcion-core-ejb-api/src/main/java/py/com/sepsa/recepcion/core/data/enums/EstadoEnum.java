/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.enums;

import fa.gs.utils.collections.enums.EnumerableAdapter;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum EstadoEnum {
    INDEFINIDO("0000", "INDEFINIDO"),
    ABIERTO("0001", "ABIERTO"),
    CERRADO("0002", "CERRADO"),
    ACEPTADO("0003", "ACEPTADO"),
    RECHAZADO("0004", "RECHAZADO");
    private final String codigo;
    private final String descripcion;

    private EstadoEnum(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static EnumerableAdapter<EstadoEnum> adapter() {
        return new Adapter0();
    }

    public static EstadoEnum from(String codigo) {
        for (EstadoEnum monedaEnum : EstadoEnum.values()) {
            if (Assertions.equals(monedaEnum.codigo, codigo)) {
                return monedaEnum;
            }
        }

        return EstadoEnum.INDEFINIDO;
    }

    public String codigo() {
        return codigo;
    }

    public String descripcion() {
        return descripcion;
    }

    private static class Adapter0 implements EnumerableAdapter<EstadoEnum> {

        @Override
        public EstadoEnum getEnumerable(Object obj) {
            if (obj instanceof String) {
                String codigo = (String) obj;
                return EstadoEnum.from(codigo);
            }

            throw Errors.unsupported();
        }
    }

}
