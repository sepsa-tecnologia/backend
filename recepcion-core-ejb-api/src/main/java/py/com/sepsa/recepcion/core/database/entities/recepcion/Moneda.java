package py.com.sepsa.recepcion.core.database.entities.recepcion;

import fa.gs.utils.database.criteria.column.JpaColumn;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "moneda", catalog = "sepsa", schema = "recepcion")
@DynamicInsert
public class Moneda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_id")
    @SequenceGenerator(name = "gen_id", sequenceName = "recepcion.moneda_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("nextval('recepcion.moneda_id_seq'::regclass)")
    private Long id;

    @Column(name = "codigo", nullable = false)
    @Basic(optional = false)
    private String codigo;

    @Column(name = "descripcion", nullable = false)
    @Basic(optional = false)
    private String descripcion;

    @Column(name = "codigo_alfabetico", nullable = false)
    @Basic(optional = false)
    private String codigoAlfabetico;

    @Column(name = "codigo_numerico", nullable = false)
    @Basic(optional = false)
    private String codigoNumerico;

    @Column(name = "simbolo", nullable = false)
    @Basic(optional = false)
    private String simbolo;

    @Column(name = "meta__shard", nullable = true)
    @Basic(optional = true)
    @org.hibernate.annotations.Type(type = "org.hibernate.type.PostgresUUIDType")
    private java.util.UUID metaShard;

    @Column(name = "meta__eliminado", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("'0'::bpchar")
    private String metaEliminado;

    @Column(name = "meta__fecha_eliminacion", nullable = true)
    @Basic(optional = true)
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date metaFechaEliminacion;

    public void setId(Long value) {
        this.id = value;
    }

    public void setCodigo(String value) {
        this.codigo = value;
    }

    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    public void setCodigoAlfabetico(String value) {
        this.codigoAlfabetico = value;
    }

    public void setCodigoNumerico(String value) {
        this.codigoNumerico = value;
    }

    public void setSimbolo(String value) {
        this.simbolo = value;
    }

    public void setMetaShard(java.util.UUID value) {
        this.metaShard = value;
    }

    public void setMetaEliminado(String value) {
        this.metaEliminado = value;
    }

    public void setMetaFechaEliminacion(java.util.Date value) {
        this.metaFechaEliminacion = value;
    }

    public Long getId() {
        return this.id;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public String getCodigoAlfabetico() {
        return this.codigoAlfabetico;
    }

    public String getCodigoNumerico() {
        return this.codigoNumerico;
    }

    public String getSimbolo() {
        return this.simbolo;
    }

    public java.util.UUID getMetaShard() {
        return this.metaShard;
    }

    public String getMetaEliminado() {
        return this.metaEliminado;
    }

    public java.util.Date getMetaFechaEliminacion() {
        return this.metaFechaEliminacion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.codigo);
        hash = 53 * hash + Objects.hashCode(this.descripcion);
        hash = 53 * hash + Objects.hashCode(this.codigoAlfabetico);
        hash = 53 * hash + Objects.hashCode(this.codigoNumerico);
        hash = 53 * hash + Objects.hashCode(this.simbolo);
        hash = 53 * hash + Objects.hashCode(this.metaShard);
        hash = 53 * hash + Objects.hashCode(this.metaEliminado);
        hash = 53 * hash + Objects.hashCode(this.metaFechaEliminacion);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Moneda other = (Moneda) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        if (!Objects.equals(this.descripcion, other.descripcion)) {
            return false;
        }
        if (!Objects.equals(this.codigoAlfabetico, other.codigoAlfabetico)) {
            return false;
        }
        if (!Objects.equals(this.codigoNumerico, other.codigoNumerico)) {
            return false;
        }
        if (!Objects.equals(this.simbolo, other.simbolo)) {
            return false;
        }
        if (!Objects.equals(this.metaShard, other.metaShard)) {
            return false;
        }
        if (!Objects.equals(this.metaEliminado, other.metaEliminado)) {
            return false;
        }
        if (!Objects.equals(this.metaFechaEliminacion, other.metaFechaEliminacion)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Moneda[");
        builder.append("id = ").append(id);
        builder.append(", codigo = ").append(codigo);
        builder.append(", descripcion = ").append(descripcion);
        builder.append(", codigoAlfabetico = ").append(codigoAlfabetico);
        builder.append(", codigoNumerico = ").append(codigoNumerico);
        builder.append(", simbolo = ").append(simbolo);
        builder.append(", metaShard = ").append(metaShard);
        builder.append(", metaEliminado = ").append(metaEliminado);
        builder.append(", metaFechaEliminacion = ").append(metaFechaEliminacion);
        builder.append("]");
        return builder.toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Columnas">
    public static class COLUMNS implements Serializable {

        public static final JpaColumn<Long> id = JpaColumn.instance("id", Long.class, false);
        public static final JpaColumn<String> codigo = JpaColumn.instance("codigo", String.class, false);
        public static final JpaColumn<String> descripcion = JpaColumn.instance("descripcion", String.class, false);
        public static final JpaColumn<String> codigoAlfabetico = JpaColumn.instance("codigoAlfabetico", String.class, false);
        public static final JpaColumn<String> codigoNumerico = JpaColumn.instance("codigoNumerico", String.class, false);
        public static final JpaColumn<String> simbolo = JpaColumn.instance("simbolo", String.class, false);
        public static final JpaColumn<java.util.UUID> metaShard = JpaColumn.instance("metaShard", java.util.UUID.class, true);
        public static final JpaColumn<String> metaEliminado = JpaColumn.instance("metaEliminado", String.class, false);
        public static final JpaColumn<java.util.Date> metaFechaEliminacion = JpaColumn.instance("metaFechaEliminacion", java.util.Date.class, true);
    }
    //</editor-fold>

}
