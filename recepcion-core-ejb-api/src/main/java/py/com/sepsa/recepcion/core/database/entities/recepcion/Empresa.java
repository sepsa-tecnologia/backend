package py.com.sepsa.recepcion.core.database.entities.recepcion;

import fa.gs.utils.database.criteria.column.JpaColumn;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "empresa", catalog = "sepsa", schema = "recepcion")
@DynamicInsert
public class Empresa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_id")
    @SequenceGenerator(name = "gen_id", sequenceName = "recepcion.empresa_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("nextval('recepcion.empresa_id_seq'::regclass)")
    private Long id;

    @Column(name = "razon_social", nullable = false)
    @Basic(optional = false)
    private String razonSocial;

    @Column(name = "ruc", nullable = false)
    @Basic(optional = false)
    private String ruc;

    @Column(name = "fecha_registro", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("now()")
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date fechaRegistro;

    @Column(name = "meta__shard", nullable = true)
    @Basic(optional = true)
    @org.hibernate.annotations.Type(type = "org.hibernate.type.PostgresUUIDType")
    private java.util.UUID metaShard;

    @Column(name = "meta__eliminado", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("'0'::bpchar")
    private String metaEliminado;

    @Column(name = "meta__fecha_eliminacion", nullable = true)
    @Basic(optional = true)
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date metaFechaEliminacion;

    public void setId(Long value) {
        this.id = value;
    }

    public void setRazonSocial(String value) {
        this.razonSocial = value;
    }

    public void setRuc(String value) {
        this.ruc = value;
    }

    public void setFechaRegistro(java.util.Date value) {
        this.fechaRegistro = value;
    }

    public void setMetaShard(java.util.UUID value) {
        this.metaShard = value;
    }

    public void setMetaEliminado(String value) {
        this.metaEliminado = value;
    }

    public void setMetaFechaEliminacion(java.util.Date value) {
        this.metaFechaEliminacion = value;
    }

    public Long getId() {
        return this.id;
    }

    public String getRazonSocial() {
        return this.razonSocial;
    }

    public String getRuc() {
        return this.ruc;
    }

    public java.util.Date getFechaRegistro() {
        return this.fechaRegistro;
    }

    public java.util.UUID getMetaShard() {
        return this.metaShard;
    }

    public String getMetaEliminado() {
        return this.metaEliminado;
    }

    public java.util.Date getMetaFechaEliminacion() {
        return this.metaFechaEliminacion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.razonSocial);
        hash = 53 * hash + Objects.hashCode(this.ruc);
        hash = 53 * hash + Objects.hashCode(this.fechaRegistro);
        hash = 53 * hash + Objects.hashCode(this.metaShard);
        hash = 53 * hash + Objects.hashCode(this.metaEliminado);
        hash = 53 * hash + Objects.hashCode(this.metaFechaEliminacion);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Empresa other = (Empresa) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.razonSocial, other.razonSocial)) {
            return false;
        }
        if (!Objects.equals(this.ruc, other.ruc)) {
            return false;
        }
        if (!Objects.equals(this.fechaRegistro, other.fechaRegistro)) {
            return false;
        }
        if (!Objects.equals(this.metaShard, other.metaShard)) {
            return false;
        }
        if (!Objects.equals(this.metaEliminado, other.metaEliminado)) {
            return false;
        }
        if (!Objects.equals(this.metaFechaEliminacion, other.metaFechaEliminacion)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Empresa[");
        builder.append("id = ").append(id);
        builder.append(", razonSocial = ").append(razonSocial);
        builder.append(", ruc = ").append(ruc);
        builder.append(", fechaRegistro = ").append(fechaRegistro);
        builder.append(", metaShard = ").append(metaShard);
        builder.append(", metaEliminado = ").append(metaEliminado);
        builder.append(", metaFechaEliminacion = ").append(metaFechaEliminacion);
        builder.append("]");
        return builder.toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Columnas">
    public static class COLUMNS implements Serializable {

        public static final JpaColumn<Long> id = JpaColumn.instance("id", Long.class, false);
        public static final JpaColumn<String> razonSocial = JpaColumn.instance("razonSocial", String.class, false);
        public static final JpaColumn<String> ruc = JpaColumn.instance("ruc", String.class, false);
        public static final JpaColumn<java.util.Date> fechaRegistro = JpaColumn.instance("fechaRegistro", java.util.Date.class, false);
        public static final JpaColumn<java.util.UUID> metaShard = JpaColumn.instance("metaShard", java.util.UUID.class, true);
        public static final JpaColumn<String> metaEliminado = JpaColumn.instance("metaEliminado", String.class, false);
        public static final JpaColumn<java.util.Date> metaFechaEliminacion = JpaColumn.instance("metaFechaEliminacion", java.util.Date.class, true);
    }
    //</editor-fold>

}
