/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.adapters;

import com.google.gson.JsonElement;
import fa.gs.utils.adapters.impl.json.ToJsonAdapter;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import py.com.sepsa.recepcion.core.data.pojos.EmpaqueInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
public class EmpaqueInfo2JsonAdapter extends ToJsonAdapter<EmpaqueInfo> {

    @Override
    protected JsonElement adapt0(EmpaqueInfo obj) {
        JsonObjectBuilder json = JsonObjectBuilder.instance();
        json.add("id", obj.getId());
        json.add("tipo", obj.getTipoEmpaque().codigo());
        json.add("codigos", adaptCodigos(obj));
        json.add("alto", obj.getAlto());
        json.add("ancho", obj.getAncho());
        json.add("profundo", obj.getProfundo());
        json.add("peso", obj.getPeso());
        return json.build();
    }

    private JsonElement adaptCodigos(EmpaqueInfo obj) {
        JsonObjectBuilder json = JsonObjectBuilder.instance();
        json.add("sscc", obj.getCodigoSscc());
        return json.build();
    }

}
