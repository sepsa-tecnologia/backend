/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.result.simple.Result;
import java.util.Collection;
import java.util.Map;
import javax.ejb.Remote;
import py.com.sepsa.recepcion.core.data.pojos.FacturaDetalleInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Remote
public interface ObtenerDetallesFacturasAction extends BusinessLogicAction {

    Result<Collection<FacturaDetalleInfo>> obtenerDetallesFactura(Long idFactura);

    Result<Map<Long, Collection<FacturaDetalleInfo>>> obtenerDetallesFacturas(Long[] idNotasRemision);

}
