/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.enums;

import java.util.Objects;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum SiediVersion {
    NONE("0"),
    V1("1"),
    V2("3");
    private final String kind;

    private SiediVersion(String kind) {
        this.kind = kind;
    }

    public static SiediVersion from(String kind) {
        for (SiediVersion version : SiediVersion.values()) {
            if (Objects.equals(kind, version.kind)) {
                return version;
            }
        }

        return SiediVersion.NONE;
    }
}
