/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.result.simple.Result;
import javax.ejb.Remote;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;
import py.com.sepsa.recepcion.core.data.enums.TipoObservacionEnum;
import py.com.sepsa.recepcion.core.data.pojos.ObservacionInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Remote
public interface AgregarObservacionAction extends BusinessLogicAction {

    public Result<ObservacionInfo> agregarObservacion(Long idEntidad, TipoEntidadEnum tipoEntidad, TipoObservacionEnum tipoObservacion, String descripcion);

}
