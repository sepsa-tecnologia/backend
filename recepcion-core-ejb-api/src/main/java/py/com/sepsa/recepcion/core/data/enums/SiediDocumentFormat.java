/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.enums;

import java.util.Objects;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum SiediDocumentFormat {
    DESCONOCIDO(null),
    TXT("txt"),
    XML("xml");
    private final String formato;

    private SiediDocumentFormat(String formato) {
        this.formato = formato;
    }

    public static SiediDocumentFormat from(String formato) {
        for (SiediDocumentFormat documentFormatEnum : SiediDocumentFormat.values()) {
            if (Objects.equals(formato, documentFormatEnum.formato)) {
                return documentFormatEnum;
            }
        }

        return SiediDocumentFormat.DESCONOCIDO;
    }
}
