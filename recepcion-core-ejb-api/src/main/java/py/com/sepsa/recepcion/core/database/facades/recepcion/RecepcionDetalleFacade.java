package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.Facade;
import javax.ejb.Remote;
import py.com.sepsa.recepcion.core.database.entities.recepcion.RecepcionDetalle;

@Remote
public interface RecepcionDetalleFacade extends Facade<RecepcionDetalle> {
}
