/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.result.simple.Result;
import java.util.Collection;
import javax.ejb.Remote;
import py.com.sepsa.recepcion.core.data.pojos.NotaRemisionInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Remote
public interface ObtenerNotasRemisionAction extends BusinessLogicAction {

    Result<NotaRemisionInfo> obtenerNotaRemision(Long idNotaRemision);

    Result<Collection<NotaRemisionInfo>> obtenerNotasRemision(Long[] idNotasRemision);

    Result<Collection<NotaRemisionInfo>> obtenerNotasRemision(String razonSocialORucProveedor, String nroDocumento);

}
