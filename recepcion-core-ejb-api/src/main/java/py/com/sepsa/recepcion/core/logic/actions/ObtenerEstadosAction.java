/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.result.simple.Result;
import javax.ejb.Remote;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Estado;

/**
 *
 * @author Fabio A. González Sosa
 */
@Remote
public interface ObtenerEstadosAction extends BusinessLogicAction {

    Result<Estado> obtenerEstado(EstadoEnum estadoEnum);

}
