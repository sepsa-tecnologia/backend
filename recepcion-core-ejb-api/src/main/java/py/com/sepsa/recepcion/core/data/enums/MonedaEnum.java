/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.enums;

import fa.gs.utils.collections.enums.EnumerableAdapter;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum MonedaEnum {
    GUARANI("0001", "Guaraní", "PYG", "600", "Gs"),
    DOLAR("0002", "Dólar", "USD", "840", "$");
    private final String codigo;
    private final String descripcion;
    private final String codigoAlfabetico;
    private final String codigoNumerico;
    private final String simbolo;

    private MonedaEnum(String codigo, String descripcion, String codigoAlfabetico, String codigoNumerico, String simbolo) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.codigoAlfabetico = codigoAlfabetico;
        this.codigoNumerico = codigoNumerico;
        this.simbolo = simbolo;
    }

    public static EnumerableAdapter<MonedaEnum> adapter() {
        return new Adapter0();
    }

    public static MonedaEnum from(String codigo) {
        for (MonedaEnum monedaEnum : MonedaEnum.values()) {
            if (Assertions.equals(monedaEnum.codigo, codigo)) {
                return monedaEnum;
            }
        }

        return MonedaEnum.GUARANI;
    }

    public String codigo() {
        return codigo;
    }

    public String descripcion() {
        return descripcion;
    }

    public String codigoAlfabetico() {
        return codigoAlfabetico;
    }

    public String codigoNumerico() {
        return codigoNumerico;
    }

    public String simbolo() {
        return simbolo;
    }

    private static class Adapter0 implements EnumerableAdapter<MonedaEnum> {

        @Override
        public MonedaEnum getEnumerable(Object obj) {
            if (obj instanceof String) {
                String codigo = (String) obj;
                return MonedaEnum.from(codigo);
            }

            throw Errors.unsupported();
        }
    }

}
