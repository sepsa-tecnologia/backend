package py.com.sepsa.recepcion.core.database.entities.recepcion;

import fa.gs.utils.database.criteria.column.JpaColumn;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "usuario_permisos", catalog = "sepsa", schema = "recepcion")
@DynamicInsert
public class UsuarioPermisos implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_id")
    @SequenceGenerator(name = "gen_id", sequenceName = "recepcion.usuario_permisos_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("nextval('recepcion.usuario_permisos_id_seq'::regclass)")
    private Long id;

    @Column(name = "id_usuario", nullable = false)
    @Basic(optional = false)
    private Long idUsuario;

    @Column(name = "id_permiso", nullable = false)
    @Basic(optional = false)
    private Long idPermiso;

    public void setId(Long value) {
        this.id = value;
    }

    public void setIdUsuario(Long value) {
        this.idUsuario = value;
    }

    public void setIdPermiso(Long value) {
        this.idPermiso = value;
    }

    public Long getId() {
        return this.id;
    }

    public Long getIdUsuario() {
        return this.idUsuario;
    }

    public Long getIdPermiso() {
        return this.idPermiso;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.idUsuario);
        hash = 53 * hash + Objects.hashCode(this.idPermiso);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UsuarioPermisos other = (UsuarioPermisos) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.idUsuario, other.idUsuario)) {
            return false;
        }
        if (!Objects.equals(this.idPermiso, other.idPermiso)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("UsuarioPermisos[");
        builder.append("id = ").append(id);
        builder.append(", idUsuario = ").append(idUsuario);
        builder.append(", idPermiso = ").append(idPermiso);
        builder.append("]");
        return builder.toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Columnas">
    public static class COLUMNS implements Serializable {

        public static final JpaColumn<Long> id = JpaColumn.instance("id", Long.class, false);
        public static final JpaColumn<Long> idUsuario = JpaColumn.instance("idUsuario", Long.class, false);
        public static final JpaColumn<Long> idPermiso = JpaColumn.instance("idPermiso", Long.class, false);
    }
    //</editor-fold>

}
