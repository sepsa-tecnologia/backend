package py.com.sepsa.recepcion.core.database.entities.recepcion;

import fa.gs.utils.database.criteria.column.JpaColumn;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "producto_entrega", catalog = "sepsa", schema = "recepcion")
@DynamicInsert
public class ProductoEntrega implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_id")
    @SequenceGenerator(name = "gen_id", sequenceName = "recepcion.producto_entrega_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("nextval('recepcion.producto_entrega_id_seq'::regclass)")
    private Long id;

    @Column(name = "id_producto", nullable = true)
    @Basic(optional = true)
    private Long idProducto;

    @Column(name = "lote_origen", nullable = true)
    @Basic(optional = true)
    private String loteOrigen;

    @Column(name = "fecha_vencimiento", nullable = true)
    @Basic(optional = true)
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date fechaVencimiento;

    @Column(name = "id_empaque_l1", nullable = true)
    @Basic(optional = true)
    private Long idEmpaqueL1;

    @Column(name = "id_empaque_l2", nullable = true)
    @Basic(optional = true)
    private Long idEmpaqueL2;

    @Column(name = "id_empaque_l3", nullable = true)
    @Basic(optional = true)
    private Long idEmpaqueL3;

    @Column(name = "id_empaque_l4", nullable = true)
    @Basic(optional = true)
    private Long idEmpaqueL4;

    @Column(name = "fecha_entrega_tentativa", nullable = true)
    @Basic(optional = true)
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date fechaEntregaTentativa;

    @Column(name = "fecha_entrega_agendada", nullable = true)
    @Basic(optional = true)
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date fechaEntregaAgendada;

    public void setId(Long value) {
        this.id = value;
    }

    public void setIdProducto(Long value) {
        this.idProducto = value;
    }

    public void setLoteOrigen(String value) {
        this.loteOrigen = value;
    }

    public void setFechaVencimiento(java.util.Date value) {
        this.fechaVencimiento = value;
    }

    public void setIdEmpaqueL1(Long value) {
        this.idEmpaqueL1 = value;
    }

    public void setIdEmpaqueL2(Long value) {
        this.idEmpaqueL2 = value;
    }

    public void setIdEmpaqueL3(Long value) {
        this.idEmpaqueL3 = value;
    }

    public void setIdEmpaqueL4(Long value) {
        this.idEmpaqueL4 = value;
    }

    public void setFechaEntregaTentativa(java.util.Date value) {
        this.fechaEntregaTentativa = value;
    }

    public void setFechaEntregaAgendada(java.util.Date value) {
        this.fechaEntregaAgendada = value;
    }

    public Long getId() {
        return this.id;
    }

    public Long getIdProducto() {
        return this.idProducto;
    }

    public String getLoteOrigen() {
        return this.loteOrigen;
    }

    public java.util.Date getFechaVencimiento() {
        return this.fechaVencimiento;
    }

    public Long getIdEmpaqueL1() {
        return this.idEmpaqueL1;
    }

    public Long getIdEmpaqueL2() {
        return this.idEmpaqueL2;
    }

    public Long getIdEmpaqueL3() {
        return this.idEmpaqueL3;
    }

    public Long getIdEmpaqueL4() {
        return this.idEmpaqueL4;
    }

    public java.util.Date getFechaEntregaTentativa() {
        return this.fechaEntregaTentativa;
    }

    public java.util.Date getFechaEntregaAgendada() {
        return this.fechaEntregaAgendada;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.idProducto);
        hash = 53 * hash + Objects.hashCode(this.loteOrigen);
        hash = 53 * hash + Objects.hashCode(this.fechaVencimiento);
        hash = 53 * hash + Objects.hashCode(this.idEmpaqueL1);
        hash = 53 * hash + Objects.hashCode(this.idEmpaqueL2);
        hash = 53 * hash + Objects.hashCode(this.idEmpaqueL3);
        hash = 53 * hash + Objects.hashCode(this.idEmpaqueL4);
        hash = 53 * hash + Objects.hashCode(this.fechaEntregaTentativa);
        hash = 53 * hash + Objects.hashCode(this.fechaEntregaAgendada);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductoEntrega other = (ProductoEntrega) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.idProducto, other.idProducto)) {
            return false;
        }
        if (!Objects.equals(this.loteOrigen, other.loteOrigen)) {
            return false;
        }
        if (!Objects.equals(this.fechaVencimiento, other.fechaVencimiento)) {
            return false;
        }
        if (!Objects.equals(this.idEmpaqueL1, other.idEmpaqueL1)) {
            return false;
        }
        if (!Objects.equals(this.idEmpaqueL2, other.idEmpaqueL2)) {
            return false;
        }
        if (!Objects.equals(this.idEmpaqueL3, other.idEmpaqueL3)) {
            return false;
        }
        if (!Objects.equals(this.idEmpaqueL4, other.idEmpaqueL4)) {
            return false;
        }
        if (!Objects.equals(this.fechaEntregaTentativa, other.fechaEntregaTentativa)) {
            return false;
        }
        if (!Objects.equals(this.fechaEntregaAgendada, other.fechaEntregaAgendada)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ProductoEntrega[");
        builder.append("id = ").append(id);
        builder.append(", idProducto = ").append(idProducto);
        builder.append(", loteOrigen = ").append(loteOrigen);
        builder.append(", fechaVencimiento = ").append(fechaVencimiento);
        builder.append(", idEmpaqueL1 = ").append(idEmpaqueL1);
        builder.append(", idEmpaqueL2 = ").append(idEmpaqueL2);
        builder.append(", idEmpaqueL3 = ").append(idEmpaqueL3);
        builder.append(", idEmpaqueL4 = ").append(idEmpaqueL4);
        builder.append(", fechaEntregaTentativa = ").append(fechaEntregaTentativa);
        builder.append(", fechaEntregaAgendada = ").append(fechaEntregaAgendada);
        builder.append("]");
        return builder.toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Columnas">
    public static class COLUMNS implements Serializable {

        public static final JpaColumn<Long> id = JpaColumn.instance("id", Long.class, false);
        public static final JpaColumn<Long> idProducto = JpaColumn.instance("idProducto", Long.class, true);
        public static final JpaColumn<String> loteOrigen = JpaColumn.instance("loteOrigen", String.class, true);
        public static final JpaColumn<java.util.Date> fechaVencimiento = JpaColumn.instance("fechaVencimiento", java.util.Date.class, true);
        public static final JpaColumn<Long> idEmpaqueL1 = JpaColumn.instance("idEmpaqueL1", Long.class, true);
        public static final JpaColumn<Long> idEmpaqueL2 = JpaColumn.instance("idEmpaqueL2", Long.class, true);
        public static final JpaColumn<Long> idEmpaqueL3 = JpaColumn.instance("idEmpaqueL3", Long.class, true);
        public static final JpaColumn<Long> idEmpaqueL4 = JpaColumn.instance("idEmpaqueL4", Long.class, true);
        public static final JpaColumn<java.util.Date> fechaEntregaTentativa = JpaColumn.instance("fechaEntregaTentativa", java.util.Date.class, true);
        public static final JpaColumn<java.util.Date> fechaEntregaAgendada = JpaColumn.instance("fechaEntregaAgendada", java.util.Date.class, true);
    }
    //</editor-fold>

}
