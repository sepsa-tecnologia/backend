/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.configs;

import fa.gs.utils.misc.Assertions;
import java.util.UUID;
import java.util.prefs.Preferences;

/**
 *
 * @author Fabio A. González Sosa
 */
public class RecepcionConfigV1 implements RecepcionConfig {

    private final Preferences configPreferences;

    public RecepcionConfigV1(Preferences confiPreferences) {
        this.configPreferences = confiPreferences;
    }

    @Override
    public boolean isStandaloneApp() {
        boolean isStandalone = configPreferences.node("recepcion").getBoolean("standalone", false);
        return isStandalone;
    }

    @Override
    public UUID getStandaloneShard() {
        String shard0 = configPreferences.node("recepcion").get("shard", "");
        if (Assertions.stringNullOrEmpty(shard0)) {
            return null;
        } else {
            return UUID.fromString(shard0);
        }
    }

}
