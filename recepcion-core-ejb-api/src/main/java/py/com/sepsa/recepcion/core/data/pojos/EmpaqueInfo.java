/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import fa.gs.utils.collections.maps.ResultSetMap;
import fa.gs.utils.database.criteria.column.NativeColumn;
import java.io.Serializable;
import java.math.BigDecimal;
import py.com.sepsa.recepcion.core.data.enums.TipoEmpaqueEnum;

/**
 *
 * @author Fabio A. González Sosa
 */
public class EmpaqueInfo implements Serializable {

    private Long id;
    private TipoEmpaqueEnum tipoEmpaque;
    private String codigoSscc;
    private BigDecimal alto;
    private BigDecimal ancho;
    private BigDecimal profundo;
    private BigDecimal peso;

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public Long getId() {
        return this.id;
    }

    public void setId(Long value) {
        this.id = value;
    }

    public TipoEmpaqueEnum getTipoEmpaque() {
        return this.tipoEmpaque;
    }

    public void setTipoEmpaque(TipoEmpaqueEnum value) {
        this.tipoEmpaque = value;
    }

    public String getCodigoSscc() {
        return this.codigoSscc;
    }

    public void setCodigoSscc(String value) {
        this.codigoSscc = value;
    }

    public BigDecimal getAlto() {
        return this.alto;
    }

    public void setAlto(BigDecimal value) {
        this.alto = value;
    }

    public BigDecimal getAncho() {
        return this.ancho;
    }

    public void setAncho(BigDecimal value) {
        this.ancho = value;
    }

    public BigDecimal getProfundo() {
        return this.profundo;
    }

    public void setProfundo(BigDecimal value) {
        this.profundo = value;
    }

    public BigDecimal getPeso() {
        return this.peso;
    }

    public void setPeso(BigDecimal value) {
        this.peso = value;
    }
    //</editor-fold>

    public static class COLUMNS {

        public static final NativeColumn<Long> id = NativeColumn.instance("id", Long.class);
        public static final NativeColumn<String> tipoEmpaqueCodigo = NativeColumn.instance("tipoEmpaqueCodigo", String.class);
        public static final NativeColumn<String> codigoSscc = NativeColumn.instance("codigoSscc", String.class);
        public static final NativeColumn<BigDecimal> alto = NativeColumn.instance("alto", BigDecimal.class);
        public static final NativeColumn<BigDecimal> ancho = NativeColumn.instance("ancho", BigDecimal.class);
        public static final NativeColumn<BigDecimal> profundo = NativeColumn.instance("profundo", BigDecimal.class);
        public static final NativeColumn<BigDecimal> peso = NativeColumn.instance("peso", BigDecimal.class);
    }

    public static class Mapper extends fa.gs.utils.database.utils.ResultSetMapper<EmpaqueInfo> {

        @Override
        protected EmpaqueInfo getEmptyAdaptee() {
            return new EmpaqueInfo();
        }

        @Override
        protected EmpaqueInfo adapt(EmpaqueInfo obj, ResultSetMap resultSet) {
            Long id0 = resultSet.long0(COLUMNS.id);
            TipoEmpaqueEnum tipoEmpaque0 = resultSet.enumerable(COLUMNS.tipoEmpaqueCodigo, TipoEmpaqueEnum.adapter());
            String codigoSscc0 = resultSet.string(COLUMNS.codigoSscc);
            BigDecimal alto0 = resultSet.bigdecimal(COLUMNS.alto);
            BigDecimal ancho0 = resultSet.bigdecimal(COLUMNS.ancho);
            BigDecimal profundo0 = resultSet.bigdecimal(COLUMNS.profundo);
            BigDecimal peso0 = resultSet.bigdecimal(COLUMNS.peso);

            obj.setId(id0);
            obj.setTipoEmpaque(tipoEmpaque0);
            obj.setCodigoSscc(codigoSscc0);
            obj.setAlto(alto0);
            obj.setAncho(ancho0);
            obj.setProfundo(profundo0);
            obj.setPeso(peso0);
            return obj;
        }
    }
}
