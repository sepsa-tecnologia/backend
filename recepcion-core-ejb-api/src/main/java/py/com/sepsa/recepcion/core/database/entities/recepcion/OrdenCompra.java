package py.com.sepsa.recepcion.core.database.entities.recepcion;

import fa.gs.utils.database.criteria.column.JpaColumn;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "orden_compra", catalog = "sepsa", schema = "recepcion")
@DynamicInsert
public class OrdenCompra implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_id")
    @SequenceGenerator(name = "gen_id", sequenceName = "recepcion.orden_compra_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("nextval('recepcion.orden_compra_id_seq'::regclass)")
    private Long id;

    @Column(name = "id_documento", nullable = true)
    @Basic(optional = true)
    private Long idDocumento;

    @Column(name = "nro_documento", nullable = false)
    @Basic(optional = false)
    private String nroDocumento;

    @Column(name = "id_estado", nullable = true)
    @Basic(optional = true)
    private Long idEstado;

    @Column(name = "gln_origen", nullable = false)
    @Basic(optional = false)
    private String glnOrigen;

    @Column(name = "razon_social_origen", nullable = true)
    @Basic(optional = true)
    private String razonSocialOrigen;

    @Column(name = "ruc_origen", nullable = true)
    @Basic(optional = true)
    private String rucOrigen;

    @Column(name = "gln_destino", nullable = false)
    @Basic(optional = false)
    private String glnDestino;

    @Column(name = "razon_social_destino", nullable = true)
    @Basic(optional = true)
    private String razonSocialDestino;

    @Column(name = "ruc_destino", nullable = true)
    @Basic(optional = true)
    private String rucDestino;

    @Column(name = "fecha_registro", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("now()")
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date fechaRegistro;

    @Column(name = "fecha_emision", nullable = false)
    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date fechaEmision;

    @Column(name = "id_informacion_legal", nullable = true)
    @Basic(optional = true)
    private Long idInformacionLegal;

    @Column(name = "id_informacion_entrega", nullable = true)
    @Basic(optional = true)
    private Long idInformacionEntrega;

    @Column(name = "meta__shard", nullable = true)
    @Basic(optional = true)
    @org.hibernate.annotations.Type(type = "org.hibernate.type.PostgresUUIDType")
    private java.util.UUID metaShard;

    @Column(name = "meta__eliminado", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("'0'::bpchar")
    private String metaEliminado;

    @Column(name = "meta__fecha_eliminacion", nullable = true)
    @Basic(optional = true)
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date metaFechaEliminacion;

    public void setId(Long value) {
        this.id = value;
    }

    public void setIdDocumento(Long value) {
        this.idDocumento = value;
    }

    public void setNroDocumento(String value) {
        this.nroDocumento = value;
    }

    public void setIdEstado(Long value) {
        this.idEstado = value;
    }

    public void setGlnOrigen(String value) {
        this.glnOrigen = value;
    }

    public void setRazonSocialOrigen(String value) {
        this.razonSocialOrigen = value;
    }

    public void setRucOrigen(String value) {
        this.rucOrigen = value;
    }

    public void setGlnDestino(String value) {
        this.glnDestino = value;
    }

    public void setRazonSocialDestino(String value) {
        this.razonSocialDestino = value;
    }

    public void setRucDestino(String value) {
        this.rucDestino = value;
    }

    public void setFechaRegistro(java.util.Date value) {
        this.fechaRegistro = value;
    }

    public void setFechaEmision(java.util.Date value) {
        this.fechaEmision = value;
    }

    public void setIdInformacionLegal(Long value) {
        this.idInformacionLegal = value;
    }

    public void setIdInformacionEntrega(Long value) {
        this.idInformacionEntrega = value;
    }

    public void setMetaShard(java.util.UUID value) {
        this.metaShard = value;
    }

    public void setMetaEliminado(String value) {
        this.metaEliminado = value;
    }

    public void setMetaFechaEliminacion(java.util.Date value) {
        this.metaFechaEliminacion = value;
    }

    public Long getId() {
        return this.id;
    }

    public Long getIdDocumento() {
        return this.idDocumento;
    }

    public String getNroDocumento() {
        return this.nroDocumento;
    }

    public Long getIdEstado() {
        return this.idEstado;
    }

    public String getGlnOrigen() {
        return this.glnOrigen;
    }

    public String getRazonSocialOrigen() {
        return this.razonSocialOrigen;
    }

    public String getRucOrigen() {
        return this.rucOrigen;
    }

    public String getGlnDestino() {
        return this.glnDestino;
    }

    public String getRazonSocialDestino() {
        return this.razonSocialDestino;
    }

    public String getRucDestino() {
        return this.rucDestino;
    }

    public java.util.Date getFechaRegistro() {
        return this.fechaRegistro;
    }

    public java.util.Date getFechaEmision() {
        return this.fechaEmision;
    }

    public Long getIdInformacionLegal() {
        return this.idInformacionLegal;
    }

    public Long getIdInformacionEntrega() {
        return this.idInformacionEntrega;
    }

    public java.util.UUID getMetaShard() {
        return this.metaShard;
    }

    public String getMetaEliminado() {
        return this.metaEliminado;
    }

    public java.util.Date getMetaFechaEliminacion() {
        return this.metaFechaEliminacion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.idDocumento);
        hash = 53 * hash + Objects.hashCode(this.nroDocumento);
        hash = 53 * hash + Objects.hashCode(this.idEstado);
        hash = 53 * hash + Objects.hashCode(this.glnOrigen);
        hash = 53 * hash + Objects.hashCode(this.razonSocialOrigen);
        hash = 53 * hash + Objects.hashCode(this.rucOrigen);
        hash = 53 * hash + Objects.hashCode(this.glnDestino);
        hash = 53 * hash + Objects.hashCode(this.razonSocialDestino);
        hash = 53 * hash + Objects.hashCode(this.rucDestino);
        hash = 53 * hash + Objects.hashCode(this.fechaRegistro);
        hash = 53 * hash + Objects.hashCode(this.fechaEmision);
        hash = 53 * hash + Objects.hashCode(this.idInformacionLegal);
        hash = 53 * hash + Objects.hashCode(this.idInformacionEntrega);
        hash = 53 * hash + Objects.hashCode(this.metaShard);
        hash = 53 * hash + Objects.hashCode(this.metaEliminado);
        hash = 53 * hash + Objects.hashCode(this.metaFechaEliminacion);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrdenCompra other = (OrdenCompra) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.idDocumento, other.idDocumento)) {
            return false;
        }
        if (!Objects.equals(this.nroDocumento, other.nroDocumento)) {
            return false;
        }
        if (!Objects.equals(this.idEstado, other.idEstado)) {
            return false;
        }
        if (!Objects.equals(this.glnOrigen, other.glnOrigen)) {
            return false;
        }
        if (!Objects.equals(this.razonSocialOrigen, other.razonSocialOrigen)) {
            return false;
        }
        if (!Objects.equals(this.rucOrigen, other.rucOrigen)) {
            return false;
        }
        if (!Objects.equals(this.glnDestino, other.glnDestino)) {
            return false;
        }
        if (!Objects.equals(this.razonSocialDestino, other.razonSocialDestino)) {
            return false;
        }
        if (!Objects.equals(this.rucDestino, other.rucDestino)) {
            return false;
        }
        if (!Objects.equals(this.fechaRegistro, other.fechaRegistro)) {
            return false;
        }
        if (!Objects.equals(this.fechaEmision, other.fechaEmision)) {
            return false;
        }
        if (!Objects.equals(this.idInformacionLegal, other.idInformacionLegal)) {
            return false;
        }
        if (!Objects.equals(this.idInformacionEntrega, other.idInformacionEntrega)) {
            return false;
        }
        if (!Objects.equals(this.metaShard, other.metaShard)) {
            return false;
        }
        if (!Objects.equals(this.metaEliminado, other.metaEliminado)) {
            return false;
        }
        if (!Objects.equals(this.metaFechaEliminacion, other.metaFechaEliminacion)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("OrdenCompra[");
        builder.append("id = ").append(id);
        builder.append(", idDocumento = ").append(idDocumento);
        builder.append(", nroDocumento = ").append(nroDocumento);
        builder.append(", idEstado = ").append(idEstado);
        builder.append(", glnOrigen = ").append(glnOrigen);
        builder.append(", razonSocialOrigen = ").append(razonSocialOrigen);
        builder.append(", rucOrigen = ").append(rucOrigen);
        builder.append(", glnDestino = ").append(glnDestino);
        builder.append(", razonSocialDestino = ").append(razonSocialDestino);
        builder.append(", rucDestino = ").append(rucDestino);
        builder.append(", fechaRegistro = ").append(fechaRegistro);
        builder.append(", fechaEmision = ").append(fechaEmision);
        builder.append(", idInformacionLegal = ").append(idInformacionLegal);
        builder.append(", idInformacionEntrega = ").append(idInformacionEntrega);
        builder.append(", metaShard = ").append(metaShard);
        builder.append(", metaEliminado = ").append(metaEliminado);
        builder.append(", metaFechaEliminacion = ").append(metaFechaEliminacion);
        builder.append("]");
        return builder.toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Columnas">
    public static class COLUMNS implements Serializable {

        public static final JpaColumn<Long> id = JpaColumn.instance("id", Long.class, false);
        public static final JpaColumn<Long> idDocumento = JpaColumn.instance("idDocumento", Long.class, true);
        public static final JpaColumn<String> nroDocumento = JpaColumn.instance("nroDocumento", String.class, false);
        public static final JpaColumn<Long> idEstado = JpaColumn.instance("idEstado", Long.class, true);
        public static final JpaColumn<String> glnOrigen = JpaColumn.instance("glnOrigen", String.class, false);
        public static final JpaColumn<String> razonSocialOrigen = JpaColumn.instance("razonSocialOrigen", String.class, true);
        public static final JpaColumn<String> rucOrigen = JpaColumn.instance("rucOrigen", String.class, true);
        public static final JpaColumn<String> glnDestino = JpaColumn.instance("glnDestino", String.class, false);
        public static final JpaColumn<String> razonSocialDestino = JpaColumn.instance("razonSocialDestino", String.class, true);
        public static final JpaColumn<String> rucDestino = JpaColumn.instance("rucDestino", String.class, true);
        public static final JpaColumn<java.util.Date> fechaRegistro = JpaColumn.instance("fechaRegistro", java.util.Date.class, false);
        public static final JpaColumn<java.util.Date> fechaEmision = JpaColumn.instance("fechaEmision", java.util.Date.class, false);
        public static final JpaColumn<Long> idInformacionLegal = JpaColumn.instance("idInformacionLegal", Long.class, true);
        public static final JpaColumn<Long> idInformacionEntrega = JpaColumn.instance("idInformacionEntrega", Long.class, true);
        public static final JpaColumn<java.util.UUID> metaShard = JpaColumn.instance("metaShard", java.util.UUID.class, true);
        public static final JpaColumn<String> metaEliminado = JpaColumn.instance("metaEliminado", String.class, false);
        public static final JpaColumn<java.util.Date> metaFechaEliminacion = JpaColumn.instance("metaFechaEliminacion", java.util.Date.class, true);
    }
    //</editor-fold>

}
