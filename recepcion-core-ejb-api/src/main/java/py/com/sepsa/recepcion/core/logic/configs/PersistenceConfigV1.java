/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.configs;

import fa.gs.utils.misc.Assertions;
import java.util.UUID;
import java.util.prefs.Preferences;

/**
 *
 * @author Fabio A. González Sosa
 */
public class PersistenceConfigV1 implements PersistenceConfig {

    private final Preferences configPreferences;

    public PersistenceConfigV1(Preferences confiPreferences) {
        this.configPreferences = confiPreferences;
    }

    @Override
    public String getDatasourceName() {
        Preferences node = configPreferences.node("persistence");
        if (node != null) {
            return node.get("datasource", "");
        } else {
            return "";
        }
    }

}
