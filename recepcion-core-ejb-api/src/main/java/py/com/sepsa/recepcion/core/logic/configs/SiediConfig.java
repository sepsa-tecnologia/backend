/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.configs;

import java.io.File;
import java.io.Serializable;
import py.com.sepsa.recepcion.core.data.enums.SiediDocumentFormat;
import py.com.sepsa.recepcion.core.data.enums.SiediVersion;

/**
 *
 * @author Fabio A. González Sosa
 */
public interface SiediConfig extends Serializable {

    SiediVersion getSiediVersion();

    File getCarpetaSalida();

    SiediDocumentFormat getFormatoSalida();

    File getCarpetaEntrada();

    SiediDocumentFormat getFormatoEntrada();

}
