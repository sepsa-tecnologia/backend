/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.adapters;

import com.google.gson.JsonElement;
import fa.gs.utils.adapters.impl.json.ToJsonAdapter;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import py.com.sepsa.recepcion.core.data.pojos.MonedaInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
public class MonedaInfo2JsonAdapter extends ToJsonAdapter<MonedaInfo> {

    @Override
    protected JsonElement adapt0(MonedaInfo obj) {
        JsonObjectBuilder json = JsonObjectBuilder.instance();
        json.add("id", obj.getId());
        json.add("codigo", obj.getMoneda().codigo());
        return json.build();
    }

}
