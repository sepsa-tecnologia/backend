package py.com.sepsa.recepcion.core.database.entities.recepcion;

import fa.gs.utils.database.criteria.column.JpaColumn;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "informacion_entrega", catalog = "sepsa", schema = "recepcion")
@DynamicInsert
public class InformacionEntrega implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_id")
    @SequenceGenerator(name = "gen_id", sequenceName = "recepcion.informacion_entrega_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("nextval('recepcion.informacion_entrega_id_seq'::regclass)")
    private Long id;

    @Column(name = "fecha_inicio_plazo_entrega", nullable = true)
    @Basic(optional = true)
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date fechaInicioPlazoEntrega;

    @Column(name = "dias_plazo_entrega", nullable = true)
    @Basic(optional = true)
    private Integer diasPlazoEntrega;

    @Column(name = "condiciones_especiales", nullable = true)
    @Basic(optional = true)
    private String condicionesEspeciales;

    public void setId(Long value) {
        this.id = value;
    }

    public void setFechaInicioPlazoEntrega(java.util.Date value) {
        this.fechaInicioPlazoEntrega = value;
    }

    public void setDiasPlazoEntrega(Integer value) {
        this.diasPlazoEntrega = value;
    }

    public void setCondicionesEspeciales(String value) {
        this.condicionesEspeciales = value;
    }

    public Long getId() {
        return this.id;
    }

    public java.util.Date getFechaInicioPlazoEntrega() {
        return this.fechaInicioPlazoEntrega;
    }

    public Integer getDiasPlazoEntrega() {
        return this.diasPlazoEntrega;
    }

    public String getCondicionesEspeciales() {
        return this.condicionesEspeciales;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.fechaInicioPlazoEntrega);
        hash = 53 * hash + Objects.hashCode(this.diasPlazoEntrega);
        hash = 53 * hash + Objects.hashCode(this.condicionesEspeciales);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InformacionEntrega other = (InformacionEntrega) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.fechaInicioPlazoEntrega, other.fechaInicioPlazoEntrega)) {
            return false;
        }
        if (!Objects.equals(this.diasPlazoEntrega, other.diasPlazoEntrega)) {
            return false;
        }
        if (!Objects.equals(this.condicionesEspeciales, other.condicionesEspeciales)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("InformacionEntrega[");
        builder.append("id = ").append(id);
        builder.append(", fechaInicioPlazoEntrega = ").append(fechaInicioPlazoEntrega);
        builder.append(", diasPlazoEntrega = ").append(diasPlazoEntrega);
        builder.append(", condicionesEspeciales = ").append(condicionesEspeciales);
        builder.append("]");
        return builder.toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Columnas">
    public static class COLUMNS implements Serializable {

        public static final JpaColumn<Long> id = JpaColumn.instance("id", Long.class, false);
        public static final JpaColumn<java.util.Date> fechaInicioPlazoEntrega = JpaColumn.instance("fechaInicioPlazoEntrega", java.util.Date.class, true);
        public static final JpaColumn<Integer> diasPlazoEntrega = JpaColumn.instance("diasPlazoEntrega", Integer.class, true);
        public static final JpaColumn<String> condicionesEspeciales = JpaColumn.instance("condicionesEspeciales", String.class, true);
    }
    //</editor-fold>

}
