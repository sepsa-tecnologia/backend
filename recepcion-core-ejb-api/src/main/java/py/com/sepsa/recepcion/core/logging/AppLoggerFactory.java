/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logging;

import fa.gs.utils.logging.Logger;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.logging.log4j2.Log4j2LoggerFactory;
import java.io.InputStream;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author Fabio A. González Sosa
 */
public class AppLoggerFactory extends Log4j2LoggerFactory {

    static {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream is = loader.getResourceAsStream("py/com/sepsa/recepcion/core/logging/Log.xml");
        Log4j2LoggerFactory.configure(is);
    }

    private static AppLogger logger(String name) {
        org.apache.logging.log4j.core.Logger impl = (org.apache.logging.log4j.core.Logger) LogManager.getLogger(name);
        return new AppLogger(impl);
    }

    public static AppLogger core() {
        AppLogger logger = logger("recepcion.core.logger");
        logger.tag("origen", "recepcion.core");
        return logger;
    }

    public static AppLogger api() {
        AppLogger logger = logger("recepcion.api.logger");
        logger.tag("origen", "recepcion.api");
        return logger;
    }

    public static AppLogger workers() {
        AppLogger logger = logger("recepcion.workers.logger");
        logger.tag("origen", "recepcion.workers");
        return logger;
    }

    @Override
    public Logger getLogger(String name) {
        return AppLoggerFactory.logger(name);
    }

}
