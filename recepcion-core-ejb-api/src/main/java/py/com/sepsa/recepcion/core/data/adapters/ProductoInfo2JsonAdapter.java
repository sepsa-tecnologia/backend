/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.adapters;

import com.google.gson.JsonElement;
import fa.gs.utils.adapters.impl.json.ToJsonAdapter;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import py.com.sepsa.recepcion.core.data.pojos.ProductoInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ProductoInfo2JsonAdapter extends ToJsonAdapter<ProductoInfo> {

    @Override
    protected JsonElement adapt0(ProductoInfo obj) {
        JsonObjectBuilder json = JsonObjectBuilder.instance();
        json.add("id", obj.getId());
        json.add("descripcion", adaptDescripcion(obj));
        json.add("marca", obj.getMarca());
        json.add("procedencia", obj.getPaisProcedencia());
        json.add("codigos", adaptCodigos(obj));
        return json.build();
    }

    private JsonElement adaptDescripcion(ProductoInfo obj) {
        JsonObjectBuilder json = JsonObjectBuilder.instance();
        json.add("corta", obj.getDescripcionCorta());
        json.add("larga", obj.getDescripcionLarga());
        return json.build();
    }

    private JsonElement adaptCodigos(ProductoInfo obj) {
        JsonObjectBuilder json = JsonObjectBuilder.instance();
        json.add("gtin", obj.getCodigoGtin());
        json.add("itnerno", obj.getCodigoInterno());
        return json.build();
    }

}
