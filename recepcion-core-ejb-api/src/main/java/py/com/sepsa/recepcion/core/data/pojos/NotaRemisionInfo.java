/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.criteria.column.NativeColumn;
import fa.gs.utils.database.mapping.Mapping;
import fa.gs.utils.database.mapping.mappers.DateMapper;
import fa.gs.utils.database.mapping.mappers.EnumMapper;
import fa.gs.utils.database.mapping.mappers.LongMapper;
import fa.gs.utils.database.mapping.mappers.PojoMapper;
import fa.gs.utils.database.mapping.mappers.StringMapper;
import fa.gs.utils.database.mapping.mappings.Mappings;
import fa.gs.utils.misc.Assertions;
import java.util.Collection;
import java.util.Date;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;

/**
 *
 * @author Fabio A. González Sosa
 */
public class NotaRemisionInfo implements DocumentoInfo, DocumentoConEstado, DocumentoConNro {

    private Long id;
    private EstadoEnum estado;
    private Long idDocumento;
    private String nroDocumento;
    private Long idOrdenCompra;
    private String rucProveedor;
    private String razonSocialProveedor;
    private Date fechaRegistro;
    private Date fechaEmision;
    private final Collection<NotaRemisionDetalleInfo> detalles;

    public NotaRemisionInfo() {
        this.detalles = Lists.empty();
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }

    public Long getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Long idDocumento) {
        this.idDocumento = idDocumento;
    }

    @Override
    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public Long getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Long idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public String getRucProveedor() {
        return rucProveedor;
    }

    public void setRucProveedor(String rucProveedor) {
        this.rucProveedor = rucProveedor;
    }

    public String getRazonSocialProveedor() {
        return razonSocialProveedor;
    }

    public void setRazonSocialProveedor(String razonSocialProveedor) {
        this.razonSocialProveedor = razonSocialProveedor;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Collection<NotaRemisionDetalleInfo> getDetalles() {
        return detalles;
    }

    public void setDetalles(Collection<NotaRemisionDetalleInfo> detalles) {
        if (!Assertions.isNullOrEmpty(detalles)) {
            this.detalles.addAll(detalles);
        }
    }

    @Override
    public TipoEntidadEnum getTipoDocumento() {
        return TipoEntidadEnum.NOTA_REMISION;
    }
    //</editor-fold>

    public static class MAPPINGS {

        public static final Mapping<Long> id = Mappings.instance("id", Long.class);
        public static final Mapping<String> estadoCodigo = Mappings.instance("estado", String.class);
        public static final Mapping<Long> idDocumento = Mappings.instance("idDocumento", Long.class);
        public static final Mapping<String> nroDocumento = Mappings.instance("nroDocumento", String.class);
        public static final Mapping<Long> idOrdenCompra = Mappings.instance("idOrdenCompra", Long.class);
        public static final Mapping<String> rucProveedor = Mappings.instance("rucProveedor", String.class);
        public static final Mapping<String> razonSocialProveedor = Mappings.instance("razonSocialProveedor", String.class);
        public static final Mapping<Date> fechaRegistro = Mappings.instance("fechaRegistro", Date.class);
        public static final Mapping<Date> fechaEmision = Mappings.instance("fechaEmision", Date.class);
    }

    public static class COLUMNS {

        public static final NativeColumn<Long> nr_id = NativeColumn.instance("nr", "id", Long.class);
        public static final NativeColumn<String> e_estado_codigo = NativeColumn.instance("e", "codigo", String.class);
        public static final NativeColumn<Long> nr_id_documento = NativeColumn.instance("nr", "id_documento", Long.class);
        public static final NativeColumn<String> nr_nro_documento = NativeColumn.instance("nr", "nro_documento", String.class);
        public static final NativeColumn<Long> nr_id_orden_compra = NativeColumn.instance("nr", "id_orden_compra", Long.class);
        public static final NativeColumn<Long> oc_ruc_proveedor = NativeColumn.instance("oc", "ruc_destino", Long.class);
        public static final NativeColumn<Long> oc_razon_social_proveedor = NativeColumn.instance("oc", "razon_social_destino", Long.class);
        public static final NativeColumn<Date> nr_fecha_registro = NativeColumn.instance("nr", "fecha_registro", Date.class);
        public static final NativeColumn<Date> nr_fecha_emision = NativeColumn.instance("nr", "fecha_emision", Date.class);
    }

    public static class MAPPER extends PojoMapper<MAPPER, NotaRemisionInfo> {

        public static MAPPER INSTANCE;

        static {
            MAPPER.INSTANCE = new MAPPER();
            MAPPER.INSTANCE.with(LongMapper.instance(MAPPINGS.id));
            MAPPER.INSTANCE.with(EnumMapper.instance(MAPPINGS.estadoCodigo, EstadoEnum.adapter()));
            MAPPER.INSTANCE.with(LongMapper.instance(MAPPINGS.idDocumento));
            MAPPER.INSTANCE.with(StringMapper.instance(MAPPINGS.nroDocumento));
            MAPPER.INSTANCE.with(LongMapper.instance(MAPPINGS.idOrdenCompra));
            MAPPER.INSTANCE.with(StringMapper.instance(MAPPINGS.rucProveedor));
            MAPPER.INSTANCE.with(StringMapper.instance(MAPPINGS.razonSocialProveedor));
            MAPPER.INSTANCE.with(DateMapper.instance(MAPPINGS.fechaRegistro));
            MAPPER.INSTANCE.with(DateMapper.instance(MAPPINGS.fechaEmision));
        }

        @Override
        protected NotaRemisionInfo getEmptyAdaptee() {
            return new NotaRemisionInfo();
        }

    }

}
