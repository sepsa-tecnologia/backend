/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.enums;

import fa.gs.utils.collections.enums.EnumerableAdapter;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum TipoObservacionEnum {
    PREDETERMINADO("0001", "Observación por defecto."),
    VALIDACION_AUTOMATICA_PRODUCTO("0002", "Mensaje generado automáticamente para un producto pedido."),
    VALIDACION_MANUAL_PRODUCTO("0003", "Mensaje generado manualmente para un producto pedido.");
    private final String codigo;
    private final String descripcion;

    private TipoObservacionEnum(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static EnumerableAdapter<TipoObservacionEnum> adapter() {
        return new Adapter0();
    }

    public static TipoObservacionEnum from(String codigo) {
        for (TipoObservacionEnum tipoEmpaqueEnum : TipoObservacionEnum.values()) {
            if (Assertions.equals(codigo, tipoEmpaqueEnum.codigo)) {
                return tipoEmpaqueEnum;
            }
        }

        return TipoObservacionEnum.PREDETERMINADO;
    }

    public String codigo() {
        return codigo;
    }

    public String descripcion() {
        return descripcion;
    }

    private static class Adapter0 implements EnumerableAdapter<TipoObservacionEnum> {

        @Override
        public TipoObservacionEnum getEnumerable(Object obj) {
            if (obj instanceof String) {
                String codigo = (String) obj;
                return TipoObservacionEnum.from(codigo);
            }

            throw Errors.unsupported();
        }
    }

}
