package py.com.sepsa.recepcion.core.database.entities.recepcion;

import fa.gs.utils.database.criteria.column.JpaColumn;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "producto", catalog = "sepsa", schema = "recepcion")
@DynamicInsert
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_id")
    @SequenceGenerator(name = "gen_id", sequenceName = "recepcion.producto_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("nextval('recepcion.producto_id_seq'::regclass)")
    private Long id;

    @Column(name = "descripcion_corta", nullable = false)
    @Basic(optional = false)
    private String descripcionCorta;

    @Column(name = "descripcion_larga", nullable = true)
    @Basic(optional = true)
    private String descripcionLarga;

    @Column(name = "codigo_gtin", nullable = false)
    @Basic(optional = false)
    private String codigoGtin;

    @Column(name = "codigo_interno", nullable = true)
    @Basic(optional = true)
    private String codigoInterno;

    @Column(name = "id_marca", nullable = true)
    @Basic(optional = true)
    private Long idMarca;

    @Column(name = "id_procedencia", nullable = true)
    @Basic(optional = true)
    private Long idProcedencia;

    @Column(name = "meta__shard", nullable = true)
    @Basic(optional = true)
    @org.hibernate.annotations.Type(type = "org.hibernate.type.PostgresUUIDType")
    private java.util.UUID metaShard;

    @Column(name = "meta__eliminado", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("'0'::bpchar")
    private String metaEliminado;

    @Column(name = "meta__fecha_eliminacion", nullable = true)
    @Basic(optional = true)
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date metaFechaEliminacion;

    public void setId(Long value) {
        this.id = value;
    }

    public void setDescripcionCorta(String value) {
        this.descripcionCorta = value;
    }

    public void setDescripcionLarga(String value) {
        this.descripcionLarga = value;
    }

    public void setCodigoGtin(String value) {
        this.codigoGtin = value;
    }

    public void setCodigoInterno(String value) {
        this.codigoInterno = value;
    }

    public void setIdMarca(Long value) {
        this.idMarca = value;
    }

    public void setIdProcedencia(Long value) {
        this.idProcedencia = value;
    }

    public void setMetaShard(java.util.UUID value) {
        this.metaShard = value;
    }

    public void setMetaEliminado(String value) {
        this.metaEliminado = value;
    }

    public void setMetaFechaEliminacion(java.util.Date value) {
        this.metaFechaEliminacion = value;
    }

    public Long getId() {
        return this.id;
    }

    public String getDescripcionCorta() {
        return this.descripcionCorta;
    }

    public String getDescripcionLarga() {
        return this.descripcionLarga;
    }

    public String getCodigoGtin() {
        return this.codigoGtin;
    }

    public String getCodigoInterno() {
        return this.codigoInterno;
    }

    public Long getIdMarca() {
        return this.idMarca;
    }

    public Long getIdProcedencia() {
        return this.idProcedencia;
    }

    public java.util.UUID getMetaShard() {
        return this.metaShard;
    }

    public String getMetaEliminado() {
        return this.metaEliminado;
    }

    public java.util.Date getMetaFechaEliminacion() {
        return this.metaFechaEliminacion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.descripcionCorta);
        hash = 53 * hash + Objects.hashCode(this.descripcionLarga);
        hash = 53 * hash + Objects.hashCode(this.codigoGtin);
        hash = 53 * hash + Objects.hashCode(this.codigoInterno);
        hash = 53 * hash + Objects.hashCode(this.idMarca);
        hash = 53 * hash + Objects.hashCode(this.idProcedencia);
        hash = 53 * hash + Objects.hashCode(this.metaShard);
        hash = 53 * hash + Objects.hashCode(this.metaEliminado);
        hash = 53 * hash + Objects.hashCode(this.metaFechaEliminacion);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Producto other = (Producto) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.descripcionCorta, other.descripcionCorta)) {
            return false;
        }
        if (!Objects.equals(this.descripcionLarga, other.descripcionLarga)) {
            return false;
        }
        if (!Objects.equals(this.codigoGtin, other.codigoGtin)) {
            return false;
        }
        if (!Objects.equals(this.codigoInterno, other.codigoInterno)) {
            return false;
        }
        if (!Objects.equals(this.idMarca, other.idMarca)) {
            return false;
        }
        if (!Objects.equals(this.idProcedencia, other.idProcedencia)) {
            return false;
        }
        if (!Objects.equals(this.metaShard, other.metaShard)) {
            return false;
        }
        if (!Objects.equals(this.metaEliminado, other.metaEliminado)) {
            return false;
        }
        if (!Objects.equals(this.metaFechaEliminacion, other.metaFechaEliminacion)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Producto[");
        builder.append("id = ").append(id);
        builder.append(", descripcionCorta = ").append(descripcionCorta);
        builder.append(", descripcionLarga = ").append(descripcionLarga);
        builder.append(", codigoGtin = ").append(codigoGtin);
        builder.append(", codigoInterno = ").append(codigoInterno);
        builder.append(", idMarca = ").append(idMarca);
        builder.append(", idProcedencia = ").append(idProcedencia);
        builder.append(", metaShard = ").append(metaShard);
        builder.append(", metaEliminado = ").append(metaEliminado);
        builder.append(", metaFechaEliminacion = ").append(metaFechaEliminacion);
        builder.append("]");
        return builder.toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Columnas">
    public static class COLUMNS implements Serializable {

        public static final JpaColumn<Long> id = JpaColumn.instance("id", Long.class, false);
        public static final JpaColumn<String> descripcionCorta = JpaColumn.instance("descripcionCorta", String.class, false);
        public static final JpaColumn<String> descripcionLarga = JpaColumn.instance("descripcionLarga", String.class, true);
        public static final JpaColumn<String> codigoGtin = JpaColumn.instance("codigoGtin", String.class, false);
        public static final JpaColumn<String> codigoInterno = JpaColumn.instance("codigoInterno", String.class, true);
        public static final JpaColumn<Long> idMarca = JpaColumn.instance("idMarca", Long.class, true);
        public static final JpaColumn<Long> idProcedencia = JpaColumn.instance("idProcedencia", Long.class, true);
        public static final JpaColumn<java.util.UUID> metaShard = JpaColumn.instance("metaShard", java.util.UUID.class, true);
        public static final JpaColumn<String> metaEliminado = JpaColumn.instance("metaEliminado", String.class, false);
        public static final JpaColumn<java.util.Date> metaFechaEliminacion = JpaColumn.instance("metaFechaEliminacion", java.util.Date.class, true);
    }
    //</editor-fold>

}
