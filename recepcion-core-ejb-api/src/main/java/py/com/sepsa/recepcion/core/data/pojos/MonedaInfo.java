/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import fa.gs.utils.collections.maps.ResultSetMap;
import fa.gs.utils.database.criteria.column.NativeColumn;
import java.io.Serializable;
import py.com.sepsa.recepcion.core.data.enums.MonedaEnum;

/**
 *
 * @author Fabio A. González Sosa
 */
public class MonedaInfo implements Serializable {

    private Long id;
    private MonedaEnum moneda;

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MonedaEnum getMoneda() {
        return moneda;
    }

    public void setMoneda(MonedaEnum moneda) {
        this.moneda = moneda;
    }
    //</editor-fold>

    public static class COLUMNS implements Serializable {

        public static final NativeColumn<Long> id = NativeColumn.instance("id", Long.class);
        public static final NativeColumn<String> codigo = NativeColumn.instance("codigo", String.class);
        public static final NativeColumn<String> descripcion = NativeColumn.instance("descripcion", String.class);
        public static final NativeColumn<String> codigoAlfabetico = NativeColumn.instance("codigoAlfabetico", String.class);
        public static final NativeColumn<String> codigoNumerico = NativeColumn.instance("codigoNumerico", String.class);
        public static final NativeColumn<String> simbolo = NativeColumn.instance("simbolo", String.class);
    }

    public static class Mapper extends fa.gs.utils.database.utils.ResultSetMapper<MonedaInfo> {

        @Override
        protected MonedaInfo getEmptyAdaptee() {
            return new MonedaInfo();
        }

        @Override
        protected MonedaInfo adapt(MonedaInfo adaptee, ResultSetMap resultSet) {
            Long id0 = resultSet.long0(COLUMNS.id);
            String codigo0 = resultSet.string(COLUMNS.codigo);

            adaptee.setId(id0);
            adaptee.setMoneda(MonedaEnum.from(codigo0));
            return adaptee;
        }

    }

}
