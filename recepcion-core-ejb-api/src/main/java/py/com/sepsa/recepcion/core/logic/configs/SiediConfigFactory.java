/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.configs;

import java.util.prefs.Preferences;
import py.com.sepsa.recepcion.core.data.enums.SiediVersion;
import static py.com.sepsa.recepcion.core.data.enums.SiediVersion.V1;
import static py.com.sepsa.recepcion.core.data.enums.SiediVersion.V2;

/**
 *
 * @author Fabio A. González Sosa
 */
public class SiediConfigFactory {

    public static SiediConfig instance() throws Throwable {
        final Preferences configPreferences = ConfigPreferences.get();
        final SiediConfig config;

        SiediVersion version = getSiediVersion(configPreferences);
        switch (version) {
            case V1:
                config = new SiediConfigV1(configPreferences);
                break;
            case V2:
                throw new UnsupportedOperationException();
            default:
                config = null;
        }

        return config;
    }

    private static SiediVersion getSiediVersion(Preferences configPreferences) throws Throwable {
        String kind = configPreferences.node("recepcion").get("siedi.kind", "0");
        return SiediVersion.from(kind);
    }

}
