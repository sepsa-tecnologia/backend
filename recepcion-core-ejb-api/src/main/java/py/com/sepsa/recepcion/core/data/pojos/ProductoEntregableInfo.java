/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import fa.gs.utils.collections.maps.ResultSetMap;
import fa.gs.utils.database.criteria.column.NativeColumn;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ProductoEntregableInfo implements Serializable {

    private Long id;
    private ProductoInfo producto;
    private String loteOrigen;
    private Date fechaVencimiento;
    private EmpaqueInfo empaqueL1;
    private EmpaqueInfo empaqueL2;
    private EmpaqueInfo empaqueL3;
    private EmpaqueInfo empaqueL4;
    private Date fechaEntregaTentativa;
    private Date fechaEntregaAgendada;

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public Long getId() {
        return this.id;
    }

    public void setId(Long value) {
        this.id = value;
    }

    public ProductoInfo getProducto() {
        return this.producto;
    }

    public void setProducto(ProductoInfo value) {
        this.producto = value;
    }

    public String getLoteOrigen() {
        return this.loteOrigen;
    }

    public void setLoteOrigen(String value) {
        this.loteOrigen = value;
    }

    public Date getFechaVencimiento() {
        return this.fechaVencimiento;
    }

    public void setFechaVencimiento(Date value) {
        this.fechaVencimiento = value;
    }

    public EmpaqueInfo getEmpaqueL1() {
        return this.empaqueL1;
    }

    public void setEmpaqueL1(EmpaqueInfo value) {
        this.empaqueL1 = value;
    }

    public EmpaqueInfo getEmpaqueL2() {
        return this.empaqueL2;
    }

    public void setEmpaqueL2(EmpaqueInfo value) {
        this.empaqueL2 = value;
    }

    public EmpaqueInfo getEmpaqueL3() {
        return this.empaqueL3;
    }

    public void setEmpaqueL3(EmpaqueInfo value) {
        this.empaqueL3 = value;
    }

    public EmpaqueInfo getEmpaqueL4() {
        return this.empaqueL4;
    }

    public void setEmpaqueL4(EmpaqueInfo value) {
        this.empaqueL4 = value;
    }

    public Date getFechaEntregaTentativa() {
        return this.fechaEntregaTentativa;
    }

    public void setFechaEntregaTentativa(Date value) {
        this.fechaEntregaTentativa = value;
    }

    public Date getFechaEntregaAgendada() {
        return this.fechaEntregaAgendada;
    }

    public void setFechaEntregaAgendada(Date value) {
        this.fechaEntregaAgendada = value;
    }
    //</editor-fold>

    public static class COLUMNS {

        public static final NativeColumn<Long> id = NativeColumn.instance("id", Long.class);
        public static final NativeColumn<Long> idProducto = NativeColumn.instance("idProducto", Long.class);
        public static final NativeColumn<String> loteOrigen = NativeColumn.instance("loteOrigen", String.class);
        public static final NativeColumn<Date> fechaVencimiento = NativeColumn.instance("fechaVencimiento", Date.class);
        public static final NativeColumn<Long> idEmpaqueL1 = NativeColumn.instance("idEmpaqueL1", Long.class);
        public static final NativeColumn<Long> idEmpaqueL2 = NativeColumn.instance("idEmpaqueL2", Long.class);
        public static final NativeColumn<Long> idEmpaqueL3 = NativeColumn.instance("idEmpaqueL3", Long.class);
        public static final NativeColumn<Long> idEmpaqueL4 = NativeColumn.instance("idEmpaqueL4", Long.class);
        public static final NativeColumn<Date> fechaEntregaTentativa = NativeColumn.instance("fechaEntregaTentativa", Date.class);
        public static final NativeColumn<Date> fechaEntregaAgendada = NativeColumn.instance("fechaEntregaAgendada", Date.class);
    }

    public static class Mapper extends fa.gs.utils.database.utils.ResultSetMapper<ProductoEntregableInfo> {

        @Override
        public ProductoEntregableInfo getEmptyAdaptee() {
            return new ProductoEntregableInfo();
        }

        @Override
        public ProductoEntregableInfo adapt(final ProductoEntregableInfo obj, ResultSetMap resultSet) {
            Long id0 = resultSet.long0(COLUMNS.id);
            Long idProducto0 = resultSet.long0(COLUMNS.idProducto);
            String loteOrigen0 = resultSet.string(COLUMNS.loteOrigen);
            Date fechaVencimiento0 = resultSet.date(COLUMNS.fechaVencimiento);
            Long idEmpaqueL10 = resultSet.long0(COLUMNS.idEmpaqueL1);
            Long idEmpaqueL20 = resultSet.long0(COLUMNS.idEmpaqueL2);
            Long idEmpaqueL30 = resultSet.long0(COLUMNS.idEmpaqueL3);
            Long idEmpaqueL40 = resultSet.long0(COLUMNS.idEmpaqueL4);
            Date fechaEntregaTentativa0 = resultSet.date(COLUMNS.fechaEntregaTentativa);
            Date fechaEntregaAgendada0 = resultSet.date(COLUMNS.fechaEntregaAgendada);

            // Identificador de producto.
            ProductoInfo producto0 = new ProductoInfo();
            producto0.setId(idProducto0);

            // Identificadores de empaque.
            EmpaqueInfo empaqueL10 = crearEmpaque(idEmpaqueL10);
            EmpaqueInfo empaqueL20 = crearEmpaque(idEmpaqueL20);
            EmpaqueInfo empaqueL30 = crearEmpaque(idEmpaqueL30);
            EmpaqueInfo empaqueL40 = crearEmpaque(idEmpaqueL40);

            obj.setId(id0);
            obj.setProducto(producto0);
            obj.setLoteOrigen(loteOrigen0);
            obj.setFechaVencimiento(fechaVencimiento0);
            obj.setEmpaqueL1(empaqueL10);
            obj.setEmpaqueL2(empaqueL20);
            obj.setEmpaqueL3(empaqueL30);
            obj.setEmpaqueL4(empaqueL40);
            obj.setFechaEntregaTentativa(fechaEntregaTentativa0);
            obj.setFechaEntregaAgendada(fechaEntregaAgendada0);
            return obj;
        }

        private EmpaqueInfo crearEmpaque(Long id) {
            EmpaqueInfo empaque = new EmpaqueInfo();
            empaque.setId(id);
            return empaque;
        }
    }
}
