/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import fa.gs.utils.collections.maps.ResultSetMap;
import fa.gs.utils.database.criteria.column.NativeColumn;
import java.io.Serializable;
import java.util.Date;
import py.com.sepsa.recepcion.core.data.enums.TipoObservacionEnum;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ObservacionInfo implements Serializable {

    private Long id;
    private Long idEntidad;
    private String tipoEntidad;
    private TipoObservacionEnum tipoObservacion;
    private String descripcion;
    private Date fechaRegistro;

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdEntidad() {
        return idEntidad;
    }

    public void setIdEntidad(Long idEntidad) {
        this.idEntidad = idEntidad;
    }

    public String getTipoEntidad() {
        return tipoEntidad;
    }

    public void setTipoEntidad(String tipoEntidad) {
        this.tipoEntidad = tipoEntidad;
    }

    public TipoObservacionEnum getTipoObservacion() {
        return tipoObservacion;
    }

    public void setTipoObservacion(TipoObservacionEnum tipoObservacion) {
        this.tipoObservacion = tipoObservacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
    //</editor-fold>

    public static class COLUMNS {

        public static final NativeColumn<Long> id = NativeColumn.instance("id", Long.class);
        public static final NativeColumn<Long> idEntidad = NativeColumn.instance("idEntidad", Long.class);
        public static final NativeColumn<String> tipoEntidad = NativeColumn.instance("tipoEntidad", String.class);
        public static final NativeColumn<String> tipoObservacionCodigo = NativeColumn.instance("tipoObservacionCodigo", String.class);
        public static final NativeColumn<String> descripcion = NativeColumn.instance("descripcion", String.class);
        public static final NativeColumn<Date> fechaRegistro = NativeColumn.instance("fechaRegistro", Date.class);
    }

    public static class Mapper extends fa.gs.utils.database.utils.ResultSetMapper<ObservacionInfo> {

        @Override
        public ObservacionInfo getEmptyAdaptee() {
            return new ObservacionInfo();
        }

        @Override
        public ObservacionInfo adapt(final ObservacionInfo obj, ResultSetMap resultSet) {
            Long id0 = resultSet.long0(COLUMNS.id);
            Long idEntidad0 = resultSet.long0(COLUMNS.idEntidad);
            String tipoEntidad0 = resultSet.string(COLUMNS.tipoEntidad);
            TipoObservacionEnum tipoObservacion0 = resultSet.enumerable(COLUMNS.tipoObservacionCodigo, TipoObservacionEnum.adapter());
            String descripcion0 = resultSet.string(COLUMNS.descripcion);
            Date fechaRegistro0 = resultSet.date(COLUMNS.fechaRegistro);

            obj.setId(id0);
            obj.setIdEntidad(idEntidad0);
            obj.setTipoEntidad(tipoEntidad0);
            obj.setTipoObservacion(tipoObservacion0);
            obj.setDescripcion(descripcion0);
            obj.setFechaRegistro(fechaRegistro0);
            return obj;
        }
    }
}
