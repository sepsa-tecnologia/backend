package py.com.sepsa.recepcion.core.database.entities.recepcion;

import fa.gs.utils.database.criteria.column.JpaColumn;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "recepcion", catalog = "sepsa", schema = "recepcion")
@DynamicInsert
public class Recepcion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_id")
    @SequenceGenerator(name = "gen_id", sequenceName = "recepcion.recepcion_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("nextval('recepcion.recepcion_id_seq'::regclass)")
    private Long id;

    @Column(name = "id_documento", nullable = true)
    @Basic(optional = true)
    private Long idDocumento;

    @Column(name = "nro_documento", nullable = true)
    @Basic(optional = true)
    private String nroDocumento;

    @Column(name = "id_nota_remision", nullable = true)
    @Basic(optional = true)
    private Long idNotaRemision;

    @Column(name = "id_factura", nullable = true)
    @Basic(optional = true)
    private Long idFactura;

    @Column(name = "id_estado", nullable = true)
    @Basic(optional = true)
    private Long idEstado;

    @Column(name = "fecha_emision", nullable = false)
    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date fechaEmision;

    @Column(name = "fecha_registro", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("now()")
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date fechaRegistro;

    @Column(name = "meta__shard", nullable = true)
    @Basic(optional = true)
    @org.hibernate.annotations.Type(type = "org.hibernate.type.PostgresUUIDType")
    private java.util.UUID metaShard;

    @Column(name = "meta__eliminado", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("'0'::bpchar")
    private String metaEliminado;

    @Column(name = "meta__fecha_eliminacion", nullable = true)
    @Basic(optional = true)
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date metaFechaEliminacion;

    public void setId(Long value) {
        this.id = value;
    }

    public void setIdDocumento(Long value) {
        this.idDocumento = value;
    }

    public void setNroDocumento(String value) {
        this.nroDocumento = value;
    }

    public void setIdNotaRemision(Long value) {
        this.idNotaRemision = value;
    }

    public void setIdFactura(Long value) {
        this.idFactura = value;
    }

    public void setIdEstado(Long value) {
        this.idEstado = value;
    }

    public void setFechaEmision(java.util.Date value) {
        this.fechaEmision = value;
    }

    public void setFechaRegistro(java.util.Date value) {
        this.fechaRegistro = value;
    }

    public void setMetaShard(java.util.UUID value) {
        this.metaShard = value;
    }

    public void setMetaEliminado(String value) {
        this.metaEliminado = value;
    }

    public void setMetaFechaEliminacion(java.util.Date value) {
        this.metaFechaEliminacion = value;
    }

    public Long getId() {
        return this.id;
    }

    public Long getIdDocumento() {
        return this.idDocumento;
    }

    public String getNroDocumento() {
        return this.nroDocumento;
    }

    public Long getIdNotaRemision() {
        return this.idNotaRemision;
    }

    public Long getIdFactura() {
        return this.idFactura;
    }

    public Long getIdEstado() {
        return this.idEstado;
    }

    public java.util.Date getFechaEmision() {
        return this.fechaEmision;
    }

    public java.util.Date getFechaRegistro() {
        return this.fechaRegistro;
    }

    public java.util.UUID getMetaShard() {
        return this.metaShard;
    }

    public String getMetaEliminado() {
        return this.metaEliminado;
    }

    public java.util.Date getMetaFechaEliminacion() {
        return this.metaFechaEliminacion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.idDocumento);
        hash = 53 * hash + Objects.hashCode(this.nroDocumento);
        hash = 53 * hash + Objects.hashCode(this.idNotaRemision);
        hash = 53 * hash + Objects.hashCode(this.idFactura);
        hash = 53 * hash + Objects.hashCode(this.idEstado);
        hash = 53 * hash + Objects.hashCode(this.fechaEmision);
        hash = 53 * hash + Objects.hashCode(this.fechaRegistro);
        hash = 53 * hash + Objects.hashCode(this.metaShard);
        hash = 53 * hash + Objects.hashCode(this.metaEliminado);
        hash = 53 * hash + Objects.hashCode(this.metaFechaEliminacion);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Recepcion other = (Recepcion) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.idDocumento, other.idDocumento)) {
            return false;
        }
        if (!Objects.equals(this.nroDocumento, other.nroDocumento)) {
            return false;
        }
        if (!Objects.equals(this.idNotaRemision, other.idNotaRemision)) {
            return false;
        }
        if (!Objects.equals(this.idFactura, other.idFactura)) {
            return false;
        }
        if (!Objects.equals(this.idEstado, other.idEstado)) {
            return false;
        }
        if (!Objects.equals(this.fechaEmision, other.fechaEmision)) {
            return false;
        }
        if (!Objects.equals(this.fechaRegistro, other.fechaRegistro)) {
            return false;
        }
        if (!Objects.equals(this.metaShard, other.metaShard)) {
            return false;
        }
        if (!Objects.equals(this.metaEliminado, other.metaEliminado)) {
            return false;
        }
        if (!Objects.equals(this.metaFechaEliminacion, other.metaFechaEliminacion)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Recepcion[");
        builder.append("id = ").append(id);
        builder.append(", idDocumento = ").append(idDocumento);
        builder.append(", nroDocumento = ").append(nroDocumento);
        builder.append(", idNotaRemision = ").append(idNotaRemision);
        builder.append(", idFactura = ").append(idFactura);
        builder.append(", idEstado = ").append(idEstado);
        builder.append(", fechaEmision = ").append(fechaEmision);
        builder.append(", fechaRegistro = ").append(fechaRegistro);
        builder.append(", metaShard = ").append(metaShard);
        builder.append(", metaEliminado = ").append(metaEliminado);
        builder.append(", metaFechaEliminacion = ").append(metaFechaEliminacion);
        builder.append("]");
        return builder.toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Columnas">
    public static class COLUMNS implements Serializable {

        public static final JpaColumn<Long> id = JpaColumn.instance("id", Long.class, false);
        public static final JpaColumn<Long> idDocumento = JpaColumn.instance("idDocumento", Long.class, true);
        public static final JpaColumn<String> nroDocumento = JpaColumn.instance("nroDocumento", String.class, true);
        public static final JpaColumn<Long> idNotaRemision = JpaColumn.instance("idNotaRemision", Long.class, true);
        public static final JpaColumn<Long> idFactura = JpaColumn.instance("idFactura", Long.class, true);
        public static final JpaColumn<Long> idEstado = JpaColumn.instance("idEstado", Long.class, true);
        public static final JpaColumn<java.util.Date> fechaEmision = JpaColumn.instance("fechaEmision", java.util.Date.class, false);
        public static final JpaColumn<java.util.Date> fechaRegistro = JpaColumn.instance("fechaRegistro", java.util.Date.class, false);
        public static final JpaColumn<java.util.UUID> metaShard = JpaColumn.instance("metaShard", java.util.UUID.class, true);
        public static final JpaColumn<String> metaEliminado = JpaColumn.instance("metaEliminado", String.class, false);
        public static final JpaColumn<java.util.Date> metaFechaEliminacion = JpaColumn.instance("metaFechaEliminacion", java.util.Date.class, true);
    }
    //</editor-fold>

}
