/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.adapters;

import com.google.gson.JsonElement;
import fa.gs.utils.adapters.impl.json.ToJsonAdapter;
import fa.gs.utils.misc.json.Json;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import py.com.sepsa.recepcion.core.data.pojos.NotaRemisionInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
public class NotaRemisionInfo2JsonAdapter extends ToJsonAdapter<NotaRemisionInfo> {

    @Override
    protected JsonElement adapt0(NotaRemisionInfo obj) {
        JsonObjectBuilder json = JsonObjectBuilder.instance();
        json.add("id", obj.getId());
        json.add("tipo", obj.getTipoDocumento().codigo());
        json.add("nro", obj.getNroDocumento());
        json.add("estado", obj.getEstado().codigo());
        json.add("fecha_emision", obj.getFechaEmision());
        json.add("fecha_registro", obj.getFechaRegistro());
        json.add("proveedor", adaptProveedor(obj));
        json.add("detalles", adaptDetalles(obj));
        return json.build();
    }

    private JsonElement adaptProveedor(NotaRemisionInfo obj) {
        JsonObjectBuilder json = JsonObjectBuilder.instance();
        json.add("razon_social", obj.getRazonSocialProveedor());
        json.add("ruc", obj.getRucProveedor());
        return json.build();
    }

    private JsonElement adaptDetalles(NotaRemisionInfo obj) {
        return Json.toArrayData(NotaRemisionDetalleInfo2JsonAdapter.class, obj.getDetalles());
    }

}
