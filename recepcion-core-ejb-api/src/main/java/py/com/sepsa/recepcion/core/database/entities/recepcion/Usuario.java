package py.com.sepsa.recepcion.core.database.entities.recepcion;

import fa.gs.utils.database.criteria.column.JpaColumn;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "usuario", catalog = "sepsa", schema = "recepcion")
@DynamicInsert
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_id")
    @SequenceGenerator(name = "gen_id", sequenceName = "recepcion.usuario_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("nextval('recepcion.usuario_id_seq'::regclass)")
    private Long id;

    @Column(name = "id_empresa", nullable = true)
    @Basic(optional = true)
    private Long idEmpresa;

    @Column(name = "id_persona", nullable = true)
    @Basic(optional = true)
    private Long idPersona;

    @Column(name = "nombre", nullable = false)
    @Basic(optional = false)
    private String nombre;

    @Column(name = "contrasenha", nullable = false)
    @Basic(optional = false)
    private String contrasenha;

    @Column(name = "fecha_registro", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("now()")
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date fechaRegistro;

    @Column(name = "codigo_interno", nullable = true)
    @Basic(optional = true)
    private String codigoInterno;

    @Column(name = "meta__shard", nullable = true)
    @Basic(optional = true)
    @org.hibernate.annotations.Type(type = "org.hibernate.type.PostgresUUIDType")
    private java.util.UUID metaShard;

    @Column(name = "meta__eliminado", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("'0'::bpchar")
    private String metaEliminado;

    @Column(name = "meta__fecha_eliminacion", nullable = true)
    @Basic(optional = true)
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date metaFechaEliminacion;

    public void setId(Long value) {
        this.id = value;
    }

    public void setIdEmpresa(Long value) {
        this.idEmpresa = value;
    }

    public void setIdPersona(Long value) {
        this.idPersona = value;
    }

    public void setNombre(String value) {
        this.nombre = value;
    }

    public void setContrasenha(String value) {
        this.contrasenha = value;
    }

    public void setFechaRegistro(java.util.Date value) {
        this.fechaRegistro = value;
    }

    public void setCodigoInterno(String value) {
        this.codigoInterno = value;
    }

    public void setMetaShard(java.util.UUID value) {
        this.metaShard = value;
    }

    public void setMetaEliminado(String value) {
        this.metaEliminado = value;
    }

    public void setMetaFechaEliminacion(java.util.Date value) {
        this.metaFechaEliminacion = value;
    }

    public Long getId() {
        return this.id;
    }

    public Long getIdEmpresa() {
        return this.idEmpresa;
    }

    public Long getIdPersona() {
        return this.idPersona;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getContrasenha() {
        return this.contrasenha;
    }

    public java.util.Date getFechaRegistro() {
        return this.fechaRegistro;
    }

    public String getCodigoInterno() {
        return this.codigoInterno;
    }

    public java.util.UUID getMetaShard() {
        return this.metaShard;
    }

    public String getMetaEliminado() {
        return this.metaEliminado;
    }

    public java.util.Date getMetaFechaEliminacion() {
        return this.metaFechaEliminacion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.idEmpresa);
        hash = 53 * hash + Objects.hashCode(this.idPersona);
        hash = 53 * hash + Objects.hashCode(this.nombre);
        hash = 53 * hash + Objects.hashCode(this.contrasenha);
        hash = 53 * hash + Objects.hashCode(this.fechaRegistro);
        hash = 53 * hash + Objects.hashCode(this.codigoInterno);
        hash = 53 * hash + Objects.hashCode(this.metaShard);
        hash = 53 * hash + Objects.hashCode(this.metaEliminado);
        hash = 53 * hash + Objects.hashCode(this.metaFechaEliminacion);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.idEmpresa, other.idEmpresa)) {
            return false;
        }
        if (!Objects.equals(this.idPersona, other.idPersona)) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.contrasenha, other.contrasenha)) {
            return false;
        }
        if (!Objects.equals(this.fechaRegistro, other.fechaRegistro)) {
            return false;
        }
        if (!Objects.equals(this.codigoInterno, other.codigoInterno)) {
            return false;
        }
        if (!Objects.equals(this.metaShard, other.metaShard)) {
            return false;
        }
        if (!Objects.equals(this.metaEliminado, other.metaEliminado)) {
            return false;
        }
        if (!Objects.equals(this.metaFechaEliminacion, other.metaFechaEliminacion)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Usuario[");
        builder.append("id = ").append(id);
        builder.append(", idEmpresa = ").append(idEmpresa);
        builder.append(", idPersona = ").append(idPersona);
        builder.append(", nombre = ").append(nombre);
        builder.append(", contrasenha = ").append(contrasenha);
        builder.append(", fechaRegistro = ").append(fechaRegistro);
        builder.append(", codigoInterno = ").append(codigoInterno);
        builder.append(", metaShard = ").append(metaShard);
        builder.append(", metaEliminado = ").append(metaEliminado);
        builder.append(", metaFechaEliminacion = ").append(metaFechaEliminacion);
        builder.append("]");
        return builder.toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Columnas">
    public static class COLUMNS implements Serializable {

        public static final JpaColumn<Long> id = JpaColumn.instance("id", Long.class, false);
        public static final JpaColumn<Long> idEmpresa = JpaColumn.instance("idEmpresa", Long.class, true);
        public static final JpaColumn<Long> idPersona = JpaColumn.instance("idPersona", Long.class, true);
        public static final JpaColumn<String> nombre = JpaColumn.instance("nombre", String.class, false);
        public static final JpaColumn<String> contrasenha = JpaColumn.instance("contrasenha", String.class, false);
        public static final JpaColumn<java.util.Date> fechaRegistro = JpaColumn.instance("fechaRegistro", java.util.Date.class, false);
        public static final JpaColumn<String> codigoInterno = JpaColumn.instance("codigoInterno", String.class, true);
        public static final JpaColumn<java.util.UUID> metaShard = JpaColumn.instance("metaShard", java.util.UUID.class, true);
        public static final JpaColumn<String> metaEliminado = JpaColumn.instance("metaEliminado", String.class, false);
        public static final JpaColumn<java.util.Date> metaFechaEliminacion = JpaColumn.instance("metaFechaEliminacion", java.util.Date.class, true);
    }
    //</editor-fold>

}
