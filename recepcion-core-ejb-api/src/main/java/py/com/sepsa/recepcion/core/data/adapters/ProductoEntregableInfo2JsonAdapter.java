/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.adapters;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import fa.gs.utils.adapters.Adapters;
import fa.gs.utils.adapters.impl.json.JsonArrayAdapter;
import fa.gs.utils.adapters.impl.json.ToJsonAdapter;
import fa.gs.utils.misc.json.JsonArrayBuilder;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import py.com.sepsa.recepcion.core.data.pojos.EmpaqueInfo;
import py.com.sepsa.recepcion.core.data.pojos.ProductoEntregableInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ProductoEntregableInfo2JsonAdapter extends ToJsonAdapter<ProductoEntregableInfo> {

    @Override
    protected JsonElement adapt0(ProductoEntregableInfo obj) {
        JsonObjectBuilder json = JsonObjectBuilder.instance();
        json.add("id", obj.getId());
        json.add("producto", adaptProducto(obj));
        json.add("lote", obj.getLoteOrigen());
        json.add("vencimiento", obj.getFechaVencimiento());
        json.add("entrega", adaptEntrega(obj));
        json.add("empaques", adaptEmpaques(obj));
        return json.build();
    }

    private JsonElement adaptProducto(ProductoEntregableInfo obj) {
        return Adapters.adapt(ProductoInfo2JsonAdapter.class, obj.getProducto());
    }

    private JsonElement adaptEntrega(ProductoEntregableInfo obj) {
        JsonObjectBuilder json = JsonObjectBuilder.instance();
        json.add("fecha_agendada", obj.getFechaEntregaAgendada());
        json.add("fecha_tentativa", obj.getFechaEntregaTentativa());
        return json.build();
    }

    private JsonElement adaptEmpaques(ProductoEntregableInfo obj) {
        JsonArrayBuilder json = JsonArrayBuilder.instance();
        adaptEmpaque(json, obj.getEmpaqueL1());
        adaptEmpaque(json, obj.getEmpaqueL2());
        adaptEmpaque(json, obj.getEmpaqueL3());
        adaptEmpaque(json, obj.getEmpaqueL4());
        JsonArray array = json.build();
        return Adapters.adapt(JsonArrayAdapter.class, array);
    }

    private void adaptEmpaque(JsonArrayBuilder json, EmpaqueInfo obj) {
        if (obj != null && obj.getId() != null) {
            JsonElement json0 = Adapters.adapt(EmpaqueInfo2JsonAdapter.class, obj);
            json.add(json0);
        }
    }

}
