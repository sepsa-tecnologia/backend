/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic;

import java.io.Serializable;
import javax.ejb.Remote;

/**
 *
 * @author Fabio A. González Sosa
 */
@Remote
public interface BackgroundWorkers extends Serializable {

    boolean start();

    boolean stop();

}
