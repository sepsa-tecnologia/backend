package py.com.sepsa.recepcion.core.database.entities.recepcion;

import fa.gs.utils.database.criteria.column.JpaColumn;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "producto_entrega_serializacion", catalog = "sepsa", schema = "recepcion")
@DynamicInsert
public class ProductoEntregaSerializacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_id")
    @SequenceGenerator(name = "gen_id", sequenceName = "recepcion.producto_entrega_serializacion_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("nextval('recepcion.producto_entrega_serializacion_id_seq'::regclass)")
    private Long id;

    @Column(name = "id_producto_entrega", nullable = true)
    @Basic(optional = true)
    private Long idProductoEntrega;

    @Column(name = "nro_serie", nullable = false)
    @Basic(optional = false)
    private String nroSerie;

    public void setId(Long value) {
        this.id = value;
    }

    public void setIdProductoEntrega(Long value) {
        this.idProductoEntrega = value;
    }

    public void setNroSerie(String value) {
        this.nroSerie = value;
    }

    public Long getId() {
        return this.id;
    }

    public Long getIdProductoEntrega() {
        return this.idProductoEntrega;
    }

    public String getNroSerie() {
        return this.nroSerie;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.idProductoEntrega);
        hash = 53 * hash + Objects.hashCode(this.nroSerie);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductoEntregaSerializacion other = (ProductoEntregaSerializacion) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.idProductoEntrega, other.idProductoEntrega)) {
            return false;
        }
        if (!Objects.equals(this.nroSerie, other.nroSerie)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ProductoEntregaSerializacion[");
        builder.append("id = ").append(id);
        builder.append(", idProductoEntrega = ").append(idProductoEntrega);
        builder.append(", nroSerie = ").append(nroSerie);
        builder.append("]");
        return builder.toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Columnas">
    public static class COLUMNS implements Serializable {

        public static final JpaColumn<Long> id = JpaColumn.instance("id", Long.class, false);
        public static final JpaColumn<Long> idProductoEntrega = JpaColumn.instance("idProductoEntrega", Long.class, true);
        public static final JpaColumn<String> nroSerie = JpaColumn.instance("nroSerie", String.class, false);
    }
    //</editor-fold>

}
