/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import fa.gs.utils.collections.maps.ResultSetMap;
import fa.gs.utils.database.criteria.column.NativeColumn;
import java.util.Date;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;

/**
 *
 * @author Fabio A. González Sosa
 */
public class NotaRemisionCabeceraInfo implements DocumentoInfo, DocumentoConEstado, DocumentoConNro {

    private Long idNotaRemision;
    private EstadoEnum estado;
    private Long idDocumento;
    private String nroDocumentoNotaRemision;
    private Long idOrdenCompra;
    private String nroDocumentoOrdenCompra;
    private String glnProveedor;
    private String razonSocialProveedor;
    private String rucProveedor;
    private Date fechaEmision;
    private Date fechaRegistro;

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">    
    @Override
    public Long getId() {
        return getIdNotaRemision();
    }

    public Long getIdNotaRemision() {
        return idNotaRemision;
    }

    public void setIdNotaRemision(Long idNotaRemision) {
        this.idNotaRemision = idNotaRemision;
    }

    @Override
    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }

    public Long getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Long idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getNroDocumentoNotaRemision() {
        return nroDocumentoNotaRemision;
    }

    public void setNroDocumentoNotaRemision(String nroDocumentoNotaRemision) {
        this.nroDocumentoNotaRemision = nroDocumentoNotaRemision;
    }

    @Override
    public String getNroDocumento() {
        return getNroDocumentoNotaRemision();
    }

    public Long getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Long idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public String getNroDocumentoOrdenCompra() {
        return nroDocumentoOrdenCompra;
    }

    public void setNroDocumentoOrdenCompra(String nroDocumentoOrdenCompra) {
        this.nroDocumentoOrdenCompra = nroDocumentoOrdenCompra;
    }

    public String getGlnProveedor() {
        return glnProveedor;
    }

    public void setGlnProveedor(String glnProveedor) {
        this.glnProveedor = glnProveedor;
    }

    public String getRazonSocialProveedor() {
        return razonSocialProveedor;
    }

    public void setRazonSocialProveedor(String razonSocialProveedor) {
        this.razonSocialProveedor = razonSocialProveedor;
    }

    public String getRucProveedor() {
        return rucProveedor;
    }

    public void setRucProveedor(String rucProveedor) {
        this.rucProveedor = rucProveedor;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public TipoEntidadEnum getTipoDocumento() {
        return TipoEntidadEnum.NOTA_REMISION;
    }
    //</editor-fold>    

    public static class COLUMNS {

        public static final NativeColumn<Long> idNotaRemision = NativeColumn.instance("idNotaRemision", Long.class);
        public static final NativeColumn<String> estadoCodigo = NativeColumn.instance("estadoCodigo", String.class);
        public static final NativeColumn<Long> idDocumento = NativeColumn.instance("idDocumento", Long.class);
        public static final NativeColumn<String> nroDocumentoNotaRemision = NativeColumn.instance("nroDocumentoNotaRemision", String.class);
        public static final NativeColumn<Long> idOrdenCompra = NativeColumn.instance("idOrdenCompra", Long.class);
        public static final NativeColumn<String> nroDocumentoOrdenCompra = NativeColumn.instance("nroDocumentoOrdenCompra", String.class);
        public static final NativeColumn<String> glnProveedor = NativeColumn.instance("glnProveedor", String.class);
        public static final NativeColumn<String> razonSocialProveedor = NativeColumn.instance("razonSocialProveedor", String.class);
        public static final NativeColumn<String> rucProveedor = NativeColumn.instance("rucProveedor", String.class);
        public static final NativeColumn<Date> fechaEmision = NativeColumn.instance("fechaEmision", Date.class);
        public static final NativeColumn<Date> fechaRegistro = NativeColumn.instance("fechaRegistro", Date.class);
    }

    public static class Mapper extends fa.gs.utils.database.utils.ResultSetMapper<NotaRemisionCabeceraInfo> {

        @Override
        public NotaRemisionCabeceraInfo getEmptyAdaptee() {
            return new NotaRemisionCabeceraInfo();
        }

        @Override
        public NotaRemisionCabeceraInfo adapt(final NotaRemisionCabeceraInfo obj, ResultSetMap resultSet) {
            Long idNotaRemision0 = resultSet.long0(COLUMNS.idNotaRemision);
            EstadoEnum estado0 = resultSet.enumerable(COLUMNS.estadoCodigo, EstadoEnum.adapter());
            Long idDocumento0 = resultSet.long0(COLUMNS.idDocumento);
            String nroDocumentoNotaRemision0 = resultSet.string(COLUMNS.nroDocumentoNotaRemision);
            Long idOrdenCompra0 = resultSet.long0(COLUMNS.idOrdenCompra);
            String nroDocumentoOrdenCompra0 = resultSet.string(COLUMNS.nroDocumentoOrdenCompra);
            String glnProveedor0 = resultSet.string(COLUMNS.glnProveedor);
            String razonSocialProveedor0 = resultSet.string(COLUMNS.razonSocialProveedor);
            String rucProveedor0 = resultSet.string(COLUMNS.rucProveedor);
            Date fechaEmision0 = resultSet.date(COLUMNS.fechaEmision);
            Date fechaRegistro0 = resultSet.date(COLUMNS.fechaRegistro);

            obj.setIdNotaRemision(idNotaRemision0);
            obj.setEstado(estado0);
            obj.setIdDocumento(idDocumento0);
            obj.setNroDocumentoNotaRemision(nroDocumentoNotaRemision0);
            obj.setIdOrdenCompra(idOrdenCompra0);
            obj.setNroDocumentoOrdenCompra(nroDocumentoOrdenCompra0);
            obj.setGlnProveedor(glnProveedor0);
            obj.setRazonSocialProveedor(razonSocialProveedor0);
            obj.setRucProveedor(rucProveedor0);
            obj.setFechaEmision(fechaEmision0);
            obj.setFechaRegistro(fechaRegistro0);
            return obj;
        }
    }
}
