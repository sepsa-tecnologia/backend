/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.configs;

import java.io.IOException;
import java.util.prefs.Preferences;
import org.ini4j.Ini;
import org.ini4j.IniPreferences;
import py.com.sepsa.recepcion.core.logic.Configs;
import py.com.sepsa.recepcion.core.logic.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ConfigPreferences {

    static Preferences get() throws IOException {
        Configs cofig0 = Injection.bean(Configs.class);
        Ini ini = new Ini(cofig0.getConfigFile());
        return new IniPreferences(ini);
    }

}
