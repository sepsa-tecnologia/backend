/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.criteria.column.NativeColumn;
import fa.gs.utils.database.mapping.Mapping;
import fa.gs.utils.database.mapping.mappers.DateMapper;
import fa.gs.utils.database.mapping.mappers.EnumMapper;
import fa.gs.utils.database.mapping.mappers.LongMapper;
import fa.gs.utils.database.mapping.mappers.PojoMapper;
import fa.gs.utils.database.mapping.mappers.StringMapper;
import fa.gs.utils.database.mapping.mappings.Mappings;
import fa.gs.utils.misc.Assertions;
import java.util.Collection;
import java.util.Date;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;

/**
 *
 * @author Fabio A. González Sosa
 */
public class RecepcionInfo implements DocumentoInfo, DocumentoConEstado, DocumentoConNro {

    private Long id;
    private EstadoEnum estado;
    private Long idDocumento;
    private String nroDocumento;
    private Long idOrdenCompra;
    private Long idNotaRemision;
    private Long idFactura;
    private Date fechaRegistro;
    private Date fechaEmision;
    private final Collection<RecepcionDetalleInfo> detalles;

    public RecepcionInfo() {
        this.detalles = Lists.empty();
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }

    public Long getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Long idDocumento) {
        this.idDocumento = idDocumento;
    }

    @Override
    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public Long getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Long idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public Long getIdNotaRemision() {
        return idNotaRemision;
    }

    public void setIdNotaRemision(Long idNotaRemision) {
        this.idNotaRemision = idNotaRemision;
    }

    public Long getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Long idFactura) {
        this.idFactura = idFactura;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Collection<RecepcionDetalleInfo> getDetalles() {
        return detalles;
    }

    public void setDetalles(Collection<RecepcionDetalleInfo> detalles) {
        if (!Assertions.isNullOrEmpty(detalles)) {
            this.detalles.addAll(detalles);
        }
    }

    @Override
    public TipoEntidadEnum getTipoDocumento() {
        return TipoEntidadEnum.RECEPCION;
    }
    //</editor-fold>

    public static class MAPPINGS {

        public static final Mapping<Long> id = Mappings.instance("id", Long.class);
        public static final Mapping<String> estadoCodigo = Mappings.instance("estado", String.class);
        public static final Mapping<Long> idDocumento = Mappings.instance("idDocumento", Long.class);
        public static final Mapping<String> nroDocumento = Mappings.instance("nroDocumento", String.class);
        public static final Mapping<Long> idOrdenCompra = Mappings.instance("idOrdenCompra", Long.class);
        public static final Mapping<Long> idNotaRemision = Mappings.instance("idNotaRemision", Long.class);
        public static final Mapping<Long> idFactura = Mappings.instance("idFactura", Long.class);
        public static final Mapping<Date> fechaRegistro = Mappings.instance("fechaRegistro", Date.class);
        public static final Mapping<Date> fechaEmision = Mappings.instance("fechaEmision", Date.class);
    }

    public static class COLUMNS {

        public static final NativeColumn<Long> r_id = NativeColumn.instance("r", "id", Long.class);
        public static final NativeColumn<String> e_estado_codigo = NativeColumn.instance("e", "codigo", String.class);
        public static final NativeColumn<String> r_id_documento = NativeColumn.instance("r", "id_documento", String.class);
        public static final NativeColumn<String> r_nro_documento = NativeColumn.instance("r", "nro_documento", String.class);
        public static final NativeColumn<Long> r_id_nota_remision = NativeColumn.instance("r", "id_nota_remision", Long.class);
        public static final NativeColumn<Long> r_id_factura = NativeColumn.instance("r", "id_factura", Long.class);
        public static final NativeColumn<Date> r_fecha_registro = NativeColumn.instance("r", "fecha_registro", Date.class);
        public static final NativeColumn<Date> r_fecha_emision = NativeColumn.instance("r", "fecha_emision", Date.class);
    }

    public static class MAPPER extends PojoMapper<MAPPER, RecepcionInfo> {

        public static MAPPER INSTANCE;

        static {
            MAPPER.INSTANCE = new MAPPER();
            MAPPER.INSTANCE.with(LongMapper.instance(MAPPINGS.id));
            MAPPER.INSTANCE.with(EnumMapper.instance(MAPPINGS.estadoCodigo, EstadoEnum.adapter()));
            MAPPER.INSTANCE.with(LongMapper.instance(MAPPINGS.idDocumento));
            MAPPER.INSTANCE.with(StringMapper.instance(MAPPINGS.nroDocumento));
            MAPPER.INSTANCE.with(LongMapper.instance(MAPPINGS.idOrdenCompra));
            MAPPER.INSTANCE.with(LongMapper.instance(MAPPINGS.idNotaRemision));
            MAPPER.INSTANCE.with(LongMapper.instance(MAPPINGS.idFactura));
            MAPPER.INSTANCE.with(DateMapper.instance(MAPPINGS.fechaRegistro));
            MAPPER.INSTANCE.with(DateMapper.instance(MAPPINGS.fechaEmision));
        }

        @Override
        protected RecepcionInfo getEmptyAdaptee() {
            return new RecepcionInfo();
        }

    }

}
