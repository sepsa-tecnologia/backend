/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import fa.gs.utils.collections.maps.ResultSetMap;
import fa.gs.utils.database.criteria.column.NativeColumn;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Fabio A. González Sosa
 */
public class OrdenCompraDetalleInfo implements Serializable {

    private Long idOrdenCompra;
    private Long idProducto;
    private ProductoInfo producto;
    private Long cantidad;
    private BigDecimal precio;
    private Long idMoneda;
    private MonedaInfo moneda;

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public Long getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Long idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public Long getIdProducto() {
        return this.idProducto;
    }

    public void setIdProducto(Long value) {
        this.idProducto = value;
    }

    public ProductoInfo getProducto() {
        return this.producto;
    }

    public void setProducto(ProductoInfo value) {
        this.producto = value;
    }

    public Long getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(Long value) {
        this.cantidad = value;
    }

    public BigDecimal getPrecio() {
        return this.precio;
    }

    public void setPrecio(BigDecimal value) {
        this.precio = value;
    }

    public Long getIdMoneda() {
        return this.idMoneda;
    }

    public void setIdMoneda(Long value) {
        this.idMoneda = value;
    }

    public MonedaInfo getMoneda() {
        return this.moneda;
    }

    public void setMoneda(MonedaInfo value) {
        this.moneda = value;
    }
    //</editor-fold>

    public static class COLUMNS {

        public static final NativeColumn<Long> idOrdenCompra = NativeColumn.instance("idOrdenCompra", Long.class);
        public static final NativeColumn<Long> idProducto = NativeColumn.instance("idProducto", Long.class);
        public static final NativeColumn<Long> cantidad = NativeColumn.instance("cantidad", Long.class);
        public static final NativeColumn<BigDecimal> precio = NativeColumn.instance("precio", BigDecimal.class);
        public static final NativeColumn<Long> idMoneda = NativeColumn.instance("idMoneda", Long.class);
    }

    public static class Mapper extends fa.gs.utils.database.utils.ResultSetMapper<OrdenCompraDetalleInfo> {

        @Override
        public OrdenCompraDetalleInfo getEmptyAdaptee() {
            return new OrdenCompraDetalleInfo();
        }

        @Override
        public OrdenCompraDetalleInfo adapt(final OrdenCompraDetalleInfo obj, ResultSetMap resultSet) {
            Long idOrdenCompra0 = resultSet.long0(COLUMNS.idOrdenCompra);
            Long idProducto0 = resultSet.long0(COLUMNS.idProducto);
            Long cantidad0 = resultSet.long0(COLUMNS.cantidad);
            BigDecimal precio0 = resultSet.bigdecimal(COLUMNS.precio);
            Long idMoneda0 = resultSet.long0(COLUMNS.idMoneda);

            obj.setIdOrdenCompra(idOrdenCompra0);
            obj.setIdProducto(idProducto0);
            obj.setCantidad(cantidad0);
            obj.setPrecio(precio0);
            obj.setIdMoneda(idMoneda0);
            return obj;
        }
    }
}
