package py.com.sepsa.recepcion.core.database.entities.recepcion;

import fa.gs.utils.database.criteria.column.JpaColumn;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "empaque", catalog = "sepsa", schema = "recepcion")
@DynamicInsert
public class Empaque implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_id")
    @SequenceGenerator(name = "gen_id", sequenceName = "recepcion.empaque_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("nextval('recepcion.empaque_id_seq'::regclass)")
    private Long id;

    @Column(name = "id_tipo_empaque", nullable = true)
    @Basic(optional = true)
    private Long idTipoEmpaque;

    @Column(name = "codigo_sscc", nullable = true)
    @Basic(optional = true)
    private String codigoSscc;

    @Column(name = "alto", nullable = true)
    @Basic(optional = true)
    private java.math.BigInteger alto;

    @Column(name = "ancho", nullable = true)
    @Basic(optional = true)
    private java.math.BigInteger ancho;

    @Column(name = "profundo", nullable = true)
    @Basic(optional = true)
    private java.math.BigInteger profundo;

    @Column(name = "peso", nullable = true)
    @Basic(optional = true)
    private java.math.BigInteger peso;

    public void setId(Long value) {
        this.id = value;
    }

    public void setIdTipoEmpaque(Long value) {
        this.idTipoEmpaque = value;
    }

    public void setCodigoSscc(String value) {
        this.codigoSscc = value;
    }

    public void setAlto(java.math.BigInteger value) {
        this.alto = value;
    }

    public void setAncho(java.math.BigInteger value) {
        this.ancho = value;
    }

    public void setProfundo(java.math.BigInteger value) {
        this.profundo = value;
    }

    public void setPeso(java.math.BigInteger value) {
        this.peso = value;
    }

    public Long getId() {
        return this.id;
    }

    public Long getIdTipoEmpaque() {
        return this.idTipoEmpaque;
    }

    public String getCodigoSscc() {
        return this.codigoSscc;
    }

    public java.math.BigInteger getAlto() {
        return this.alto;
    }

    public java.math.BigInteger getAncho() {
        return this.ancho;
    }

    public java.math.BigInteger getProfundo() {
        return this.profundo;
    }

    public java.math.BigInteger getPeso() {
        return this.peso;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.idTipoEmpaque);
        hash = 53 * hash + Objects.hashCode(this.codigoSscc);
        hash = 53 * hash + Objects.hashCode(this.alto);
        hash = 53 * hash + Objects.hashCode(this.ancho);
        hash = 53 * hash + Objects.hashCode(this.profundo);
        hash = 53 * hash + Objects.hashCode(this.peso);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Empaque other = (Empaque) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.idTipoEmpaque, other.idTipoEmpaque)) {
            return false;
        }
        if (!Objects.equals(this.codigoSscc, other.codigoSscc)) {
            return false;
        }
        if (!Objects.equals(this.alto, other.alto)) {
            return false;
        }
        if (!Objects.equals(this.ancho, other.ancho)) {
            return false;
        }
        if (!Objects.equals(this.profundo, other.profundo)) {
            return false;
        }
        if (!Objects.equals(this.peso, other.peso)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Empaque[");
        builder.append("id = ").append(id);
        builder.append(", idTipoEmpaque = ").append(idTipoEmpaque);
        builder.append(", codigoSscc = ").append(codigoSscc);
        builder.append(", alto = ").append(alto);
        builder.append(", ancho = ").append(ancho);
        builder.append(", profundo = ").append(profundo);
        builder.append(", peso = ").append(peso);
        builder.append("]");
        return builder.toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Columnas">
    public static class COLUMNS implements Serializable {

        public static final JpaColumn<Long> id = JpaColumn.instance("id", Long.class, false);
        public static final JpaColumn<Long> idTipoEmpaque = JpaColumn.instance("idTipoEmpaque", Long.class, true);
        public static final JpaColumn<String> codigoSscc = JpaColumn.instance("codigoSscc", String.class, true);
        public static final JpaColumn<java.math.BigInteger> alto = JpaColumn.instance("alto", java.math.BigInteger.class, true);
        public static final JpaColumn<java.math.BigInteger> ancho = JpaColumn.instance("ancho", java.math.BigInteger.class, true);
        public static final JpaColumn<java.math.BigInteger> profundo = JpaColumn.instance("profundo", java.math.BigInteger.class, true);
        public static final JpaColumn<java.math.BigInteger> peso = JpaColumn.instance("peso", java.math.BigInteger.class, true);
    }
    //</editor-fold>

}
