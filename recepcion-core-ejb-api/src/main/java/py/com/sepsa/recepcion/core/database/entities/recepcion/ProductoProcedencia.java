package py.com.sepsa.recepcion.core.database.entities.recepcion;

import fa.gs.utils.database.criteria.column.JpaColumn;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "producto_procedencia", catalog = "sepsa", schema = "recepcion")
@DynamicInsert
public class ProductoProcedencia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_id")
    @SequenceGenerator(name = "gen_id", sequenceName = "recepcion.producto_procedencia_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("nextval('recepcion.producto_procedencia_id_seq'::regclass)")
    private Long id;

    @Column(name = "pais", nullable = false)
    @Basic(optional = false)
    private String pais;

    @Column(name = "meta__shard", nullable = true)
    @Basic(optional = true)
    @org.hibernate.annotations.Type(type = "org.hibernate.type.PostgresUUIDType")
    private java.util.UUID metaShard;

    @Column(name = "meta__eliminado", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("'0'::bpchar")
    private String metaEliminado;

    @Column(name = "meta__fecha_eliminacion", nullable = true)
    @Basic(optional = true)
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date metaFechaEliminacion;

    public void setId(Long value) {
        this.id = value;
    }

    public void setPais(String value) {
        this.pais = value;
    }

    public void setMetaShard(java.util.UUID value) {
        this.metaShard = value;
    }

    public void setMetaEliminado(String value) {
        this.metaEliminado = value;
    }

    public void setMetaFechaEliminacion(java.util.Date value) {
        this.metaFechaEliminacion = value;
    }

    public Long getId() {
        return this.id;
    }

    public String getPais() {
        return this.pais;
    }

    public java.util.UUID getMetaShard() {
        return this.metaShard;
    }

    public String getMetaEliminado() {
        return this.metaEliminado;
    }

    public java.util.Date getMetaFechaEliminacion() {
        return this.metaFechaEliminacion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.pais);
        hash = 53 * hash + Objects.hashCode(this.metaShard);
        hash = 53 * hash + Objects.hashCode(this.metaEliminado);
        hash = 53 * hash + Objects.hashCode(this.metaFechaEliminacion);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductoProcedencia other = (ProductoProcedencia) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.pais, other.pais)) {
            return false;
        }
        if (!Objects.equals(this.metaShard, other.metaShard)) {
            return false;
        }
        if (!Objects.equals(this.metaEliminado, other.metaEliminado)) {
            return false;
        }
        if (!Objects.equals(this.metaFechaEliminacion, other.metaFechaEliminacion)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ProductoProcedencia[");
        builder.append("id = ").append(id);
        builder.append(", pais = ").append(pais);
        builder.append(", metaShard = ").append(metaShard);
        builder.append(", metaEliminado = ").append(metaEliminado);
        builder.append(", metaFechaEliminacion = ").append(metaFechaEliminacion);
        builder.append("]");
        return builder.toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Columnas">
    public static class COLUMNS implements Serializable {

        public static final JpaColumn<Long> id = JpaColumn.instance("id", Long.class, false);
        public static final JpaColumn<String> pais = JpaColumn.instance("pais", String.class, false);
        public static final JpaColumn<java.util.UUID> metaShard = JpaColumn.instance("metaShard", java.util.UUID.class, true);
        public static final JpaColumn<String> metaEliminado = JpaColumn.instance("metaEliminado", String.class, false);
        public static final JpaColumn<java.util.Date> metaFechaEliminacion = JpaColumn.instance("metaFechaEliminacion", java.util.Date.class, true);
    }
    //</editor-fold>

}
