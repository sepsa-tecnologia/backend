/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.configs;

import java.util.prefs.Preferences;

/**
 *
 * @author Fabio A. González Sosa
 */
public class PersistenceConfigFactory {

    public static PersistenceConfig instance() throws Throwable {
        Preferences configPreferences = ConfigPreferences.get();
        return new PersistenceConfigV1(configPreferences);
    }

}
