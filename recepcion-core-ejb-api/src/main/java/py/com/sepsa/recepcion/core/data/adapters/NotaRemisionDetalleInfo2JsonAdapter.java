/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.adapters;

import com.google.gson.JsonElement;
import fa.gs.utils.adapters.Adapters;
import fa.gs.utils.adapters.impl.json.ToJsonAdapter;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import py.com.sepsa.recepcion.core.data.pojos.NotaRemisionDetalleInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
public class NotaRemisionDetalleInfo2JsonAdapter extends ToJsonAdapter<NotaRemisionDetalleInfo> {

    @Override
    protected JsonElement adapt0(NotaRemisionDetalleInfo obj) {
        JsonObjectBuilder json = JsonObjectBuilder.instance();
        json.add("item", adaptProductoEntregable(obj));
        json.add("cantidad", obj.getCantidad());
        json.add("total", adaptPrecio(obj));
        return json.build();
    }

    private JsonElement adaptProductoEntregable(NotaRemisionDetalleInfo obj) {
        return Adapters.adapt(ProductoEntregableInfo2JsonAdapter.class, obj.getProductoEntregable());
    }

    private JsonElement adaptPrecio(NotaRemisionDetalleInfo obj) {
        JsonObjectBuilder json = JsonObjectBuilder.instance();
        json.add("precio", obj.getPrecio());
        json.add("moneda", adaptMoneda(obj));
        return json.build();
    }

    private JsonElement adaptMoneda(NotaRemisionDetalleInfo obj) {
        return Adapters.adapt(MonedaInfo2JsonAdapter.class, obj.getMoneda());
    }

}
