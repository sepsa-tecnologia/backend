/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.result.simple.Result;
import java.util.Collection;
import java.util.Map;
import javax.ejb.Remote;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;
import py.com.sepsa.recepcion.core.data.pojos.ObservacionInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Remote
public interface ObtenerObservacionesAction extends BusinessLogicAction {

    Result<Collection<ObservacionInfo>> obtenerObservacion(Long idEntidad, TipoEntidadEnum entidad);

    Result<Map<Long, Collection<ObservacionInfo>>> obtenerObservaciones(Long[] idEntidades, TipoEntidadEnum entidad);

}
