/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import fa.gs.utils.collections.maps.ResultSetMap;
import fa.gs.utils.database.criteria.column.NativeColumn;
import java.math.BigDecimal;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;

/**
 *
 * @author Fabio A. González Sosa
 */
public class NotaRemisionDetalleInfo extends DocumentoDetalleInfo {

    private Long idNotaRemision;

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public Long getIdNotaRemision() {
        return idNotaRemision;
    }

    public void setIdNotaRemision(Long idNotaRemision) {
        this.idNotaRemision = idNotaRemision;
    }

    @Override
    public TipoEntidadEnum getTipoDocumento() {
        return TipoEntidadEnum.NOTA_REMISION_DETALLE;
    }
    //</editor-fold>

    public static class COLUMNS {

        public static final NativeColumn<Long> idNotaRemision = NativeColumn.instance("idNotaRemision", Long.class);
        public static final NativeColumn<Long> idProductoEntregable = DocumentoDetalleInfo.COLUMNS.idProductoEntregable;
        public static final NativeColumn<Long> cantidad = DocumentoDetalleInfo.COLUMNS.cantidad;
        public static final NativeColumn<BigDecimal> precio = DocumentoDetalleInfo.COLUMNS.precio;
        public static final NativeColumn<Long> idMoneda = DocumentoDetalleInfo.COLUMNS.idMoneda;
    }

    public static class Mapper extends DocumentoDetalleInfo.Mapper<NotaRemisionDetalleInfo> {

        @Override
        public NotaRemisionDetalleInfo getEmptyAdaptee() {
            return new NotaRemisionDetalleInfo();
        }

        @Override
        public NotaRemisionDetalleInfo adapt(final NotaRemisionDetalleInfo obj, ResultSetMap resultSet) {
            super.adapt(obj, resultSet);

            Long idNotaRemision0 = resultSet.long0(NotaRemisionDetalleInfo.COLUMNS.idNotaRemision);

            obj.setIdNotaRemision(idNotaRemision0);
            return obj;
        }

    }

}
