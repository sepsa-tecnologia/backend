/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic;

import fa.gs.utils.injection.Jndi;
import fa.gs.utils.injection.JndiNameGenerator;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.result.simple.Result;
import javax.naming.Context;
import py.com.sepsa.recepcion.core.logging.AppLoggerFactory;

/**
 *
 * @author Fabio A. González Sosa
 */
public final class Injection {

    private static final AppLogger log = AppLoggerFactory.core();

    public static final String EAR_NAME = "recepcion-core-0.0.1";

    public static final String EJB_NAME = "py.com.sepsa.recepcion-recepcion-core-ejb-0.0.1";

    public static JndiNameGenerator nameGenerator = new JndiPortableGlobalNameGenerator();

    public static final <T> T bean(Context context, Class<T> beanClass) {
        String jndi = nameGenerator.getJndiName(beanClass);
        Result<T> inyectionResult = Jndi.lookup(context, jndi);
        return extractInyectedObject(inyectionResult);
    }

    public static final <T> T bean(Class<T> beanClass) {
        String jndi = nameGenerator.getJndiName(beanClass);
        Result<T> inyectionResult = Jndi.lookup(jndi);
        return extractInyectedObject(inyectionResult);
    }

    private static <T> T extractInyectedObject(Result<T> inyectionResult) {
        if (inyectionResult.isFailure()) {
            log.error().failure(inyectionResult.failure()).log();
            return null;
        } else {
            Object obj = inyectionResult.value(null);
            return (obj == null) ? null : (T) obj;
        }
    }

    public static class JndiPortableGlobalNameGenerator implements JndiNameGenerator {

        @Override
        public String getJndiName(Class<?> klass) {
            String simpleName = klass.getSimpleName();
            return String.format("java:global/%s/%s/%s", EAR_NAME, EJB_NAME, simpleName);
        }

    }

    public static class JndiFullPortableGlobalNameGenerator implements JndiNameGenerator {

        @Override
        public String getJndiName(Class<?> klass) {
            String simpleName = klass.getSimpleName();
            String fullName = klass.getCanonicalName();
            return String.format("java:global/%s/%s/%s!%s", EAR_NAME, EJB_NAME, simpleName, fullName);
        }

    }

    public static class JndiSimpleNameGenerator implements JndiNameGenerator {

        @Override
        public String getJndiName(Class<?> klass) {
            return klass.getSimpleName();
        }

    }

}
