/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.enums;

import fa.gs.utils.collections.enums.EnumerableAdapter;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum PermisoEnum {
    ADAPTER(null, null),
    PREDETERMINADO("0001", "PREDETERMINADO"),
    PUEDE_VER_INFORMACION_LEGAL_OC("0002", "PUEDE_VER_INFORMACION_LEGAL_OC"),
    PUEDE_USAR_APP_RECEPCION("0003", "PUEDE_USAR_APP_RECEPCION");
    private final String codigo;
    private final String descripcion;

    private PermisoEnum(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static EnumerableAdapter<PermisoEnum> adapter() {
        return new Adapter0();
    }

    public static PermisoEnum from(String codigo) {
        for (PermisoEnum permisoEnum : PermisoEnum.values()) {
            if (Assertions.equals(codigo, permisoEnum.codigo)) {
                return permisoEnum;
            }
        }

        return PermisoEnum.PREDETERMINADO;
    }

    public String codigo() {
        return codigo;
    }

    public String descripcion() {
        return descripcion;
    }

    private static class Adapter0 implements EnumerableAdapter<PermisoEnum> {

        @Override
        public PermisoEnum getEnumerable(Object obj) {
            if (obj instanceof String) {
                String codigo = (String) obj;
                return PermisoEnum.from(codigo);
            }

            throw Errors.unsupported();
        }
    }

}
