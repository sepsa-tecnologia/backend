/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.enums;

import fa.gs.utils.collections.enums.EnumerableAdapter;
import fa.gs.utils.misc.errors.Errors;
import java.util.Objects;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum TipoEntidadEnum {
    ORDEN_COMPRA("orden_compra", "0001"),
    NOTA_REMISION("nota_remision", "0002"),
    NOTA_REMISION_DETALLE("nota_remision_detalle", "0003"),
    FACTURA("factura", "0004"),
    FACTURA_DETALLE("factura_detalle", "0005"),
    RECEPCION("recepcion", "0006"),
    RECEPCION_DETALLE("recepcion_detalle", "0007");
    private final String descripcion;
    private final String codigo;

    private TipoEntidadEnum(String descripcion, String codigo) {
        this.descripcion = descripcion;
        this.codigo = codigo;
    }

    public static TipoEntidadEnum from(String codigo) {
        for (TipoEntidadEnum tipoEntidad : TipoEntidadEnum.values()) {
            if (Objects.equals(tipoEntidad.codigo(), codigo)) {
                return tipoEntidad;
            }
        }

        throw Errors.unsupported();
    }

    public static EnumerableAdapter<TipoEntidadEnum> adapter() {
        return new Adapter0();
    }

    public String descripcion() {
        return descripcion;
    }

    public String codigo() {
        return codigo;
    }

    private static class Adapter0 implements EnumerableAdapter<TipoEntidadEnum> {

        @Override
        public TipoEntidadEnum getEnumerable(Object value) {
            if (value instanceof String) {
                String descripcion = (String) value;
                return TipoEntidadEnum.from(descripcion);
            } else {
                throw Errors.unsupported();
            }
        }

    }

}
