/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.maps.ResultSetMap;
import fa.gs.utils.database.criteria.column.NativeColumn;
import fa.gs.utils.misc.Assertions;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

/**
 *
 * @author Fabio A. González Sosa
 */
public abstract class DocumentoDetalleInfo implements DocumentoInfo, DocumentoConObservaciones, Serializable {

    private Long id;
    private ProductoEntregableInfo productoEntregable;
    private Long cantidad;
    private BigDecimal precio;
    private MonedaInfo moneda;
    private final Collection<ObservacionInfo> observaciones;

    public DocumentoDetalleInfo() {
        this.observaciones = Lists.empty();
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    @Override
    public final Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProductoEntregableInfo getProductoEntregable() {
        return this.productoEntregable;
    }

    public void setProductoEntregable(ProductoEntregableInfo value) {
        this.productoEntregable = value;
    }

    public Long getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(Long value) {
        this.cantidad = value;
    }

    public BigDecimal getPrecio() {
        return this.precio;
    }

    public void setPrecio(BigDecimal value) {
        this.precio = value;
    }

    public MonedaInfo getMoneda() {
        return this.moneda;
    }

    public void setMoneda(MonedaInfo value) {
        this.moneda = value;
    }

    @Override
    public Collection<ObservacionInfo> getObservaciones() {
        return observaciones;
    }

    @Override
    public void setObservaciones(Collection<ObservacionInfo> observaciones) {
        if (!Assertions.isNullOrEmpty(observaciones)) {
            this.observaciones.addAll(observaciones);
        }
    }
    //</editor-fold>

    public static class COLUMNS {

        public static final NativeColumn<Long> id = NativeColumn.instance("id", Long.class);
        public static final NativeColumn<Long> idProductoEntregable = NativeColumn.instance("idProductoEntregable", Long.class);
        public static final NativeColumn<Long> cantidad = NativeColumn.instance("cantidad", Long.class);
        public static final NativeColumn<BigDecimal> precio = NativeColumn.instance("precio", BigDecimal.class);
        public static final NativeColumn<Long> idMoneda = NativeColumn.instance("idMoneda", Long.class);
    }

    public static abstract class Mapper<T extends DocumentoDetalleInfo> extends fa.gs.utils.database.utils.ResultSetMapper<T> {

        @Override
        public T adapt(final T obj, ResultSetMap resultSet) {
            Long id0 = resultSet.long0(COLUMNS.id);
            Long idProductoEntregable0 = resultSet.long0(COLUMNS.idProductoEntregable);
            Long cantidad0 = resultSet.long0(COLUMNS.cantidad);
            BigDecimal precio0 = resultSet.bigdecimal(COLUMNS.precio);
            Long idMoneda0 = resultSet.long0(COLUMNS.idMoneda);

            // Identificador de entrega de producto.
            ProductoEntregableInfo productoEntregable0 = new ProductoEntregableInfo();
            productoEntregable0.setId(idProductoEntregable0);

            // Identificador de moneda.
            MonedaInfo moneda0 = new MonedaInfo();
            moneda0.setId(idMoneda0);

            obj.setId(id0);
            obj.setProductoEntregable(productoEntregable0);
            obj.setCantidad(cantidad0);
            obj.setPrecio(precio0);
            obj.setMoneda(moneda0);
            return obj;
        }

    }
}
