/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import fa.gs.utils.collections.maps.ResultSetMap;
import fa.gs.utils.database.criteria.column.NativeColumn;
import java.math.BigDecimal;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;

/**
 *
 * @author Fabio A. González Sosa
 */
public class FacturaDetalleInfo extends DocumentoDetalleInfo {

    private Long idFactura;

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public Long getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Long idFactura) {
        this.idFactura = idFactura;
    }

    @Override
    public TipoEntidadEnum getTipoDocumento() {
        return TipoEntidadEnum.FACTURA_DETALLE;
    }
    //</editor-fold>

    public static class COLUMNS {

        public static final NativeColumn<Long> idFactura = NativeColumn.instance("idFactura", Long.class);
        public static final NativeColumn<Long> idProductoEntregable = DocumentoDetalleInfo.COLUMNS.idProductoEntregable;
        public static final NativeColumn<Long> cantidad = DocumentoDetalleInfo.COLUMNS.cantidad;
        public static final NativeColumn<BigDecimal> precio = DocumentoDetalleInfo.COLUMNS.precio;
        public static final NativeColumn<Long> idMoneda = DocumentoDetalleInfo.COLUMNS.idMoneda;
    }

    public static class Mapper extends DocumentoDetalleInfo.Mapper<FacturaDetalleInfo> {

        @Override
        public FacturaDetalleInfo getEmptyAdaptee() {
            return new FacturaDetalleInfo();
        }

        @Override
        public FacturaDetalleInfo adapt(final FacturaDetalleInfo obj, ResultSetMap resultSet) {
            super.adapt(obj, resultSet);

            Long idFactura0 = resultSet.long0(FacturaDetalleInfo.COLUMNS.idFactura);

            obj.setIdFactura(idFactura0);
            return obj;
        }

    }

}
