/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic;

import java.io.File;
import java.io.Serializable;
import javax.ejb.Remote;
import py.com.sepsa.recepcion.core.logic.configs.PersistenceConfig;
import py.com.sepsa.recepcion.core.logic.configs.RecepcionConfig;
import py.com.sepsa.recepcion.core.logic.configs.SiediConfig;

/**
 *
 * @author Fabio A. González Sosa
 */
@Remote
public interface Configs extends Serializable {

    /**
     * Obtiene una referencia al archivo fisico que contiene las configuraciones
     * de la plataforma.
     *
     * @return Archivo de configuracion.
     */
    File getConfigFile();

    RecepcionConfig getRecepcionConfig();
    
    PersistenceConfig getPersistenceConfig();

    SiediConfig getSiediConfig();

}
