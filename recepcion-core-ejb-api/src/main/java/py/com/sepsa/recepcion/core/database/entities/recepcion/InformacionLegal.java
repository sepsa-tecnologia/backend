package py.com.sepsa.recepcion.core.database.entities.recepcion;

import fa.gs.utils.database.criteria.column.JpaColumn;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "informacion_legal", catalog = "sepsa", schema = "recepcion")
@DynamicInsert
public class InformacionLegal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_id")
    @SequenceGenerator(name = "gen_id", sequenceName = "recepcion.informacion_legal_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("nextval('recepcion.informacion_legal_id_seq'::regclass)")
    private Long id;

    @Column(name = "nro_contrato", nullable = true)
    @Basic(optional = true)
    private String nroContrato;

    @Column(name = "fecha_contrato", nullable = true)
    @Basic(optional = true)
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date fechaContrato;

    @Column(name = "nro_resolucion", nullable = true)
    @Basic(optional = true)
    private String nroResolucion;

    @Column(name = "nro_licitacion", nullable = true)
    @Basic(optional = true)
    private String nroLicitacion;

    public void setId(Long value) {
        this.id = value;
    }

    public void setNroContrato(String value) {
        this.nroContrato = value;
    }

    public void setFechaContrato(java.util.Date value) {
        this.fechaContrato = value;
    }

    public void setNroResolucion(String value) {
        this.nroResolucion = value;
    }

    public void setNroLicitacion(String value) {
        this.nroLicitacion = value;
    }

    public Long getId() {
        return this.id;
    }

    public String getNroContrato() {
        return this.nroContrato;
    }

    public java.util.Date getFechaContrato() {
        return this.fechaContrato;
    }

    public String getNroResolucion() {
        return this.nroResolucion;
    }

    public String getNroLicitacion() {
        return this.nroLicitacion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.nroContrato);
        hash = 53 * hash + Objects.hashCode(this.fechaContrato);
        hash = 53 * hash + Objects.hashCode(this.nroResolucion);
        hash = 53 * hash + Objects.hashCode(this.nroLicitacion);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InformacionLegal other = (InformacionLegal) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.nroContrato, other.nroContrato)) {
            return false;
        }
        if (!Objects.equals(this.fechaContrato, other.fechaContrato)) {
            return false;
        }
        if (!Objects.equals(this.nroResolucion, other.nroResolucion)) {
            return false;
        }
        if (!Objects.equals(this.nroLicitacion, other.nroLicitacion)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("InformacionLegal[");
        builder.append("id = ").append(id);
        builder.append(", nroContrato = ").append(nroContrato);
        builder.append(", fechaContrato = ").append(fechaContrato);
        builder.append(", nroResolucion = ").append(nroResolucion);
        builder.append(", nroLicitacion = ").append(nroLicitacion);
        builder.append("]");
        return builder.toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Columnas">
    public static class COLUMNS implements Serializable {

        public static final JpaColumn<Long> id = JpaColumn.instance("id", Long.class, false);
        public static final JpaColumn<String> nroContrato = JpaColumn.instance("nroContrato", String.class, true);
        public static final JpaColumn<java.util.Date> fechaContrato = JpaColumn.instance("fechaContrato", java.util.Date.class, true);
        public static final JpaColumn<String> nroResolucion = JpaColumn.instance("nroResolucion", String.class, true);
        public static final JpaColumn<String> nroLicitacion = JpaColumn.instance("nroLicitacion", String.class, true);
    }
    //</editor-fold>

}
