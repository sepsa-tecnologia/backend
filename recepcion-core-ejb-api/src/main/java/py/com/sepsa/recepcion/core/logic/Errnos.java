/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic;

import fa.gs.utils.misc.errors.Errno;
import fa.gs.utils.misc.errors.Errors;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Errnos {

    private static final int ERRNO_DOCUMENTO_BASE = 100;

    public static final Errno DOCUMENTO_NO_VALIDADO = Errors.errno("REC", ERRNO_DOCUMENTO_BASE + 1);

    public static final Errno DOCUMENTO_NO_EXISTE = Errors.errno("REC", ERRNO_DOCUMENTO_BASE + 2);

    public static final Errno DOCUMENTO_NO_RELACIONADO_OC = Errors.errno("REC", ERRNO_DOCUMENTO_BASE + 3);

    private static final int ERRNO_AUTH_BASE = 200;

    public static final Errno AUTH_USUARIO_NO_EXISTE = Errors.errno("REC", ERRNO_AUTH_BASE + 1);

    public static final Errno AUTH_PASSWORD_NO_CONCUERDA = Errors.errno("REC", ERRNO_AUTH_BASE + 2);

    public static final Errno AUTH_SIN_PERMISOS = Errors.errno("REC", ERRNO_AUTH_BASE + 3);

    public static final Errno AUTH_NO_PUEDE_USAR_APP = Errors.errno("REC", ERRNO_AUTH_BASE + 4);

}
