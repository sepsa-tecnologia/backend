/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import fa.gs.utils.collections.maps.ResultSetMap;
import fa.gs.utils.database.criteria.column.NativeColumn;
import java.math.BigDecimal;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;

/**
 *
 * @author Fabio A. González Sosa
 */
public class RecepcionDetalleInfo extends DocumentoDetalleInfo {

    private Long idRecepcion;
    private Long idDetalleRecepcion;

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public Long getIdDetalleRecepcion() {
        return idDetalleRecepcion;
    }

    public void setIdDetalleRecepcion(Long idDetalleRecepcion) {
        this.idDetalleRecepcion = idDetalleRecepcion;
    }

    public Long getIdRecepcion() {
        return idRecepcion;
    }

    public void setIdRecepcion(Long idRecepcion) {
        this.idRecepcion = idRecepcion;
    }

    @Override
    public TipoEntidadEnum getTipoDocumento() {
        return TipoEntidadEnum.RECEPCION_DETALLE;
    }
    //</editor-fold>

    public static class COLUMNS {

        public static final NativeColumn<Long> idDetalleRecepcion = NativeColumn.instance("idDetalleRecepcion", Long.class);
        public static final NativeColumn<Long> idRecepcion = NativeColumn.instance("idRecepcion", Long.class);
        public static final NativeColumn<Long> idProductoEntregable = DocumentoDetalleInfo.COLUMNS.idProductoEntregable;
        public static final NativeColumn<Long> cantidad = DocumentoDetalleInfo.COLUMNS.cantidad;
        public static final NativeColumn<BigDecimal> precio = DocumentoDetalleInfo.COLUMNS.precio;
        public static final NativeColumn<Long> idMoneda = DocumentoDetalleInfo.COLUMNS.idMoneda;
    }

    public static class Mapper extends DocumentoDetalleInfo.Mapper<RecepcionDetalleInfo> {

        @Override
        public RecepcionDetalleInfo getEmptyAdaptee() {
            return new RecepcionDetalleInfo();
        }

        @Override
        public RecepcionDetalleInfo adapt(final RecepcionDetalleInfo obj, ResultSetMap resultSet) {
            super.adapt(obj, resultSet);

            Long idRecepcion0 = resultSet.long0(RecepcionDetalleInfo.COLUMNS.idRecepcion);
            Long idDetalleRecepcion0 = resultSet.long0(RecepcionDetalleInfo.COLUMNS.idDetalleRecepcion);

            obj.setIdRecepcion(idRecepcion0);
            obj.setIdDetalleRecepcion(idDetalleRecepcion0);
            return obj;
        }

    }

}
