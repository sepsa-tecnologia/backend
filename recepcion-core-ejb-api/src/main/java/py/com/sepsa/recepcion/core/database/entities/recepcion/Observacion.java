package py.com.sepsa.recepcion.core.database.entities.recepcion;

import fa.gs.utils.database.criteria.column.JpaColumn;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "observacion", catalog = "sepsa", schema = "recepcion")
@DynamicInsert
public class Observacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_id")
    @SequenceGenerator(name = "gen_id", sequenceName = "recepcion.observacion_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("nextval('recepcion.observacion_id_seq'::regclass)")
    private Long id;

    @Column(name = "id_entidad", nullable = false)
    @Basic(optional = false)
    private Long idEntidad;

    @Column(name = "entidad", nullable = false)
    @Basic(optional = false)
    private String entidad;

    @Column(name = "id_tipo_observacion", nullable = true)
    @Basic(optional = true)
    private Long idTipoObservacion;

    @Column(name = "descripcion", nullable = false)
    @Basic(optional = false)
    private String descripcion;

    @Column(name = "fecha_registro", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("now()")
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date fechaRegistro;

    public void setId(Long value) {
        this.id = value;
    }

    public void setIdEntidad(Long value) {
        this.idEntidad = value;
    }

    public void setEntidad(String value) {
        this.entidad = value;
    }

    public void setIdTipoObservacion(Long value) {
        this.idTipoObservacion = value;
    }

    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    public void setFechaRegistro(java.util.Date value) {
        this.fechaRegistro = value;
    }

    public Long getId() {
        return this.id;
    }

    public Long getIdEntidad() {
        return this.idEntidad;
    }

    public String getEntidad() {
        return this.entidad;
    }

    public Long getIdTipoObservacion() {
        return this.idTipoObservacion;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public java.util.Date getFechaRegistro() {
        return this.fechaRegistro;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.idEntidad);
        hash = 53 * hash + Objects.hashCode(this.entidad);
        hash = 53 * hash + Objects.hashCode(this.idTipoObservacion);
        hash = 53 * hash + Objects.hashCode(this.descripcion);
        hash = 53 * hash + Objects.hashCode(this.fechaRegistro);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Observacion other = (Observacion) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.idEntidad, other.idEntidad)) {
            return false;
        }
        if (!Objects.equals(this.entidad, other.entidad)) {
            return false;
        }
        if (!Objects.equals(this.idTipoObservacion, other.idTipoObservacion)) {
            return false;
        }
        if (!Objects.equals(this.descripcion, other.descripcion)) {
            return false;
        }
        if (!Objects.equals(this.fechaRegistro, other.fechaRegistro)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Observacion[");
        builder.append("id = ").append(id);
        builder.append(", idEntidad = ").append(idEntidad);
        builder.append(", entidad = ").append(entidad);
        builder.append(", idTipoObservacion = ").append(idTipoObservacion);
        builder.append(", descripcion = ").append(descripcion);
        builder.append(", fechaRegistro = ").append(fechaRegistro);
        builder.append("]");
        return builder.toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Columnas">
    public static class COLUMNS implements Serializable {

        public static final JpaColumn<Long> id = JpaColumn.instance("id", Long.class, false);
        public static final JpaColumn<Long> idEntidad = JpaColumn.instance("idEntidad", Long.class, false);
        public static final JpaColumn<String> entidad = JpaColumn.instance("entidad", String.class, false);
        public static final JpaColumn<Long> idTipoObservacion = JpaColumn.instance("idTipoObservacion", Long.class, true);
        public static final JpaColumn<String> descripcion = JpaColumn.instance("descripcion", String.class, false);
        public static final JpaColumn<java.util.Date> fechaRegistro = JpaColumn.instance("fechaRegistro", java.util.Date.class, false);
    }
    //</editor-fold>

}
