/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.configs;

import fa.gs.utils.misc.Assertions;
import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;
import py.com.sepsa.recepcion.core.data.enums.SiediDocumentFormat;
import py.com.sepsa.recepcion.core.data.enums.SiediVersion;

/**
 *
 * @author Fabio A. González Sosa
 */
public class SiediConfigV1 implements SiediConfig {

    private final Preferences config;

    public SiediConfigV1(final Preferences config) throws IOException {
        this.config = config;
    }

    @Override
    public SiediVersion getSiediVersion() {
        return SiediVersion.V1;
    }

    @Override
    public File getCarpetaSalida() {
        String path = config.node("siedi.v1").get("documentos.salida.carpeta", "");
        if (Assertions.stringNullOrEmpty(path)) {
            return null;
        } else {
            return new File(path);
        }
    }

    @Override
    public SiediDocumentFormat getFormatoSalida() {
        String format = config.node("siedi.v1").get("documentos.salida.formato", "");
        if (Assertions.stringNullOrEmpty(format)) {
            return null;
        } else {
            return SiediDocumentFormat.from(format);
        }
    }

    @Override
    public File getCarpetaEntrada() {
        String path = config.node("siedi.v1").get("documentos.entrada.carpeta", "");
        if (Assertions.stringNullOrEmpty(path)) {
            return null;
        } else {
            return new File(path);
        }
    }

    @Override
    public SiediDocumentFormat getFormatoEntrada() {
        String format = config.node("siedi.v1").get("documentos.entrada.formato", "");
        if (Assertions.stringNullOrEmpty(format)) {
            return null;
        } else {
            return SiediDocumentFormat.from(format);
        }
    }

}
