/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.enums;

import fa.gs.utils.collections.enums.EnumerableAdapter;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum TipoEmpaqueEnum {
    UNIDAD("0001", "Unidad");
    private final String codigo;
    private final String descripcion;

    private TipoEmpaqueEnum(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static EnumerableAdapter<TipoEmpaqueEnum> adapter() {
        return new Adapter0();
    }

    public static TipoEmpaqueEnum from(String codigo) {
        for (TipoEmpaqueEnum tipoEmpaqueEnum : TipoEmpaqueEnum.values()) {
            if (Assertions.equals(codigo, tipoEmpaqueEnum.codigo)) {
                return tipoEmpaqueEnum;
            }
        }

        return TipoEmpaqueEnum.UNIDAD;
    }

    public String codigo() {
        return codigo;
    }

    public String descripcion() {
        return descripcion;
    }

    private static class Adapter0 implements EnumerableAdapter<TipoEmpaqueEnum> {

        @Override
        public TipoEmpaqueEnum getEnumerable(Object obj) {
            if (obj instanceof String) {
                String codigo = (String) obj;
                return TipoEmpaqueEnum.from(codigo);
            }

            throw Errors.unsupported();
        }
    }

}
