/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.maps.ResultSetMap;
import fa.gs.utils.database.criteria.column.NativeColumn;
import fa.gs.utils.misc.Assertions;
import java.util.Collection;
import java.util.Date;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;

/**
 *
 * @author Fabio A. González Sosa
 */
public class OrdenCompraInfo implements DocumentoInfo, DocumentoConEstado, DocumentoConNro {

    private Long id;
    private EstadoEnum estado;
    private Long idDocumento;
    private String nroDocumento;
    private String glnOrigen;
    private String razonSocialOrigen;
    private String rucOrigen;
    private String glnDestino;
    private String razonSocialDestino;
    private String rucDestino;
    private Date fechaRegistro;
    private Date fechaEmision;
    private String nroContrato;
    private Date fechaContrato;
    private String nroResolucion;
    private String nroLicitacion;
    private Date fechaInicioPlazoEntrega;
    private Integer diasPlazoEntrega;
    private String condicionesEspecialesEntrega;
    private final Collection<OrdenCompraDetalleInfo> detalles;

    public OrdenCompraInfo() {
        this.detalles = Lists.empty();
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }

    public Long getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Long idDocumento) {
        this.idDocumento = idDocumento;
    }

    @Override
    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getGlnOrigen() {
        return glnOrigen;
    }

    public void setGlnOrigen(String glnOrigen) {
        this.glnOrigen = glnOrigen;
    }

    public String getRazonSocialOrigen() {
        return razonSocialOrigen;
    }

    public void setRazonSocialOrigen(String razonSocialOrigen) {
        this.razonSocialOrigen = razonSocialOrigen;
    }

    public String getRucOrigen() {
        return rucOrigen;
    }

    public void setRucOrigen(String rucOrigen) {
        this.rucOrigen = rucOrigen;
    }

    public String getGlnDestino() {
        return glnDestino;
    }

    public void setGlnDestino(String glnDestino) {
        this.glnDestino = glnDestino;
    }

    public String getRazonSocialDestino() {
        return razonSocialDestino;
    }

    public void setRazonSocialDestino(String razonSocialDestino) {
        this.razonSocialDestino = razonSocialDestino;
    }

    public String getRucDestino() {
        return rucDestino;
    }

    public void setRucDestino(String rucDestino) {
        this.rucDestino = rucDestino;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getNroContrato() {
        return nroContrato;
    }

    public void setNroContrato(String nroContrato) {
        this.nroContrato = nroContrato;
    }

    public Date getFechaContrato() {
        return fechaContrato;
    }

    public void setFechaContrato(Date fechaContrato) {
        this.fechaContrato = fechaContrato;
    }

    public String getNroResolucion() {
        return nroResolucion;
    }

    public void setNroResolucion(String nroResolucion) {
        this.nroResolucion = nroResolucion;
    }

    public String getNroLicitacion() {
        return nroLicitacion;
    }

    public void setNroLicitacion(String nroLicitacion) {
        this.nroLicitacion = nroLicitacion;
    }

    public Date getFechaInicioPlazoEntrega() {
        return fechaInicioPlazoEntrega;
    }

    public void setFechaInicioPlazoEntrega(Date fechaInicioPlazoEntrega) {
        this.fechaInicioPlazoEntrega = fechaInicioPlazoEntrega;
    }

    public Integer getDiasPlazoEntrega() {
        return diasPlazoEntrega;
    }

    public void setDiasPlazoEntrega(Integer diasPlazoEntrega) {
        this.diasPlazoEntrega = diasPlazoEntrega;
    }

    public String getCondicionesEspecialesEntrega() {
        return condicionesEspecialesEntrega;
    }

    public void setCondicionesEspecialesEntrega(String condicionesEspecialesEntrega) {
        this.condicionesEspecialesEntrega = condicionesEspecialesEntrega;
    }

    public Collection<OrdenCompraDetalleInfo> getDetalles() {
        return detalles;
    }

    public void setDetalles(Collection<OrdenCompraDetalleInfo> detalles) {
        if (!Assertions.isNullOrEmpty(detalles)) {
            this.detalles.addAll(detalles);
        }
    }

    @Override
    public TipoEntidadEnum getTipoDocumento() {
        return TipoEntidadEnum.ORDEN_COMPRA;
    }
    //</editor-fold>

    public static class COLUMNS {

        public static final NativeColumn<Long> id = NativeColumn.instance("id", Long.class);
        public static final NativeColumn<String> estadoCodigo = NativeColumn.instance("estadoCodigo", String.class);
        public static final NativeColumn<Long> idDocumento = NativeColumn.instance("idDocumento", Long.class);
        public static final NativeColumn<String> nroDocumento = NativeColumn.instance("nroDocumento", String.class);
        public static final NativeColumn<String> glnOrigen = NativeColumn.instance("glnOrigen", String.class);
        public static final NativeColumn<String> razonSocialOrigen = NativeColumn.instance("razonSocialOrigen", String.class);
        public static final NativeColumn<String> rucOrigen = NativeColumn.instance("rucOrigen", String.class);
        public static final NativeColumn<String> glnDestino = NativeColumn.instance("glnDestino", String.class);
        public static final NativeColumn<String> razonSocialDestino = NativeColumn.instance("razonSocialDestino", String.class);
        public static final NativeColumn<String> rucDestino = NativeColumn.instance("rucDestino", String.class);
        public static final NativeColumn<Date> fechaRegistro = NativeColumn.instance("fechaRegistro", Date.class);
        public static final NativeColumn<Date> fechaEmision = NativeColumn.instance("fechaEmision", Date.class);
        public static final NativeColumn<String> nroContrato = NativeColumn.instance("nroContrato", String.class);
        public static final NativeColumn<Date> fechaContrato = NativeColumn.instance("fechaContrato", Date.class);
        public static final NativeColumn<String> nroResolucion = NativeColumn.instance("nroResolucion", String.class);
        public static final NativeColumn<String> nroLicitacion = NativeColumn.instance("nroLicitacion", String.class);
        public static final NativeColumn<Date> fechaInicioPlazoEntrega = NativeColumn.instance("fechaInicioPlazoEntrega", Date.class);
        public static final NativeColumn<Integer> diasPlazoEntrega = NativeColumn.instance("diasPlazoEntrega", Integer.class);
        public static final NativeColumn<String> condicionesEspecialesEntrega = NativeColumn.instance("condicionesEspecialesEntrega", String.class);
    }

    public static class Mapper extends fa.gs.utils.database.utils.ResultSetMapper<OrdenCompraInfo> {

        @Override
        protected OrdenCompraInfo getEmptyAdaptee() {
            return new OrdenCompraInfo();
        }

        @Override
        public OrdenCompraInfo adapt(final OrdenCompraInfo obj, ResultSetMap resultSet) {
            Long id0 = resultSet.long0(COLUMNS.id);
            EstadoEnum estado0 = resultSet.enumerable(COLUMNS.estadoCodigo, EstadoEnum.adapter());
            Long idDocumento0 = resultSet.long0(COLUMNS.idDocumento);
            String nroDocumento0 = resultSet.string(COLUMNS.nroDocumento);
            String glnOrigen0 = resultSet.string(COLUMNS.glnOrigen);
            String razonSocialOrigen0 = resultSet.string(COLUMNS.razonSocialOrigen);
            String rucOrigen0 = resultSet.string(COLUMNS.rucOrigen);
            String glnDestino0 = resultSet.string(COLUMNS.glnDestino);
            String razonSocialDestino0 = resultSet.string(COLUMNS.razonSocialDestino);
            String rucDestino0 = resultSet.string(COLUMNS.rucDestino);
            Date fechaRegistro0 = resultSet.date(COLUMNS.fechaRegistro);
            Date fechaEmision0 = resultSet.date(COLUMNS.fechaEmision);
            String nroContrato0 = resultSet.string(COLUMNS.nroContrato);
            Date fechaContrato0 = resultSet.date(COLUMNS.fechaContrato);
            String nroResolucion0 = resultSet.string(COLUMNS.nroResolucion);
            String nroLicitacion0 = resultSet.string(COLUMNS.nroLicitacion);
            Date fechaInicioPlazoEntrega0 = resultSet.date(COLUMNS.fechaInicioPlazoEntrega);
            Integer diasPlazoEntrega0 = resultSet.integer(COLUMNS.diasPlazoEntrega);
            String condicionesEspecialesEntrega0 = resultSet.string(COLUMNS.condicionesEspecialesEntrega);

            obj.setId(id0);
            obj.setEstado(estado0);
            obj.setIdDocumento(idDocumento0);
            obj.setNroDocumento(nroDocumento0);
            obj.setGlnOrigen(glnOrigen0);
            obj.setRazonSocialOrigen(razonSocialOrigen0);
            obj.setRucOrigen(rucOrigen0);
            obj.setGlnDestino(glnDestino0);
            obj.setRazonSocialDestino(razonSocialDestino0);
            obj.setRucDestino(rucDestino0);
            obj.setFechaRegistro(fechaRegistro0);
            obj.setFechaEmision(fechaEmision0);
            obj.setNroContrato(nroContrato0);
            obj.setFechaContrato(fechaContrato0);
            obj.setNroResolucion(nroResolucion0);
            obj.setNroLicitacion(nroLicitacion0);
            obj.setFechaInicioPlazoEntrega(fechaInicioPlazoEntrega0);
            obj.setDiasPlazoEntrega(diasPlazoEntrega0);
            obj.setCondicionesEspecialesEntrega(condicionesEspecialesEntrega0);
            return obj;
        }

    }
}
