/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.data.pojos;

import java.util.Collection;

/**
 *
 * @author Fabio A. González Sosa
 */
public interface DocumentoConObservaciones {

    Collection<ObservacionInfo> getObservaciones();

    void setObservaciones(Collection<ObservacionInfo> observaciones);

}
