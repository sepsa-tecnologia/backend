/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic;

import fa.gs.utils.misc.Assertions;
import java.util.Objects;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.data.pojos.DocumentoConEstado;

/**
 *
 * @author Fabio A. González Sosa
 */
public class CheckEstados {

    public static boolean documentoEstaPendienteDeValidacion(DocumentoConEstado info) {
        return documentoTieneAlgunEstado(info, EstadoEnum.INDEFINIDO, EstadoEnum.ABIERTO);
    }

    public static boolean documentoEstaAceptado(DocumentoConEstado info) {
        return (info.getEstado() == EstadoEnum.ACEPTADO);
    }

    public static boolean documentoTieneAlgunEstado(DocumentoConEstado info, EstadoEnum... estados) {
        if (Assertions.isNullOrEmpty(estados)) {
            return false;
        }

        EstadoEnum estado0 = info.getEstado();
        for (EstadoEnum estado : estados) {
            if (Objects.equals(estado0, estado)) {
                return true;
            }
        }

        return false;
    }

}
