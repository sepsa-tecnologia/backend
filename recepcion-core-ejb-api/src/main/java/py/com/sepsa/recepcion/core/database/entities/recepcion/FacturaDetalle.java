package py.com.sepsa.recepcion.core.database.entities.recepcion;

import fa.gs.utils.database.criteria.column.JpaColumn;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "factura_detalle", catalog = "sepsa", schema = "recepcion")
@DynamicInsert
public class FacturaDetalle implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_id")
    @SequenceGenerator(name = "gen_id", sequenceName = "recepcion.factura_detalle_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("nextval('recepcion.factura_detalle_id_seq'::regclass)")
    private Long id;

    @Column(name = "id_factura", nullable = false)
    @Basic(optional = false)
    private Long idFactura;

    @Column(name = "id_producto_entrega", nullable = true)
    @Basic(optional = true)
    private Long idProductoEntrega;

    @Column(name = "cantidad", nullable = false)
    @Basic(optional = false)
    @ColumnDefault("0")
    private Long cantidad;

    @Column(name = "precio", nullable = true)
    @Basic(optional = true)
    private java.math.BigInteger precio;

    @Column(name = "id_moneda", nullable = true)
    @Basic(optional = true)
    private Long idMoneda;

    public void setId(Long value) {
        this.id = value;
    }

    public void setIdFactura(Long value) {
        this.idFactura = value;
    }

    public void setIdProductoEntrega(Long value) {
        this.idProductoEntrega = value;
    }

    public void setCantidad(Long value) {
        this.cantidad = value;
    }

    public void setPrecio(java.math.BigInteger value) {
        this.precio = value;
    }

    public void setIdMoneda(Long value) {
        this.idMoneda = value;
    }

    public Long getId() {
        return this.id;
    }

    public Long getIdFactura() {
        return this.idFactura;
    }

    public Long getIdProductoEntrega() {
        return this.idProductoEntrega;
    }

    public Long getCantidad() {
        return this.cantidad;
    }

    public java.math.BigInteger getPrecio() {
        return this.precio;
    }

    public Long getIdMoneda() {
        return this.idMoneda;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.idFactura);
        hash = 53 * hash + Objects.hashCode(this.idProductoEntrega);
        hash = 53 * hash + Objects.hashCode(this.cantidad);
        hash = 53 * hash + Objects.hashCode(this.precio);
        hash = 53 * hash + Objects.hashCode(this.idMoneda);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FacturaDetalle other = (FacturaDetalle) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.idFactura, other.idFactura)) {
            return false;
        }
        if (!Objects.equals(this.idProductoEntrega, other.idProductoEntrega)) {
            return false;
        }
        if (!Objects.equals(this.cantidad, other.cantidad)) {
            return false;
        }
        if (!Objects.equals(this.precio, other.precio)) {
            return false;
        }
        if (!Objects.equals(this.idMoneda, other.idMoneda)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FacturaDetalle[");
        builder.append("id = ").append(id);
        builder.append(", idFactura = ").append(idFactura);
        builder.append(", idProductoEntrega = ").append(idProductoEntrega);
        builder.append(", cantidad = ").append(cantidad);
        builder.append(", precio = ").append(precio);
        builder.append(", idMoneda = ").append(idMoneda);
        builder.append("]");
        return builder.toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Columnas">
    public static class COLUMNS implements Serializable {

        public static final JpaColumn<Long> id = JpaColumn.instance("id", Long.class, false);
        public static final JpaColumn<Long> idFactura = JpaColumn.instance("idFactura", Long.class, false);
        public static final JpaColumn<Long> idProductoEntrega = JpaColumn.instance("idProductoEntrega", Long.class, true);
        public static final JpaColumn<Long> cantidad = JpaColumn.instance("cantidad", Long.class, false);
        public static final JpaColumn<java.math.BigInteger> precio = JpaColumn.instance("precio", java.math.BigInteger.class, true);
        public static final JpaColumn<Long> idMoneda = JpaColumn.instance("idMoneda", Long.class, true);
    }
    //</editor-fold>

}
