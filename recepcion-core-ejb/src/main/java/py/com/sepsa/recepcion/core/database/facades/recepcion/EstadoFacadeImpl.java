package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Estado;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "EstadoFacade", mappedName = "EstadoFacade")
@Remote(EstadoFacade.class)
public class EstadoFacadeImpl extends AbstractFacade<Estado> implements EstadoFacade {

    @EJB
    private Persistence persistence;

    public EstadoFacadeImpl() {
        super(Estado.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
