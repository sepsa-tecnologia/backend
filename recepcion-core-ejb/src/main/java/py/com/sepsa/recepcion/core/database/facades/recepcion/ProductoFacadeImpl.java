package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Producto;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "ProductoFacade", mappedName = "ProductoFacade")
@Remote(ProductoFacade.class)
public class ProductoFacadeImpl extends AbstractFacade<Producto> implements ProductoFacade {

    @EJB
    private Persistence persistence;

    public ProductoFacadeImpl() {
        super(Producto.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
