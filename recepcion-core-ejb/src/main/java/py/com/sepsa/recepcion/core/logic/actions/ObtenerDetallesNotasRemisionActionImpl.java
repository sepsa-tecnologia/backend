/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.collections.maps.CollectionGroupMap;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.DocumentoDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.NotaRemisionDetalleInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerDetallesNotasRemisionAction", mappedName = "ObtenerDetallesNotasRemisionAction")
@Remote(ObtenerDetallesNotasRemisionAction.class)
public class ObtenerDetallesNotasRemisionActionImpl implements ObtenerDetallesNotasRemisionAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @EJB
    private CompletarObservacionesDocumentoDetalleAction completarObservacionesDocumentoDetalleAction;

    @EJB
    private CompletarProductosEntregablesDocumentoDetalleAction completarProductosEntregablesDocumentoDetalleAction;

    @EJB
    private CompletarMonedasDocumentoDetalleAction completarMonedasDocumentoDetalleAction;

    @Override
    public Result<Collection<NotaRemisionDetalleInfo>> obtenerDetallesNotaRemision(Long idNotaRemision) {
        Result<Collection<NotaRemisionDetalleInfo>> result;

        try {
            Long[] ids = {idNotaRemision};
            Result<Map<Long, Collection<NotaRemisionDetalleInfo>>> resDetalles = obtenerDetallesNotasRemision(ids);
            resDetalles.raise();

            Map<Long, Collection<NotaRemisionDetalleInfo>> detalles = resDetalles.value(Maps.empty());
            Collection<NotaRemisionDetalleInfo> detallesNotaRemision = Maps.get(detalles, idNotaRemision, Lists.empty());

            result = Results.ok()
                    .value(detallesNotaRemision)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo detalles de nota de remisión")
                    .tag("nota_remision.id", idNotaRemision)
                    .build();
        }

        return result;
    }

    @Override
    public Result<Map<Long, Collection<NotaRemisionDetalleInfo>>> obtenerDetallesNotasRemision(Long[] idNotasRemision) {
        Result<Map<Long, Collection<NotaRemisionDetalleInfo>>> result;

        try {
            // Control de seguridad.
            if (Assertions.isNullOrEmpty(idNotasRemision)) {
                return Results.ok()
                        .value(Maps.empty())
                        .build();
            }

            // Criterios de busqueda.
            QueryCriteria criteria = QueryCriteria.select();
            Criterias.from(criteria, buildSubquery(), "tmp");
            Criterias.where(criteria, Conditions.natives().in(NotaRemisionDetalleInfo.COLUMNS.idNotaRemision, idNotasRemision));

            // Obtener detalles de nota de remision.
            String sql = Criterias.toQuery(criteria);
            Collection<NotaRemisionDetalleInfo> detalles = nativeQuerySelectAction.select(sql, new NotaRemisionDetalleInfo.Mapper());

            // Asociar informacion de monedas.
            completarMonedasDocumentoDetalleAction.completar(detalles);

            // Asociar informacion de entrega de productos.
            completarProductosEntregablesDocumentoDetalleAction.completar(detalles);

            // Asociar informacion de observaciones.
            completarObservacionesDocumentoDetalleAction.completar(detalles);

            // Agrupar registros por orden de compra.
            CollectionGroupMap<Long, NotaRemisionDetalleInfo> groups = Maps.groupBy(detalles, "idNotaRemision", Long.class);

            result = Results.ok()
                    .value(groups.getMap())
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo detalles de notas de remisión")
                    .build();
        }

        return result;
    }

    private String buildSubquery() {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, "recepcion.nota_remision_detalle", "nrd");
        Criterias.select(criteria, "nrd.id", DocumentoDetalleInfo.COLUMNS.id);
        Criterias.select(criteria, "nrd.id_nota_remision", NotaRemisionDetalleInfo.COLUMNS.idNotaRemision);
        Criterias.select(criteria, "nrd.id_producto_entrega", NotaRemisionDetalleInfo.COLUMNS.idProductoEntregable);
        Criterias.select(criteria, "nrd.cantidad", NotaRemisionDetalleInfo.COLUMNS.cantidad);
        Criterias.select(criteria, "nrd.precio", NotaRemisionDetalleInfo.COLUMNS.precio);
        Criterias.select(criteria, "nrd.id_moneda", NotaRemisionDetalleInfo.COLUMNS.idMoneda);
        return Criterias.toQuery(criteria);
    }

}
