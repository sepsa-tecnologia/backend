/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.criteria.crud.Obtener;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Arrays;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Estado;
import py.com.sepsa.recepcion.core.database.facades.recepcion.EstadoFacade;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerEstadosAction", mappedName = "ObtenerEstadosAction")
@Remote(ObtenerEstadosAction.class)
public class ObtenerEstadosActionImpl implements ObtenerEstadosAction {

    @EJB
    private EstadoFacade estadoFacade;

    @Override
    public Result<Estado> obtenerEstado(EstadoEnum estadoEnum) {
        Result<Estado> result;

        try {
            // Obtener estados.
            EstadoEnum[] estadosEnum = {estadoEnum};
            Result<Collection<Estado>> resEstados = obtenerEstados(estadosEnum);
            resEstados.raise();

            // Obtener primer resultado.
            Collection<Estado> estados = resEstados.value(Lists.empty());
            Estado estado = Lists.first(estados);

            result = Results.ok()
                    .value(estado)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo estado")
                    .tag("estado.tipo", estadoEnum.descripcion())
                    .build();
        }

        return result;
    }

    private Result<Collection<Estado>> obtenerEstados(EstadoEnum[] estadosEnum) {
        Result<Collection<Estado>> result;

        try {
            // Control de seguridad.
            if (Assertions.isNullOrEmpty(estadosEnum)) {
                return Results.ok()
                        .value(Lists.<EstadoEnum>empty())
                        .build();
            }

            // Obtener codigos de tipo.
            String[] codigos = Arrays.asList(estadosEnum).stream()
                    .map(to -> to.codigo())
                    .toArray(String[]::new);

            // Criterios de busqueda.
            QueryCriteria criteria = QueryCriteria.select();
            Criterias.where(criteria, Conditions.managed().in(Estado.COLUMNS.codigo, codigos));
            result = Obtener.todos(estadoFacade, criteria);
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo estados")
                    .build();
        }

        return result;
    }

}
