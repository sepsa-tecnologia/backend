/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.workers;

import fa.gs.utils.collections.Maps;
import fa.gs.utils.workers.ApplicationWorkerExecutor;
import fa.gs.utils.workers.impl.PeriodicApplicationWorker;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import py.com.sepsa.recepcion.core.logic.Configs;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.configs.SiediConfig;

/**
 *
 * @author Fabio A. González Sosa
 */
public abstract class AbstractIngestWorker extends PeriodicApplicationWorker {

    private Configs config;

    public AbstractIngestWorker(ApplicationWorkerExecutor executor) {
        super(executor);
        this.config = Injection.bean(Configs.class);
    }

    protected SiediConfig getSiediConfig() {
        try {
            return config.getSiediConfig();
        } catch (Throwable thr) {
            return null;
        }
    }

    @Override
    public long getSleepInterval() {
        return TimeUnit.SECONDS.toMillis(30);
    }

    @Override
    public String getWorkerId() {
        return getClass().getCanonicalName();
    }

    @Override
    public Map<String, String> getActiveConfigurations() {
        return Maps.empty();
    }

}
