package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.ProductoMarca;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "ProductoMarcaFacade", mappedName = "ProductoMarcaFacade")
@Remote(ProductoMarcaFacade.class)
public class ProductoMarcaFacadeImpl extends AbstractFacade<ProductoMarca> implements ProductoMarcaFacade {

    @EJB
    private Persistence persistence;

    public ProductoMarcaFacadeImpl() {
        super(ProductoMarca.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
