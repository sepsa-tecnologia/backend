package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Empresa;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "EmpresaFacade", mappedName = "EmpresaFacade")
@Remote(EmpresaFacade.class)
public class EmpresaFacadeImpl extends AbstractFacade<Empresa> implements EmpresaFacade {

    @EJB
    private Persistence persistence;

    public EmpresaFacadeImpl() {
        super(Empresa.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
