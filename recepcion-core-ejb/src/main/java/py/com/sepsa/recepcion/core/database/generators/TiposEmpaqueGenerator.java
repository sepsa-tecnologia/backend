/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.database.generators;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.criteria.crud.Crear;
import fa.gs.utils.database.criteria.crud.Editar;
import fa.gs.utils.database.criteria.crud.Obtener;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.result.simple.Result;
import java.util.Collection;
import java.util.Objects;
import java.util.UUID;
import py.com.sepsa.recepcion.core.data.enums.TipoEmpaqueEnum;
import py.com.sepsa.recepcion.core.database.entities.recepcion.TipoEmpaque;
import py.com.sepsa.recepcion.core.database.facades.recepcion.TipoEmpaqueFacade;
import py.com.sepsa.recepcion.core.logic.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
public class TiposEmpaqueGenerator extends ShardedValuesGenerator<TipoEmpaque, TipoEmpaqueEnum> {

    private TipoEmpaqueFacade facade;

    public TiposEmpaqueGenerator(UUID shard) {
        super(shard);
        this.facade = Injection.bean(TipoEmpaqueFacade.class);
    }

    @Override
    public Collection<TipoEmpaque> generarValores() {
        Collection<TipoEmpaque> valores = Lists.empty();
        for (TipoEmpaqueEnum valorEnum : TipoEmpaqueEnum.values()) {
            TipoEmpaque valor = obtenerOCrearValor(valorEnum);
            if (valor != null) {
                valores.add(valor);
            }
        }
        return valores;
    }

    @Override
    protected Result<TipoEmpaque> obtenerValor(TipoEmpaqueEnum valorEnum, Object... args) {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.where(criteria, TipoEmpaque.COLUMNS.codigo, Operator.EQUALS, valorEnum.codigo());
        return Obtener.primero(facade, criteria);
    }

    @Override
    protected Result<TipoEmpaque> crearValor(TipoEmpaqueEnum valorEnum, Object... args) {
        TipoEmpaque valor = new TipoEmpaque();
        fill(valor, valorEnum);
        return Crear.entity(facade, valor);
    }

    @Override
    protected boolean puedeModificar(TipoEmpaque valorExistente, TipoEmpaqueEnum valorEnum, Object... args) {
        boolean a = Objects.equals(valorExistente.getCodigo(), valorEnum.codigo());
        boolean b = Objects.equals(valorExistente.getDescripcion(), valorEnum.descripcion());
        return a && !b;
    }

    @Override
    protected Result<TipoEmpaque> modificarValor(TipoEmpaque valor, TipoEmpaqueEnum valorEnum, Object... args) {
        fill(valor, valorEnum);
        return Editar.entity(facade, valor);
    }

    private void fill(TipoEmpaque valor, TipoEmpaqueEnum valorEnum) {
        valor.setCodigo(valorEnum.codigo());
        valor.setDescripcion(valorEnum.descripcion());
    }

}
