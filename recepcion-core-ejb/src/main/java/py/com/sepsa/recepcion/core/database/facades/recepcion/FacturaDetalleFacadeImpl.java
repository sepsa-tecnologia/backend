package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.FacturaDetalle;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "FacturaDetalleFacade", mappedName = "FacturaDetalleFacade")
@Remote(FacturaDetalleFacade.class)
public class FacturaDetalleFacadeImpl extends AbstractFacade<FacturaDetalle> implements FacturaDetalleFacade {

    @EJB
    private Persistence persistence;

    public FacturaDetalleFacadeImpl() {
        super(FacturaDetalle.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
