/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.maps.ResultSetMap;
import fa.gs.utils.database.sql.build.SqlClean;
import fa.gs.utils.database.utils.ResultSetMapper;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.hibernate.transform.Transformers;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "NativeQuerySelectAction", mappedName = "NativeQuerySelectAction")
@LocalBean
public class NativeQuerySelectAction {

    @EJB
    private Persistence persistence;

    public Collection<Map<String, Object>> select(String sql) {
        // Limpiar query.
        sql = SqlClean.sanitizeQuery(sql);

        // Ejecutar la query e indicar que necesitamos mapear el resultset a un mapa, valga la redundancia.
        org.hibernate.Query query = persistence.getEntityManager().createNativeQuery(sql)
                .unwrap(org.hibernate.Query.class)
                .setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

        return (Collection<Map<String, Object>>) query.list();
    }

    public <T> Collection<T> select(String sql, ResultSetMapper<T> mapper) {
        // Ejecutar la query y obtener la coleccion de mapas que representan el resultset.
        Collection<Map<String, Object>> maps = select(sql);

        // Mapear cada registro a un objeto.
        Collection<T> objects = new LinkedList<>();
        for (Map<String, Object> map : maps) {
            T obj = mapper.adapt(new ResultSetMap(map));
            if (obj != null) {
                objects.add(obj);
            }
        }
        return objects;
    }

}
