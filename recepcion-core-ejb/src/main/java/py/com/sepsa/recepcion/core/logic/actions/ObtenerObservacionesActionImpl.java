/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.collections.maps.CollectionGroupMap;
import fa.gs.utils.database.criteria.JoinKind;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.database.sql.build.SqlLiterals;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;
import py.com.sepsa.recepcion.core.data.pojos.ObservacionInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerObservacionesAction", mappedName = "ObtenerObservacionesAction")
@Remote(ObtenerObservacionesAction.class)
public class ObtenerObservacionesActionImpl implements ObtenerObservacionesAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @Override
    public Result<Collection<ObservacionInfo>> obtenerObservacion(Long idEntidad, TipoEntidadEnum entidad) {
        Result<Collection<ObservacionInfo>> result;

        try {
            // Obtener observaciones.
            Long[] ids = {idEntidad};
            Result<Map<Long, Collection<ObservacionInfo>>> resObservaciones = obtenerObservaciones(ids, entidad);
            resObservaciones.raise();

            // Retornar primer elemento.
            Map<Long, Collection<ObservacionInfo>> observaciones0 = resObservaciones.value(Maps.empty());
            Collection<ObservacionInfo> observaciones = Maps.get(observaciones0, idEntidad, Lists.empty());

            result = Results.ok()
                    .value(observaciones)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo observaciones")
                    .tag("entidad.id", idEntidad)
                    .tag("entidad.tipo", entidad.codigo())
                    .build();
        }

        return result;
    }

    @Override
    public Result<Map<Long, Collection<ObservacionInfo>>> obtenerObservaciones(Long[] idEntidades, TipoEntidadEnum entidad) {
        Result<Map<Long, Collection<ObservacionInfo>>> result;

        try {
            // Control de seguridad.
            if (Assertions.isNullOrEmpty(idEntidades)) {
                return Results.ok()
                        .value(Maps.<Long, Collection<ObservacionInfo>>empty())
                        .build();
            }

            // Criterios de busqueda.
            QueryCriteria criteria = QueryCriteria.select();
            Criterias.from(criteria, buildSubquery(), "tmp");
            Criterias.where(criteria, Conditions.natives().in(ObservacionInfo.COLUMNS.idEntidad, idEntidades));
            Criterias.where(criteria, ObservacionInfo.COLUMNS.tipoEntidad, Operator.EQUALS, SqlLiterals.string(entidad.codigo()));

            // Obtener registros.
            String query = Criterias.toQuery(criteria);
            Collection<ObservacionInfo> observaciones = nativeQuerySelectAction.select(query, new ObservacionInfo.Mapper());

            // Agrupar observaciones por entidad.
            CollectionGroupMap<Long, ObservacionInfo> groups = Maps.groupBy(observaciones, "idEntidad", Long.class);

            result = Results.ok()
                    .value(groups.getMap())
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo observaciones")
                    .tag("entidad.tipo", entidad.codigo())
                    .build();
        }

        return result;
    }

    private String buildSubquery() {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, "recepcion.observacion", "o");
        Criterias.select(criteria, "o.id", ObservacionInfo.COLUMNS.id);
        Criterias.select(criteria, "o.id_entidad", ObservacionInfo.COLUMNS.idEntidad);
        Criterias.select(criteria, "o.entidad", ObservacionInfo.COLUMNS.tipoEntidad);
        Criterias.select(criteria, "to_.codigo", ObservacionInfo.COLUMNS.tipoObservacionCodigo);
        Criterias.select(criteria, "o.descripcion", ObservacionInfo.COLUMNS.descripcion);
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.tipo_observacion", "to_", "o.id_tipo_observacion", Operator.EQUALS, "to_.id");
        return Criterias.toQuery(criteria);
    }

}
