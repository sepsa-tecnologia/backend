/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic;

import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.OS;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.recepcion.core.logging.AppLoggerFactory;
import py.com.sepsa.recepcion.core.logic.configs.PersistenceConfig;
import py.com.sepsa.recepcion.core.logic.configs.PersistenceConfigFactory;
import py.com.sepsa.recepcion.core.logic.configs.RecepcionConfig;
import py.com.sepsa.recepcion.core.logic.configs.RecepcionConfigFactory;
import py.com.sepsa.recepcion.core.logic.configs.SiediConfig;
import py.com.sepsa.recepcion.core.logic.configs.SiediConfigFactory;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "Configs", mappedName = "Configs")
@Remote(Configs.class)
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@Lock(LockType.READ)
public class ConfigsImpl implements Configs {

    protected final AppLogger log = AppLoggerFactory.core();

    private RecepcionConfig recepcionConfig;

    private SiediConfig siediConfig;

    private PersistenceConfig persistenceConfig;

    @PostConstruct
    public void init() {
        try {
            File configFile = getConfigFile();

            if (!configFile.exists()) {
                throw new IOException("No se encontró un archivo de configuración válido.");
            }

            if (!configFile.canRead()) {
                throw new IOException("No se cuenta con permiso de lectura sobre archivo de configuración.");
            }

            this.recepcionConfig = null;
            this.siediConfig = null;
        } catch (IOException io) {
            log.error(io, "No se pudo inicializar archivo de configuración.");
            throw new IllegalStateException("No se pudo inicializar archivo de configuración", io);
        }
    }

    @Override
    public File getConfigFile() {
        Path path;

        String folderEnv = System.getenv().getOrDefault("FOLDER_PATH", "");
        if (!Assertions.stringNullOrEmpty(folderEnv)) {
            path = Paths.get(folderEnv);
        } else {
            if (OS.isWindows()) {
                path = Paths.get("c:", "home", "sepsa", "recepcion", "config.ini");
            } else {
                path = Paths.get("home", "sepsa", "recepcion", "config.ini");
            }
        }

        return path.toFile();
    }

    @Override
    @Lock(LockType.WRITE)
    public RecepcionConfig getRecepcionConfig() {
        try {
            if (recepcionConfig == null) {
                recepcionConfig = RecepcionConfigFactory.instance();
            }

            return recepcionConfig;
        } catch (Throwable thr) {
            return null;
        }
    }

    @Override
    @Lock(LockType.WRITE)
    public PersistenceConfig getPersistenceConfig() {
        try {
            if (persistenceConfig == null) {
                persistenceConfig = PersistenceConfigFactory.instance();
            }

            return persistenceConfig;
        } catch (Throwable thr) {
            return null;
        }
    }

    @Override
    @Lock(LockType.WRITE)
    public SiediConfig getSiediConfig() {
        try {
            if (siediConfig == null) {
                boolean isStandalone = getRecepcionConfig().isStandaloneApp();
                siediConfig = (isStandalone) ? SiediConfigFactory.instance() : null;
            }

            return siediConfig;
        } catch (Throwable thr) {
            return null;
        }
    }

}
