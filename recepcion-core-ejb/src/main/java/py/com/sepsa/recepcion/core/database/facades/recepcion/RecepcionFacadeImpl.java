package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Recepcion;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "RecepcionFacade", mappedName = "RecepcionFacade")
@Remote(RecepcionFacade.class)
public class RecepcionFacadeImpl extends AbstractFacade<Recepcion> implements RecepcionFacade {

    @EJB
    private Persistence persistence;

    public RecepcionFacadeImpl() {
        super(Recepcion.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
