/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.database.sql.build.SqlLiterals;
import fa.gs.utils.misc.text.StringBuilder2;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "AgendarFechaEntregaAction", mappedName = "AgendarFechaEntregaAction")
@Remote(AgendarFechaEntregaAction.class)
public class AgendarFechaEntregaActionImpl implements AgendarFechaEntregaAction {

    @EJB
    private NativeQueryUpdateAction nativeQueryUpdateAction;

    @Override
    public Result<Date> agendarFechaEntrega(Long idProductoEntregable, Date fecha) {
        Result<Date> result;

        try {
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" UPDATE ");
            builder.append(" recepcion.producto_entrega ");
            builder.append(" SET ");
            builder.append(" fecha_entrega_agendada = %s ", SqlLiterals.fecha(fecha));
            builder.append(" WHERE ");
            builder.append(" id = %s ", idProductoEntregable);
            builder.append(" ;");

            String sql = builder.toString();
            nativeQueryUpdateAction.executeUpdate(sql);

            result = Results.ok()
                    .value(fecha)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrio un error estableciendo fecha agendada de entrega")
                    .tag("producto_entregable.id", idProductoEntregable)
                    .build();
        }

        return result;
    }

}
