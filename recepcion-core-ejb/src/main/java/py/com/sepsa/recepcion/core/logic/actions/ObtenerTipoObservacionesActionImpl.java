/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.criteria.crud.Obtener;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Arrays;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import py.com.sepsa.recepcion.core.data.enums.TipoObservacionEnum;
import py.com.sepsa.recepcion.core.database.entities.recepcion.TipoObservacion;
import py.com.sepsa.recepcion.core.database.facades.recepcion.TipoObservacionFacade;

/**
 *
 * @author Fabio A. González Sosa
 */
@Singleton(name = "ObtenerTipoObservacionesAction", mappedName = "ObtenerTipoObservacionesAction")
@Remote(ObtenerTipoObservacionesAction.class)
public class ObtenerTipoObservacionesActionImpl implements ObtenerTipoObservacionesAction {

    @EJB
    private TipoObservacionFacade tipoObservacionFacade;

    @Override
    public Result<TipoObservacion> obtenerTipoObservacion(TipoObservacionEnum tipoObservacionEnum) {
        Result<TipoObservacion> result;

        try {
            // Obtener tipos de observacion.
            TipoObservacionEnum[] tipos = {tipoObservacionEnum};
            Result<Collection<TipoObservacion>> resTiposObservacion = obtenerTiposObservaciones(tipos);
            resTiposObservacion.raise();

            // Obtener primer resultado.
            Collection<TipoObservacion> tiposObservaciones = resTiposObservacion.value(Lists.empty());
            TipoObservacion tipoObservacion = Lists.first(tiposObservaciones);

            result = Results.ok()
                    .value(tipoObservacion)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo tipo de observación")
                    .tag("observacion.tipo", tipoObservacionEnum.descripcion())
                    .build();
        }

        return result;
    }

    @Override
    public Result<Collection<TipoObservacion>> obtenerTiposObservaciones(TipoObservacionEnum[] tipoObservacionesEnum) {
        Result<Collection<TipoObservacion>> result;

        try {
            // Control de seguridad.
            if (Assertions.isNullOrEmpty(tipoObservacionesEnum)) {
                return Results.ok()
                        .value(Lists.<TipoObservacion>empty())
                        .build();
            }

            // Obtener codigos de tipo.
            String[] codigos = Arrays.asList(tipoObservacionesEnum).stream()
                    .map(to -> to.codigo())
                    .toArray(String[]::new);

            // Criterios de busqueda.
            QueryCriteria criteria = QueryCriteria.select();
            Criterias.where(criteria, Conditions.managed().in(TipoObservacion.COLUMNS.codigo, codigos));
            result = Obtener.todos(tipoObservacionFacade, criteria);
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo tipos de observación")
                    .build();
        }

        return result;
    }

}
