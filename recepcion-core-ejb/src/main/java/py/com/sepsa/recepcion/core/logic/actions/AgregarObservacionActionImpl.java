/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.database.criteria.crud.Crear;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;
import py.com.sepsa.recepcion.core.data.enums.TipoObservacionEnum;
import py.com.sepsa.recepcion.core.data.pojos.ObservacionInfo;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Observacion;
import py.com.sepsa.recepcion.core.database.entities.recepcion.TipoObservacion;
import py.com.sepsa.recepcion.core.database.facades.recepcion.ObservacionFacade;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "AgregarObservacionAction", mappedName = "AgregarObservacionAction")
@Remote(AgregarObservacionAction.class)
public class AgregarObservacionActionImpl implements AgregarObservacionAction {

    @EJB
    private ObservacionFacade observacionFacade;

    @EJB
    private ObtenerTipoObservacionesAction obtenerTipoObservacionesAction;

    @Override
    public Result<ObservacionInfo> agregarObservacion(Long idEntidad, TipoEntidadEnum tipoEntidad, TipoObservacionEnum tipoObservacionEnum, String descripcion) {
        Result<ObservacionInfo> result;

        try {
            // Obtener tipo de observacion.
            Result<TipoObservacion> resTipoObservacion = obtenerTipoObservacionesAction.obtenerTipoObservacion(tipoObservacionEnum);
            resTipoObservacion.raise(true);
            TipoObservacion tipoObservacion = resTipoObservacion.value();

            // Inicializar entidad.
            Observacion observacion = new Observacion();
            observacion.setIdEntidad(idEntidad);
            observacion.setEntidad(tipoEntidad.codigo());
            observacion.setIdTipoObservacion(tipoObservacion.getId());
            observacion.setDescripcion(descripcion);

            // Crear entidad.
            Result<Observacion> resObservacion = Crear.entity(observacionFacade, observacion);
            resObservacion.raise(true);
            observacion = resObservacion.value();

            // Adaptar observacion.
            ObservacionInfo info = new ObservacionInfo();
            info.setId(observacion.getId());
            info.setIdEntidad(observacion.getIdEntidad());
            info.setTipoEntidad(observacion.getEntidad());
            info.setTipoObservacion(tipoObservacionEnum);
            info.setDescripcion(observacion.getDescripcion());
            info.setFechaRegistro(observacion.getFechaRegistro());

            result = Results.ok()
                    .value(info)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error agregando observación")
                    .tag("entidad.id", idEntidad)
                    .tag("entidad.tipo", tipoEntidad.codigo())
                    .build();
        }

        return result;
    }

}
