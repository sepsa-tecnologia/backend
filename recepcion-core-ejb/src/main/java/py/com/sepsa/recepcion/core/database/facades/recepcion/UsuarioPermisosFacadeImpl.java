package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.UsuarioPermisos;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "UsuarioPermisosFacade", mappedName = "UsuarioPermisosFacade")
@Remote(UsuarioPermisosFacade.class)
public class UsuarioPermisosFacadeImpl extends AbstractFacade<UsuarioPermisos> implements UsuarioPermisosFacade {

    @EJB
    private Persistence persistence;

    public UsuarioPermisosFacadeImpl() {
        super(UsuarioPermisos.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
