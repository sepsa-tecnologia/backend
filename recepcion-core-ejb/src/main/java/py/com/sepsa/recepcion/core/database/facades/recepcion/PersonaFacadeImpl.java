package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Persona;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "PersonaFacade", mappedName = "PersonaFacade")
@Remote(PersonaFacade.class)
public class PersonaFacadeImpl extends AbstractFacade<Persona> implements PersonaFacade {

    @EJB
    private Persistence persistence;

    public PersonaFacadeImpl() {
        super(Persona.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
