package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.NotaRemisionDetalle;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "NotaRemisionDetalleFacade", mappedName = "NotaRemisionDetalleFacade")
@Remote(NotaRemisionDetalleFacade.class)
public class NotaRemisionDetalleFacadeImpl extends AbstractFacade<NotaRemisionDetalle> implements NotaRemisionDetalleFacade {

    @EJB
    private Persistence persistence;

    public NotaRemisionDetalleFacadeImpl() {
        super(NotaRemisionDetalle.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
