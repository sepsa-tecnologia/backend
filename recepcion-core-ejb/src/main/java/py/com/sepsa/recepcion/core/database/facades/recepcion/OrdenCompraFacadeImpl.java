package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.OrdenCompra;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "OrdenCompraFacade", mappedName = "OrdenCompraFacade")
@Remote(OrdenCompraFacade.class)
public class OrdenCompraFacadeImpl extends AbstractFacade<OrdenCompra> implements OrdenCompraFacade {

    @EJB
    private Persistence persistence;

    public OrdenCompraFacadeImpl() {
        super(OrdenCompra.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
