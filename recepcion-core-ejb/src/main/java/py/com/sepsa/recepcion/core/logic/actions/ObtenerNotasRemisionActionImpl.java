/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.database.criteria.Condition;
import fa.gs.utils.database.criteria.JoinKind;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.sql.build.Builders;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.database.sql.build.SqlLiterals;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.NotaRemisionDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.NotaRemisionInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerNotasRemisionAction", mappedName = "ObtenerNotasRemisionAction")
@Remote(ObtenerNotasRemisionAction.class)
public class ObtenerNotasRemisionActionImpl implements ObtenerNotasRemisionAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @EJB
    private ObtenerDetallesNotasRemisionAction obtenerNotasRemisionDetallesAction;

    @Override
    public Result<NotaRemisionInfo> obtenerNotaRemision(Long idNotaRemision) {
        Result<NotaRemisionInfo> result;

        try {
            // Obtener registros.
            Collection<NotaRemisionInfo> notasRemision = obtenerNotasRemision(new Condition[]{
                Builders.condition().left(NotaRemisionInfo.COLUMNS.nr_id).op(Operator.EQUALS).right(idNotaRemision).build()
            });

            // Retornar primer elemento.
            NotaRemisionInfo notaRemision = Lists.first(notasRemision);

            result = Results.ok()
                    .value(notaRemision)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo nota de remisión")
                    .tag("nota_remision.id", idNotaRemision)
                    .build();
        }

        return result;
    }

    @Override
    public Result<Collection<NotaRemisionInfo>> obtenerNotasRemision(Long[] idNotasRemision) {
        Result<Collection<NotaRemisionInfo>> result;

        try {
            // Control de seguridad.
            if (Assertions.isNullOrEmpty(idNotasRemision)) {
                return Results.ok()
                        .value(Lists.<NotaRemisionInfo>empty())
                        .build();
            }

            // Obtener registros.
            Collection<NotaRemisionInfo> remisiones = obtenerNotasRemision(new Condition[]{
                Conditions.natives().in(NotaRemisionInfo.COLUMNS.nr_id, idNotasRemision)
            });

            // Asociar informacion de detalles de notas de remision.
            completarDetallesNotaRemision(remisiones);

            result = Results.ok()
                    .value(remisiones)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo órdenes de compra")
                    .build();
        }

        return result;
    }

    @Override
    public Result<Collection<NotaRemisionInfo>> obtenerNotasRemision(String razonSocialORuc, String nroDocumento) {
        Result<Collection<NotaRemisionInfo>> result;

        try {
            // Obtener registros.
            Collection<NotaRemisionInfo> facturas = obtenerNotasRemision(new Condition[]{
                Builders.condition().left(NotaRemisionInfo.COLUMNS.nr_nro_documento).op(Operator.EQUALS).right(SqlLiterals.string(nroDocumento)).build(),
                Conditions.any(
                Builders.condition().left(NotaRemisionInfo.COLUMNS.oc_ruc_proveedor).op(Operator.LIKE).right(SqlLiterals.like(razonSocialORuc)).build(),
                Builders.condition().left(NotaRemisionInfo.COLUMNS.oc_razon_social_proveedor).op(Operator.LIKE).right(SqlLiterals.like(razonSocialORuc)).build()
                )
            });

            result = Results.ok()
                    .value(facturas)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo órdenes de compra")
                    .build();
        }

        return result;
    }

    private Collection<NotaRemisionInfo> obtenerNotasRemision(Condition[] conditions) throws Throwable {
        // Criterio de busqueda.
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, buildSubquery(conditions), "tmp");

        // Obtener registros.
        String sql = Criterias.toQuery(criteria);
        Collection<NotaRemisionInfo> notasRemision = nativeQuerySelectAction.select(sql, NotaRemisionInfo.MAPPER.INSTANCE);

        // Asociar informacion de detalles de notas de remision.
        completarDetallesNotaRemision(notasRemision);

        return notasRemision;
    }

    private static String buildSubquery(Condition[] conditions) {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, "recepcion.nota_remision", "nr");
        Criterias.select(criteria, NotaRemisionInfo.COLUMNS.nr_id, NotaRemisionInfo.MAPPINGS.id);
        Criterias.select(criteria, NotaRemisionInfo.COLUMNS.e_estado_codigo, NotaRemisionInfo.MAPPINGS.estadoCodigo);
        Criterias.select(criteria, NotaRemisionInfo.COLUMNS.nr_id_documento, NotaRemisionInfo.MAPPINGS.idDocumento);
        Criterias.select(criteria, NotaRemisionInfo.COLUMNS.nr_nro_documento, NotaRemisionInfo.MAPPINGS.nroDocumento);
        Criterias.select(criteria, NotaRemisionInfo.COLUMNS.nr_id_orden_compra, NotaRemisionInfo.MAPPINGS.idOrdenCompra);
        Criterias.select(criteria, NotaRemisionInfo.COLUMNS.oc_ruc_proveedor, NotaRemisionInfo.MAPPINGS.rucProveedor);
        Criterias.select(criteria, NotaRemisionInfo.COLUMNS.oc_razon_social_proveedor, NotaRemisionInfo.MAPPINGS.razonSocialProveedor);
        Criterias.select(criteria, NotaRemisionInfo.COLUMNS.nr_fecha_registro, NotaRemisionInfo.MAPPINGS.fechaRegistro);
        Criterias.select(criteria, NotaRemisionInfo.COLUMNS.nr_fecha_emision, NotaRemisionInfo.MAPPINGS.fechaEmision);
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.orden_compra", "oc", "nr.id_orden_compra", Operator.EQUALS, "oc.id");
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.estado", "e", "nr.id_estado", Operator.EQUALS, "e.id");
        Criterias.where(criteria, conditions);
        return Criterias.toQuery(criteria);
    }

    private void completarDetallesNotaRemision(Collection<NotaRemisionInfo> remisiones) throws Throwable {
        // Obtener identificadores de notas de remision.
        Long[] idNotasRemision = remisiones.stream()
                .map(o -> o.getId())
                .distinct()
                .toArray(Long[]::new);

        // Obtener detalles disponibles.
        Result<Map<Long, Collection<NotaRemisionDetalleInfo>>> resDetalles = obtenerNotasRemisionDetallesAction.obtenerDetallesNotasRemision(idNotasRemision);
        resDetalles.raise();
        Map<Long, Collection<NotaRemisionDetalleInfo>> detalles = resDetalles.value(Maps.empty());

        // Asociar detalles con remisiones.
        for (NotaRemisionInfo remision : remisiones) {
            Collection<NotaRemisionDetalleInfo> detalleRemision = Maps.get(detalles, remision.getId());
            remision.setDetalles(detalleRemision);
        }
    }

}
