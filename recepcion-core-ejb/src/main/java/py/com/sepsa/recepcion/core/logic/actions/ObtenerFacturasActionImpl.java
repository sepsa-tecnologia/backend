/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.database.criteria.Condition;
import fa.gs.utils.database.criteria.JoinKind;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.sql.build.Builders;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.database.sql.build.SqlLiterals;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.FacturaDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.FacturaInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerFacturasAction", mappedName = "ObtenerFacturasAction")
@Remote(ObtenerFacturasAction.class)
public class ObtenerFacturasActionImpl implements ObtenerFacturasAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @EJB
    private ObtenerDetallesFacturasAction obtenerFacturaDetallesAction;

    @Override
    public Result<FacturaInfo> obtenerFactura(String nroDocumento, String glnComprador, String glnProveedor) {
        Result<FacturaInfo> result;

        try {
            // Obtener registros.
            Collection<FacturaInfo> ordenes = obtenerFacturas(new Condition[]{
                Builders.condition().left(FacturaInfo.COLUMNS.f_nro_documento).op(Operator.EQUALS).right(SqlLiterals.string(nroDocumento)).build(),
                Builders.condition().left(FacturaInfo.COLUMNS.oc_gln_comprador).op(Operator.EQUALS).right(SqlLiterals.string(glnComprador)).build(),
                Builders.condition().left(FacturaInfo.COLUMNS.oc_gln_proveedor).op(Operator.EQUALS).right(SqlLiterals.string(glnProveedor)).build()
            });

            // Retornar primer elemento.
            FacturaInfo orden = Lists.first(ordenes);

            result = Results.ok()
                    .value(orden)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo factura")
                    .tag("factura.nro", nroDocumento)
                    .tag("factura.glnOrigen", glnProveedor)
                    .tag("factura.glnDestino", glnComprador)
                    .build();
        }

        return result;
    }

    @Override
    public Result<FacturaInfo> obtenerFactura(Long idFactura) {
        Result<FacturaInfo> result;

        try {
            // Obtener registros.
            Collection<FacturaInfo> facturas = obtenerFacturas(new Condition[]{
                Builders.condition().left(FacturaInfo.COLUMNS.f_id).op(Operator.EQUALS).right(idFactura).build()
            });

            // Retornar primer elemento.
            FacturaInfo factura = Lists.first(facturas);

            result = Results.ok()
                    .value(factura)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo factura")
                    .tag("factura.id", idFactura)
                    .build();
        }

        return result;
    }

    @Override
    public Result<Collection<FacturaInfo>> obtenerFacturas(Long[] idFacturas) {
        Result<Collection<FacturaInfo>> result;

        try {
            // Control de seguridad.
            if (Assertions.isNullOrEmpty(idFacturas)) {
                return Results.ok()
                        .value(Lists.<FacturaInfo>empty())
                        .build();
            }

            // Obtener registros.
            Collection<FacturaInfo> facturas = obtenerFacturas(new Condition[]{
                Conditions.natives().in(FacturaInfo.COLUMNS.f_id, idFacturas)
            });

            result = Results.ok()
                    .value(facturas)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo órdenes de compra")
                    .build();
        }

        return result;
    }

    @Override
    public Result<Collection<FacturaInfo>> obtenerFacturas(String razonSocialORuc, String nroDocumento) {
        Result<Collection<FacturaInfo>> result;

        try {
            // Obtener registros.
            Collection<FacturaInfo> facturas = obtenerFacturas(new Condition[]{
                Builders.condition().left(FacturaInfo.COLUMNS.f_nro_documento).op(Operator.EQUALS).right(SqlLiterals.string(nroDocumento)).build(),
                Conditions.any(
                Builders.condition().left(FacturaInfo.COLUMNS.oc_ruc_proveedor).op(Operator.LIKE).right(SqlLiterals.like(razonSocialORuc)).build(),
                Builders.condition().left(FacturaInfo.COLUMNS.oc_razon_social_proveedor).op(Operator.LIKE).right(SqlLiterals.like(razonSocialORuc)).build()
                )
            });

            result = Results.ok()
                    .value(facturas)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo órdenes de compra")
                    .build();
        }

        return result;
    }

    private Collection<FacturaInfo> obtenerFacturas(Condition[] conditions) throws Throwable {
        // Criterio de busqueda.
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, buildSubquery(conditions), "tmp");

        // Obtener registros.
        String sql = Criterias.toQuery(criteria);
        Collection<FacturaInfo> facturas = nativeQuerySelectAction.select(sql, FacturaInfo.MAPPER.INSTANCE);

        // Asociar informacion de detalles de notas de remision.
        completarDetallesFactura(facturas);
        return facturas;
    }

    private static String buildSubquery(Condition[] conditions) {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, "recepcion.factura", "f");
        Criterias.select(criteria, FacturaInfo.COLUMNS.f_id, FacturaInfo.MAPPINGS.id);
        Criterias.select(criteria, FacturaInfo.COLUMNS.e_estado_codigo, FacturaInfo.MAPPINGS.estadoCodigo);
        Criterias.select(criteria, FacturaInfo.COLUMNS.f_nro_timbrado, FacturaInfo.MAPPINGS.nroTimbrado);
        Criterias.select(criteria, FacturaInfo.COLUMNS.f_vto_timbrado, FacturaInfo.MAPPINGS.vtoTimbrado);
        Criterias.select(criteria, FacturaInfo.COLUMNS.f_id_documento, FacturaInfo.MAPPINGS.idDocumento);
        Criterias.select(criteria, FacturaInfo.COLUMNS.f_nro_documento, FacturaInfo.MAPPINGS.nroDocumento);
        Criterias.select(criteria, FacturaInfo.COLUMNS.f_id_orden_compra, FacturaInfo.MAPPINGS.idOrdenCompra);
        Criterias.select(criteria, FacturaInfo.COLUMNS.oc_ruc_proveedor, FacturaInfo.MAPPINGS.rucProveedor);
        Criterias.select(criteria, FacturaInfo.COLUMNS.oc_razon_social_proveedor, FacturaInfo.MAPPINGS.razonSocialProveedor);
        Criterias.select(criteria, FacturaInfo.COLUMNS.f_fecha_registro, FacturaInfo.MAPPINGS.fechaRegistro);
        Criterias.select(criteria, FacturaInfo.COLUMNS.f_fecha_emision, FacturaInfo.MAPPINGS.fechaEmision);
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.orden_compra", "oc", "f.id_orden_compra", Operator.EQUALS, "oc.id");
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.estado", "e", "f.id_estado", Operator.EQUALS, "e.id");
        Criterias.where(criteria, conditions);
        return Criterias.toQuery(criteria);
    }

    private void completarDetallesFactura(Collection<FacturaInfo> remisiones) throws Throwable {
        // Obtener identificadores de notas de remision.
        Long[] idFacturas = remisiones.stream()
                .map(o -> o.getId())
                .distinct()
                .toArray(Long[]::new);

        // Obtener detalles disponibles.
        Result<Map<Long, Collection<FacturaDetalleInfo>>> resDetalles = obtenerFacturaDetallesAction.obtenerDetallesFacturas(idFacturas);
        resDetalles.raise();
        Map<Long, Collection<FacturaDetalleInfo>> detalles = resDetalles.value(Maps.empty());

        // Asociar detalles con remisiones.
        for (FacturaInfo remision : remisiones) {
            Collection<FacturaDetalleInfo> detalleRemision = Maps.get(detalles, remision.getId());
            remision.setDetalles(detalleRemision);
        }
    }

}
