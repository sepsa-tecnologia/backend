/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.criteria.Condition;
import fa.gs.utils.database.criteria.JoinKind;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.sql.build.Builders;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.database.sql.build.SqlLiterals;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.ProductoInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerProductosAction", mappedName = "ObtenerProductosAction")
@Remote(ObtenerProductosAction.class)
public class ObtenerProductosActionImpl implements ObtenerProductosAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @Override
    public Result<ProductoInfo> obtenerProducto(String codigoGtinOInterno) {
        Result<ProductoInfo> result;

        try {
            // Obtener registros.
            Collection<ProductoInfo> productos = obtenerProductos(new Condition[]{
                Conditions.any(
                Builders.condition().left(ProductoInfo.COLUMNS.codigoGtin).op(Operator.EQUALS).right(SqlLiterals.string(codigoGtinOInterno)).build(),
                Builders.condition().left(ProductoInfo.COLUMNS.codigoInterno).op(Operator.EQUALS).right(SqlLiterals.string(codigoGtinOInterno)).build()
                )
            });

            // Retornar primer elemento.
            ProductoInfo producto = Lists.first(productos);

            result = Results.ok()
                    .value(producto)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo información de producto")
                    .tag("producto.codigoGtinOInterno", codigoGtinOInterno)
                    .build();
        }

        return result;
    }

    @Override
    public Result<ProductoInfo> obtenerProducto(Long idProducto) {
        Result<ProductoInfo> result;

        try {
            // Obtener registros.
            Collection<ProductoInfo> productos = obtenerProductos(new Condition[]{
                Builders.condition().left(ProductoInfo.COLUMNS.id).op(Operator.EQUALS).right(idProducto).build()
            });

            // Retornar primer elemento.
            ProductoInfo producto = Lists.first(productos);

            result = Results.ok()
                    .value(producto)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo información de producto")
                    .tag("producto.id", idProducto)
                    .build();
        }

        return result;
    }

    @Override
    public Result<Collection<ProductoInfo>> obtenerProductos(Long[] idProductos) {
        Result<Collection<ProductoInfo>> result;

        try {
            // Control de seguridad.
            if (Assertions.isNullOrEmpty(idProductos)) {
                return Results.ok()
                        .value(Lists.<ProductoInfo>empty())
                        .build();
            }

            // Obtener registros.
            Collection<ProductoInfo> productos = obtenerProductos(new Condition[]{
                Conditions.natives().in(ProductoInfo.COLUMNS.id, idProductos)
            });

            result = Results.ok()
                    .value(productos)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo información de productos")
                    .build();
        }

        return result;
    }

    private Collection<ProductoInfo> obtenerProductos(Condition[] conditions) throws Throwable {
        // Criterios de busqueda.
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, buildSubquery(), "tmp");
        Criterias.where(criteria, conditions);

        // Obtener registros.
        String query = Criterias.toQuery(criteria);
        Collection<ProductoInfo> productos = nativeQuerySelectAction.select(query, new ProductoInfo.Mapper());
        return productos;
    }

    private String buildSubquery() {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.select(criteria, "p.id", ProductoInfo.COLUMNS.id);
        Criterias.select(criteria, "p.descripcion_corta", ProductoInfo.COLUMNS.descripcionCorta);
        Criterias.select(criteria, "p.descripcion_larga", ProductoInfo.COLUMNS.descripcionLarga);
        Criterias.select(criteria, "p.codigo_gtin", ProductoInfo.COLUMNS.codigoGtin);
        Criterias.select(criteria, "p.codigo_interno", ProductoInfo.COLUMNS.codigoInterno);
        Criterias.select(criteria, "pm.descripcion", ProductoInfo.COLUMNS.marca);
        Criterias.select(criteria, "pp.pais", ProductoInfo.COLUMNS.paisProcedencia);
        Criterias.from(criteria, "recepcion.producto", "p");
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.producto_marca", "pm", "p.id_marca", Operator.EQUALS, "pm.id");
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.producto_procedencia", "pp", "p.id_procedencia", Operator.EQUALS, "pp.id");
        return Criterias.toQuery(criteria);
    }

}
