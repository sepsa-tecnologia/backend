package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.ProductoProcedencia;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "ProductoProcedenciaFacade", mappedName = "ProductoProcedenciaFacade")
@Remote(ProductoProcedenciaFacade.class)
public class ProductoProcedenciaFacadeImpl extends AbstractFacade<ProductoProcedencia> implements ProductoProcedenciaFacade {

    @EJB
    private Persistence persistence;

    public ProductoProcedenciaFacadeImpl() {
        super(ProductoProcedencia.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
