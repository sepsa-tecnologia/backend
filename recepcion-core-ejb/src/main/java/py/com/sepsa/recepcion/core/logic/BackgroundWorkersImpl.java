/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.workers.ApplicationWorker;
import fa.gs.utils.workers.ApplicationWorkerExecutor;
import fa.gs.utils.workers.ApplicationWorkerExecutor.Status;
import fa.gs.utils.workers.impl.AbstractCachedWorkersExecutor;
import java.util.Collection;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.DependsOn;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.recepcion.core.logging.AppLoggerFactory;
import py.com.sepsa.recepcion.core.logic.workers.IngestFacturasWorker;
import py.com.sepsa.recepcion.core.logic.workers.IngestOrdenesWorker;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@DependsOn({"Configs", "DatabaseValuesGenerator"})
@Singleton(name = "BackgroundWorkers", mappedName = "BackgroundWorkers")
@Remote(BackgroundWorkers.class)
public class BackgroundWorkersImpl implements BackgroundWorkers {

    ApplicationWorkerExecutor executor;

    @PostConstruct
    public void init() {
        this.executor = new BackgroundWorkersImpl.Executor();
        this.executor.start();
    }

    @PreDestroy
    public void finish() {
        this.executor.stop();
    }

    @Override
    public boolean start() {
        try {
            executor.start();
            return (executor.status() == Status.INICIADO);
        } catch (Throwable thr) {
            return false;
        }
    }

    @Override
    public boolean stop() {
        try {
            executor.stop();
            return (executor.status() == Status.TERMINADO);
        } catch (Throwable thr) {
            return false;
        }
    }

    private final class Executor extends AbstractCachedWorkersExecutor {

        @Override
        public AppLogger getLogger() {
            return AppLoggerFactory.workers();
        }

        @Override
        public Collection<Class<? extends ApplicationWorker>> populateWorkers() {
            Collection<Class<? extends ApplicationWorker>> workers = Lists.empty();
            workers.add(IngestFacturasWorker.class);
            workers.add(IngestOrdenesWorker.class);
            return workers;
        }

    }
}
