/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.database.persistence;

import fa.gs.utils.misc.Assertions;
import java.io.Serializable;
import java.util.Collections;
import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.spi.PersistenceUnitInfo;
import org.hibernate.jpa.HibernatePersistenceProvider;
import py.com.sepsa.recepcion.core.logic.Configs;
import py.com.sepsa.recepcion.core.logic.configs.PersistenceConfig;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@DependsOn("Configs")
@Singleton(name = "Persistence", mappedName = "Persistence")
@LocalBean
public class Persistence implements Serializable {

    @EJB
    private Configs configs;

    private EntityManagerFactory entityManagerFactory;

    @PostConstruct
    public void init() {
        PersistenceUnitInfo persistenceUnitInfo = PersistenceUnit.instance(getDatasource());
        HibernatePersistenceProvider persistenceProvider = new HibernatePersistenceProvider();
        entityManagerFactory = persistenceProvider.createContainerEntityManagerFactory(persistenceUnitInfo, Collections.EMPTY_MAP);
    }

    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    private String getDatasource() {
        PersistenceConfig config = configs.getPersistenceConfig();
        if (config != null) {
            String datasource = config.getDatasourceName();
            if (Assertions.stringNullOrEmpty(datasource)) {
                return PersistenceUnit.DEFAULT_DATASOURCE_NAME;
            } else {
                return datasource;
            }
        } else {
            return PersistenceUnit.DEFAULT_DATASOURCE_NAME;
        }
    }

}
