/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.result.simple.Result;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;
import py.com.sepsa.recepcion.core.data.pojos.DocumentoConObservaciones;
import py.com.sepsa.recepcion.core.data.pojos.DocumentoInfo;
import py.com.sepsa.recepcion.core.data.pojos.ObservacionInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "CompletarObservacionesDocumentoDetalleAction", mappedName = "CompletarObservacionesDocumentoDetalleAction")
@LocalBean
public class CompletarObservacionesDocumentoDetalleAction {

    @EJB
    private ObtenerObservacionesAction obtenerObservacionesAction;

    public <T extends DocumentoInfo & DocumentoConObservaciones> void completar(Collection<T> detalles) throws Throwable {
        // Obtener identificadores de detalles.
        Long[] idDocumentos = detalles.stream()
                .map(d -> d.getId())
                .distinct()
                .toArray(Long[]::new);

        // Tipo de documento.
        Set<TipoEntidadEnum> tipoDocumentos = detalles.stream()
                .map(d -> d.getTipoDocumento())
                .collect(Collectors.toSet());

        // Obtener observaciones disponibles.
        Result<Map<Long, Collection<ObservacionInfo>>> resObservaciones = obtenerObservacionesAction.obtenerObservaciones(idDocumentos, Lists.first(tipoDocumentos));
        resObservaciones.raise();
        Map<Long, Collection<ObservacionInfo>> observaciones = resObservaciones.value(Maps.empty());

        // Asociar observaciones con detalle correspondiente.
        for (T detalle : detalles) {
            Collection<ObservacionInfo> observaciones0 = Maps.get(observaciones, detalle.getId());
            detalle.setObservaciones(observaciones0);
        }
    }

}
