/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.criteria.crud.Obtener;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.misc.text.StringBuilder2;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Estado;
import py.com.sepsa.recepcion.core.database.facades.recepcion.EstadoFacade;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "CambiarEstadoAction", mappedName = "CambiarEstadoAction")
@Remote(CambiarEstadoAction.class)
public class CambiarEstadoActionImpl implements CambiarEstadoAction {

    @EJB
    private EstadoFacade estadoFacade;

    @EJB
    private NativeQueryUpdateAction nativeQueryUpdateAction;

    @Override
    public Result<Void> cambiarEstadoNotaRemision(Long idNotaRemision, EstadoEnum estadoEnum) {
        Result<Void> result;

        try {
            // Cambiar estado.
            Estado estado = obtenerEstado(estadoEnum);
            cambiarEstado("nota_remision", idNotaRemision, estado.getId());

            result = Results.ok()
                    .nullable(true)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error cambiando estado de nota de remisión")
                    .tag("nota_remision.id", idNotaRemision)
                    .build();
        }

        return result;
    }

    @Override
    public Result<Void> cambiarEstadoFactura(Long idFactura, EstadoEnum estadoEnum) {
        Result<Void> result;

        try {
            // Cambiar estado.
            Estado estado = obtenerEstado(estadoEnum);
            cambiarEstado("factura", idFactura, estado.getId());

            result = Results.ok()
                    .nullable(true)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error cambiando estado de factura")
                    .tag("factura.id", idFactura)
                    .build();
        }

        return result;
    }

    private void cambiarEstado(String table, Long idRegistro, Long idEstado) {
        // Crear query.
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" UPDATE ");
        builder.append(" recepcion.%s ", table);
        builder.append(" SET id_estado = %s ", idEstado);
        builder.append(" WHERE id = %s ", idRegistro);
        builder.append(" ;");

        String sql = builder.toString();
        nativeQueryUpdateAction.executeUpdate(sql);
    }

    private Estado obtenerEstado(EstadoEnum estadoEnum) throws Throwable {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.where(criteria, Estado.COLUMNS.codigo, Operator.EQUALS, estadoEnum.codigo());
        Result<Estado> resEstado = Obtener.primero(estadoFacade, criteria);
        resEstado.raise(true);
        return resEstado.value();
    }

}
