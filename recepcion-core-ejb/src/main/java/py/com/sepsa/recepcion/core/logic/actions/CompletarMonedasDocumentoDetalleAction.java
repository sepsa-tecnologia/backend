/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.DocumentoDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.MonedaInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "CompletarMonedasDocumentoDetalleAction", mappedName = "CompletarMonedasDocumentoDetalleAction")
@LocalBean
public class CompletarMonedasDocumentoDetalleAction {

    @EJB
    private ObtenerMonedasAction obtenerMonedasAction;

    public <T extends DocumentoDetalleInfo> void completar(Collection<T> detalles) throws Throwable {
        // Obtener identificadores de monedas.
        Long[] idMonedas = detalles.stream()
                .filter(d -> d.getMoneda() != null)
                .map(d -> d.getMoneda().getId())
                .distinct()
                .toArray(Long[]::new);

        // Obtener monedas disponibles.
        Result<Collection<MonedaInfo>> resMonedas = obtenerMonedasAction.obtenerMonedas(idMonedas);
        resMonedas.raise();
        Collection<MonedaInfo> monedas = resMonedas.value(Lists.empty());

        // Asociar monedas con detalle correspondiente.
        for (T detalle : detalles) {
            Long idMoneda = detalle.getMoneda().getId();
            for (MonedaInfo moneda : monedas) {
                if (Assertions.equals(idMoneda, moneda.getId())) {
                    detalle.setMoneda(moneda);
                    break;
                }
            }
        }
    }

}
