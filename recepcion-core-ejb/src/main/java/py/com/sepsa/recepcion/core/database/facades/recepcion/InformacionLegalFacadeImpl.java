package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.InformacionLegal;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "InformacionLegalFacade", mappedName = "InformacionLegalFacade")
@Remote(InformacionLegalFacade.class)
public class InformacionLegalFacadeImpl extends AbstractFacade<InformacionLegal> implements InformacionLegalFacade {

    @EJB
    private Persistence persistence;

    public InformacionLegalFacadeImpl() {
        super(InformacionLegal.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
