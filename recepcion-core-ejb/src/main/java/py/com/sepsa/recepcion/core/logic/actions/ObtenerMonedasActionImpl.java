/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.criteria.Condition;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.sql.build.Builders;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.database.sql.build.SqlLiterals;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.MonedaInfo;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Moneda;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerMonedasAction", mappedName = "ObtenerMonedasAction")
@Remote(ObtenerMonedasAction.class)
public class ObtenerMonedasActionImpl implements ObtenerMonedasAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @Override
    public Result<MonedaInfo> obtenerMoneda(String codigoAlfabeticoONumerico) {
        Result<MonedaInfo> result;

        try {
            // Obtener registros.
            Collection<MonedaInfo> monedas = obtenerMonedas(new Condition[]{
                Conditions.any(
                Builders.condition().left(MonedaInfo.COLUMNS.codigoAlfabetico).op(Operator.EQUALS).right(SqlLiterals.string(codigoAlfabeticoONumerico)).build(),
                Builders.condition().left(MonedaInfo.COLUMNS.codigoNumerico).op(Operator.EQUALS).right(SqlLiterals.string(codigoAlfabeticoONumerico)).build()
                )
            });

            // Retornar primer elemento.
            MonedaInfo moneda = Lists.first(monedas);

            result = Results.ok()
                    .value(moneda)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo información de moneda")
                    .tag("moneda.codigo.alfabetico", codigoAlfabeticoONumerico)
                    .tag("moneda.codigo.numerico", codigoAlfabeticoONumerico)
                    .build();
        }

        return result;
    }

    @Override
    public Result<MonedaInfo> obtenerMoneda(Long idMoneda) {
        Result<MonedaInfo> result;

        try {
            // Obtener registros.
            Collection<MonedaInfo> monedas = obtenerMonedas(new Condition[]{
                Builders.condition().left(MonedaInfo.COLUMNS.id).op(Operator.EQUALS).right(idMoneda).build()
            });

            // Retornar primer elemento.
            MonedaInfo moneda = Lists.first(monedas);

            result = Results.ok()
                    .value(moneda)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo información de moneda")
                    .tag("moneda.id", idMoneda)
                    .build();
        }

        return result;
    }

    @Override
    public Result<Collection<MonedaInfo>> obtenerMonedas(Long[] idMonedas) {
        Result<Collection<MonedaInfo>> result;

        try {
            // Control de seguridad.
            if (Assertions.isNullOrEmpty(idMonedas)) {
                return Results.ok()
                        .value(Lists.<MonedaInfo>empty())
                        .build();
            }

            // Obtener registros.
            Collection<MonedaInfo> monedas = obtenerMonedas(new Condition[]{
                Conditions.natives().in(Moneda.COLUMNS.id, idMonedas)
            });

            result = Results.ok()
                    .value(monedas)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo información de monedas")
                    .build();
        }

        return result;
    }

    private Collection<MonedaInfo> obtenerMonedas(Condition[] conditions) throws Throwable {
        // Criterios de busqueda.

        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, buildSubquery(), "tmp");
        Criterias.where(criteria, conditions);

        // Obtener registros.
        String sql = Criterias.toQuery(criteria);
        Collection<MonedaInfo> monedas = nativeQuerySelectAction.select(sql, new MonedaInfo.Mapper());
        return monedas;
    }

    private static String buildSubquery() {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, "recepcion.moneda", "m");
        Criterias.select(criteria, "m.id", MonedaInfo.COLUMNS.id);
        Criterias.select(criteria, "m.codigo", MonedaInfo.COLUMNS.codigo);
        Criterias.select(criteria, "m.codigo_numerico", MonedaInfo.COLUMNS.codigoAlfabetico);
        Criterias.select(criteria, "m.codigo_alfabetico", MonedaInfo.COLUMNS.codigoNumerico);
        Criterias.select(criteria, "m.descripcion", MonedaInfo.COLUMNS.descripcion);
        Criterias.select(criteria, "m.simbolo", MonedaInfo.COLUMNS.simbolo);
        return Criterias.toQuery(criteria);
    }

}
