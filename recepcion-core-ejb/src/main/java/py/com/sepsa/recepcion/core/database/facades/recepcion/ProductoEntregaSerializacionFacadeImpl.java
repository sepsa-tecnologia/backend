package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.ProductoEntregaSerializacion;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "ProductoEntregaSerializacionFacade", mappedName = "ProductoEntregaSerializacionFacade")
@Remote(ProductoEntregaSerializacionFacade.class)
public class ProductoEntregaSerializacionFacadeImpl extends AbstractFacade<ProductoEntregaSerializacion> implements ProductoEntregaSerializacionFacade {

    @EJB
    private Persistence persistence;

    public ProductoEntregaSerializacionFacadeImpl() {
        super(ProductoEntregaSerializacion.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
