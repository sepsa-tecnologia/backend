/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.utils.ValuesGenerator;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Ids;
import java.util.Collection;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.recepcion.core.database.generators.EstadosGenerator;
import py.com.sepsa.recepcion.core.database.generators.MonedasGenerator;
import py.com.sepsa.recepcion.core.database.generators.PermisosGenerator;
import py.com.sepsa.recepcion.core.database.generators.TiposEmpaqueGenerator;
import py.com.sepsa.recepcion.core.database.generators.TiposObservacionGenerator;
import py.com.sepsa.recepcion.core.logging.AppLoggerFactory;
import py.com.sepsa.recepcion.core.logic.configs.RecepcionConfig;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@DependsOn("Configs")
@Singleton(name = "DatabaseValuesGenerator", mappedName = "DatabaseValuesGenerator")
@Remote(DatabaseValuesGenerator.class)
public class DatabaseValuesGeneratorImpl implements DatabaseValuesGenerator {

    private final AppLogger log = AppLoggerFactory.core();

    @EJB
    private Configs configs;

    @PostConstruct
    public void init() {
        // Inicializar valores por defecto para clientes standalone.
        RecepcionConfig configs0 = configs.getRecepcionConfig();
        if (configs0.isStandaloneApp()) {
            // Obtener shard de cliente.
            UUID shard = configs0.getStandaloneShard();

            // Ejecutar generadores de valores.
            Collection<ValuesGenerator> generators = collectGenerators(shard);
            for (ValuesGenerator generator : generators) {
                generateValues(generator);
            }
        }
    }

    private Collection<ValuesGenerator> collectGenerators(UUID shard) {
        Collection<ValuesGenerator> generators = Lists.empty();
        generators.add(new EstadosGenerator(shard));
        generators.add(new MonedasGenerator(shard));
        generators.add(new PermisosGenerator(shard));
        generators.add(new TiposEmpaqueGenerator(shard));
        generators.add(new TiposObservacionGenerator(shard));
        return generators;
    }

    private void generateValues(ValuesGenerator generator) {
        String traza = Ids.id();
        log.inicio(traza);
        try {
            log.info("[%s] Generando valores.", generator.getClass().getCanonicalName());
            generator.generarValores();
            log.info("[%s] Generacion ok.", generator.getClass().getCanonicalName());
        } catch (Throwable thr) {
            log.error(thr, "[%s] Ocurrió un error generando valores.", generator.getClass().getCanonicalName());
        } finally {
            log.fin(traza);
        }
    }

}
