/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.collections.maps.CollectionGroupMap;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.MonedaInfo;
import py.com.sepsa.recepcion.core.data.pojos.OrdenCompraDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.ProductoInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerDetallesOrdenesCompraAction", mappedName = "ObtenerDetallesOrdenesCompraAction")
@Remote(ObtenerDetallesOrdenesCompraAction.class)
public class ObtenerDetallesOrdenesCompraActionImpl implements ObtenerDetallesOrdenesCompraAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @EJB
    private ObtenerProductosAction obtenerProductosAction;

    @EJB
    private ObtenerMonedasAction obtenerMonedasAction;

    @Override
    public Result<Collection<OrdenCompraDetalleInfo>> obtenerDetallesOrdenCompra(Long idOrdenCompra) {
        Result<Collection<OrdenCompraDetalleInfo>> result;

        try {
            Long[] ids = {idOrdenCompra};
            Result<Map<Long, Collection<OrdenCompraDetalleInfo>>> resDetalles = obtenerDetallesOrdenCompra(ids);
            resDetalles.raise();

            Map<Long, Collection<OrdenCompraDetalleInfo>> detalles = resDetalles.value(Maps.empty());
            Collection<OrdenCompraDetalleInfo> detallesOrdenCompra = Maps.get(detalles, idOrdenCompra, Lists.empty());

            result = Results.ok()
                    .value(detallesOrdenCompra)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo detalles de órden de compra")
                    .tag("orden_compra.id", idOrdenCompra)
                    .build();
        }

        return result;
    }

    @Override
    public Result<Map<Long, Collection<OrdenCompraDetalleInfo>>> obtenerDetallesOrdenCompra(Long[] idOrdenesCompra) {
        Result<Map<Long, Collection<OrdenCompraDetalleInfo>>> result;

        try {
            // Criterios de busqueda.
            QueryCriteria criteria = QueryCriteria.select();
            Criterias.from(criteria, buildSubquery(), "tmp");
            Criterias.where(criteria, Conditions.natives().in(OrdenCompraDetalleInfo.COLUMNS.idOrdenCompra, idOrdenesCompra));

            // Obtener registros.
            String sql = Criterias.toQuery(criteria);
            Collection<OrdenCompraDetalleInfo> detalles = nativeQuerySelectAction.select(sql, new OrdenCompraDetalleInfo.Mapper());

            // Asociar informacion de productos.
            completarProductos(detalles);

            // Asociar informacion de monedas.
            completarMonedas(detalles);

            // Agrupar detalles por orden de compra.
            CollectionGroupMap<Long, OrdenCompraDetalleInfo> groups = Maps.groupBy(detalles, "idOrdenCompra", Long.class);

            result = Results.ok()
                    .value(groups.getMap())
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo detalles de ordenes de compra")
                    .build();
        }

        return result;
    }

    private String buildSubquery() {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, "recepcion.orden_compra_detalle", "ocd");
        Criterias.select(criteria, "ocd.id_orden_compra", OrdenCompraDetalleInfo.COLUMNS.idOrdenCompra);
        Criterias.select(criteria, "ocd.id_producto", OrdenCompraDetalleInfo.COLUMNS.idProducto);
        Criterias.select(criteria, "ocd.cantidad", OrdenCompraDetalleInfo.COLUMNS.cantidad);
        Criterias.select(criteria, "ocd.precio", OrdenCompraDetalleInfo.COLUMNS.precio);
        Criterias.select(criteria, "ocd.id_moneda", OrdenCompraDetalleInfo.COLUMNS.idMoneda);
        return Criterias.toQuery(criteria);
    }

    private void completarProductos(Collection<OrdenCompraDetalleInfo> detalles) throws Throwable {
        // Obtener identificadores de productos.
        Long[] idProductos = detalles.stream()
                .map(d -> d.getIdProducto())
                .distinct()
                .toArray(Long[]::new);

        // Obtener productos disponibles.
        Result<Collection<ProductoInfo>> resProductos = obtenerProductosAction.obtenerProductos(idProductos);
        resProductos.raise();
        Collection<ProductoInfo> productos = resProductos.value(Lists.empty());

        // Asociar productos con detalle correspondiente.
        for (OrdenCompraDetalleInfo detalle : detalles) {
            for (ProductoInfo producto : productos) {
                if (Assertions.equals(detalle.getIdProducto(), producto.getId())) {
                    detalle.setProducto(producto);
                    break;
                }
            }
        }
    }

    private void completarMonedas(Collection<OrdenCompraDetalleInfo> detalles) throws Throwable {
        // Obtener identificadores de monedas.
        Long[] idMonedas = detalles.stream()
                .map(d -> d.getIdMoneda())
                .distinct()
                .toArray(Long[]::new);

        // Obtener monedas disponibles.
        Result<Collection<MonedaInfo>> resMonedas = obtenerMonedasAction.obtenerMonedas(idMonedas);
        resMonedas.raise();
        Collection<MonedaInfo> monedas = resMonedas.value(Lists.empty());

        // Asociar monedas con detalle correspondiente.
        for (OrdenCompraDetalleInfo detalle : detalles) {
            for (MonedaInfo moneda : monedas) {
                if (Assertions.equals(detalle.getIdMoneda(), moneda.getId())) {
                    detalle.setMoneda(moneda);
                    break;
                }
            }
        }
    }

}
