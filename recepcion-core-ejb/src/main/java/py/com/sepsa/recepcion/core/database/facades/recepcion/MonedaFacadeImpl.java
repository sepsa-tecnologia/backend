package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Moneda;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "MonedaFacade", mappedName = "MonedaFacade")
@Remote(MonedaFacade.class)
public class MonedaFacadeImpl extends AbstractFacade<Moneda> implements MonedaFacade {

    @EJB
    private Persistence persistence;

    public MonedaFacadeImpl() {
        super(Moneda.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
