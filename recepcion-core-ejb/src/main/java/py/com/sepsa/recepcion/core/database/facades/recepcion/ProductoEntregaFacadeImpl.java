package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.ProductoEntrega;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "ProductoEntregaFacade", mappedName = "ProductoEntregaFacade")
@Remote(ProductoEntregaFacade.class)
public class ProductoEntregaFacadeImpl extends AbstractFacade<ProductoEntrega> implements ProductoEntregaFacade {

    @EJB
    private Persistence persistence;

    public ProductoEntregaFacadeImpl() {
        super(ProductoEntrega.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
