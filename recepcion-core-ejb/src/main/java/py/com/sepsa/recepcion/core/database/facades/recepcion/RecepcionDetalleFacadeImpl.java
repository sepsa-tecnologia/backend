package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.RecepcionDetalle;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "RecepcionDetalleFacade", mappedName = "RecepcionDetalleFacade")
@Remote(RecepcionDetalleFacade.class)
public class RecepcionDetalleFacadeImpl extends AbstractFacade<RecepcionDetalle> implements RecepcionDetalleFacade {

    @EJB
    private Persistence persistence;

    public RecepcionDetalleFacadeImpl() {
        super(RecepcionDetalle.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
