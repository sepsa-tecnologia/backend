/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.database.criteria.Condition;
import fa.gs.utils.database.criteria.JoinKind;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.Pagination;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.criteria.Sorting;
import fa.gs.utils.database.sql.build.Criterias;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.RecepcionCabeceraInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerCabecerasRecepcionesAction", mappedName = "ObtenerCabecerasRecepcionesAction")
@Remote(ObtenerCabecerasRecepcionesAction.class)
public class ObtenerCabecerasRecepcionesActionImpl implements ObtenerCabecerasRecepcionesAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @EJB
    private NativeQueryCountAction nativeQueryCountAction;

    @Override
    public Long countItems(Condition[] conditions) {
        QueryCriteria criteria = QueryCriteria.count();
        Criterias.from(criteria, buildSubquery(), "tmp");
        Criterias.where(criteria, conditions);

        String query = Criterias.toQuery(criteria);
        return nativeQueryCountAction.count(query);
    }

    @Override
    public Collection<RecepcionCabeceraInfo> loadItems(Pagination pagination, Condition[] conditions, Sorting[] sortings) {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, buildSubquery(), "tmp");
        Criterias.where(criteria, conditions);
        Criterias.order(criteria, sortings);
        Criterias.limit(criteria, pagination);

        String query = Criterias.toQuery(criteria);
        return nativeQuerySelectAction.select(query, new RecepcionCabeceraInfo.Mapper());
    }

    private static String buildSubquery() {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, "recepcion.recepcion", "r");
        Criterias.select(criteria, "r.id", RecepcionCabeceraInfo.COLUMNS.idRecepcion);
        Criterias.select(criteria, "e.codigo", RecepcionCabeceraInfo.COLUMNS.estadoCodigo);
        Criterias.select(criteria, "r.id_documento", RecepcionCabeceraInfo.COLUMNS.idDocumento);
        Criterias.select(criteria, "r.nro_documento", RecepcionCabeceraInfo.COLUMNS.nroDocumentoRecepcion);
        Criterias.select(criteria, "oc.id", RecepcionCabeceraInfo.COLUMNS.idOrdenCompra);
        Criterias.select(criteria, "oc.nro_documento", RecepcionCabeceraInfo.COLUMNS.nroDocumentoOrdenCompra);
        Criterias.select(criteria, "oc.gln_destino", RecepcionCabeceraInfo.COLUMNS.glnProveedor);
        Criterias.select(criteria, "oc.razon_social_destino", RecepcionCabeceraInfo.COLUMNS.razonSocialProveedor);
        Criterias.select(criteria, "oc.ruc_destino", RecepcionCabeceraInfo.COLUMNS.rucProveedor);
        Criterias.select(criteria, "r.fecha_emision", RecepcionCabeceraInfo.COLUMNS.fechaEmision);
        Criterias.select(criteria, "r.fecha_registro", RecepcionCabeceraInfo.COLUMNS.fechaRegistro);
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.factura", "f", "f.id", Operator.EQUALS, "r.id_factura");
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.nota_remision", "nr", "nr.id", Operator.EQUALS, "r.id_nota_remision");
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.orden_compra", "oc", "oc.id", Operator.EQUALS, "coalesce(f.id_orden_compra, nr.id_orden_compra)");
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.estado", "e", "e.id", Operator.EQUALS, "r.id_estado");
        return Criterias.toQuery(criteria);
    }

}
