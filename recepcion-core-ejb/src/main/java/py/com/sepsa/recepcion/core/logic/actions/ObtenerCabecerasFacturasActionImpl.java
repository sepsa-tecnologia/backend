/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.database.criteria.Condition;
import fa.gs.utils.database.criteria.JoinKind;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.Pagination;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.criteria.Sorting;
import fa.gs.utils.database.sql.build.Criterias;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.FacturaCabeceraInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerCabecerasFacturasAction", mappedName = "ObtenerCabecerasFacturasAction")
@Remote(ObtenerCabecerasFacturasAction.class)
public class ObtenerCabecerasFacturasActionImpl implements ObtenerCabecerasFacturasAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @EJB
    private NativeQueryCountAction nativeQueryCountAction;

    @Override
    public Long countItems(Condition[] conditions) {
        QueryCriteria criteria = QueryCriteria.count();
        Criterias.from(criteria, buildSubquery(), "tmp");
        Criterias.where(criteria, conditions);

        String query = Criterias.toQuery(criteria);
        return nativeQueryCountAction.count(query);
    }

    @Override
    public Collection<FacturaCabeceraInfo> loadItems(Pagination pagination, Condition[] conditions, Sorting[] sortings) {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, buildSubquery(), "tmp");
        Criterias.where(criteria, conditions);
        Criterias.order(criteria, sortings);
        Criterias.limit(criteria, pagination);

        String query = Criterias.toQuery(criteria);
        return nativeQuerySelectAction.select(query, new FacturaCabeceraInfo.Mapper());
    }

    private static String buildSubquery() {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, "recepcion.factura", "f");
        Criterias.select(criteria, "f.id", FacturaCabeceraInfo.COLUMNS.idFactura);
        Criterias.select(criteria, "e.codigo", FacturaCabeceraInfo.COLUMNS.estadoCodigo);
        Criterias.select(criteria, "f.id_documento", FacturaCabeceraInfo.COLUMNS.idDocumento);
        Criterias.select(criteria, "f.nro_documento", FacturaCabeceraInfo.COLUMNS.nroDocumentoFactura);
        Criterias.select(criteria, "f.nro_timbrado", FacturaCabeceraInfo.COLUMNS.nroTimbradoFactura);
        Criterias.select(criteria, "f.vencimiento_timbrado", FacturaCabeceraInfo.COLUMNS.vtoTimbradoFactura);
        Criterias.select(criteria, "oc.id", FacturaCabeceraInfo.COLUMNS.idOrdenCompra);
        Criterias.select(criteria, "oc.nro_documento", FacturaCabeceraInfo.COLUMNS.nroDocumentoOrdenCompra);
        Criterias.select(criteria, "oc.gln_destino", FacturaCabeceraInfo.COLUMNS.glnProveedor);
        Criterias.select(criteria, "oc.razon_social_destino", FacturaCabeceraInfo.COLUMNS.razonSocialProveedor);
        Criterias.select(criteria, "oc.ruc_destino", FacturaCabeceraInfo.COLUMNS.rucProveedor);
        Criterias.select(criteria, "f.fecha_emision", FacturaCabeceraInfo.COLUMNS.fechaEmision);
        Criterias.select(criteria, "f.fecha_registro", FacturaCabeceraInfo.COLUMNS.fechaRegistro);
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.orden_compra", "oc", "f.id_orden_compra", Operator.EQUALS, "oc.id");
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.estado", "e", "f.id_estado", Operator.EQUALS, "e.id");
        return Criterias.toQuery(criteria);
    }

}
