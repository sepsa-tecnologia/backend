/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.collections.maps.CollectionGroupMap;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.RecepcionDetalleInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerDetallesRecepcionesAction", mappedName = "ObtenerDetallesRecepcionesAction")
@Remote(ObtenerDetallesRecepcionesAction.class)
public class ObtenerDetallesRecepcionesActionImpl implements ObtenerDetallesRecepcionesAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @EJB
    private CompletarObservacionesDocumentoDetalleAction completarObservacionesDocumentoDetalleAction;

    @EJB
    private CompletarProductosEntregablesDocumentoDetalleAction completarProductosEntregablesDocumentoDetalleAction;

    @EJB
    private CompletarMonedasDocumentoDetalleAction completarMonedasDocumentoDetalleAction;

    @Override
    public Result<Collection<RecepcionDetalleInfo>> obtenerDetallesRecepcion(Long idRecepcion) {
        Result<Collection<RecepcionDetalleInfo>> result;

        try {
            Long[] ids = {idRecepcion};
            Result<Map<Long, Collection<RecepcionDetalleInfo>>> resDetalles = obtenerDetallesRecepciones(ids);
            resDetalles.raise();

            Map<Long, Collection<RecepcionDetalleInfo>> detalles = resDetalles.value(Maps.empty());
            Collection<RecepcionDetalleInfo> detallesRecepcion = Maps.get(detalles, idRecepcion, Lists.empty());

            result = Results.ok()
                    .value(detallesRecepcion)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo detalles de nota de remisión")
                    .tag("nota_remision.id", idRecepcion)
                    .build();
        }

        return result;
    }

    @Override
    public Result<Map<Long, Collection<RecepcionDetalleInfo>>> obtenerDetallesRecepciones(Long[] idRecepciones) {
        Result<Map<Long, Collection<RecepcionDetalleInfo>>> result;

        try {
            // Criterios de busqueda.
            QueryCriteria criteria = QueryCriteria.select();
            Criterias.from(criteria, buildSubquery(), "tmp");
            Criterias.where(criteria, Conditions.natives().in(RecepcionDetalleInfo.COLUMNS.idRecepcion, idRecepciones));

            // Obtener detalles de nota de remision.
            String sql = Criterias.toQuery(criteria);
            Collection<RecepcionDetalleInfo> detalles = nativeQuerySelectAction.select(sql, new RecepcionDetalleInfo.Mapper());

            // Asociar informacion de monedas.
            completarMonedasDocumentoDetalleAction.completar(detalles);

            // Asociar informacion de entrega de productos.
            completarProductosEntregablesDocumentoDetalleAction.completar(detalles);

            // Asociar informacion de observaciones.
            completarObservacionesDocumentoDetalleAction.completar(detalles);

            // Agrupar registros por orden de compra.
            CollectionGroupMap<Long, RecepcionDetalleInfo> groups = Maps.groupBy(detalles, "idRecepcion", Long.class);

            result = Results.ok()
                    .value(groups.getMap())
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo detalles de notas de remisión")
                    .build();
        }

        return result;
    }

    private String buildSubquery() {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, "recepcion.recepcion_detalle", "rd");
        Criterias.select(criteria, "rd.id", RecepcionDetalleInfo.COLUMNS.idDetalleRecepcion);
        Criterias.select(criteria, "rd.id_recepcion", RecepcionDetalleInfo.COLUMNS.idRecepcion);
        Criterias.select(criteria, "rd.id_producto_entrega", RecepcionDetalleInfo.COLUMNS.idProductoEntregable);
        Criterias.select(criteria, "rd.cantidad", RecepcionDetalleInfo.COLUMNS.cantidad);
        return Criterias.toQuery(criteria);
    }

}
