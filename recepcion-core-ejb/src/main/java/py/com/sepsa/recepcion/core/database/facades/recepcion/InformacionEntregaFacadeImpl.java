package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.InformacionEntrega;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "InformacionEntregaFacade", mappedName = "InformacionEntregaFacade")
@Remote(InformacionEntregaFacade.class)
public class InformacionEntregaFacadeImpl extends AbstractFacade<InformacionEntrega> implements InformacionEntregaFacade {

    @EJB
    private Persistence persistence;

    public InformacionEntregaFacadeImpl() {
        super(InformacionEntrega.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
