/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.database.persistence;

import fa.gs.utils.collections.Arrays;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.injection.Jndi;
import fa.gs.utils.result.simple.Result;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import javax.persistence.SharedCacheMode;
import javax.persistence.ValidationMode;
import javax.persistence.spi.ClassTransformer;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.persistence.spi.PersistenceUnitTransactionType;
import javax.sql.DataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Empaque;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Empresa;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Estado;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Factura;
import py.com.sepsa.recepcion.core.database.entities.recepcion.FacturaDetalle;
import py.com.sepsa.recepcion.core.database.entities.recepcion.InformacionEntrega;
import py.com.sepsa.recepcion.core.database.entities.recepcion.InformacionLegal;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Moneda;
import py.com.sepsa.recepcion.core.database.entities.recepcion.NotaRemision;
import py.com.sepsa.recepcion.core.database.entities.recepcion.NotaRemisionDetalle;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Observacion;
import py.com.sepsa.recepcion.core.database.entities.recepcion.OrdenCompra;
import py.com.sepsa.recepcion.core.database.entities.recepcion.OrdenCompraDetalle;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Permiso;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Persona;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Producto;
import py.com.sepsa.recepcion.core.database.entities.recepcion.ProductoEntrega;
import py.com.sepsa.recepcion.core.database.entities.recepcion.ProductoEntregaSerializacion;
import py.com.sepsa.recepcion.core.database.entities.recepcion.ProductoMarca;
import py.com.sepsa.recepcion.core.database.entities.recepcion.ProductoProcedencia;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Recepcion;
import py.com.sepsa.recepcion.core.database.entities.recepcion.RecepcionDetalle;
import py.com.sepsa.recepcion.core.database.entities.recepcion.TipoEmpaque;
import py.com.sepsa.recepcion.core.database.entities.recepcion.TipoObservacion;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Usuario;
import py.com.sepsa.recepcion.core.database.entities.recepcion.UsuarioPermisos;

/**
 *
 * @author Fabio A. González Sosa
 */
public class PersistenceUnit implements PersistenceUnitInfo {

    public static final String DEFAULT_DATASOURCE_NAME = "jdbc/sepsa_postgres_recepcion";

    private final Properties props;

    private final String datasourceName;

    private PersistenceUnit(String datasourceName) {
        this.datasourceName = datasourceName;
        this.props = new Properties();
        this.props.put("hibernate.show_sql", true);
        this.props.put("hibernate.format_sql", true);
        this.props.put("hibernate.generate_statistics", true);
        this.props.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect");
        this.props.put("hibernate.transaction.jta.platform", "org.hibernate.service.jta.platform.internal.SunOneJtaPlatform");
    }

    public static PersistenceUnitInfo instance(String datasourceName) {
        return new PersistenceUnit(datasourceName);
    }

    @Override
    public Properties getProperties() {
        return props;
    }

    @Override
    public List<String> getManagedClassNames() {
        String[] classNames = {
            Empaque.class.getName(),
            Empresa.class.getName(),
            Estado.class.getName(),
            Factura.class.getName(),
            FacturaDetalle.class.getName(),
            InformacionEntrega.class.getName(),
            InformacionLegal.class.getName(),
            Moneda.class.getName(),
            NotaRemision.class.getName(),
            NotaRemisionDetalle.class.getName(),
            Observacion.class.getName(),
            OrdenCompra.class.getName(),
            OrdenCompraDetalle.class.getName(),
            Permiso.class.getName(),
            Persona.class.getName(),
            Producto.class.getName(),
            ProductoEntrega.class.getName(),
            ProductoEntregaSerializacion.class.getName(),
            ProductoMarca.class.getName(),
            ProductoProcedencia.class.getName(),
            Recepcion.class.getName(),
            RecepcionDetalle.class.getName(),
            TipoEmpaque.class.getName(),
            TipoObservacion.class.getName(),
            Usuario.class.getName(),
            UsuarioPermisos.class.getName()
        };

        return Arrays.list(classNames);
    }

    @Override
    public String getPersistenceUnitName() {
        return "AppPU";
    }

    @Override
    public String getPersistenceProviderClassName() {
        return HibernatePersistenceProvider.class.getName();
    }

    @Override
    public PersistenceUnitTransactionType getTransactionType() {
        return PersistenceUnitTransactionType.JTA;
    }

    @Override
    public DataSource getJtaDataSource() {
        Result<DataSource> result = Jndi.lookup(datasourceName);
        return result.value(null);
    }

    @Override
    public DataSource getNonJtaDataSource() {
        return null;
    }

    @Override
    public List<String> getMappingFileNames() {
        return null;
    }

    @Override
    public List<URL> getJarFileUrls() {
        return Lists.empty();
    }

    @Override
    public URL getPersistenceUnitRootUrl() {
        return null;
    }

    @Override
    public boolean excludeUnlistedClasses() {
        return false;
    }

    @Override
    public SharedCacheMode getSharedCacheMode() {
        return SharedCacheMode.UNSPECIFIED;
    }

    @Override
    public ValidationMode getValidationMode() {
        return ValidationMode.AUTO;
    }

    @Override
    public String getPersistenceXMLSchemaVersion() {
        return "2.1";
    }

    @Override
    public ClassLoader getClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    @Override
    public void addTransformer(ClassTransformer transformer) {
        ;
    }

    @Override
    public ClassLoader getNewTempClassLoader() {
        return null;
    }

}
