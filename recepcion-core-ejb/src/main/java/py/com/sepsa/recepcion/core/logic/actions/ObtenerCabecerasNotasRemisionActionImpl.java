/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.database.criteria.Condition;
import fa.gs.utils.database.criteria.JoinKind;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.Pagination;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.criteria.Sorting;
import fa.gs.utils.database.sql.build.Criterias;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.NotaRemisionCabeceraInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerCabecerasNotasRemisionAction", mappedName = "ObtenerCabecerasNotasRemisionAction")
@Remote(ObtenerCabecerasNotasRemisionAction.class)
public class ObtenerCabecerasNotasRemisionActionImpl implements ObtenerCabecerasNotasRemisionAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @EJB
    private NativeQueryCountAction nativeQueryCountAction;

    @Override
    public Long countItems(Condition[] conditions) {
        QueryCriteria criteria = QueryCriteria.count();
        Criterias.from(criteria, buildSubquery(), "tmp");
        Criterias.where(criteria, conditions);

        String query = Criterias.toQuery(criteria);
        return nativeQueryCountAction.count(query);
    }

    @Override
    public Collection<NotaRemisionCabeceraInfo> loadItems(Pagination pagination, Condition[] conditions, Sorting[] sortings) {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, buildSubquery(), "tmp");
        Criterias.where(criteria, conditions);
        Criterias.order(criteria, sortings);
        Criterias.limit(criteria, pagination);

        String query = Criterias.toQuery(criteria);
        return nativeQuerySelectAction.select(query, new NotaRemisionCabeceraInfo.Mapper());
    }

    private static String buildSubquery() {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, "recepcion.nota_remision", "nr");
        Criterias.select(criteria, "nr.id", NotaRemisionCabeceraInfo.COLUMNS.idNotaRemision);
        Criterias.select(criteria, "e.codigo", NotaRemisionCabeceraInfo.COLUMNS.estadoCodigo);
        Criterias.select(criteria, "nr.id_documento", NotaRemisionCabeceraInfo.COLUMNS.idDocumento);
        Criterias.select(criteria, "nr.nro_documento", NotaRemisionCabeceraInfo.COLUMNS.nroDocumentoNotaRemision);
        Criterias.select(criteria, "oc.id", NotaRemisionCabeceraInfo.COLUMNS.idOrdenCompra);
        Criterias.select(criteria, "oc.nro_documento", NotaRemisionCabeceraInfo.COLUMNS.nroDocumentoOrdenCompra);
        Criterias.select(criteria, "oc.gln_destino", NotaRemisionCabeceraInfo.COLUMNS.glnProveedor);
        Criterias.select(criteria, "oc.razon_social_destino", NotaRemisionCabeceraInfo.COLUMNS.razonSocialProveedor);
        Criterias.select(criteria, "oc.ruc_destino", NotaRemisionCabeceraInfo.COLUMNS.rucProveedor);
        Criterias.select(criteria, "nr.fecha_emision", NotaRemisionCabeceraInfo.COLUMNS.fechaEmision);
        Criterias.select(criteria, "nr.fecha_registro", NotaRemisionCabeceraInfo.COLUMNS.fechaRegistro);
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.orden_compra", "oc", "nr.id_orden_compra", Operator.EQUALS, "oc.id");
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.estado", "e", "nr.id_estado", Operator.EQUALS, "e.id");
        return Criterias.toQuery(criteria);
    }

}
