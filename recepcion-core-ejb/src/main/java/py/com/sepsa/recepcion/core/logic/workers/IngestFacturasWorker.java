/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.workers;

import fa.gs.utils.workers.ApplicationWorkerExecutor;
import java.io.File;
import java.util.Collection;
import py.com.sepsa.recepcion.core.logic.configs.SiediConfig;
import py.com.sepsa.recepcion.ingest.FileIngester;
import py.com.sepsa.recepcion.ingest.documentos.IngestFacturas;

/**
 *
 * @author Fabio A. González Sosa
 */
public class IngestFacturasWorker extends AbstractIngestWorker {

    public IngestFacturasWorker(ApplicationWorkerExecutor executor) {
        super(executor);
    }

    @Override
    public void onRun() throws Throwable {
        SiediConfig config = getSiediConfig();
        FileIngester ingestor = new IngestFacturas(config);
        Collection<File> files = ingestor.listFiles(config.getCarpetaEntrada());
        ingestor.ingest(files);
    }

}
