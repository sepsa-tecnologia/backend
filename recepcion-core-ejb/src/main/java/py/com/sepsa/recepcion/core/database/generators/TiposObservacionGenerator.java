/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.database.generators;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.criteria.crud.Crear;
import fa.gs.utils.database.criteria.crud.Editar;
import fa.gs.utils.database.criteria.crud.Obtener;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.result.simple.Result;
import java.util.Collection;
import java.util.Objects;
import java.util.UUID;
import py.com.sepsa.recepcion.core.data.enums.TipoObservacionEnum;
import py.com.sepsa.recepcion.core.database.entities.recepcion.TipoObservacion;
import py.com.sepsa.recepcion.core.database.facades.recepcion.TipoObservacionFacade;
import py.com.sepsa.recepcion.core.logic.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
public class TiposObservacionGenerator extends ShardedValuesGenerator<TipoObservacion, TipoObservacionEnum> {

    private TipoObservacionFacade facade;

    public TiposObservacionGenerator(UUID shard) {
        super(shard);
        this.facade = Injection.bean(TipoObservacionFacade.class);
    }

    @Override
    public Collection<TipoObservacion> generarValores() {
        Collection<TipoObservacion> valores = Lists.empty();
        for (TipoObservacionEnum valorEnum : TipoObservacionEnum.values()) {
            TipoObservacion valor = obtenerOCrearValor(valorEnum);
            if (valor != null) {
                valores.add(valor);
            }
        }
        return valores;
    }

    @Override
    protected Result<TipoObservacion> obtenerValor(TipoObservacionEnum valorEnum, Object... args) {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.where(criteria, TipoObservacion.COLUMNS.codigo, Operator.EQUALS, valorEnum.codigo());
        return Obtener.primero(facade, criteria);
    }

    @Override
    protected Result<TipoObservacion> crearValor(TipoObservacionEnum valorEnum, Object... args) {
        TipoObservacion valor = new TipoObservacion();
        fill(valor, valorEnum);
        return Crear.entity(facade, valor);
    }

    @Override
    protected boolean puedeModificar(TipoObservacion valorExistente, TipoObservacionEnum valorEnum, Object... args) {
        boolean a = Objects.equals(valorExistente.getCodigo(), valorEnum.codigo());
        boolean b = Objects.equals(valorExistente.getDescripcion(), valorEnum.descripcion());
        return a && !b;
    }

    @Override
    protected Result<TipoObservacion> modificarValor(TipoObservacion valor, TipoObservacionEnum valorEnum, Object... args) {
        fill(valor, valorEnum);
        return Editar.entity(facade, valor);
    }

    private void fill(TipoObservacion valor, TipoObservacionEnum valorEnum) {
        valor.setCodigo(valorEnum.codigo());
        valor.setDescripcion(valorEnum.descripcion());
    }

}
