/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Sets;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.EmpaqueInfo;
import py.com.sepsa.recepcion.core.data.pojos.ProductoEntregableInfo;
import py.com.sepsa.recepcion.core.data.pojos.ProductoInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerProductosEntregablesAction", mappedName = "ObtenerProductosEntregablesAction")
@Remote(ObtenerProductosEntregablesAction.class)
public class ObtenerProductosEntregablesActionImpl implements ObtenerProductosEntregablesAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @EJB
    private ObtenerEmpaquesAction obtenerEmpaquesAction;

    @EJB
    private ObtenerProductosAction obtenerProductosAction;

    @Override
    public Result<ProductoEntregableInfo> obtenerProductoEntregable(Long idProductoEntrega) {
        Result<ProductoEntregableInfo> result;

        try {
            // Obtener productos entregables.
            Long[] ids = {idProductoEntrega};
            Result<Collection<ProductoEntregableInfo>> resProductos = obtenerProductosEntragables(ids);
            resProductos.raise();

            // Retornar primer elemento.
            Collection<ProductoEntregableInfo> productos = resProductos.value(Lists.empty());
            ProductoEntregableInfo producto = Lists.first(productos);

            result = Results.ok()
                    .value(producto)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo información de producto")
                    .tag("producto.id", idProductoEntrega)
                    .build();
        }

        return result;
    }

    @Override
    public Result<Collection<ProductoEntregableInfo>> obtenerProductosEntragables(Long[] idProductosEntrega) {
        Result<Collection<ProductoEntregableInfo>> result;

        try {
            // Control de seguridad.
            if (Assertions.isNullOrEmpty(idProductosEntrega)) {
                return Results.ok()
                        .value(Lists.<ProductoEntregableInfo>empty())
                        .build();
            }

            // Criterios de busqueda.
            QueryCriteria criteria = QueryCriteria.select();
            Criterias.from(criteria, buildSubquery(), "tmp");
            criteria.where(Conditions.natives().in(ProductoEntregableInfo.COLUMNS.id, idProductosEntrega));

            // Obtener registros.
            String sql = Criterias.toQuery(criteria);
            Collection<ProductoEntregableInfo> productosEntregables = nativeQuerySelectAction.select(sql, new ProductoEntregableInfo.Mapper());

            // Asociar informacion de productos.
            completarProductos(productosEntregables);

            // Asociar informacion de empaques.
            completarEmpaques(productosEntregables);

            result = Results.ok()
                    .value(productosEntregables)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo información de productos")
                    .build();
        }

        return result;
    }

    private String buildSubquery() {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.select(criteria, "p.id", ProductoEntregableInfo.COLUMNS.id);
        Criterias.select(criteria, "p.id_producto", ProductoEntregableInfo.COLUMNS.idProducto);
        Criterias.select(criteria, "p.lote_origen", ProductoEntregableInfo.COLUMNS.loteOrigen);
        Criterias.select(criteria, "p.fecha_vencimiento", ProductoEntregableInfo.COLUMNS.fechaVencimiento);
        Criterias.select(criteria, "p.fecha_entrega_tentativa", ProductoEntregableInfo.COLUMNS.fechaEntregaTentativa);
        Criterias.select(criteria, "p.fecha_entrega_agendada", ProductoEntregableInfo.COLUMNS.fechaEntregaAgendada);
        Criterias.select(criteria, "p.id_empaque_l1", ProductoEntregableInfo.COLUMNS.idEmpaqueL1);
        Criterias.select(criteria, "p.id_empaque_l2", ProductoEntregableInfo.COLUMNS.idEmpaqueL2);
        Criterias.select(criteria, "p.id_empaque_l3", ProductoEntregableInfo.COLUMNS.idEmpaqueL3);
        Criterias.select(criteria, "p.id_empaque_l4", ProductoEntregableInfo.COLUMNS.idEmpaqueL4);
        Criterias.from(criteria, "recepcion.producto_entrega", "p");
        return Criterias.toQuery(criteria);
    }

    private void completarProductos(Collection<ProductoEntregableInfo> productosEntregables) throws Throwable {
        // Obtener identificadores de productos.
        Long[] idProductos = productosEntregables.stream()
                .map(p -> p.getProducto().getId())
                .filter(i -> i != null)
                .distinct()
                .toArray(Long[]::new);

        // Obtener productos disponibles.
        Result<Collection<ProductoInfo>> resProductos = obtenerProductosAction.obtenerProductos(idProductos);
        resProductos.raise();
        Collection<ProductoInfo> productos = resProductos.value(Lists.empty());

        // Asociar productos con productos entregables.
        for (ProductoEntregableInfo productoEntregable : productosEntregables) {
            for (ProductoInfo producto : productos) {
                if (Assertions.equals(productoEntregable.getProducto().getId(), producto.getId())) {
                    productoEntregable.setProducto(producto);
                    break;
                }
            }
        }
    }

    private void completarEmpaques(Collection<ProductoEntregableInfo> productosEntregables) throws Throwable {
        // Obtener identificadores de empaques.
        Set<Long> idEmpaques0 = Sets.empty();
        for (ProductoEntregableInfo p : productosEntregables) {
            idEmpaques0.add(p.getEmpaqueL1().getId());
            idEmpaques0.add(p.getEmpaqueL2().getId());
            idEmpaques0.add(p.getEmpaqueL3().getId());
            idEmpaques0.add(p.getEmpaqueL4().getId());
        }

        Long[] idEmpaques = idEmpaques0.stream()
                .filter(i -> i != null)
                .distinct()
                .toArray(Long[]::new);

        // Obtener empaques disponibles.
        Result<Collection<EmpaqueInfo>> resEmpaques = obtenerEmpaquesAction.obtenerEmpaques(idEmpaques);
        resEmpaques.raise();
        Collection<EmpaqueInfo> empaques = resEmpaques.value(Lists.empty());

        // Asociar empaques con productos entregables.
        for (ProductoEntregableInfo productoEntregable : productosEntregables) {
            for (EmpaqueInfo empaque : empaques) {
                // Empaque primario.
                if (Assertions.equals(productoEntregable.getEmpaqueL1().getId(), empaque.getId())) {
                    productoEntregable.setEmpaqueL1(empaque);
                }

                // Empaque secundario.
                if (Assertions.equals(productoEntregable.getEmpaqueL2().getId(), empaque.getId())) {
                    productoEntregable.setEmpaqueL2(empaque);
                }

                // Empaque terciario.
                if (Assertions.equals(productoEntregable.getEmpaqueL3().getId(), empaque.getId())) {
                    productoEntregable.setEmpaqueL3(empaque);
                }

                // Empaque cuaternario.
                if (Assertions.equals(productoEntregable.getEmpaqueL4().getId(), empaque.getId())) {
                    productoEntregable.setEmpaqueL4(empaque);
                }
            }
        }
    }

}
