package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Factura;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "FacturaFacade", mappedName = "FacturaFacade")
@Remote(FacturaFacade.class)
public class FacturaFacadeImpl extends AbstractFacade<Factura> implements FacturaFacade {

    @EJB
    private Persistence persistence;

    public FacturaFacadeImpl() {
        super(Factura.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
