/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.database.generators;

import fa.gs.utils.database.utils.ValuesGenerator;
import java.util.UUID;

/**
 *
 * @author Fabio A. González Sosa
 */
public abstract class ShardedValuesGenerator<T, K> extends ValuesGenerator<T, K> {

    private final UUID shard;

    public ShardedValuesGenerator(UUID shard) {
        this.shard = shard;
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public UUID getShard() {
        return shard;
    }
    //</editor-fold>
}
