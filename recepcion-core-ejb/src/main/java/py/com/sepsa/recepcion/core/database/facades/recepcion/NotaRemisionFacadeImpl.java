package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.NotaRemision;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "NotaRemisionFacade", mappedName = "NotaRemisionFacade")
@Remote(NotaRemisionFacade.class)
public class NotaRemisionFacadeImpl extends AbstractFacade<NotaRemision> implements NotaRemisionFacade {

    @EJB
    private Persistence persistence;

    public NotaRemisionFacadeImpl() {
        super(NotaRemision.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
