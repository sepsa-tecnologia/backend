package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Permiso;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "PermisoFacade", mappedName = "PermisoFacade")
@Remote(PermisoFacade.class)
public class PermisoFacadeImpl extends AbstractFacade<Permiso> implements PermisoFacade {

    @EJB
    private Persistence persistence;

    public PermisoFacadeImpl() {
        super(Permiso.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
