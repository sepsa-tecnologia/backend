/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.database.sql.build.SqlClean;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "NativeQueryUpdateAction", mappedName = "NativeQueryUpdateAction")
@LocalBean
public class NativeQueryUpdateAction {

    @EJB
    private Persistence persistence;

    public Long executeUpdate(String sql) {
        // Limpiar query.
        sql = SqlClean.sanitizeQuery(sql);

        Query q = persistence.getEntityManager().createNativeQuery(sql);
        return (long) q.executeUpdate();
    }

}
