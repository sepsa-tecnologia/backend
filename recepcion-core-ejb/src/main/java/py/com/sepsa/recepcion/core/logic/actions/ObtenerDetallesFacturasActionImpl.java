/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.collections.maps.CollectionGroupMap;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.DocumentoDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.FacturaDetalleInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerDetallesFacturasAction", mappedName = "ObtenerDetallesFacturasAction")
@Remote(ObtenerDetallesFacturasAction.class)
public class ObtenerDetallesFacturasActionImpl implements ObtenerDetallesFacturasAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @EJB
    private CompletarObservacionesDocumentoDetalleAction completarObservacionesDocumentoDetalleAction;

    @EJB
    private CompletarMonedasDocumentoDetalleAction completarMonedasDocumentoDetalleAction;

    @EJB
    private CompletarProductosEntregablesDocumentoDetalleAction completarProductosEntregablesDocumentoDetalleAction;

    @Override
    public Result<Collection<FacturaDetalleInfo>> obtenerDetallesFactura(Long idFactura) {
        Result<Collection<FacturaDetalleInfo>> result;

        try {
            Long[] ids = {idFactura};
            Result<Map<Long, Collection<FacturaDetalleInfo>>> resDetalles = obtenerDetallesFacturas(ids);
            resDetalles.raise();

            Map<Long, Collection<FacturaDetalleInfo>> detalles = resDetalles.value(Maps.empty());
            Collection<FacturaDetalleInfo> detallesFactura = Maps.get(detalles, idFactura, Lists.empty());

            result = Results.ok()
                    .value(detallesFactura)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo detalles de nota de remisión")
                    .tag("nota_remision.id", idFactura)
                    .build();
        }

        return result;
    }

    @Override
    public Result<Map<Long, Collection<FacturaDetalleInfo>>> obtenerDetallesFacturas(Long[] idFacturas) {
        Result<Map<Long, Collection<FacturaDetalleInfo>>> result;

        try {
            // Control de seguridad.
            if (Assertions.isNullOrEmpty(idFacturas)) {
                return Results.ok()
                        .value(Maps.empty())
                        .build();
            }

            // Criterios de busqueda.
            QueryCriteria criteria = QueryCriteria.select();
            Criterias.from(criteria, buildSubquery(), "tmp");
            Criterias.where(criteria, Conditions.natives().in(FacturaDetalleInfo.COLUMNS.idFactura, idFacturas));

            // Obtener detalles de nota de remision.
            String sql = Criterias.toQuery(criteria);
            Collection<FacturaDetalleInfo> detalles = nativeQuerySelectAction.select(sql, new FacturaDetalleInfo.Mapper());

            // Asociar informacion de monedas.
            completarMonedasDocumentoDetalleAction.completar(detalles);

            // Asociar informacion de entrega de productos.
            completarProductosEntregablesDocumentoDetalleAction.completar(detalles);

            // Asociar informacion de observaciones.
            completarObservacionesDocumentoDetalleAction.completar(detalles);

            // Agrupar registros por orden de compra.
            CollectionGroupMap<Long, FacturaDetalleInfo> groups = Maps.groupBy(detalles, "idFactura", Long.class);

            result = Results.ok()
                    .value(groups.getMap())
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo detalles de notas de remisión")
                    .build();
        }

        return result;
    }

    private String buildSubquery() {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, "recepcion.factura_detalle", "fd");
        Criterias.select(criteria, "fd.id", DocumentoDetalleInfo.COLUMNS.id);
        Criterias.select(criteria, "fd.id_factura", FacturaDetalleInfo.COLUMNS.idFactura);
        Criterias.select(criteria, "fd.id_producto_entrega", FacturaDetalleInfo.COLUMNS.idProductoEntregable);
        Criterias.select(criteria, "fd.cantidad", FacturaDetalleInfo.COLUMNS.cantidad);
        Criterias.select(criteria, "fd.precio", FacturaDetalleInfo.COLUMNS.precio);
        Criterias.select(criteria, "fd.id_moneda", FacturaDetalleInfo.COLUMNS.idMoneda);
        return Criterias.toQuery(criteria);
    }

}
