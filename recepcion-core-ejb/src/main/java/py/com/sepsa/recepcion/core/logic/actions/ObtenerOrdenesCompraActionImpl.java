/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.database.criteria.Condition;
import fa.gs.utils.database.criteria.JoinKind;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.Pagination;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.criteria.Sorting;
import fa.gs.utils.database.sql.build.Builders;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.database.sql.build.SqlLiterals;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.OrdenCompraDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.OrdenCompraInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerOrdenesCompraAction", mappedName = "ObtenerOrdenesCompraAction")
@Remote(ObtenerOrdenesCompraAction.class)
public class ObtenerOrdenesCompraActionImpl implements ObtenerOrdenesCompraAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @EJB
    private NativeQueryCountAction nativeQueryCountAction;

    @EJB
    private ObtenerDetallesOrdenesCompraAction obtenerOrdenesCompraDetallesAction;

    //<editor-fold defaultstate="collapsed" desc="Implementacion de Data Loader">
    @Override
    public Long countItems(Condition[] conditions) {
        QueryCriteria criteria = QueryCriteria.count();
        Criterias.from(criteria, buildSubquery(), "tmp");
        Criterias.where(criteria, conditions);

        String query = Criterias.toQuery(criteria);
        return nativeQueryCountAction.count(query);
    }

    @Override
    public Collection<OrdenCompraInfo> loadItems(Pagination pagination, Condition[] conditions, Sorting[] sortings) {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, buildSubquery(), "tmp");
        Criterias.where(criteria, conditions);
        Criterias.order(criteria, sortings);
        Criterias.limit(criteria, pagination);

        String query = Criterias.toQuery(criteria);
        return nativeQuerySelectAction.select(query, new OrdenCompraInfo.Mapper());
    }
    //</editor-fold>

    @Override
    public Result<OrdenCompraInfo> obtenerOrdenCompra(String nroDocumento, String glnComprador, String glnProveedor) {
        Result<OrdenCompraInfo> result;

        try {
            // Obtener registros.
            Collection<OrdenCompraInfo> ordenes = obtenerOrdenesCompra(new Condition[]{
                Builders.condition().left(OrdenCompraInfo.COLUMNS.nroDocumento).op(Operator.EQUALS).right(SqlLiterals.string(nroDocumento)).build(),
                Builders.condition().left(OrdenCompraInfo.COLUMNS.glnOrigen).op(Operator.EQUALS).right(SqlLiterals.string(glnComprador)).build(),
                Builders.condition().left(OrdenCompraInfo.COLUMNS.glnDestino).op(Operator.EQUALS).right(SqlLiterals.string(glnProveedor)).build()
            });

            // Retornar primer elemento.
            OrdenCompraInfo orden = Lists.first(ordenes);

            result = Results.ok()
                    .value(orden)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo órden de compra")
                    .tag("orden_compra.nro", nroDocumento)
                    .tag("orden_compra.glnOrigen", glnComprador)
                    .tag("orden_compra.glnDestino", glnProveedor)
                    .build();
        }

        return result;
    }

    @Override
    public Result<OrdenCompraInfo> obtenerOrdenCompra(Long idOrdenCompra) {
        Result<OrdenCompraInfo> result;

        try {
            // Obtener registros.
            Collection<OrdenCompraInfo> ordenes = obtenerOrdenesCompra(new Condition[]{
                Builders.condition().left(OrdenCompraInfo.COLUMNS.id).op(Operator.EQUALS).right(idOrdenCompra).build()
            });

            // Retornar primer elemento.
            OrdenCompraInfo orden = Lists.first(ordenes);

            result = Results.ok()
                    .value(orden)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo órden de compra")
                    .tag("orden_compra.id", idOrdenCompra)
                    .build();
        }

        return result;
    }

    @Override
    public Result<Collection<OrdenCompraInfo>> obtenerOrdenesCompra(Long[] idOrdenesCompra) {
        Result<Collection<OrdenCompraInfo>> result;

        try {
            // Control de seguridad.
            if (Assertions.isNullOrEmpty(idOrdenesCompra)) {
                return Results.ok()
                        .value(Lists.<OrdenCompraInfo>empty())
                        .build();
            }

            // Obtener registros.
            Collection<OrdenCompraInfo> ordenes = obtenerOrdenesCompra(new Condition[]{
                Conditions.natives().in(OrdenCompraInfo.COLUMNS.id, idOrdenesCompra)
            });

            result = Results.ok()
                    .value(ordenes)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo órdenes de compra")
                    .build();
        }

        return result;
    }

    private Collection<OrdenCompraInfo> obtenerOrdenesCompra(Condition[] conditions) throws Throwable {
        // Criterios de busqueda.
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, buildSubquery(), "tmp");
        Criterias.where(criteria, conditions);

        // Obtener registros.
        String sql = Criterias.toQuery(criteria);
        Collection<OrdenCompraInfo> ordenes = nativeQuerySelectAction.select(sql, new OrdenCompraInfo.Mapper());

        // Asociar informacion de detalles de orden de compra.
        completarDetallesOrdenCompra(ordenes);

        return ordenes;
    }

    private static String buildSubquery() {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, "recepcion.orden_compra", "oc");
        Criterias.select(criteria, "oc.id", OrdenCompraInfo.COLUMNS.id);
        Criterias.select(criteria, "e.codigo", OrdenCompraInfo.COLUMNS.estadoCodigo);
        Criterias.select(criteria, "oc.id_documento", OrdenCompraInfo.COLUMNS.idDocumento);
        Criterias.select(criteria, "oc.nro_documento", OrdenCompraInfo.COLUMNS.nroDocumento);
        Criterias.select(criteria, "oc.gln_origen", OrdenCompraInfo.COLUMNS.glnOrigen);
        Criterias.select(criteria, "oc.razon_social_origen", OrdenCompraInfo.COLUMNS.razonSocialOrigen);
        Criterias.select(criteria, "oc.ruc_origen", OrdenCompraInfo.COLUMNS.rucOrigen);
        Criterias.select(criteria, "oc.gln_destino", OrdenCompraInfo.COLUMNS.glnDestino);
        Criterias.select(criteria, "oc.razon_social_destino", OrdenCompraInfo.COLUMNS.razonSocialDestino);
        Criterias.select(criteria, "oc.ruc_destino", OrdenCompraInfo.COLUMNS.rucDestino);
        Criterias.select(criteria, "oc.fecha_registro", OrdenCompraInfo.COLUMNS.fechaRegistro);
        Criterias.select(criteria, "oc.fecha_emision", OrdenCompraInfo.COLUMNS.fechaEmision);
        Criterias.select(criteria, "il.nro_contrato", OrdenCompraInfo.COLUMNS.nroContrato);
        Criterias.select(criteria, "il.fecha_contrato", OrdenCompraInfo.COLUMNS.fechaContrato);
        Criterias.select(criteria, "il.nro_resolucion", OrdenCompraInfo.COLUMNS.nroResolucion);
        Criterias.select(criteria, "il.nro_licitacion", OrdenCompraInfo.COLUMNS.nroLicitacion);
        Criterias.select(criteria, "ie.fecha_inicio_plazo_entrega", OrdenCompraInfo.COLUMNS.fechaInicioPlazoEntrega);
        Criterias.select(criteria, "ie.dias_plazo_entrega", OrdenCompraInfo.COLUMNS.diasPlazoEntrega);
        Criterias.select(criteria, "ie.condiciones_especiales", OrdenCompraInfo.COLUMNS.condicionesEspecialesEntrega);
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.estado", "e", "oc.id_estado", Operator.EQUALS, "e.id");
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.informacion_entrega", "ie", "oc.id_informacion_entrega", Operator.EQUALS, "ie.id");
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.informacion_legal", "il", "oc.id_informacion_legal", Operator.EQUALS, "il.id");
        return Criterias.toQuery(criteria);
    }

    private void completarDetallesOrdenCompra(Collection<OrdenCompraInfo> ordenes) throws Throwable {
        // Obtener identificadores de ordenes de compra.
        Long[] idOrdenesCompra = ordenes.stream()
                .map(o -> o.getId())
                .distinct()
                .toArray(Long[]::new);

        // Obtener detalles disponibles.
        Result<Map<Long, Collection<OrdenCompraDetalleInfo>>> resDetalles = obtenerOrdenesCompraDetallesAction.obtenerDetallesOrdenCompra(idOrdenesCompra);
        resDetalles.raise();
        Map<Long, Collection<OrdenCompraDetalleInfo>> detalles = resDetalles.value(Maps.empty());

        // Asociar detalles con ordenes.
        for (OrdenCompraInfo orden : ordenes) {
            Collection<OrdenCompraDetalleInfo> detallesOrden = Maps.get(detalles, orden.getId());
            orden.setDetalles(detallesOrden);
        }
    }

}
