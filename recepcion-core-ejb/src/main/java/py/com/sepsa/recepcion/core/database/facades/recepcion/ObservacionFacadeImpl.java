package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Observacion;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "ObservacionFacade", mappedName = "ObservacionFacade")
@Remote(ObservacionFacade.class)
public class ObservacionFacadeImpl extends AbstractFacade<Observacion> implements ObservacionFacade {

    @EJB
    private Persistence persistence;

    public ObservacionFacadeImpl() {
        super(Observacion.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
