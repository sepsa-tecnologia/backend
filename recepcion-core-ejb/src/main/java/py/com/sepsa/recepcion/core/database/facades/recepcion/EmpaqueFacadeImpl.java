package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Empaque;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "EmpaqueFacade", mappedName = "EmpaqueFacade")
@Remote(EmpaqueFacade.class)
public class EmpaqueFacadeImpl extends AbstractFacade<Empaque> implements EmpaqueFacade {

    @EJB
    private Persistence persistence;

    public EmpaqueFacadeImpl() {
        super(Empaque.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
