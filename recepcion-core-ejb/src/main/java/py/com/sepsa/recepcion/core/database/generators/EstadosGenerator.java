/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.database.generators;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.criteria.crud.Crear;
import fa.gs.utils.database.criteria.crud.Editar;
import fa.gs.utils.database.criteria.crud.Obtener;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.result.simple.Result;
import java.util.Collection;
import java.util.Objects;
import java.util.UUID;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Estado;
import py.com.sepsa.recepcion.core.database.facades.recepcion.EstadoFacade;
import py.com.sepsa.recepcion.core.logic.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
public class EstadosGenerator extends ShardedValuesGenerator<Estado, EstadoEnum> {

    private EstadoFacade facade;

    public EstadosGenerator(UUID shard) {
        super(shard);
        this.facade = Injection.bean(EstadoFacade.class);
    }

    @Override
    public Collection<Estado> generarValores() {
        Collection<Estado> valores = Lists.empty();
        for (EstadoEnum valorEnum : EstadoEnum.values()) {
            Estado valor = obtenerOCrearValor(valorEnum);
            if (valor != null) {
                valores.add(valor);
            }
        }
        return valores;
    }

    @Override
    protected Result<Estado> obtenerValor(EstadoEnum valorEnum, Object... args) {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.where(criteria, Estado.COLUMNS.codigo, Operator.EQUALS, valorEnum.codigo());
        return Obtener.primero(facade, criteria);
    }

    @Override
    protected Result<Estado> crearValor(EstadoEnum valorEnum, Object... args) {
        Estado valor = new Estado();
        fill(valor, valorEnum);
        return Crear.entity(facade, valor);
    }

    @Override
    protected boolean puedeModificar(Estado valor, EstadoEnum valorEnum, Object... args) {
        boolean a = Objects.equals(valor.getCodigo(), valorEnum.codigo());
        boolean b = Objects.equals(valor.getDescripcion(), valorEnum.descripcion());
        return a && !b;
    }

    @Override
    protected Result<Estado> modificarValor(Estado valor, EstadoEnum valorEnum, Object... args) {
        fill(valor, valorEnum);
        return Editar.entity(facade, valor);
    }

    private void fill(Estado valor, EstadoEnum valorEnum) {
        valor.setCodigo(valorEnum.codigo());
        valor.setDescripcion(valorEnum.descripcion());
    }

}
