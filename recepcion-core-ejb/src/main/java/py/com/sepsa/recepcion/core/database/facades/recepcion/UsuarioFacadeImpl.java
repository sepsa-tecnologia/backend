package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Usuario;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "UsuarioFacade", mappedName = "UsuarioFacade")
@Remote(UsuarioFacade.class)
public class UsuarioFacadeImpl extends AbstractFacade<Usuario> implements UsuarioFacade {

    @EJB
    private Persistence persistence;

    public UsuarioFacadeImpl() {
        super(Usuario.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
