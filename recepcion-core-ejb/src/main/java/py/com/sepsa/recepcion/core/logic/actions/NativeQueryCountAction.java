/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.maps.ResultSetMap;
import fa.gs.utils.database.sql.build.SqlClean;
import fa.gs.utils.database.sql.build.SqlLiterals;
import fa.gs.utils.database.utils.ResultSetMapper;
import java.util.Collection;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "NativeQueryCountAction", mappedName = "NativeQueryCountAction")
@LocalBean
public class NativeQueryCountAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    private ResultSetMapper<Long> mapper;

    @PostConstruct
    public void init() {
        this.mapper = new Mapper0();
    }

    public Long count(String sql) {
        // Limpiar query.
        sql = SqlClean.sanitizeQuery(sql);

        // Ejecutar consulta de tipo count.
        Collection<Long> rows = nativeQuerySelectAction.select(sql, mapper);
        return Lists.first(rows);
    }

    private static class Mapper0 extends ResultSetMapper<Long> {

        @Override
        protected Long getEmptyAdaptee() {
            return null;
        }

        @Override
        protected Long adapt(Long arg0, ResultSetMap arg1) {
            return arg1.long0(SqlLiterals.TOTAL_COLUMN_NAME, 0L);
        }

    }

}
