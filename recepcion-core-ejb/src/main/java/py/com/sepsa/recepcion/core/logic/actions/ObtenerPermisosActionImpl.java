/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.criteria.crud.Obtener;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Permiso;
import py.com.sepsa.recepcion.core.database.entities.recepcion.UsuarioPermisos;
import py.com.sepsa.recepcion.core.database.facades.recepcion.PermisoFacade;
import py.com.sepsa.recepcion.core.database.facades.recepcion.UsuarioPermisosFacade;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerPermisosAction", mappedName = "ObtenerPermisosAction")
@Remote(ObtenerPermisosAction.class)
public class ObtenerPermisosActionImpl implements ObtenerPermisosAction {

    @EJB
    private UsuarioPermisosFacade usuarioPermisosFacade;

    @EJB
    private PermisoFacade permisoFacade;

    @Override
    public Result<Collection<Permiso>> obtenerPermisos(Long idUsuario) {
        Result<Collection<Permiso>> result;

        try {
            // Criterios de busqueda.
            QueryCriteria criteria = QueryCriteria.select();
            Criterias.where(criteria, UsuarioPermisos.COLUMNS.idUsuario, Operator.EQUALS, idUsuario);

            // Obtener registros.
            Result<Collection<UsuarioPermisos>> resUsuarioPermisos = Obtener.todos(usuarioPermisosFacade, criteria);
            resUsuarioPermisos.raise();

            // Control de seguridad.
            Collection<UsuarioPermisos> usuarioPermisos = resUsuarioPermisos.value(Lists.empty());
            if (Assertions.isNullOrEmpty(usuarioPermisos)) {
                return Results.ok()
                        .value(usuarioPermisos)
                        .build();
            }

            // Obtener identificadores de permisos.
            Long[] idPermisos = usuarioPermisos
                    .stream()
                    .map(up -> up.getIdPermiso())
                    .distinct()
                    .toArray(Long[]::new);

            // Obtener permisos concretos.
            criteria = QueryCriteria.select();
            Criterias.where(criteria, Conditions.natives().in(Permiso.COLUMNS.id, idPermisos));
            result = Obtener.todos(permisoFacade, criteria);
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo permisos")
                    .tag("usuario.id", idUsuario)
                    .build();
        }

        return result;
    }

}
