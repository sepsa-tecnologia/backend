/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.DocumentoDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.ProductoEntregableInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "CompletarProductosEntregablesDocumentoDetalleAction", mappedName = "CompletarProductosEntregablesDocumentoDetalleAction")
@LocalBean
public class CompletarProductosEntregablesDocumentoDetalleAction {

    @EJB
    private ObtenerProductosEntregablesAction obtenerProductosEntregablesAction;

    public <T extends DocumentoDetalleInfo> void completar(Collection<T> detalles) throws Throwable {
        // Obtener identificadores de productos entregables.
        Long[] idProductos = detalles.stream()
                .map(d -> d.getProductoEntregable().getId())
                .distinct()
                .toArray(Long[]::new);

        // Obtener productos disponibles.
        Result<Collection<ProductoEntregableInfo>> resProductos = obtenerProductosEntregablesAction.obtenerProductosEntragables(idProductos);
        resProductos.raise();
        Collection<ProductoEntregableInfo> productos = resProductos.value(Lists.empty());

        // Asociar productos con detalle correspondiente.
        for (T detalle : detalles) {
            for (ProductoEntregableInfo producto : productos) {
                if (Assertions.equals(detalle.getProductoEntregable().getId(), producto.getId())) {
                    detalle.setProductoEntregable(producto);
                    break;
                }
            }
        }
    }

}
