/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.database.criteria.Condition;
import fa.gs.utils.database.criteria.JoinKind;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.Pagination;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.criteria.Sorting;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.RecepcionDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.RecepcionInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerRecepcionesAction", mappedName = "ObtenerRecepcionesAction")
@Remote(ObtenerRecepcionesAction.class)
public class ObtenerRecepcionesActionImpl implements ObtenerRecepcionesAction {

    @EJB
    private NativeQueryCountAction nativeQueryCountAction;

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @EJB
    private ObtenerDetallesRecepcionesAction obtenerRecepcionDetallesAction;

    //<editor-fold defaultstate="collapsed" desc="Implementacion de Data Loader">
    @Override
    public Long countItems(Condition[] conditions) {
        QueryCriteria criteria = QueryCriteria.count();
        Criterias.from(criteria, buildSubquery(conditions), "tmp");

        String query = Criterias.toQuery(criteria);
        return nativeQueryCountAction.count(query);
    }

    @Override
    public Collection<RecepcionInfo> loadItems(Pagination pagination, Condition[] conditions, Sorting[] sortings) {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, buildSubquery(conditions), "tmp");
        Criterias.order(criteria, sortings);
        Criterias.limit(criteria, pagination);

        String query = Criterias.toQuery(criteria);
        return nativeQuerySelectAction.select(query, RecepcionInfo.MAPPER.INSTANCE);
    }
    //</editor-fold>

    @Override
    public Result<RecepcionInfo> obtenerRecepcion(Long idRecepcion) {
        Result<RecepcionInfo> result;

        try {
            // Obtener nota de remision.
            Long[] ids = {idRecepcion};
            Result<Collection<RecepcionInfo>> resOrdenes = obtenerRecepciones(ids);
            resOrdenes.raise();

            // Retornar primer elemento.
            Collection<RecepcionInfo> remisiones = resOrdenes.value(Lists.empty());
            RecepcionInfo remision = Lists.first(remisiones);

            result = Results.ok()
                    .value(remision)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo nota de remisión")
                    .tag("nota_remision.id", idRecepcion)
                    .build();
        }

        return result;
    }

    @Override
    public Result<Collection<RecepcionInfo>> obtenerRecepciones(Long[] idRecepciones) {
        Result<Collection<RecepcionInfo>> result;

        try {
            // Control de seguridad.
            if (Assertions.isNullOrEmpty(idRecepciones)) {
                return Results.ok()
                        .value(Lists.<RecepcionInfo>empty())
                        .build();
            }

            Condition[] conditions = {
                Conditions.natives().in(RecepcionInfo.COLUMNS.r_id, idRecepciones)
            };

            // Criterio de busqueda.
            QueryCriteria criteria = QueryCriteria.select();
            Criterias.from(criteria, buildSubquery(conditions), "tmp");

            // Obtener registros.
            String sql = Criterias.toQuery(criteria);
            Collection<RecepcionInfo> remisiones = nativeQuerySelectAction.select(sql, RecepcionInfo.MAPPER.INSTANCE);

            // Asociar informacion de detalles de notas de remision.
            completarDetallesRecepcion(remisiones);

            result = Results.ok()
                    .value(remisiones)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo órdenes de compra")
                    .build();
        }

        return result;
    }

    private static String buildSubquery(Condition[] conditions) {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.from(criteria, "recepcion.recepcion", "r");
        Criterias.select(criteria, RecepcionInfo.COLUMNS.r_id, RecepcionInfo.MAPPINGS.id);
        Criterias.select(criteria, RecepcionInfo.COLUMNS.e_estado_codigo, RecepcionInfo.MAPPINGS.estadoCodigo);
        Criterias.select(criteria, RecepcionInfo.COLUMNS.r_id_documento, RecepcionInfo.MAPPINGS.idDocumento);
        Criterias.select(criteria, RecepcionInfo.COLUMNS.r_nro_documento, RecepcionInfo.MAPPINGS.nroDocumento);
        Criterias.select(criteria, "coalesce(nr.id_orden_compra, f.id_orden_compra)", RecepcionInfo.MAPPINGS.idOrdenCompra);
        Criterias.select(criteria, RecepcionInfo.COLUMNS.r_fecha_registro, RecepcionInfo.MAPPINGS.fechaRegistro);
        Criterias.select(criteria, RecepcionInfo.COLUMNS.r_fecha_emision, RecepcionInfo.MAPPINGS.fechaEmision);
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.factura", "f", "f.id", Operator.EQUALS, "r.id_factura");
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.nota_remision", "nr", "nr.id", Operator.EQUALS, "r.id_nota_remision");
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.orden_compra", "oc", "oc.id", Operator.EQUALS, "coalesce(f.id_orden_compra, nr.id_orden_compra)");
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.estado", "e", "e.id", Operator.EQUALS, "r.id_estado");
        Criterias.where(criteria, conditions);
        return Criterias.toQuery(criteria);
    }

    private void completarDetallesRecepcion(Collection<RecepcionInfo> recepciones) throws Throwable {
        // Obtener identificadores de recepciones.
        Long[] idRecepcion = recepciones.stream()
                .map(o -> o.getId())
                .distinct()
                .toArray(Long[]::new);

        // Obtener detalles disponibles.
        Result<Map<Long, Collection<RecepcionDetalleInfo>>> resDetalles = obtenerRecepcionDetallesAction.obtenerDetallesRecepciones(idRecepcion);
        resDetalles.raise();
        Map<Long, Collection<RecepcionDetalleInfo>> detalles = resDetalles.value(Maps.empty());

        // Asociar detalles con remisiones.
        for (RecepcionInfo remision : recepciones) {
            Collection<RecepcionDetalleInfo> detalleRemision = Maps.get(detalles, remision.getId());
            remision.setDetalles(detalleRemision);
        }
    }

}
