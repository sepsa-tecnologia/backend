/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.misc.text.Strings;
import fa.gs.utils.result.simple.Result;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;
import py.com.sepsa.recepcion.core.data.pojos.FacturaInfo;
import py.com.sepsa.recepcion.core.data.pojos.NotaRemisionInfo;
import py.com.sepsa.recepcion.core.logic.CheckEstados;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "DocumentoEstaValidadoAction", mappedName = "DocumentoEstaValidadoAction")
@Remote(DocumentoEstaValidadoAction.class)
public class DocumentoEstaValidadoActionImpl implements DocumentoEstaValidadoAction {

    @EJB
    private ObtenerNotasRemisionAction obtenerNotasRemisionAction;

    @EJB
    private ObtenerFacturasAction obtenerFacturasAction;

    @Override
    public boolean check(Long id, TipoEntidadEnum tipoDocumento) {
        try {
            switch (tipoDocumento) {
                case FACTURA:
                    return facturaEstaValidada(id);
                case NOTA_REMISION:
                    return notaRemisionEstaValidada(id);
                default:
                    throw new UnsupportedOperationException(Strings.format("Tipo de documento '%s' no soportado", tipoDocumento.codigo()));
            }
        } catch (Throwable thr) {
            return false;
        }
    }

    private boolean facturaEstaValidada(Long idFactura) throws Throwable {
        Result<FacturaInfo> resDocumento = obtenerFacturasAction.obtenerFactura(idFactura);
        resDocumento.raise(true);
        FacturaInfo documento = resDocumento.value();
        return CheckEstados.documentoEstaAceptado(documento);
    }

    private boolean notaRemisionEstaValidada(Long idNotaRemision) throws Throwable {
        Result<NotaRemisionInfo> resDocumento = obtenerNotasRemisionAction.obtenerNotaRemision(idNotaRemision);
        resDocumento.raise(true);
        NotaRemisionInfo documento = resDocumento.value();
        return CheckEstados.documentoEstaAceptado(documento);
    }

}
