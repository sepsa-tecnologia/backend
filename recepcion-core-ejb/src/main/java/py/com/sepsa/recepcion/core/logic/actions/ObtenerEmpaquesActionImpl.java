/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.criteria.JoinKind;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.sql.build.Conditions;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.data.pojos.EmpaqueInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerEmpaquesAction", mappedName = "ObtenerEmpaquesAction")
@Remote(ObtenerEmpaquesAction.class)
public class ObtenerEmpaquesActionImpl implements ObtenerEmpaquesAction {

    @EJB
    private NativeQuerySelectAction nativeQuerySelectAction;

    @Override
    public Result<EmpaqueInfo> obtenerEmpaque(Long idEmpaque) {
        Result<EmpaqueInfo> result;

        try {
            // Obtener empaques.
            Long[] idEmpaques = {idEmpaque};
            Result<Collection<EmpaqueInfo>> resEmpaques = obtenerEmpaques(idEmpaques);
            resEmpaques.raise();

            // Retornar primer elemento.
            Collection<EmpaqueInfo> empaques = resEmpaques.value();
            EmpaqueInfo empaque = Lists.first(empaques);

            result = Results.ok()
                    .value(empaque)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo información de empaque")
                    .tag("empaque.id", idEmpaque)
                    .build();
        }

        return result;
    }

    @Override
    public Result<Collection<EmpaqueInfo>> obtenerEmpaques(Long[] idEmpaques) {
        Result<Collection<EmpaqueInfo>> result;

        try {
            // Control de seguridad.
            if (Assertions.isNullOrEmpty(idEmpaques)) {
                return Results.ok()
                        .value(Lists.<EmpaqueInfo>empty())
                        .build();
            }

            // Criterios de busqueda.
            QueryCriteria criteria = QueryCriteria.select();
            Criterias.from(criteria, buildSubquery(), "tmp");
            criteria.where(Conditions.natives().in(EmpaqueInfo.COLUMNS.id, idEmpaques));

            // Obtener registros.
            String query = Criterias.toQuery(criteria);
            Collection<EmpaqueInfo> empaques = nativeQuerySelectAction.select(query, new EmpaqueInfo.Mapper());

            result = Results.ok()
                    .value(empaques)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo información de empaques")
                    .build();
        }

        return result;
    }

    private String buildSubquery() {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.select(criteria, "em.id", EmpaqueInfo.COLUMNS.id);
        Criterias.select(criteria, "te.codigo", EmpaqueInfo.COLUMNS.tipoEmpaqueCodigo);
        Criterias.select(criteria, "em.codigo_sscc", EmpaqueInfo.COLUMNS.codigoSscc);
        Criterias.select(criteria, "em.alto", EmpaqueInfo.COLUMNS.alto);
        Criterias.select(criteria, "em.ancho", EmpaqueInfo.COLUMNS.ancho);
        Criterias.select(criteria, "em.profundo", EmpaqueInfo.COLUMNS.profundo);
        Criterias.select(criteria, "em.peso", EmpaqueInfo.COLUMNS.peso);
        Criterias.from(criteria, "recepcion.empaque", "em");
        Criterias.join(criteria, JoinKind.LEFT, "recepcion.tipo_empaque", "te", "em.id_tipo_empaque", Operator.EQUALS, "te.id");
        return Criterias.toQuery(criteria);
    }

}
