/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.database.generators;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.criteria.crud.Crear;
import fa.gs.utils.database.criteria.crud.Editar;
import fa.gs.utils.database.criteria.crud.Obtener;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.result.simple.Result;
import java.util.Collection;
import java.util.Objects;
import java.util.UUID;
import py.com.sepsa.recepcion.core.data.enums.PermisoEnum;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Permiso;
import py.com.sepsa.recepcion.core.database.facades.recepcion.PermisoFacade;
import py.com.sepsa.recepcion.core.logic.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
public class PermisosGenerator extends ShardedValuesGenerator<Permiso, PermisoEnum> {

    private PermisoFacade facade;

    public PermisosGenerator(UUID shard) {
        super(shard);
        this.facade = Injection.bean(PermisoFacade.class);
    }

    @Override
    public Collection<Permiso> generarValores() {
        Collection<Permiso> valores = Lists.empty();
        for (PermisoEnum valorEnum : PermisoEnum.values()) {
            if (valorEnum != PermisoEnum.ADAPTER) {
                Permiso valor = obtenerOCrearValor(valorEnum);
                if (valor != null) {
                    valores.add(valor);
                }
            }
        }
        return valores;
    }

    @Override
    protected Result<Permiso> obtenerValor(PermisoEnum valorEnum, Object... args) {
        QueryCriteria criteria = QueryCriteria.select();
        Criterias.where(criteria, Permiso.COLUMNS.codigo, Operator.EQUALS, valorEnum.codigo());
        return Obtener.primero(facade, criteria);
    }

    @Override
    protected Result<Permiso> crearValor(PermisoEnum valorEnum, Object... args) {
        Permiso valor = new Permiso();
        fill(valor, valorEnum);
        return Crear.entity(facade, valor);
    }

    @Override
    protected boolean puedeModificar(Permiso valorExistente, PermisoEnum valorEnum, Object... args) {
        boolean a = Objects.equals(valorExistente.getCodigo(), valorEnum.codigo());
        boolean b = Objects.equals(valorExistente.getDescripcion(), valorEnum.descripcion());
        return a && !b;
    }

    @Override
    protected Result<Permiso> modificarValor(Permiso valor, PermisoEnum valorEnum, Object... args) {
        fill(valor, valorEnum);
        return Editar.entity(facade, valor);
    }

    private void fill(Permiso valor, PermisoEnum valorEnum) {
        valor.setCodigo(valorEnum.codigo());
        valor.setDescripcion(valorEnum.descripcion());
    }

}
