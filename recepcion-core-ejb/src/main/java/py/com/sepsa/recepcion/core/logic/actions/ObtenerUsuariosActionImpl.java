/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.core.logic.actions;

import fa.gs.utils.database.criteria.Operator;
import fa.gs.utils.database.criteria.QueryCriteria;
import fa.gs.utils.database.criteria.crud.Obtener;
import fa.gs.utils.database.sql.build.Criterias;
import fa.gs.utils.database.sql.build.SqlLiterals;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Usuario;
import py.com.sepsa.recepcion.core.database.facades.recepcion.UsuarioFacade;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ObtenerUsuariosAction", mappedName = "ObtenerUsuariosAction")
@Remote(ObtenerUsuariosAction.class)
public class ObtenerUsuariosActionImpl implements ObtenerUsuariosAction {

    @EJB
    private UsuarioFacade usuarioFacade;

    @Override
    public Result<Usuario> obtenerUsuario(Long idUsuario) {
        Result<Usuario> result;

        try {
            result = Obtener.pk(usuarioFacade, idUsuario);
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo credenciales de usuario")
                    .tag("usuario.id", idUsuario)
                    .build();
        }

        return result;
    }

    @Override
    public Result<Collection<Usuario>> obtenerUsuarios(String username) {
        return obtenerUsuarios(username, true);
    }

    @Override
    public Result<Collection<Usuario>> obtenerUsuarios(String username, boolean soloActivos) {
        Result<Collection<Usuario>> result;

        try {
            // Criterios de busqueda.
            QueryCriteria criteria = QueryCriteria.select();
            Criterias.where(criteria, Usuario.COLUMNS.nombre, Operator.EQUALS, username);
            if (soloActivos) {
                Criterias.where(criteria, Usuario.COLUMNS.metaEliminado, Operator.EQUALS, SqlLiterals.FALSE_CHAR);
            }

            result = Obtener.todos(usuarioFacade, criteria);
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo credenciales de usuario")
                    .tag("usuario.name", username)
                    .build();
        }

        return result;
    }

}
