package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.TipoEmpaque;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "TipoEmpaqueFacade", mappedName = "TipoEmpaqueFacade")
@Remote(TipoEmpaqueFacade.class)
public class TipoEmpaqueFacadeImpl extends AbstractFacade<TipoEmpaque> implements TipoEmpaqueFacade {

    @EJB
    private Persistence persistence;

    public TipoEmpaqueFacadeImpl() {
        super(TipoEmpaque.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
