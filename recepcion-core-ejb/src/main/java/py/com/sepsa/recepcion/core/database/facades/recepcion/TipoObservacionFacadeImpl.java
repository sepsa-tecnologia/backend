package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.TipoObservacion;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "TipoObservacionFacade", mappedName = "TipoObservacionFacade")
@Remote(TipoObservacionFacade.class)
public class TipoObservacionFacadeImpl extends AbstractFacade<TipoObservacion> implements TipoObservacionFacade {

    @EJB
    private Persistence persistence;

    public TipoObservacionFacadeImpl() {
        super(TipoObservacion.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
