package py.com.sepsa.recepcion.core.database.facades.recepcion;

import fa.gs.utils.database.facades.AbstractFacade;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import py.com.sepsa.recepcion.core.database.entities.recepcion.OrdenCompraDetalle;
import py.com.sepsa.recepcion.core.database.persistence.Persistence;

@Stateless(name = "OrdenCompraDetalleFacade", mappedName = "OrdenCompraDetalleFacade")
@Remote(OrdenCompraDetalleFacade.class)
public class OrdenCompraDetalleFacadeImpl extends AbstractFacade<OrdenCompraDetalle> implements OrdenCompraDetalleFacade {

    @EJB
    private Persistence persistence;

    public OrdenCompraDetalleFacadeImpl() {
        super(OrdenCompraDetalle.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return persistence.getEntityManager();
    }

}
