/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.ingest.utils;

import fa.gs.utils.collections.maps.CollectionGroupMap;
import fa.gs.utils.database.criteria.crud.Crear;
import fa.gs.utils.result.simple.Result;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.edi.core.commons.Data;
import py.com.sepsa.edi.invoice.v2_4.handler.Invoice;
import py.com.sepsa.edi.invoice.v2_4.pojos.InvoiceDetail;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.data.pojos.FacturaInfo;
import py.com.sepsa.recepcion.core.data.pojos.MonedaInfo;
import py.com.sepsa.recepcion.core.data.pojos.ProductoInfo;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Estado;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Factura;
import py.com.sepsa.recepcion.core.database.entities.recepcion.FacturaDetalle;
import py.com.sepsa.recepcion.core.database.entities.recepcion.ProductoEntrega;
import py.com.sepsa.recepcion.core.database.facades.recepcion.FacturaDetalleFacade;
import py.com.sepsa.recepcion.core.database.facades.recepcion.FacturaFacade;
import py.com.sepsa.recepcion.core.database.facades.recepcion.ProductoEntregaFacade;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerFacturasAction;

/**
 *
 * @author Fabio A. González Sosa
 */
public class FacturaUtils {

    private static final ObtenerFacturasAction obtenerFacturaAction;

    private static final FacturaFacade facturaFacade;

    private static final FacturaDetalleFacade facturaDetalleFacade;

    private static final ProductoEntregaFacade productoEntregaFacade;

    static {
        obtenerFacturaAction = Injection.bean(ObtenerFacturasAction.class);
        facturaFacade = Injection.bean(FacturaFacade.class);
        facturaDetalleFacade = Injection.bean(FacturaDetalleFacade.class);
        productoEntregaFacade = Injection.bean(ProductoEntregaFacade.class);
    }

    public static Date fechaEmisionOcAsociada(InvoiceDetail obj) {
        Data data = obj.getOcDate();
        return DataUtils.date(data);
    }

    public static String nroOcAsociada(InvoiceDetail detail) {
        Data data = detail.getOcNum();
        return DataUtils.string(data);
    }

    public static Long obtener(Invoice obj) throws Throwable {
        // Obtener datos principales de cabecera.
        String nroDocumento = DocumentoUtils.nroDocumento(obj);
        String glnComprador = DocumentoUtils.glnComprador(obj);
        String glnProveedor = DocumentoUtils.glnProveedor(obj);

        // Verificar si documento ya existe.
        return obtener(nroDocumento, glnComprador, glnProveedor);
    }

    public static Long obtener(String nroDocumento, String glnComprador, String glnProveedor) {
        Result<FacturaInfo> resFactura = obtenerFacturaAction.obtenerFactura(nroDocumento, glnComprador, glnProveedor);
        if (!resFactura.isFailure()) {
            FacturaInfo factura0 = resFactura.value();
            if (factura0 != null) {
                return factura0.getId();
            }
        }

        return null;
    }

    public static Long obtenerOc(Invoice obj, String nroDocOrdenCompra) {
        // Obtener datos principales de cabecera.
        String nroDocumento = nroDocOrdenCompra;
        String glnComprador = DocumentoUtils.glnComprador(obj);
        String glnProveedor = DocumentoUtils.glnProveedor(obj);

        // Verificar si documento ya existe.
        return OrdenUtils.obtener(nroDocumento, glnComprador, glnProveedor);
    }

    public static Factura registrar(Invoice obj, Long idOrdenCompra, String nroDocOrdenCompra) throws Throwable {
        // Obtener datos de cabecera.
        String nroDocumento = DocumentoUtils.nroDocumento(obj);
        Date fechaEmision = DocumentoUtils.fechaEmision(obj);
        String nroTimbrado = DocumentoUtils.nroTimbrado(obj);
        Date vencimientoTimbrado = DocumentoUtils.vencimientoTimbrado(obj);

        // Obtener estado inicial.
        Estado estado = DocumentoUtils.estado(EstadoEnum.ABIERTO);

        // Crear cabecera de factura.
        Factura factura = new Factura();
        factura.setFechaEmision(fechaEmision);
        factura.setIdEstado(estado.getId());
        factura.setIdOrdenCompra(idOrdenCompra);
        factura.setNroDocumento(nroDocumento);
        factura.setNroTimbrado(nroTimbrado);
        factura.setVencimientoTimbrado(vencimientoTimbrado);

        Result<Factura> resFactura = Crear.entity(facturaFacade, factura);
        resFactura.raise(true);
        return resFactura.value();
    }

    public static FacturaDetalle agregarDetalle(Factura factura, Invoice invoice, InvoiceDetail obj) throws Throwable {
        // Datos principales de detalle.
        BigDecimal cantidad = DocumentoUtils.cantidadPedida(obj);
        BigDecimal monto = DocumentoUtils.precio(obj);

        // Obtener informacion de entrega de producto.
        ProductoEntrega entrega = obtenerEntrega(obj);

        // Obtener informacion de moneda.
        MonedaInfo moneda = DocumentoUtils.moneda(invoice);

        // Crear detalle.
        FacturaDetalle detalle = new FacturaDetalle();
        detalle.setIdFactura(factura.getId());
        detalle.setIdProductoEntrega(entrega.getId());
        detalle.setPrecio(monto.toBigInteger());
        detalle.setIdMoneda(moneda.getId());
        detalle.setCantidad(cantidad.longValue());

        Result<FacturaDetalle> resDetalle = Crear.entity(facturaDetalleFacade, detalle);
        resDetalle.raise(true);
        return resDetalle.value();
    }

    private static ProductoEntrega obtenerEntrega(InvoiceDetail obj) throws Throwable {
        // Obtener producto.
        String gtin = DocumentoUtils.gtin(obj);
        ProductoInfo producto = ProductoUtils.obtenerOCrear(gtin);

        // Registrar entrega.
        ProductoEntrega entrega = new ProductoEntrega();
        entrega.setIdProducto(producto.getId());

        Result<ProductoEntrega> resEntrega = Crear.entity(productoEntregaFacade, entrega);
        resEntrega.raise(true);
        return resEntrega.value();
    }

    public static Map<String, Collection<InvoiceDetail>> agruparDetalles(Collection<InvoiceDetail> detalles) {
        CollectionGroupMap<String, InvoiceDetail> grupos = new CollectionGroupMap<>();
        for (InvoiceDetail detalle : detalles) {
            // Crear clave de agrupacion.
            String nroDocumentoOc = FacturaUtils.nroOcAsociada(detalle);
            grupos.put(nroDocumentoOc, detalle);
        }

        return grupos.getMap();
    }

}
