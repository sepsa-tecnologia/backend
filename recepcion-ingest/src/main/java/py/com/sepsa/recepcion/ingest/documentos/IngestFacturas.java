/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.ingest.documentos;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Ids;
import fa.gs.utils.misc.errors.Errors;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Map;
import py.com.sepsa.edi.invoice.v2_4.handler.Invoice;
import py.com.sepsa.edi.invoice.v2_4.pojos.InvoiceDetail;
import py.com.sepsa.recepcion.core.data.enums.SiediDocumentFormat;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Factura;
import py.com.sepsa.recepcion.core.database.entities.recepcion.FacturaDetalle;
import py.com.sepsa.recepcion.core.logging.AppLoggerFactory;
import py.com.sepsa.recepcion.core.logic.Errnos;
import py.com.sepsa.recepcion.core.logic.configs.SiediConfig;
import py.com.sepsa.recepcion.ingest.AbstractSiediFilesIngester;
import py.com.sepsa.recepcion.ingest.utils.FacturaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
public class IngestFacturas extends AbstractSiediFilesIngester<Invoice> {

    private static final AppLogger log = AppLoggerFactory.workers();

    public IngestFacturas(SiediConfig config) throws IOException {
        super(config);
    }

    @Override
    protected Invoice process(SiediConfig config, File file) throws Throwable {
        SiediDocumentFormat format = config.getFormatoEntrada();

        if (format == SiediDocumentFormat.TXT) {
            byte[] bytes = Files.readAllBytes(file.toPath());
            return Invoice.createInstanceFromTxt(bytes);
        }

        if (format == SiediDocumentFormat.XML) {
            byte[] bytes = Files.readAllBytes(file.toPath());
            return Invoice.createInstanceFromXml(bytes);
        }

        throw Errors.builder()
                .message("Formato de documento de entrada no soportado")
                .build();
    }

    @Override
    protected void doIngest(Invoice obj) throws Throwable {
        // Verificar si documento ya esta registrado.
        Long idFactura = FacturaUtils.obtener(obj);
        if (idFactura != null) {
            return;
        }

        // Ingestion de documento.
        ingest0(obj);
    }

    private Collection<Factura> ingest0(Invoice obj) throws Throwable {
        final String traza = Ids.randomUuid();
        log.inicio(traza);
        try {
            // Coleccion de facturas ingestadas.
            Collection<Factura> facturas = Lists.empty();

            // Agrupar detalles por orden de compra relacionada.
            Map<String, Collection<InvoiceDetail>> detalles = FacturaUtils.agruparDetalles(obj.getDetailList());
            for (Map.Entry<String, Collection<InvoiceDetail>> entry : detalles.entrySet()) {
                // Verificar si existe orden de compra relacionada.
                Long idOrdenCompra = FacturaUtils.obtenerOc(obj, entry.getKey());
                if (idOrdenCompra == null) {
                    throw Errors.builder()
                            .message("Factura no está relacionada a una órden de compra")
                            .errno(Errnos.DOCUMENTO_NO_RELACIONADO_OC)
                            .build();
                }

                // Crear cabecera de documento y agregar detalles.
                Factura factura0 = FacturaUtils.registrar(obj, idOrdenCompra, entry.getKey());
                log.debug().message("factura: %s", factura0.getId()).log();

                // Agregar detalles.            
                for (InvoiceDetail detalle0 : entry.getValue()) {
                    FacturaDetalle detalle = FacturaUtils.agregarDetalle(factura0, obj, detalle0);
                    log.debug().message("  factura detalle: %s", detalle.getId()).log();
                }
                facturas.add(factura0);
            }

            return facturas;
        } catch (Throwable thr) {
            log.error().cause(thr).log();
            throw Errors.builder()
                    .cause(thr)
                    .build();
        } finally {
            log.fin(traza);
        }
    }

}
