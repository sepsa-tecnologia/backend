/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.ingest;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import fa.gs.utils.result.utils.Failure;
import java.io.File;
import java.util.Collection;
import py.com.sepsa.recepcion.ingest.pojos.IngestResult;

/**
 *
 * @author Fabio A. González Sosa
 */
public abstract class AbstractFilesIngester<T> implements FileIngester {

    @Override
    public Collection<File> listFiles(File dir) {
        Collection<File> files = Lists.empty();

        if (dir.exists()) {
            File[] files0 = dir.listFiles();
            for (File file0 : files0) {
                files.add(file0);
            }
        }

        return files;
    }

    @Override
    public Result<Collection<IngestResult>> ingest(Collection<File> files) {
        Result<Collection<IngestResult>> result;

        try {
            Collection<IngestResult> ingestResults = Lists.empty();

            for (File file : files) {
                Failure failure;

                try {
                    T obj = process(file);
                    doIngest(obj);
                    failure = null;
                } catch (Throwable thr) {
                    failure = Failure.builder()
                            .cause(thr)
                            .message("Ocurrió un error procesando archivo '%s'", file.getAbsolutePath())
                            .build();
                }

                IngestResult ingestResult = new IngestResult();
                ingestResult.setFile(file);
                ingestResult.setFailure(failure);
                ingestResults.add(ingestResult);
            }

            result = Results.ok()
                    .value(ingestResults)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error procesando archivos")
                    .build();
        }

        return result;
    }

    protected abstract T process(File file) throws Throwable;

    protected abstract void doIngest(T obj) throws Throwable;

}
