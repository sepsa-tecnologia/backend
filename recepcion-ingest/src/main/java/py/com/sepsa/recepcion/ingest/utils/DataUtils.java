/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.ingest.utils;

import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.Numeric;
import java.math.BigDecimal;
import java.util.Date;
import py.com.sepsa.edi.core.commons.Currency;
import py.com.sepsa.edi.core.commons.Data;
import py.com.sepsa.edi.core.commons.EdiDate;
import py.com.sepsa.recepcion.core.data.enums.MonedaEnum;

/**
 *
 * @author Fabio A. González Sosa
 */
public class DataUtils {

    public static String string(Data data) {
        if (data == null) {
            return "";
        } else {
            Object obj = data.getValue();
            if (obj == null) {
                return "";
            } else {
                return String.valueOf(obj);
            }
        }
    }

    public static Date date(Data data) {
        if (data == null) {
            return null;
        }

        if (data instanceof EdiDate) {
            EdiDate data0 = (EdiDate) data;
            return data0.getValue();
        }

        return null;
    }

    public static BigDecimal number(Data data) {
        if (data != null) {
            Object obj = data.getValue();
            if (obj == null) {
                return null;
            } else {
                return Numeric.wrap(String.valueOf(obj));
            }
        } else {
            return Numeric.CERO;
        }
    }

    public static String codigoMoneda(Data data) {
        if (data == null) {
            return MonedaEnum.GUARANI.codigoAlfabetico();
        }

        if (data instanceof Currency) {
            Currency data0 = (Currency) data;
            String codigoIso = data0.getValue();
            if (Assertions.stringNullOrEmpty(codigoIso)) {
                return MonedaEnum.GUARANI.codigoAlfabetico();
            } else {
                return codigoIso;
            }
        }

        return MonedaEnum.GUARANI.codigoAlfabetico();
    }

}
