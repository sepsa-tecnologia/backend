/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.ingest.documentos;

import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Ids;
import fa.gs.utils.misc.errors.Errors;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import py.com.sepsa.edi.order.v2_4.handler.Order;
import py.com.sepsa.edi.order.v2_4.pojos.OrderDetail;
import py.com.sepsa.recepcion.core.data.enums.SiediDocumentFormat;
import py.com.sepsa.recepcion.core.database.entities.recepcion.OrdenCompra;
import py.com.sepsa.recepcion.core.database.entities.recepcion.OrdenCompraDetalle;
import py.com.sepsa.recepcion.core.logging.AppLoggerFactory;
import py.com.sepsa.recepcion.core.logic.configs.SiediConfig;
import py.com.sepsa.recepcion.ingest.AbstractSiediFilesIngester;
import py.com.sepsa.recepcion.ingest.utils.OrdenUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
public class IngestOrdenes extends AbstractSiediFilesIngester<Order> {

    private static final AppLogger log = AppLoggerFactory.workers();

    public IngestOrdenes(SiediConfig config) throws IOException {
        super(config);
    }

    @Override
    protected Order process(SiediConfig config, File file) throws Throwable {
        SiediDocumentFormat format = config.getFormatoSalida();

        if (format == SiediDocumentFormat.TXT) {
            byte[] bytes = Files.readAllBytes(file.toPath());
            return Order.createInstanceFromTxt(bytes);
        }

        if (format == SiediDocumentFormat.XML) {
            byte[] bytes = Files.readAllBytes(file.toPath());
            return Order.createInstanceFromXml(bytes);
        }

        throw Errors.builder()
                .message("Formato de documento de entrada no soportado")
                .build();
    }

    @Override
    protected void doIngest(final Order obj) throws Throwable {
        // Verificar si documento ya esta registrado.
        Long idOrden = OrdenUtils.obtener(obj);
        if (idOrden != null) {
            return;
        }

        // Ingestion de documento.
        ingest0(obj);
    }

    private OrdenCompra ingest0(final Order obj) throws Throwable {
        final String traza = Ids.randomUuid();
        log.inicio(traza);
        try {
            // Crear cabecera de documento y agregar detalles.
            OrdenCompra orden = OrdenUtils.registrar(obj);
            log.debug().message("orden: %s", orden.getId()).log();
            for (OrderDetail detalle0 : obj.getDetailList()) {
                OrdenCompraDetalle detalle = OrdenUtils.agregarDetalle(orden, detalle0);
                log.debug().message("  orden detalle: %s", detalle.getId()).log();
            }
            return orden;
        } catch (Throwable thr) {
            log.error().cause(thr).log();
            throw Errors.builder()
                    .cause(thr)
                    .build();
        } finally {
            log.fin(traza);
        }
    }

}
