/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.ingest;

import fa.gs.utils.result.simple.Result;
import java.io.File;
import java.util.Collection;
import py.com.sepsa.recepcion.ingest.pojos.IngestResult;

/**
 *
 * @author Fabio A. González Sosa
 */
public interface FileIngester {

    Collection<File> listFiles(File dir);

    Result<Collection<IngestResult>> ingest(Collection<File> files);

}
