/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.ingest.utils;

import fa.gs.utils.database.criteria.crud.Crear;
import fa.gs.utils.result.simple.Result;
import py.com.sepsa.recepcion.core.data.pojos.ProductoInfo;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Producto;
import py.com.sepsa.recepcion.core.database.facades.recepcion.ProductoFacade;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerProductosAction;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ProductoUtils {

    private static final ObtenerProductosAction obtenerProductosAction;

    private static final ProductoFacade productoFacade;

    static {
        obtenerProductosAction = Injection.bean(ObtenerProductosAction.class);
        productoFacade = Injection.bean(ProductoFacade.class);
    }

    public static ProductoInfo obtenerOCrear(String codigoGtinOinterno) throws Throwable {
        // Verificar si producto existe.
        Result<ProductoInfo> resProducto0 = obtenerProductosAction.obtenerProducto(codigoGtinOinterno);
        ProductoInfo producto0 = resProducto0.value(null);
        if (producto0 != null) {
            return producto0;
        }

        // Crear producto.
        Producto producto = new Producto();
        producto.setCodigoGtin(codigoGtinOinterno);
        producto.setCodigoInterno(codigoGtinOinterno);
        producto.setDescripcionCorta("");
        producto.setDescripcionLarga("");
        Result<Producto> resProducto = Crear.entity(productoFacade, producto);
        resProducto.raise();

        resProducto0 = obtenerProductosAction.obtenerProducto(codigoGtinOinterno);
        resProducto0.raise(true);
        return resProducto0.value();
    }

}
