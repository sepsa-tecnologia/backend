/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.ingest;

import java.io.File;
import py.com.sepsa.recepcion.core.logic.configs.SiediConfig;

/**
 *
 * @author Fabio A. González Sosa
 */
public abstract class AbstractSiediFilesIngester<T> extends AbstractFilesIngester<T> {

    private final SiediConfig siediConfig;

    public AbstractSiediFilesIngester(SiediConfig siediConfig) {
        this.siediConfig = siediConfig;
    }

    public SiediConfig getSiediConfig() {
        return siediConfig;
    }

    @Override
    protected final T process(File file) throws Throwable {
        return process(siediConfig, file);
    }

    protected abstract T process(SiediConfig config, File file) throws Throwable;

}
