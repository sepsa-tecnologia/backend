/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.ingest.utils;

import fa.gs.utils.database.criteria.crud.Crear;
import fa.gs.utils.result.simple.Result;
import java.math.BigDecimal;
import java.util.Date;
import py.com.sepsa.edi.order.v2_4.handler.Order;
import py.com.sepsa.edi.order.v2_4.pojos.OrderDetail;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.data.pojos.MonedaInfo;
import py.com.sepsa.recepcion.core.data.pojos.OrdenCompraInfo;
import py.com.sepsa.recepcion.core.data.pojos.ProductoInfo;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Estado;
import py.com.sepsa.recepcion.core.database.entities.recepcion.OrdenCompra;
import py.com.sepsa.recepcion.core.database.entities.recepcion.OrdenCompraDetalle;
import py.com.sepsa.recepcion.core.database.facades.recepcion.OrdenCompraDetalleFacade;
import py.com.sepsa.recepcion.core.database.facades.recepcion.OrdenCompraFacade;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerOrdenesCompraAction;

/**
 *
 * @author Fabio A. González Sosa
 */
public class OrdenUtils {

    private static final ObtenerOrdenesCompraAction obtenerOrdenCompraAction;

    private static final OrdenCompraFacade ordenCompraFacade;

    private static final OrdenCompraDetalleFacade ordenCompraDetalleFacade;

    static {
        obtenerOrdenCompraAction = Injection.bean(ObtenerOrdenesCompraAction.class);
        ordenCompraFacade = Injection.bean(OrdenCompraFacade.class);
        ordenCompraDetalleFacade = Injection.bean(OrdenCompraDetalleFacade.class);
    }

    public static Long obtener(Order obj) throws Throwable {
        // Obtener datos principales de cabecera.
        String nroDocumento = DocumentoUtils.nroDocumento(obj);
        String glnComprador = DocumentoUtils.glnComprador(obj);
        String glnProveedor = DocumentoUtils.glnProveedor(obj);

        // Verificar si documento ya existe.
        return obtener(nroDocumento, glnComprador, glnProveedor);
    }

    public static Long obtener(String nroDocumento, String glnComprador, String glnProveedor) {
        Result<OrdenCompraInfo> resOrdenCompra = obtenerOrdenCompraAction.obtenerOrdenCompra(nroDocumento, glnComprador, glnProveedor);
        if (!resOrdenCompra.isFailure()) {
            OrdenCompraInfo orden0 = resOrdenCompra.value();
            if (orden0 != null) {
                // Documento existe.
                return orden0.getId();
            }
        }

        return null;
    }

    public static OrdenCompra registrar(Order obj) throws Throwable {
        // Obtener datos de cabecera.
        String nroDocumento = DocumentoUtils.nroDocumento(obj);
        Date fechaEmision = DocumentoUtils.fechaEmision(obj);
        String glnOrigen = DocumentoUtils.glnComprador(obj);
        String razonSocialOrigen = DocumentoUtils.razonSocialComprador(obj);
        String rucOrigen = DocumentoUtils.rucComprador(obj);
        String glnDestino = DocumentoUtils.glnProveedor(obj);
        String razonSocialDestino = DocumentoUtils.razonSocialProveedor(obj);
        String rucDestino = DocumentoUtils.rucProveedor(obj);

        // Obtener estado inicial.
        Estado estado = DocumentoUtils.estado(EstadoEnum.ABIERTO);

        // Crear cabecera de orden de compra.
        OrdenCompra orden = new OrdenCompra();
        orden.setFechaEmision(fechaEmision);
        orden.setIdEstado(estado.getId());
        orden.setGlnDestino(glnDestino);
        orden.setGlnOrigen(glnOrigen);
        orden.setNroDocumento(nroDocumento);
        orden.setRazonSocialDestino(razonSocialDestino);
        orden.setRazonSocialOrigen(razonSocialOrigen);
        orden.setRucDestino(rucDestino);
        orden.setRucOrigen(rucOrigen);

        Result<OrdenCompra> resOrden = Crear.entity(ordenCompraFacade, orden);
        resOrden.raise(true);
        return resOrden.value();
    }

    public static OrdenCompraDetalle agregarDetalle(OrdenCompra orden, OrderDetail obj) throws Throwable {
        // Datos principales de detalle.
        String gtin = DocumentoUtils.gtin(obj);
        BigDecimal cantidad = DocumentoUtils.cantidadPedida(obj);
        BigDecimal monto = DocumentoUtils.precio(obj);

        // Obtener informacion de producto.
        ProductoInfo producto = ProductoUtils.obtenerOCrear(gtin);

        // Obtener informacion de moneda.
        MonedaInfo moneda = DocumentoUtils.moneda(obj);

        // Crear detalle.
        OrdenCompraDetalle detalle = new OrdenCompraDetalle();
        detalle.setIdOrdenCompra(orden.getId());
        detalle.setIdProducto(producto.getId());
        detalle.setPrecio(monto.toBigInteger());
        detalle.setIdMoneda(moneda.getId());
        detalle.setCantidad(cantidad.longValue());

        Result<OrdenCompraDetalle> resDetalle = Crear.entity(ordenCompraDetalleFacade, detalle);
        resDetalle.raise(true);
        return resDetalle.value();
    }

}
