/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.ingest.pojos;

import fa.gs.utils.result.utils.Failure;
import java.io.File;

/**
 *
 * @author Fabio A. González Sosa
 */
public class IngestResult {

    private File file;
    private Failure failure;

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Failure getFailure() {
        return failure;
    }

    public void setFailure(Failure failure) {
        this.failure = failure;
    }
    //</editor-fold>

}
