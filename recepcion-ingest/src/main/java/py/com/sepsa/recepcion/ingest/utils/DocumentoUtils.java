/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.ingest.utils;

import fa.gs.utils.misc.Numeric;
import fa.gs.utils.result.simple.Result;
import java.math.BigDecimal;
import java.util.Date;
import py.com.sepsa.edi.core.commons.Data;
import py.com.sepsa.edi.core.commons.DocumentDetail;
import py.com.sepsa.edi.core.commons.Gln;
import py.com.sepsa.edi.core.commons.Number;
import py.com.sepsa.edi.invoice.v2_4.handler.Invoice;
import py.com.sepsa.edi.invoice.v2_4.pojos.InvoiceDetail;
import py.com.sepsa.edi.order.v2_4.handler.Order;
import py.com.sepsa.edi.order.v2_4.pojos.OrderDetail;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.data.pojos.MonedaInfo;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Estado;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerEstadosAction;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerMonedasAction;

/**
 *
 * @author Fabio A. González Sosa
 */
public class DocumentoUtils {

    private static final ObtenerMonedasAction obtenerMonedaAction;

    private static final ObtenerEstadosAction obtenerEstadoAction;

    static {
        obtenerMonedaAction = Injection.bean(ObtenerMonedasAction.class);
        obtenerEstadoAction = Injection.bean(ObtenerEstadosAction.class);
    }

    public static String nroDocumento(Order obj) {
        Data data = obj.getDocNum();
        return DataUtils.string(data);
    }

    public static String nroDocumento(Invoice obj) {
        Data data = obj.getInvNum();
        return DataUtils.string(data);
    }

    public static Date fechaEmision(Order obj) {
        Data data = obj.getDocDate();
        return DataUtils.date(data);
    }

    public static String nroTimbrado(Invoice obj) {
        Data data = obj.getStampNum();
        return DataUtils.string(data);
    }

    public static Date vencimientoTimbrado(Invoice obj) {
        Data data = obj.getStampExpDate();
        return DataUtils.date(data);
    }

    public static Date fechaEmision(Invoice obj) {
        Data data = obj.getDocDate();
        return DataUtils.date(data);
    }

    public static String glnComprador(Order obj) {
        Gln data = obj.getBuyerGln();
        return DataUtils.string(data);
    }

    public static String glnComprador(Invoice obj) {
        Gln data = obj.getBuyerGln();
        return DataUtils.string(data);
    }

    public static String glnComprador(InvoiceDetail obj) {
        Gln data = obj.getOcOwnerGln();
        return DataUtils.string(data);
    }

    public static String razonSocialComprador(Order obj) {
        return "";
    }

    public static String razonSocialComprador(InvoiceDetail obj) {
        Data data = obj.getOcAddIdVal();
        return DataUtils.string(data);
    }

    public static String rucComprador(Order obj) {
        Data data = obj.getBuyerAddIdVal();
        return DataUtils.string(data);
    }

    public static String glnProveedor(Order obj) {
        Gln data = obj.getSellerGln();
        return DataUtils.string(data);
    }

    public static String glnProveedor(Invoice obj) {
        Gln data = obj.getSellerGln();
        return DataUtils.string(data);
    }

    public static String razonSocialProveedor(Order obj) {
        return "";
    }

    public static String rucProveedor(Order obj) {
        Data data = obj.getSellerAddIdVal();
        return DataUtils.string(data);
    }

    public static String rucComprador(InvoiceDetail obj) {
        Data data = obj.getOcAddIdVal();
        return DataUtils.string(data);
    }

    public static String gtin(DocumentDetail obj) {
        Data data = obj.getGtin();
        return DataUtils.string(data);
    }

    public static BigDecimal cantidadPedida(DocumentDetail obj) {
        Data data = obj.getReqQuant();
        return DataUtils.number(data);
    }

    public static BigDecimal precio(OrderDetail obj) {
        return precio(obj.getUpTaxExcluded(), obj.getTaxAmount(), obj.getTaxPercent());
    }

    public static BigDecimal precio(InvoiceDetail obj) {
        return precio(obj.getUpTaxExcluded(), obj.getTaxAmount(), obj.getTaxPercent());
    }

    private static BigDecimal precio(Number upTaxExcluded, Number taxAmount, Number taxPercent) {
        BigDecimal precio = Numeric.CERO;

        // Monto neto sin impuesto.
        Number data = upTaxExcluded;
        if (data != null) {
            precio = Numeric.sum(precio, data.getValueBD());
        }

        // Monto impuesto.
        data = taxAmount;
        if (data != null) {
            precio = Numeric.sum(precio, data.getValueBD());
        } else {
            data = taxPercent;
            if (data != null) {
                // Calcular impuesto en base a porcentaje de impuesto.
                BigDecimal impuesto = Numeric.div(Numeric.mul(precio, data.getValueBD()), Numeric.CIEN);
                impuesto = Numeric.round(impuesto);
                precio = Numeric.sum(precio, impuesto);
            }
        }

        return precio;
    }

    public static MonedaInfo moneda(DocumentDetail obj) throws Throwable {
        String codigoAlfanumerico = DataUtils.codigoMoneda(obj.getUpCurrency());
        return moneda(codigoAlfanumerico);
    }

    public static MonedaInfo moneda(Invoice obj) throws Throwable {
        String codigoAlfanumerico = DataUtils.codigoMoneda(obj.getCurrency());
        return moneda(codigoAlfanumerico);
    }

    private static MonedaInfo moneda(String codigoAlfanumericoONumerico) throws Throwable {
        Result<MonedaInfo> resMoneda = obtenerMonedaAction.obtenerMoneda(codigoAlfanumericoONumerico);
        resMoneda.raise(true);
        return resMoneda.value();
    }

    public static Estado estado(EstadoEnum estado) throws Throwable {
        Result<Estado> resEstado = obtenerEstadoAction.obtenerEstado(estado);
        resEstado.raise(true);
        return resEstado.value();
    }

}
