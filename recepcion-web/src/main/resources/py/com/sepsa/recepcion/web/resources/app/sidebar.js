window.Sidebar = (function (module) {

    var toggleSidebar = function () {
        $(".area-sidebar-left").toggleClass("sidebar-open");
        $(".area-sidebar-right").toggleClass("sidebar-open");
    };

    var init = function () {
        $("#sidebar-toggler").click(function (e) {
            e.preventDefault();
            toggleSidebar();
        });
    };

    module.init = init;
    module.toggleSidebar = toggleSidebar;
    return module;
})(window.Sidebar || {});

