window.Main = (function (module) {

    var initBlockUi = function () {
        $.blockUI.defaults.message = '<h3>Espere por favor ...</h3><br/><div class="loader-gif" />';
        $.blockUI.defaults.blockMsgClass = 'block-ui';
    };

    var init = function () {
        initBlockUi();
    };

    module.init = init;
    return module;
})(window.Main || {});