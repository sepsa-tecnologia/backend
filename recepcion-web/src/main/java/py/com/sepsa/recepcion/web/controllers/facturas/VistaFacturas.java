/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.facturas;

import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.jsfags.jsf.components.impl.table.events.OnExecuteActionEvent;
import fa.gs.utils.jsfags.jsf.components.impl.table.model.TableDataModel;
import fa.gs.utils.misc.text.Strings;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import py.com.sepsa.recepcion.core.data.pojos.FacturaCabeceraInfo;
import py.com.sepsa.recepcion.core.logic.CheckEstados;
import py.com.sepsa.recepcion.web.controllers.ControllerBean;
import py.com.sepsa.recepcion.web.controllers.NavigationBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("vFacturas")
@ViewScoped
public class VistaFacturas extends ControllerBean {

    @Inject
    private NavigationBean navigation;

    private TableDataModel<FacturaCabeceraInfo> model;

    @PostConstruct
    public void init() {
        model = new FacturasTableModel();
        model.loadFirstPage();
    }

    public void verDetalles(OnExecuteActionEvent event, Object item) {
        FacturaCabeceraInfo factura = (FacturaCabeceraInfo) item;
        Jsf.redirect(navigation.outcomeVerDetallesFactura(factura));
    }

    public void validar(OnExecuteActionEvent event, Object item) {
        FacturaCabeceraInfo factura = (FacturaCabeceraInfo) item;
        if (!CheckEstados.documentoEstaPendienteDeValidacion(factura)) {
            // No es necesario validar.
            showAlertMsg("El estado actual de la factura no permite su validación.");
        } else {
            // Iniciar validacion.
            Jsf.redirect(navigation.outcomeValidarFactura(factura));
        }
    }

    private void showAlertMsg(String fmt, Object... args) {
        String msg = Strings.format(fmt, args);
        Jsf.eval("showAlertMsg('%s');", msg);
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public TableDataModel<FacturaCabeceraInfo> getModel() {
        return model;
    }

    public void setModel(TableDataModel<FacturaCabeceraInfo> model) {
        this.model = model;
    }
    //</editor-fold>

}
