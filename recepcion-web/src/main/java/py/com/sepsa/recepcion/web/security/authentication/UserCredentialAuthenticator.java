/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.security.authentication;

import fa.gs.utils.authentication.AbstractUserCredentialsAuthenticator;
import fa.gs.utils.authentication.passwords.PasswordVerifier;
import fa.gs.utils.authentication.passwords.PasswordVerifier_BCRYPT;
import fa.gs.utils.authentication.user.AuthenticationInfo;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Permiso;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Usuario;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerPermisosAction;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerUsuariosAction;

/**
 *
 * @author Fabio A. González Sosa
 */
public class UserCredentialAuthenticator extends AbstractUserCredentialsAuthenticator {

    private final ObtenerUsuariosAction obtenerUsuariosAction;

    private final ObtenerPermisosAction obtenerPermisosAction;

    public UserCredentialAuthenticator() {
        this.obtenerUsuariosAction = Injection.bean(ObtenerUsuariosAction.class);
        this.obtenerPermisosAction = Injection.bean(ObtenerPermisosAction.class);
    }

    @Override
    public final Result<AuthenticationInfo> authenticateUser(Object id) {
        Result<AuthenticationInfo> result = null;

        try {
            // Controlar que exista un valor de identificacion.
            if (id == null) {
                throw Errors.builder()
                        .message("Identifier is null")
                        .build();
            }

            // Controlar que valor de identificacion sea del tipo correcto.
            if (!(id instanceof Long)) {
                throw Errors.builder()
                        .message("Invalid identifier type (%s)", id.getClass().getName())
                        .build();
            }

            Result<Usuario> resUsuario = obtenerUsuariosAction.obtenerUsuario((Long) id);
            resUsuario.raise(true);
            result = obtenerInformacionAutenticacion(resUsuario.value());
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error autenticando credenciales de usuario")
                    .tag("usuario.id", id)
                    .build();
        }

        return result;
    }

    @Override
    public final Result<AuthenticationInfo> authenticateUserCredentials(String username, String password) {
        Result<AuthenticationInfo> result;

        try {
            // Obtener usuarios disponibles.
            Result<Collection<Usuario>> resUsuarios = obtenerUsuariosAction.obtenerUsuarios(username);
            Collection<Usuario> usuarios = resUsuarios.value(Lists.empty());

            // Controlar que existan usuarios.
            if (Assertions.isNullOrEmpty(usuarios)) {
                throw Errors.builder().message("Credenciales inválidas").build();
            }

            // Obtener usuario que concuerde con contraseña indicada.
            Usuario usuario = null;
            PasswordVerifier verifier = new PasswordVerifier_BCRYPT();
            for (Usuario usuario0 : usuarios) {
                if (verifier.verifies(password, usuario0.getContrasenha())) {
                    usuario = usuario0;
                    break;
                }
            }

            result = obtenerInformacionAutenticacion(usuario);
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error autenticando credenciales de usuario")
                    .tag("usuario.nombre", username)
                    .build();
        }

        return result;
    }

    private Result<AuthenticationInfo> obtenerInformacionAutenticacion(Usuario usuario) {
        Result<AuthenticationInfo> result;

        try {
            // Controlar que exista usuario.
            if (usuario == null) {
                throw Errors.builder().message("Credenciales inválidas").build();
            }

            // Obtener permisos, si hubieren.
            Result<Collection<Permiso>> resPermisos = obtenerPermisosAction.obtenerPermisos(usuario.getId());
            resPermisos.raise();
            Collection<Permiso> permisos = resPermisos.value(Lists.empty());

            // Generar informacion de autenticacion.
            UserAuthenticationInfo info = new UserAuthenticationInfo(usuario);
            for (Permiso permiso : permisos) {
                info.addPermiso(permiso);
            }

            result = Results.ok()
                    .value(info)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .build();
        }

        return result;
    }

}
