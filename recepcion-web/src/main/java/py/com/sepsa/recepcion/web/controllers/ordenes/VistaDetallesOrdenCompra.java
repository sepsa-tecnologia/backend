/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.ordenes;

import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.jsfags.jsf.components.impl.table.model.TableDataModel;
import fa.gs.utils.misc.Type;
import fa.gs.utils.result.simple.Result;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import py.com.sepsa.recepcion.core.data.pojos.OrdenCompraDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.OrdenCompraInfo;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerOrdenesCompraAction;
import py.com.sepsa.recepcion.web.controllers.ControllerBean;
import py.com.sepsa.recepcion.web.utils.CollectionTableDataModelWrapper;
import py.com.sepsa.recepcion.web.utils.NavigationIds;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("vDetallesOrdenCompra")
@ViewScoped
public class VistaDetallesOrdenCompra extends ControllerBean {

    private ObtenerOrdenesCompraAction obtenerOrdenesCompraAction;

    private OrdenCompraInfo ordenCompra;

    private TableDataModel<OrdenCompraDetalleInfo> ordenCompraDetallesModel;

    @PostConstruct
    public void init() {
        inyectarBeans();
        inicializarDatos();
        inicializarDatosAuxiliares();
    }

    private void inyectarBeans() {
        obtenerOrdenesCompraAction = Injection.bean(ObtenerOrdenesCompraAction.class);
    }

    private void inicializarDatos() {
        // Obtener identificador desde URL.
        String idParam = Jsf.getRequestParameter("oc", Type.STRING);
        Long id = NavigationIds.unmaskId(idParam);

        // Obtener orden de compra.
        Result<OrdenCompraInfo> resOrden = obtenerOrdenesCompraAction.obtenerOrdenCompra(id);
        this.ordenCompra = resOrden.value(null);
    }

    private void inicializarDatosAuxiliares() {
        this.ordenCompraDetallesModel = new CollectionTableDataModelWrapper<>(ordenCompra.getDetalles());
        this.ordenCompraDetallesModel.loadFirstPage();
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public OrdenCompraInfo getOrdenCompra() {
        return ordenCompra;
    }

    public void setOrdenCompra(OrdenCompraInfo ordenCompra) {
        this.ordenCompra = ordenCompra;
    }

    public TableDataModel<OrdenCompraDetalleInfo> getOrdenCompraDetallesModel() {
        return ordenCompraDetallesModel;
    }

    public void setOrdenCompraDetallesModel(TableDataModel<OrdenCompraDetalleInfo> ordenCompraDetallesModel) {
        this.ordenCompraDetallesModel = ordenCompraDetallesModel;
    }
    //</editor-fold>

}
