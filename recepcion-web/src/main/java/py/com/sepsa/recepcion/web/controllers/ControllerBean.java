/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers;

import fa.gs.utils.authentication.user.AuthenticationInfo;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Ids;
import java.io.Serializable;
import javax.inject.Inject;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import py.com.sepsa.recepcion.core.logging.AppLoggerFactory;
import py.com.sepsa.recepcion.web.security.authentication.UserAuthenticationInfo;

public class ControllerBean implements Serializable {

    protected final String beanId;

    protected final AppLogger log;

    @Inject
    protected NavigationBean navigation;

    public ControllerBean() {
        this.beanId = Ids.randomUuid();
        this.log = AppLoggerFactory.core();
    }

    public UserAuthenticationInfo getAuthenticationContext() {
        UserAuthenticationInfo authenticationContext = null;
        SecurityContext securityContext = SecurityContextHolder.getContext();
        if (securityContext != null) {
            Authentication authentication = securityContext.getAuthentication();
            if (authentication != null) {
                Object obj = authentication.getDetails();
                if (obj != null) {
                    if (obj instanceof AuthenticationInfo) {
                        authenticationContext = (UserAuthenticationInfo) obj;
                    }
                }
            }
        }
        return authenticationContext;
    }

    public String getBeanId() {
        return beanId;
    }

}
