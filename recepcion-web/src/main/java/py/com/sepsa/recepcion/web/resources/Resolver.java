/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.resources;

import fa.gs.utils.jsf.resources.ResourcesResolver;
import javax.faces.application.ResourceHandler;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Resolver extends ResourcesResolver {

    public Resolver(ResourceHandler wrapped) {
        super(wrapped);
        addMatchers();
    }

    private void addMatchers() {
        addResourceMatcher(new RecepcionResourcesMatcher());
    }

}
