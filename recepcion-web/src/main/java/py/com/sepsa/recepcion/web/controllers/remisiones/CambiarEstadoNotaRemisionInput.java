/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.remisiones;

import fa.gs.utils.result.simple.Result;
import java.io.Serializable;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.data.pojos.NotaRemisionInfo;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.CambiarEstadoAction;

/**
 *
 * @author Fabio A. González Sosa
 */
public class CambiarEstadoNotaRemisionInput implements Serializable {

    private NotaRemisionInfo notaRemision;
    private EstadoEnum estado;

    CambiarEstadoNotaRemisionInput() {
        this.notaRemision = null;
        this.estado = EstadoEnum.INDEFINIDO;
    }

    public NotaRemisionInfo tryCommit() throws Throwable {
        // Datos de entrada.
        final Long idNotaRemision = getNotaRemision().getId();
        final EstadoEnum estado = getEstado();

        // Cambiar estado.
        CambiarEstadoAction cambiarEstadoAction = Injection.bean(CambiarEstadoAction.class);
        Result<Void> resCambioEstado = cambiarEstadoAction.cambiarEstadoNotaRemision(idNotaRemision, estado);
        resCambioEstado.raise();

        return notaRemision;
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public NotaRemisionInfo getNotaRemision() {
        return notaRemision;
    }

    public void setNotaRemision(NotaRemisionInfo notaRemision) {
        this.notaRemision = notaRemision;
    }

    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }
    //</editor-fold>    

}
