/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.facturas;

import fa.gs.utils.injection.Jndi;
import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.jsfags.jsf.components.ajax.AjaxEvent;
import fa.gs.utils.jsfags.jsf.components.impl.table.events.OnExecuteActionEvent;
import fa.gs.utils.jsfags.jsf.components.impl.table.model.TableDataModel;
import fa.gs.utils.misc.Type;
import fa.gs.utils.misc.fechas.Fechas;
import fa.gs.utils.misc.text.Strings;
import fa.gs.utils.result.simple.Result;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.data.pojos.FacturaDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.FacturaInfo;
import py.com.sepsa.recepcion.core.data.pojos.ObservacionInfo;
import py.com.sepsa.recepcion.core.data.pojos.OrdenCompraInfo;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerFacturasAction;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerOrdenesCompraAction;
import py.com.sepsa.recepcion.web.controllers.ControllerBean;
import py.com.sepsa.recepcion.web.controllers.NavigationBean;
import py.com.sepsa.recepcion.web.controllers.utils.AgendarEntregaInput;
import py.com.sepsa.recepcion.web.controllers.utils.AgregarObservacionInput;
import py.com.sepsa.recepcion.web.utils.CollectionTableDataModelWrapper;
import py.com.sepsa.recepcion.web.utils.NavigationIds;
import py.com.sepsa.recepcion.web.utils.TxWrapper;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("vValidarFactura")
@ViewScoped
public class VistaValidarFactura extends ControllerBean {

    @Inject
    private NavigationBean navigation;

    private ObtenerFacturasAction obtenerFacturasAction;

    private ObtenerOrdenesCompraAction obtenerOrdenesCompraAction;

    private FacturaInfo factura;

    private OrdenCompraInfo ordenCompra;

    private TableDataModel<FacturaDetalleInfo> facturaDetalleModel;

    private AgregarObservacionInput<FacturaDetalleInfo> agregarObservacionInput;

    private AgendarEntregaInput<FacturaDetalleInfo> agendarEntregaInput;

    private CambiarEstadoFacturaInput cambiarEstadoFacturaInput;

    @PostConstruct
    public void init() {
        inyectarBeans();
        inicializarDatos();
        inicializarDatosAuxiliares();
    }

    private void inyectarBeans() {
        obtenerFacturasAction = Injection.bean(ObtenerFacturasAction.class);
        obtenerOrdenesCompraAction = Injection.bean(ObtenerOrdenesCompraAction.class);
    }

    private void inicializarDatos() {
        // Obtener identificador desde URL.
        String idParam = Jsf.getRequestParameter("f", Type.STRING);
        Long id = NavigationIds.unmaskId(idParam);

        // Obtener factura.
        Result<FacturaInfo> resFactura = obtenerFacturasAction.obtenerFactura(id);
        this.factura = resFactura.value(null);

        // Obtener orden de compra.
        Result<OrdenCompraInfo> resOrden = obtenerOrdenesCompraAction.obtenerOrdenCompra(factura.getIdOrdenCompra());
        this.ordenCompra = resOrden.value(null);
    }

    private void inicializarDatosAuxiliares() {
        // Modelo de carga para detalles de factura.
        this.facturaDetalleModel = new CollectionTableDataModelWrapper<>(factura.getDetalles());
        this.facturaDetalleModel.loadFirstPage();

        // Contenedor de datos para operacion de agregar una nueva observacion.
        this.agregarObservacionInput = new AgregarObservacionInput();

        // Contenedor de datos para operacion de egendamiento de entrega.
        this.agendarEntregaInput = new AgendarEntregaInput();

        // Contenedor de datos para operacion de cambio de estado de factura.
        this.cambiarEstadoFacturaInput = new CambiarEstadoFacturaInput();
    }

    private TxWrapper getTransationWrapper() {
        Result<Object> resBean = Jndi.lookup("java:global/recepcion-web-0.0.1/TxWrapper");
        Object bean = resBean.value(null);
        return (bean != null) ? (TxWrapper) bean : null;
    }

    //<editor-fold defaultstate="collapsed" desc="Operacion: Aceptar factura">
    public void initAceptarFactura(AjaxEvent event) {
        // Preparar datos de entrada.
        cambiarEstadoFacturaInput.setFactura(factura);
        cambiarEstadoFacturaInput.setEstado(EstadoEnum.ACEPTADO);

        // Cambiar estado.
        TxWrapper tx = getTransationWrapper();
        Result<FacturaInfo> resCambioEstado = tx.execute(cambiarEstadoFacturaInput::tryCommit);
        if (resCambioEstado.isFailure()) {
            // Ocurrio un error.
            log.error().failure(resCambioEstado.failure()).log();
            //showErrorMsgAgregarObservacion(resCambioEstado.failure().message());
        } else {
            // Cambio de estado correcto, redireccionar a vista de detalles.
            Jsf.redirect(navigation.outcomeVerDetallesFactura(factura));
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Operacion: Rechazar factura">
    public void initRechazarFactura(AjaxEvent event) {
        // Preparar datos de entrada.
        cambiarEstadoFacturaInput.setFactura(factura);
        cambiarEstadoFacturaInput.setEstado(EstadoEnum.RECHAZADO);

        // Cambiar estado.
        TxWrapper tx = getTransationWrapper();
        Result<FacturaInfo> resCambioEstado = tx.execute(cambiarEstadoFacturaInput::tryCommit);
        if (resCambioEstado.isFailure()) {
            // Ocurrio un error.
            log.error().failure(resCambioEstado.failure()).log();
            //showErrorMsgAgregarObservacion(resCambioEstado.failure().message());
        } else {
            // Cambio de estado correcto, redireccionar a vista de detalles.
            Jsf.redirect(navigation.outcomeVerDetallesFactura(factura));
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Operacion: Agregar nueva observacion">
    public void initAgregarObservacion(OnExecuteActionEvent event, Object item) {
        // Preparar datos de entrada.
        FacturaDetalleInfo detalle = (FacturaDetalleInfo) item;
        agregarObservacionInput.setDetalleDocumento(detalle);
        agregarObservacionInput.setObservacion("");

        // Desplegar cuadro de dialogo para ingreso de datos.
        showAgregarObservacionDialog();
    }

    public void confirmarAgregarObservacion(AjaxEvent event) {
        // Control de entrada de datos.
        String err = agregarObservacionInput.validate();
        if (err != null) {
            showErrorMsgAgregarObservacion(err);
            return;
        }

        // Registrar observacion.
        TxWrapper tx = getTransationWrapper();
        Result<ObservacionInfo> resObservacion = tx.execute(agregarObservacionInput::tryCommit);
        if (resObservacion.isFailure()) {
            // Ocurrio un error.
            log.error().failure(resObservacion.failure()).log();
            showErrorMsgAgregarObservacion(resObservacion.failure().message());
        } else {
            /**
             * Agregar observacion persistida a referencia de entidad, ya que la
             * misma no se refresca explicitamente desde base de datos para
             * ahorrar recursos.
             */
            ObservacionInfo info = resObservacion.value();
            agregarObservacionInput.getDetalleDocumento().getObservaciones().add(info);

            // Actualizar html.
            updateTablaDatos();
            hideAgregarObservacionDialog();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Operacion: Agendar Entrega">
    public void initAgendarEntrega(OnExecuteActionEvent event, Object item) {
        // Preparar datos de entrada.
        FacturaDetalleInfo detalle = (FacturaDetalleInfo) item;
        agendarEntregaInput.setDetalleDocumento(detalle);
        agendarEntregaInput.setFechaEntrega(Fechas.now());

        // Desplegar cuadro de dialogo para ingreso de datos.
        showAgendarEntregaDialog();
    }

    public void confirmarAgendarEntrega(AjaxEvent event) {
        // Control de entrada de datos.
        String err = agendarEntregaInput.validate();
        if (err != null) {
            showErrorMsgAgendarEntrega(err);
            return;
        }

        // Registrar observacion.
        TxWrapper tx = getTransationWrapper();
        Result<Date> resAgendamiento = tx.execute(agendarEntregaInput::tryCommit);
        if (resAgendamiento.isFailure()) {
            // Ocurrio un error.
            log.error().failure(resAgendamiento.failure()).log();
            showErrorMsgAgendarEntrega(resAgendamiento.failure().message());
        } else {
            // Actualizar estado interno de datos.
            FacturaDetalleInfo info = agendarEntregaInput.getDetalleDocumento();
            info.getProductoEntregable().setFechaEntregaAgendada(agendarEntregaInput.getFechaEntrega());

            // Actualizar html.
            updateTablaDatos();
            hideAgendarEntregaDialog();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Actualizacion GUI">
    private void updateTablaDatos() {
        Jsf.update("tabla_datos");
    }

    private void showAgregarObservacionDialog() {
        showDialog("agregar_observacion_dialog");
    }

    private void hideAgregarObservacionDialog() {
        hideDialog("agregar_observacion_dialog");
    }

    private void showErrorMsgAgregarObservacion(String fmt, Object... args) {
        String msg = Strings.format(fmt, args);
        showDialogError("agregar_observacion_dialog_msg_area", msg);
    }

    private void showAgendarEntregaDialog() {
        showDialog("agendar_entrega_dialog");
    }

    private void hideAgendarEntregaDialog() {
        hideDialog("agendar_entrega_dialog");
    }

    private void showErrorMsgAgendarEntrega(String fmt, Object... args) {
        String msg = Strings.format(fmt, args);
        showDialogError("agendar_entrega_dialog_msg_area", msg);
    }

    private void showDialog(String id) {
        Jsf.eval("showDialog('%s');", id);
    }

    private void hideDialog(String id) {
        Jsf.eval("hideDialog('%s');", id);
    }

    private void showDialogError(String id, String msg) {
        Jsf.eval("showDialogErrors('%s', '%s');", id, msg);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public FacturaInfo getFactura() {
        return factura;
    }

    public void setFactura(FacturaInfo factura) {
        this.factura = factura;
    }

    public OrdenCompraInfo getOrdenCompra() {
        return ordenCompra;
    }

    public void setOrdenCompra(OrdenCompraInfo ordenCompra) {
        this.ordenCompra = ordenCompra;
    }

    public TableDataModel<FacturaDetalleInfo> getFacturaDetalleModel() {
        return facturaDetalleModel;
    }

    public void setFacturaDetalleModel(TableDataModel<FacturaDetalleInfo> facturaDetalleModel) {
        this.facturaDetalleModel = facturaDetalleModel;
    }

    public AgregarObservacionInput getAgregarObservacionInput() {
        return agregarObservacionInput;
    }

    public void setAgregarObservacionInput(AgregarObservacionInput agregarObservacionInput) {
        this.agregarObservacionInput = agregarObservacionInput;
    }

    public AgendarEntregaInput getAgendarEntregaInput() {
        return agendarEntregaInput;
    }

    public void setAgendarEntregaInput(AgendarEntregaInput agendarEntregaInput) {
        this.agendarEntregaInput = agendarEntregaInput;
    }
    //</editor-fold>

}
