/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 *
 * @author Fabio A. González Sosa
 */
public class SpringWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

    /**
     * Constructor.
     */
    public SpringWebApplicationInitializer() {
        super(SpringSecurityConfig.class);
    }

}
