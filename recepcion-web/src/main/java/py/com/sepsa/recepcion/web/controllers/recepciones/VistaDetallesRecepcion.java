/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.recepciones;

import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.jsfags.jsf.components.impl.table.model.TableDataModel;
import fa.gs.utils.misc.Type;
import fa.gs.utils.result.simple.Result;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import py.com.sepsa.recepcion.core.data.pojos.OrdenCompraInfo;
import py.com.sepsa.recepcion.core.data.pojos.RecepcionDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.RecepcionInfo;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerOrdenesCompraAction;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerRecepcionesAction;
import py.com.sepsa.recepcion.web.controllers.ControllerBean;
import py.com.sepsa.recepcion.web.utils.CollectionTableDataModelWrapper;
import py.com.sepsa.recepcion.web.utils.NavigationIds;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("vDetallesRecepcion")
@ViewScoped
public class VistaDetallesRecepcion extends ControllerBean {

    private ObtenerRecepcionesAction obtenerRecepcionesAction;

    private ObtenerOrdenesCompraAction obtenerOrdenesCompraAction;

    private RecepcionInfo recepcion;

    private OrdenCompraInfo ordenCompra;

    private TableDataModel<RecepcionDetalleInfo> recepcionDetalleModel;

    @PostConstruct
    public void init() {
        inyectarBeans();
        inicializarDatos();
        inicializarDatosAuxiliares();
    }

    private void inyectarBeans() {
        obtenerRecepcionesAction = Injection.bean(ObtenerRecepcionesAction.class);
        obtenerOrdenesCompraAction = Injection.bean(ObtenerOrdenesCompraAction.class);
    }

    private void inicializarDatos() {
        // Obtener identificador desde URL.
        String idParam = Jsf.getRequestParameter("r", Type.STRING);
        Long id = NavigationIds.unmaskId(idParam);

        // Obtener nota de remision.
        Result<RecepcionInfo> resRemision = obtenerRecepcionesAction.obtenerRecepcion(id);
        this.recepcion = resRemision.value(null);

        // Obtener orden de compra.
        Result<OrdenCompraInfo> resOrden = obtenerOrdenesCompraAction.obtenerOrdenCompra(recepcion.getIdOrdenCompra());
        this.ordenCompra = resOrden.value(null);
    }

    private void inicializarDatosAuxiliares() {
        // Modelo de carga para detalles de nota de remision.
        this.recepcionDetalleModel = new CollectionTableDataModelWrapper<>(recepcion.getDetalles());
        this.recepcionDetalleModel.loadFirstPage();
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public RecepcionInfo getRecepcion() {
        return recepcion;
    }

    public void setRecepcion(RecepcionInfo recepcion) {
        this.recepcion = recepcion;
    }

    public OrdenCompraInfo getOrdenCompra() {
        return ordenCompra;
    }

    public void setOrdenCompra(OrdenCompraInfo ordenCompra) {
        this.ordenCompra = ordenCompra;
    }

    public TableDataModel<RecepcionDetalleInfo> getRecepcionDetalleModel() {
        return recepcionDetalleModel;
    }

    public void setRecepcionDetalleModel(TableDataModel<RecepcionDetalleInfo> recepcionDetalleModel) {
        this.recepcionDetalleModel = recepcionDetalleModel;
    }
    //</editor-fold>

}
