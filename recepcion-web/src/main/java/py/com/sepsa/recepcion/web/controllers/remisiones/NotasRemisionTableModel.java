/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.remisiones;

import fa.gs.utils.jsfags.jsf.components.impl.table.model.AbstractTableDataModel;
import fa.gs.utils.misc.DataLoader;
import py.com.sepsa.recepcion.core.data.pojos.NotaRemisionCabeceraInfo;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerCabecerasNotasRemisionAction;

/**
 *
 * @author Fabio A. González Sosa
 */
public class NotasRemisionTableModel extends AbstractTableDataModel<NotaRemisionCabeceraInfo> {

    private final ObtenerCabecerasNotasRemisionAction loader;

    NotasRemisionTableModel() {
        this.loader = Injection.bean(ObtenerCabecerasNotasRemisionAction.class);
    }

    @Override
    public DataLoader<NotaRemisionCabeceraInfo> getItemsLoader() {
        return loader;
    }

}
