/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.security.authentication;

import com.google.gson.JsonObject;
import fa.gs.utils.authentication.AbstractUserTokenAuthenticator;
import fa.gs.utils.authentication.tokens.TokenDecoder;
import fa.gs.utils.authentication.tokens.jwt.JwtTokenManager;
import fa.gs.utils.authentication.user.AuthenticationInfo;
import fa.gs.utils.misc.Type;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.result.simple.Result;

/**
 *
 * @author Fabio A. González Sosa
 */
public class UserTokenAuthenticator extends AbstractUserTokenAuthenticator<JsonObject> {

    private static final String ID_USUARIO_JSON_PATH = "usuario$id";

    private final TokenDecoder<JsonObject> jwtDecoder;

    public UserTokenAuthenticator() {
        this.jwtDecoder = new JwtTokenManager();
    }

    @Override
    protected TokenDecoder<JsonObject> getTokenDecoder() {
        return jwtDecoder;
    }

    @Override
    protected AuthenticationInfo parseTokenPayload(JsonObject payload) throws Throwable {
        Long idUsuario = JsonResolver.get(payload, ID_USUARIO_JSON_PATH, Type.LONG);

        UserCredentialAuthenticator auth = new UserCredentialAuthenticator();
        Result<AuthenticationInfo> resAuth = auth.authenticateUser(idUsuario);
        resAuth.raise(true);

        return resAuth.value();
    }

    public static String issueToken(AuthenticationInfo info) {
        // Generar payload.
        JsonObjectBuilder builder = JsonObjectBuilder.instance();
        builder.add(ID_USUARIO_JSON_PATH, info.getUsuario().id());

        // Generar token.
        JsonObject json = builder.build();
        return new JwtTokenManager().encodeToken(json);
    }

}
