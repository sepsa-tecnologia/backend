/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.utils;

import fa.gs.utils.crypto.Cipher_TEA1;

/**
 *
 * @author Fabio A. González Sosa
 */
public class NavigationIds {

    /**
     * Array de 32 enteros generados aleatoriamente que sirven como clave para
     * el algoritmo TEA.
     */
    private static final int[] TEA_CIPHER_KEY = {
        0x37ffbab9, 0x3c2cec4f, 0x9f203042, 0x73c0157b,
        0x8519678e, 0x073b8a36, 0xa95f1e96, 0x1c72e7b3,
        0xf76991da, 0x6e452c30, 0x137af54b, 0x94f8f69e,
        0xc3c113d6, 0x1d56b1bc, 0x5550d56f, 0x3291e0d4,
        0x96d7f531, 0x674fdb85, 0x6460e17e, 0x69dd1877,
        0xffcdf8a0, 0x5e44e713, 0xbec49565, 0x6bb0ba07,
        0xc189c66c, 0xae6beb71, 0xecec2bba, 0xdb24b32e,
        0xf39f6622, 0x8e789f64, 0xdd5de8b7, 0xe799ed0e
    };

    private static final Cipher_TEA1 cipher = new Cipher_TEA1();

    public static String maskId(Long value) {
        long masked = cipher.encrypt(value, TEA_CIPHER_KEY);
        return Cipher_TEA1.long2string(masked);
    }

    public static Long unmaskId(String value) {
        long masked = Cipher_TEA1.string2long(value);
        long unmasked = cipher.decrypt(masked, TEA_CIPHER_KEY);
        return unmasked;
    }

}
