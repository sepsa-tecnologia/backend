/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "Starter", mappedName = "Starter")
public class Starter implements Serializable {

    @PostConstruct
    public void init() {
        // Inicializar subsistema de logging.
        setupLogging();
    }

    private void setupLogging() {
        try {
            /**
             * Cargar factoria de loggers. Esta clase contiene un inicializador
             * estatico que se encarga de configurar el subsistema de Log4j2.
             */
            Class.forName("py.com.sepsa.recepcion.core.logging.AppLoggerFactory");
        } catch (Throwable thr) {
            ;
        }
    }

}
