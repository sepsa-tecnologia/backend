/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.security;

import javax.servlet.Filter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 *
 * @author Fabio A. González Sosa
 */
@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String LOGIN_PAGE_URL = "/app/login.xhtml";
    public static final String INDEX_PAGE_URL = "/app/home.xhtml";
    public static final String DENIED_PAGE_URL = "/fragments/denied.xhtml";
    public static final String ERROR_PAGE_URL = "/fragments/error.xhtml";
    public static final String NOT_FOUND_PAGE_URL = "/fragments/unknown.xhtml";

    /**
     * Configuraciones de seguridad
     *
     * @param http Seguridad http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/javax.faces.resource/**").permitAll()
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/fragments/**").denyAll()
                .antMatchers(LOGIN_PAGE_URL).permitAll()
                .antMatchers(DENIED_PAGE_URL).permitAll()
                .antMatchers(NOT_FOUND_PAGE_URL).permitAll()
                .antMatchers(ERROR_PAGE_URL).permitAll()
                .anyRequest().authenticated()
                .and()
                .sessionManagement().maximumSessions(1)
                .and()
                .sessionFixation().changeSessionId()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .and()
                .logout()
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .and()
                .exceptionHandling().accessDeniedPage(DENIED_PAGE_URL)
                .and()
                .headers()
                .frameOptions()
                .sameOrigin()
                .and()
                .formLogin().disable()
                .httpBasic().disable()
                .addFilterBefore(ssoFilter(), UsernamePasswordAuthenticationFilter.class)
                /**
                 * Se deshabilita la proteccion CSRF para evitar la aparicion de
                 * este error cuando se pierde una sesion creada anteriormente.
                 * ERROR:
                 * https://github.com/spring-projects/spring-security/issues/3241.
                 */
                .csrf().disable();
    }

    @Bean
    public Filter ssoFilter() throws Exception {
        Filter filter = new AuthenticationFilter();
        return filter;
    }

}
