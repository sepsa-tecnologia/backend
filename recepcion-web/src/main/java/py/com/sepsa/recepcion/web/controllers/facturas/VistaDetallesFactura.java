/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.facturas;

import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.jsfags.jsf.components.impl.table.model.TableDataModel;
import fa.gs.utils.misc.Type;
import fa.gs.utils.result.simple.Result;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import py.com.sepsa.recepcion.core.data.pojos.FacturaDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.FacturaInfo;
import py.com.sepsa.recepcion.core.data.pojos.OrdenCompraInfo;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerFacturasAction;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerOrdenesCompraAction;
import py.com.sepsa.recepcion.web.controllers.ControllerBean;
import py.com.sepsa.recepcion.web.utils.CollectionTableDataModelWrapper;
import py.com.sepsa.recepcion.web.utils.NavigationIds;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("vDetallesFactura")
@ViewScoped
public class VistaDetallesFactura extends ControllerBean {

    private ObtenerFacturasAction obtenerFacturasAction;

    private ObtenerOrdenesCompraAction obtenerOrdenesCompraAction;

    private FacturaInfo notaRemision;

    private OrdenCompraInfo ordenCompra;

    private TableDataModel<FacturaDetalleInfo> notaRemisionDetalleModel;

    @PostConstruct
    public void init() {
        inyectarBeans();
        inicializarDatos();
        inicializarDatosAuxiliares();
    }

    private void inyectarBeans() {
        obtenerFacturasAction = Injection.bean(ObtenerFacturasAction.class);
        obtenerOrdenesCompraAction = Injection.bean(ObtenerOrdenesCompraAction.class);
    }

    private void inicializarDatos() {
        // Obtener identificador desde URL.
        String idParam = Jsf.getRequestParameter("f", Type.STRING);
        Long id = NavigationIds.unmaskId(idParam);

        // Obtener nota de remision.
        Result<FacturaInfo> resRemision = obtenerFacturasAction.obtenerFactura(id);
        this.notaRemision = resRemision.value(null);

        // Obtener orden de compra.
        Result<OrdenCompraInfo> resOrden = obtenerOrdenesCompraAction.obtenerOrdenCompra(notaRemision.getIdOrdenCompra());
        this.ordenCompra = resOrden.value(null);
    }

    private void inicializarDatosAuxiliares() {
        // Modelo de carga para detalles de nota de remision.
        this.notaRemisionDetalleModel = new CollectionTableDataModelWrapper<>(notaRemision.getDetalles());
        this.notaRemisionDetalleModel.loadFirstPage();
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public FacturaInfo getFactura() {
        return notaRemision;
    }

    public void setFactura(FacturaInfo notaRemision) {
        this.notaRemision = notaRemision;
    }

    public OrdenCompraInfo getOrdenCompra() {
        return ordenCompra;
    }

    public void setOrdenCompra(OrdenCompraInfo ordenCompra) {
        this.ordenCompra = ordenCompra;
    }

    public TableDataModel<FacturaDetalleInfo> getFacturaDetalleModel() {
        return notaRemisionDetalleModel;
    }

    public void setFacturaDetalleModel(TableDataModel<FacturaDetalleInfo> notaRemisionDetalleModel) {
        this.notaRemisionDetalleModel = notaRemisionDetalleModel;
    }
    //</editor-fold>

}
