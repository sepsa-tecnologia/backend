/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.facturas;

import fa.gs.utils.result.simple.Result;
import java.io.Serializable;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.data.pojos.FacturaInfo;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.CambiarEstadoAction;

/**
 *
 * @author Fabio A. González Sosa
 */
public class CambiarEstadoFacturaInput implements Serializable {

    private FacturaInfo factura;
    private EstadoEnum estado;

    CambiarEstadoFacturaInput() {
        this.factura = null;
        this.estado = EstadoEnum.INDEFINIDO;
    }

    public FacturaInfo tryCommit() throws Throwable {
        // Datos de entrada.
        final Long idFactura = getFactura().getId();
        final EstadoEnum estado = getEstado();

        // Cambiar estado.
        CambiarEstadoAction cambiarEstadoAction = Injection.bean(CambiarEstadoAction.class);
        Result<Void> resCambioEstado = cambiarEstadoAction.cambiarEstadoFactura(idFactura, estado);
        resCambioEstado.raise();

        return factura;
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public FacturaInfo getFactura() {
        return factura;
    }

    public void setFactura(FacturaInfo factura) {
        this.factura = factura;
    }

    public EstadoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoEnum estado) {
        this.estado = estado;
    }
    //</editor-fold>    

}
