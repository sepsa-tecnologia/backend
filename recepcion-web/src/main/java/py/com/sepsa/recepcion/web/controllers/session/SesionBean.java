/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.session;

import fa.gs.utils.authentication.tokens.jwt.JwtTokenExtractor;
import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.jsfags.jsf.components.ajax.AjaxEvent;
import fa.gs.utils.result.simple.Result;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.omnifaces.util.Faces;
import org.springframework.security.core.context.SecurityContextHolder;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Usuario;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerUsuariosAction;
import py.com.sepsa.recepcion.web.controllers.ControllerBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = SesionBean.NAME)
@SessionScoped
public class SesionBean extends ControllerBean {

    public static final String NAME = "bSesion";

    private ObtenerUsuariosAction obtenerUsuariosAction;

    private Usuario usuario;

    @PostConstruct
    public void init() {
        log.debug().message("[SESION INICIADA]").log();
        obtenerUsuariosAction = Injection.bean(ObtenerUsuariosAction.class);

        // Obtener informacion de usuario.
        initUsuario();
    }

    private void initUsuario() {
        Long idUsuario = (Long) getAuthenticationContext().getUsuario().id();
        Result<Usuario> resUsuario = obtenerUsuariosAction.obtenerUsuario(idUsuario);
        this.usuario = resUsuario.value(null);
    }

    @PreDestroy
    public void destroy() {
        log.debug().message("[SESION FINALIZADA]").log();
    }

    public void onLogout(AjaxEvent event) {
        try {
            // Invalidar sesiones.
            boolean ok1 = invalidateSpringSession();
            log.debug().message("Sesion JSF invalidada: [%s]", ok1 ? "OK" : "KO").log();

            boolean ok2 = invalidateJsfSession();
            log.debug().message("Sesion Spring invalidada: [%s]", ok2 ? "OK" : "KO").log();

            // Eliminar cookies de sesion.
            Faces.removeResponseCookie(JwtTokenExtractor.COOKIE_NAME, "/");
            Jsf.redirect(navigation.outcomeLogin());
        } catch (Throwable thr) {
            log.error()
                    .cause(thr)
                    .message("Ocurrió un error terminando sesion")
                    .log();
        }
    }

    private boolean invalidateJsfSession() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            return true;
        } catch (Throwable thr) {
            log.error()
                    .cause(thr)
                    .message("Ocurrió un error invalidando sesion")
                    .tag("session.context", "jsf")
                    .log();
            return false;
        }
    }

    private boolean invalidateSpringSession() {
        try {
            SecurityContextHolder.getContext().setAuthentication(null);
            SecurityContextHolder.clearContext();
            return true;
        } catch (Throwable thr) {
            log.error()
                    .cause(thr)
                    .message("Ocurrió un error invalidando sesion")
                    .tag("session.context", "spring")
                    .log();
            return false;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    //</editor-fold>

}
