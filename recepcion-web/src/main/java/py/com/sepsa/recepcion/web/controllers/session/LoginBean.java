/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.session;

import fa.gs.utils.authentication.tokens.jwt.JwtTokenExtractor;
import fa.gs.utils.authentication.user.AuthenticationInfo;
import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.jsfags.jsf.components.ajax.AjaxEvent;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import java.util.Collection;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Usuario;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerUsuariosAction;
import py.com.sepsa.recepcion.web.controllers.ControllerBean;
import py.com.sepsa.recepcion.web.security.authentication.UserCredentialAuthenticator;
import py.com.sepsa.recepcion.web.security.authentication.UserTokenAuthenticator;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = LoginBean.NAME)
@ViewScoped
public class LoginBean extends ControllerBean {

    public static final String NAME = "vLogin";

    /**
     * Nombre de usuario.
     */
    private String username;

    /**
     * Contraseña de acceso.
     */
    private String password;

    private ObtenerUsuariosAction obtenerUsuarioAction;

    @PostConstruct
    public void init() {
        this.username = "";
        this.password = "";
        this.obtenerUsuarioAction = Injection.bean(ObtenerUsuariosAction.class);
    }

    public void onLogin(AjaxEvent event) {
        // Validacion de datos de entrada.
        String err = validate();
        if (!Assertions.stringNullOrEmpty(err)) {
            queueLoginErrorMessage(err);
        } else {
            // Authenticar usuario.
            UserCredentialAuthenticator authenticator = new UserCredentialAuthenticator();
            Result<AuthenticationInfo> resAuth = authenticator.authenticateUserCredentials(username, password);
            if (resAuth.isFailure()) {
                log.debug().failure(resAuth.failure()).log();
                queueLoginErrorMessage("Credenciales inválidas");
            } else {
                // Generar token con informacion de autenticacion.
                AuthenticationInfo info = resAuth.value();
                String token = UserTokenAuthenticator.issueToken(info);

                // Generar cookie con token de autenticacion.
                Faces.addResponseCookie(JwtTokenExtractor.COOKIE_NAME, token, "/", -1);
                Jsf.redirect(navigation.outcomeHome());
            }
        }
    }

    private String validate() {
        if (Assertions.stringNullOrEmpty(username)) {
            return "Se debe especificar un nombre de usuario.";
        }

        if (Assertions.stringNullOrEmpty(password)) {
            return "Se debe especificar una contraseña.";
        }

        return null;
    }

    private String generarTokenJwt() {
        Result<Collection<Usuario>> resUsuarios = obtenerUsuarioAction.obtenerUsuarios(username);
        log.debug().message("USUARIOS: %d", resUsuarios.value().size());

        /*
        JsonObjectBuilder builder = JsonObjectBuilder.instance();
        builder.add("username", username);
        builder.add("password", password);
        JsonObject json = builder.build();

        String token = new JwtTokenManager().encodeToken(json);
        log.debug().message("TOKEN: %s", token).log();
        return token;
         */
        return null;
    }

    private void queueLoginErrorMessage(String fmt, Object... args) {
        String txt = String.format(fmt, args);
        FacesMessage msg = Jsf.msg().error().detail(txt).build();
        Jsf.getFacesContext().addMessage("login-errors", msg);
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    //</editor-fold>

}
