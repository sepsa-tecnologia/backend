/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.utils;

import fa.gs.utils.misc.fechas.Fechas;
import java.io.Serializable;
import java.util.Date;
import py.com.sepsa.recepcion.core.data.pojos.DocumentoDetalleInfo;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.AgendarFechaEntregaAction;

/**
 *
 * @author Fabio A. González Sosa
 */
public class AgendarEntregaInput<T extends DocumentoDetalleInfo> implements Serializable {

    private T detalleDocumento;
    private Date fechaEntrega;

    public AgendarEntregaInput() {
        this.detalleDocumento = null;
        this.fechaEntrega = Fechas.now();
    }

    public String validate() {
        Date now = Fechas.inicioDia(Fechas.now());

        if (fechaEntrega == null) {
            return "El campo fecha no puede ser nulo.";
        }

        if (Fechas.menor(fechaEntrega, now)) {
            return "La fecha de entrega no puede ser anterior al día de hoy.";
        }

        // TODO: COMPARAR CON FECHA DE ENTREGA EN ORDEN.
        return null;
    }

    public Date tryCommit() throws Throwable {
        // Datos de entrada.
        final Long idProductoEntregable0 = getDetalleDocumento().getProductoEntregable().getId();
        final Date fechaEntrega0 = getFechaEntrega();

        // Persistir fecha agendada.
        AgendarFechaEntregaAction agendarFechaEntregaAction = Injection.bean(AgendarFechaEntregaAction.class);
        agendarFechaEntregaAction.agendarFechaEntrega(idProductoEntregable0, fechaEntrega0);

        return fechaEntrega0;
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public T getDetalleDocumento() {
        return detalleDocumento;
    }

    public void setDetalleDocumento(T detalleDocumento) {
        this.detalleDocumento = detalleDocumento;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }
    //</editor-fold>    

}
