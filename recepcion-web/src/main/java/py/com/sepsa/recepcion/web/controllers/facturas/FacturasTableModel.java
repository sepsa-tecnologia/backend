/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.facturas;

import fa.gs.utils.jsfags.jsf.components.impl.table.model.AbstractTableDataModel;
import fa.gs.utils.misc.DataLoader;
import py.com.sepsa.recepcion.core.data.pojos.FacturaCabeceraInfo;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerCabecerasFacturasAction;

/**
 *
 * @author Fabio A. González Sosa
 */
public class FacturasTableModel extends AbstractTableDataModel<FacturaCabeceraInfo> {

    private final ObtenerCabecerasFacturasAction loader;

    FacturasTableModel() {
        this.loader = Injection.bean(ObtenerCabecerasFacturasAction.class);
    }

    @Override
    public DataLoader<FacturaCabeceraInfo> getItemsLoader() {
        return loader;
    }

}
