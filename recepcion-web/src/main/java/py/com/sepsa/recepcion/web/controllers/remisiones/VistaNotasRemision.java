/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.remisiones;

import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.jsfags.jsf.components.impl.table.events.OnExecuteActionEvent;
import fa.gs.utils.jsfags.jsf.components.impl.table.model.TableDataModel;
import fa.gs.utils.misc.text.Strings;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import py.com.sepsa.recepcion.core.data.pojos.NotaRemisionCabeceraInfo;
import py.com.sepsa.recepcion.core.logic.CheckEstados;
import py.com.sepsa.recepcion.web.controllers.ControllerBean;
import py.com.sepsa.recepcion.web.controllers.NavigationBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("vNotasRemision")
@ViewScoped
public class VistaNotasRemision extends ControllerBean {

    @Inject
    private NavigationBean navigation;

    private TableDataModel<NotaRemisionCabeceraInfo> model;

    @PostConstruct
    public void init() {
        model = new NotasRemisionTableModel();
        model.loadFirstPage();
    }

    public void verDetalles(OnExecuteActionEvent event, Object item) {
        NotaRemisionCabeceraInfo notaRemision = (NotaRemisionCabeceraInfo) item;
        Jsf.redirect(navigation.outcomeVerDetallesNotaRemision(notaRemision));
    }

    public void validar(OnExecuteActionEvent event, Object item) {
        NotaRemisionCabeceraInfo notaRemision = (NotaRemisionCabeceraInfo) item;
        if (!CheckEstados.documentoEstaPendienteDeValidacion(notaRemision)) {
            // No es necesario validar.
            showAlertMsg("El estado actual de la nota de remisión no permite su validación.");
        } else {
            // Iniciar validacion.
            Jsf.redirect(navigation.outcomeValidarNotaRemision(notaRemision));
        }
    }

    private void showAlertMsg(String fmt, Object... args) {
        String msg = Strings.format(fmt, args);
        Jsf.eval("showAlertMsg('%s');", msg);
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public TableDataModel<NotaRemisionCabeceraInfo> getModel() {
        return model;
    }

    public void setModel(TableDataModel<NotaRemisionCabeceraInfo> model) {
        this.model = model;
    }
    //</editor-fold>

}
