/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.security.authentication;

import fa.gs.utils.authentication.user.BaseAuthenticationInfo;
import fa.gs.utils.authentication.user.PermisoInfo;
import fa.gs.utils.authentication.user.UsuarioInfo;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Permiso;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Usuario;

/**
 *
 * @author Fabio A. González Sosa
 */
public class UserAuthenticationInfo extends BaseAuthenticationInfo {

    public UserAuthenticationInfo(Usuario usuario) {
        setUsuario(new UsuarioInfo0(usuario));
    }

    public void addPermiso(Permiso permiso) {
        if (permiso != null) {
            PermisoInfo info = new PermisoInfo0(permiso);
            getPermisos().add(info);
        }
    }

    private static class UsuarioInfo0 implements UsuarioInfo {

        private final Usuario usuario;

        private UsuarioInfo0(Usuario usuario) {
            this.usuario = usuario;
        }

        @Override
        public Number id() {
            return usuario.getId();
        }

        @Override
        public String username() {
            return usuario.getNombre();
        }

        @Override
        public String password() {
            return usuario.getContrasenha();
        }

    }

    private static class PermisoInfo0 implements PermisoInfo {

        private final Permiso permiso;

        public PermisoInfo0(Permiso permiso) {
            this.permiso = permiso;
        }

        @Override
        public Number id() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String name() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }
}
