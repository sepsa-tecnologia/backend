/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import py.com.sepsa.recepcion.web.utils.Texts;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("bFormat")
@ApplicationScoped
public class FormatBean {

    private Texts text;

    @PostConstruct
    public void init() {
        this.text = new Texts();
    }

    public String getNd() {
        //return Text.nd();
        return "---";
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public Texts getText() {
        return text;
    }

    public void setText(Texts text) {
        this.text = text;
    }
    //</editor-fold>

}
