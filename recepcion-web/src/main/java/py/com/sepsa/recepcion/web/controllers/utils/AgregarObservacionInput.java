/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.utils;

import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import java.io.Serializable;
import py.com.sepsa.recepcion.core.data.enums.TipoEntidadEnum;
import py.com.sepsa.recepcion.core.data.enums.TipoObservacionEnum;
import py.com.sepsa.recepcion.core.data.pojos.DocumentoDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.ObservacionInfo;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.AgregarObservacionAction;

/**
 *
 * @author Fabio A. González Sosa
 */
public class AgregarObservacionInput<T extends DocumentoDetalleInfo> implements Serializable {

    private T detalleDocumento;
    private String observacion;

    public AgregarObservacionInput() {
        this.detalleDocumento = null;
        this.observacion = "";
    }

    public String validate() {
        if (Assertions.stringNullOrEmpty(observacion)) {
            return "El campo observación no puede ser vacio";
        }

        return null;
    }

    public ObservacionInfo tryCommit() throws Throwable {
        // Datos de entrada.
        final Long idEntidad0 = getDetalleDocumento().getId();
        final TipoEntidadEnum tipoEntidad0 = getDetalleDocumento().getTipoDocumento();
        final String observacion0 = getObservacion();
        final TipoObservacionEnum tipoObservacionEnum0 = TipoObservacionEnum.VALIDACION_MANUAL_PRODUCTO;

        // Persistir observacion.
        AgregarObservacionAction agregarObservacionAction = Injection.bean(AgregarObservacionAction.class);
        Result<ObservacionInfo> resObservacion = agregarObservacionAction.agregarObservacion(idEntidad0, tipoEntidad0, tipoObservacionEnum0, observacion0);
        resObservacion.raise(true);
        return resObservacion.value();
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public T getDetalleDocumento() {
        return detalleDocumento;
    }

    public void setDetalleDocumento(T detalleDocumento) {
        this.detalleDocumento = detalleDocumento;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    //</editor-fold>

}
