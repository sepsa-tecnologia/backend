/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.recepciones;

import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.jsfags.jsf.components.impl.table.events.OnExecuteActionEvent;
import fa.gs.utils.jsfags.jsf.components.impl.table.model.TableDataModel;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import py.com.sepsa.recepcion.core.data.pojos.RecepcionCabeceraInfo;
import py.com.sepsa.recepcion.web.controllers.ControllerBean;
import py.com.sepsa.recepcion.web.controllers.NavigationBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("vRecepciones")
@ViewScoped
public class VistaRecepciones extends ControllerBean {

    @Inject
    private NavigationBean navigation;

    private TableDataModel<RecepcionCabeceraInfo> model;

    @PostConstruct
    public void init() {
        model = new RecepcionesTableModel();
        model.loadFirstPage();
    }

    public void verDetalles(OnExecuteActionEvent event, Object item) {
        RecepcionCabeceraInfo factura = (RecepcionCabeceraInfo) item;
        Jsf.redirect(navigation.outcomeVerDetallesRecepcion(factura));
    }

    public void validar(OnExecuteActionEvent event, Object item) {
        RecepcionCabeceraInfo factura = (RecepcionCabeceraInfo) item;
        /*
        if (!CheckEstados.facturaEstaPendienteDeValidacion(factura)) {
            // No es necesario validar.
            showAlertMsg("El estado actual de la factura no permite su validación.");
        } else {
            // Iniciar validacion.
            navigation.redirectValidarNotaRemision(factura);
        }
         */
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public TableDataModel<RecepcionCabeceraInfo> getModel() {
        return model;
    }

    public void setModel(TableDataModel<RecepcionCabeceraInfo> model) {
        this.model = model;
    }
    //</editor-fold>

}
