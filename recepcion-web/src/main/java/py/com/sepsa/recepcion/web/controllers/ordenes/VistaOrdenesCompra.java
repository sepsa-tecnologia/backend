/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.ordenes;

import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.jsfags.jsf.components.impl.table.events.OnExecuteActionEvent;
import fa.gs.utils.jsfags.jsf.components.impl.table.model.TableDataModel;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import py.com.sepsa.recepcion.core.data.pojos.OrdenCompraInfo;
import py.com.sepsa.recepcion.web.controllers.ControllerBean;
import py.com.sepsa.recepcion.web.controllers.NavigationBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("vOrdenesCompra")
@ViewScoped
public class VistaOrdenesCompra extends ControllerBean {

    @Inject
    private NavigationBean navigation;

    private TableDataModel<OrdenCompraInfo> model;

    @PostConstruct
    public void init() {
        model = new OrdenesCompraTableModel();
        model.loadFirstPage();
    }

    public void verDetalles(OnExecuteActionEvent event, Object item) {
        OrdenCompraInfo ordenCompra = (OrdenCompraInfo) item;
        Jsf.redirect(navigation.outcomeVerDetallesOrdenCompra(ordenCompra));
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public TableDataModel<OrdenCompraInfo> getModel() {
        return model;
    }

    public void setModel(TableDataModel<OrdenCompraInfo> model) {
        this.model = model;
    }
    //</editor-fold>

}
