/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import py.com.sepsa.recepcion.core.data.pojos.DocumentoInfo;
import py.com.sepsa.recepcion.web.security.SpringSecurityConfig;
import py.com.sepsa.recepcion.web.utils.NavigationIds;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("bNavigation")
@ApplicationScoped
public class NavigationBean {

    public String outcomeLogin() {
        return String.format("/%s?faces-redirect=true", SpringSecurityConfig.LOGIN_PAGE_URL);
    }

    public String outcomeHome() {
        return String.format("/%s?faces-redirect=true", SpringSecurityConfig.INDEX_PAGE_URL);
    }

    public String outcomeOrdenesCompraMain() {
        return "/app/ordenes/main.xhtml?faces-redirect=true";
    }

    public String outcomeNotasRemisionMain() {
        return "/app/remisiones/main.xhtml?faces-redirect=true";
    }

    public String outcomeRecepcionesMain() {
        return "/app/recepciones/main.xhtml?faces-redirect=true";
    }

    public String outcomeFacturasMain() {
        return "/app/facturas/main.xhtml?faces-redirect=true";
    }

    public String outcomeVerDetallesOrdenCompra(DocumentoInfo ordenCompra) {
        Long id = ordenCompra.getId();
        return String.format("/app/ordenes/detalles.xhtml?oc=%s&faces-redirect=true", NavigationIds.maskId(id));
    }

    public String outcomeVerDetallesNotaRemision(DocumentoInfo notaRemision) {
        Long idNotaRemision = notaRemision.getId();
        return String.format("/app/remisiones/detalles.xhtml?nr=%s&faces-redirect=true", NavigationIds.maskId(idNotaRemision));
    }

    public String outcomeValidarNotaRemision(DocumentoInfo notaRemision) {
        Long idNotaRemision = notaRemision.getId();
        return String.format("/app/remisiones/validar.xhtml?nr=%s&faces-redirect=true", NavigationIds.maskId(idNotaRemision));
    }

    public String outcomeVerDetallesFactura(DocumentoInfo factura) {
        Long idFactura = factura.getId();
        return String.format("/app/facturas/detalles.xhtml?f=%s&faces-redirect=true", NavigationIds.maskId(idFactura));
    }

    public String outcomeValidarFactura(DocumentoInfo factura) {
        Long idFactura = factura.getId();
        return String.format("/app/facturas/validar.xhtml?f=%s&faces-redirect=true", NavigationIds.maskId(idFactura));
    }

    public String outcomeVerDetallesRecepcion(DocumentoInfo recepcion) {
        Long idRecepcion = recepcion.getId();
        return String.format("/app/recepciones/detalles.xhtml?r=%s&faces-redirect=true", NavigationIds.maskId(idRecepcion));
    }

}
