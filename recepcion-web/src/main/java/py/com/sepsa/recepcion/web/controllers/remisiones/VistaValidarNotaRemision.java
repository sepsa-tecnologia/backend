/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.remisiones;

import fa.gs.utils.injection.Jndi;
import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.jsfags.jsf.components.ajax.AjaxEvent;
import fa.gs.utils.jsfags.jsf.components.impl.table.events.OnExecuteActionEvent;
import fa.gs.utils.jsfags.jsf.components.impl.table.model.TableDataModel;
import fa.gs.utils.misc.Type;
import fa.gs.utils.misc.fechas.Fechas;
import fa.gs.utils.misc.text.Strings;
import fa.gs.utils.result.simple.Result;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import py.com.sepsa.recepcion.core.data.enums.EstadoEnum;
import py.com.sepsa.recepcion.core.data.pojos.NotaRemisionDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.NotaRemisionInfo;
import py.com.sepsa.recepcion.core.data.pojos.ObservacionInfo;
import py.com.sepsa.recepcion.core.data.pojos.OrdenCompraInfo;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerNotasRemisionAction;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerOrdenesCompraAction;
import py.com.sepsa.recepcion.web.controllers.ControllerBean;
import py.com.sepsa.recepcion.web.controllers.NavigationBean;
import py.com.sepsa.recepcion.web.controllers.utils.AgendarEntregaInput;
import py.com.sepsa.recepcion.web.controllers.utils.AgregarObservacionInput;
import py.com.sepsa.recepcion.web.utils.CollectionTableDataModelWrapper;
import py.com.sepsa.recepcion.web.utils.NavigationIds;
import py.com.sepsa.recepcion.web.utils.TxWrapper;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("vValidarNotaRemision")
@ViewScoped
public class VistaValidarNotaRemision extends ControllerBean {

    @Inject
    private NavigationBean navigation;

    private ObtenerNotasRemisionAction obtenerNotasRemisionAction;

    private ObtenerOrdenesCompraAction obtenerOrdenesCompraAction;

    private NotaRemisionInfo notaRemision;

    private OrdenCompraInfo ordenCompra;

    private TableDataModel<NotaRemisionDetalleInfo> notaRemisionDetalleModel;

    private AgregarObservacionInput<NotaRemisionDetalleInfo> agregarObservacionInput;

    private AgendarEntregaInput<NotaRemisionDetalleInfo> agendarEntregaInput;

    private CambiarEstadoNotaRemisionInput cambiarEstadoNotaRemisionInput;

    @PostConstruct
    public void init() {
        inyectarBeans();
        inicializarDatos();
        inicializarDatosAuxiliares();
    }

    private void inyectarBeans() {
        obtenerNotasRemisionAction = Injection.bean(ObtenerNotasRemisionAction.class);
        obtenerOrdenesCompraAction = Injection.bean(ObtenerOrdenesCompraAction.class);
    }

    private void inicializarDatos() {
        // Obtener identificador desde URL.
        String idParam = Jsf.getRequestParameter("nr", Type.STRING);
        Long id = NavigationIds.unmaskId(idParam);

        // Obtener nota de remision.
        Result<NotaRemisionInfo> resRemision = obtenerNotasRemisionAction.obtenerNotaRemision(id);
        this.notaRemision = resRemision.value(null);

        // Obtener orden de compra.
        Result<OrdenCompraInfo> resOrden = obtenerOrdenesCompraAction.obtenerOrdenCompra(notaRemision.getIdOrdenCompra());
        this.ordenCompra = resOrden.value(null);
    }

    private void inicializarDatosAuxiliares() {
        // Modelo de carga para detalles de nota de remision.
        this.notaRemisionDetalleModel = new CollectionTableDataModelWrapper<>(notaRemision.getDetalles());
        this.notaRemisionDetalleModel.loadFirstPage();

        // Contenedor de datos para operacion de agregar una nueva observacion.
        this.agregarObservacionInput = new AgregarObservacionInput();

        // Contenedor de datos para operacion de egendamiento de entrega.
        this.agendarEntregaInput = new AgendarEntregaInput();

        // Contenedor de datos para operacion de cambio de estado de nota de remision.
        this.cambiarEstadoNotaRemisionInput = new CambiarEstadoNotaRemisionInput();
    }

    private TxWrapper getTransationWrapper() {
        Result<Object> resBean = Jndi.lookup("java:global/recepcion-web-0.0.1/TxWrapper");
        Object bean = resBean.value(null);
        return (bean != null) ? (TxWrapper) bean : null;
    }

    //<editor-fold defaultstate="collapsed" desc="Operacion: Aceptar nota de remision">
    public void initAceptarNotaRemision(AjaxEvent event) {
        // Preparar datos de entrada.
        cambiarEstadoNotaRemisionInput.setNotaRemision(notaRemision);
        cambiarEstadoNotaRemisionInput.setEstado(EstadoEnum.ACEPTADO);

        // Cambiar estado.
        TxWrapper tx = getTransationWrapper();
        Result<NotaRemisionInfo> resCambioEstado = tx.execute(cambiarEstadoNotaRemisionInput::tryCommit);
        if (resCambioEstado.isFailure()) {
            // Ocurrio un error.
            log.error().failure(resCambioEstado.failure()).log();
            //showErrorMsgAgregarObservacion(resCambioEstado.failure().message());
        } else {
            // Cambio de estado correcto, redireccionar a vista de detalles.
            Jsf.redirect(navigation.outcomeVerDetallesNotaRemision(notaRemision));
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Operacion: Rechazar nota de remision">
    public void initRechazarNotaRemision(AjaxEvent event) {
        // Preparar datos de entrada.
        cambiarEstadoNotaRemisionInput.setNotaRemision(notaRemision);
        cambiarEstadoNotaRemisionInput.setEstado(EstadoEnum.RECHAZADO);

        // Cambiar estado.
        TxWrapper tx = getTransationWrapper();
        Result<NotaRemisionInfo> resCambioEstado = tx.execute(cambiarEstadoNotaRemisionInput::tryCommit);
        if (resCambioEstado.isFailure()) {
            // Ocurrio un error.
            log.error().failure(resCambioEstado.failure()).log();
            //showErrorMsgAgregarObservacion(resCambioEstado.failure().message());
        } else {
            // Cambio de estado correcto, redireccionar a vista de detalles.
            Jsf.redirect(navigation.outcomeVerDetallesNotaRemision(notaRemision));
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Operacion: Agregar nueva observacion">
    public void initAgregarObservacion(OnExecuteActionEvent event, Object item) {
        // Preparar datos de entrada.
        NotaRemisionDetalleInfo detalle = (NotaRemisionDetalleInfo) item;
        agregarObservacionInput.setDetalleDocumento(detalle);
        agregarObservacionInput.setObservacion("");

        // Desplegar cuadro de dialogo para ingreso de datos.
        showAgregarObservacionDialog();
    }

    public void confirmarAgregarObservacion(AjaxEvent event) {
        // Control de entrada de datos.
        String err = agregarObservacionInput.validate();
        if (err != null) {
            showErrorMsgAgregarObservacion(err);
            return;
        }

        // Registrar observacion.
        TxWrapper tx = getTransationWrapper();
        Result<ObservacionInfo> resObservacion = tx.execute(agregarObservacionInput::tryCommit);
        if (resObservacion.isFailure()) {
            // Ocurrio un error.
            log.error().failure(resObservacion.failure()).log();
            showErrorMsgAgregarObservacion(resObservacion.failure().message());
        } else {
            /**
             * Agregar observacion persistida a referencia de entidad, ya que la
             * misma no se refresca explicitamente desde base de datos para
             * ahorrar recursos.
             */
            ObservacionInfo info = resObservacion.value();
            agregarObservacionInput.getDetalleDocumento().getObservaciones().add(info);

            // Actualizar html.
            updateTablaDatos();
            hideAgregarObservacionDialog();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Operacion: Agendar Entrega">
    public void initAgendarEntrega(OnExecuteActionEvent event, Object item) {
        // Preparar datos de entrada.
        NotaRemisionDetalleInfo detalle = (NotaRemisionDetalleInfo) item;
        agendarEntregaInput.setDetalleDocumento(detalle);
        agendarEntregaInput.setFechaEntrega(Fechas.now());

        // Desplegar cuadro de dialogo para ingreso de datos.
        showAgendarEntregaDialog();
    }

    public void confirmarAgendarEntrega(AjaxEvent event) {
        // Control de entrada de datos.
        String err = agendarEntregaInput.validate();
        if (err != null) {
            showErrorMsgAgendarEntrega(err);
            return;
        }

        // Registrar observacion.
        TxWrapper tx = getTransationWrapper();
        Result<Date> resAgendamiento = tx.execute(agendarEntregaInput::tryCommit);
        if (resAgendamiento.isFailure()) {
            // Ocurrio un error.
            log.error().failure(resAgendamiento.failure()).log();
            showErrorMsgAgendarEntrega(resAgendamiento.failure().message());
        } else {
            // Actualizar estado interno de datos.
            NotaRemisionDetalleInfo info = agendarEntregaInput.getDetalleDocumento();
            info.getProductoEntregable().setFechaEntregaAgendada(agendarEntregaInput.getFechaEntrega());

            // Actualizar html.
            updateTablaDatos();
            hideAgendarEntregaDialog();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Actualizacion GUI">
    private void updateTablaDatos() {
        Jsf.update("tabla_datos");
    }

    private void showAgregarObservacionDialog() {
        showDialog("agregar_observacion_dialog");
    }

    private void hideAgregarObservacionDialog() {
        hideDialog("agregar_observacion_dialog");
    }

    private void showErrorMsgAgregarObservacion(String fmt, Object... args) {
        String msg = Strings.format(fmt, args);
        showDialogError("agregar_observacion_dialog_msg_area", msg);
    }

    private void showAgendarEntregaDialog() {
        showDialog("agendar_entrega_dialog");
    }

    private void hideAgendarEntregaDialog() {
        hideDialog("agendar_entrega_dialog");
    }

    private void showErrorMsgAgendarEntrega(String fmt, Object... args) {
        String msg = Strings.format(fmt, args);
        showDialogError("agendar_entrega_dialog_msg_area", msg);
    }

    private void showDialog(String id) {
        Jsf.eval("showDialog('%s');", id);
    }

    private void hideDialog(String id) {
        Jsf.eval("hideDialog('%s');", id);
    }

    private void showDialogError(String id, String msg) {
        Jsf.eval("showDialogErrors('%s', '%s');", id, msg);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public NotaRemisionInfo getNotaRemision() {
        return notaRemision;
    }

    public void setNotaRemision(NotaRemisionInfo notaRemision) {
        this.notaRemision = notaRemision;
    }

    public OrdenCompraInfo getOrdenCompra() {
        return ordenCompra;
    }

    public void setOrdenCompra(OrdenCompraInfo ordenCompra) {
        this.ordenCompra = ordenCompra;
    }

    public TableDataModel<NotaRemisionDetalleInfo> getNotaRemisionDetalleModel() {
        return notaRemisionDetalleModel;
    }

    public void setNotaRemisionDetalleModel(TableDataModel<NotaRemisionDetalleInfo> notaRemisionDetalleModel) {
        this.notaRemisionDetalleModel = notaRemisionDetalleModel;
    }

    public AgregarObservacionInput getAgregarObservacionInput() {
        return agregarObservacionInput;
    }

    public void setAgregarObservacionInput(AgregarObservacionInput agregarObservacionInput) {
        this.agregarObservacionInput = agregarObservacionInput;
    }

    public AgendarEntregaInput getAgendarEntregaInput() {
        return agendarEntregaInput;
    }

    public void setAgendarEntregaInput(AgendarEntregaInput agendarEntregaInput) {
        this.agendarEntregaInput = agendarEntregaInput;
    }
    //</editor-fold>

}
