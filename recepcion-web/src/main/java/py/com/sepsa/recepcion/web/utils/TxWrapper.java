/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.utils;

import fa.gs.utils.injection.Jndi;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.io.Serializable;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.UserTransaction;
import py.com.sepsa.recepcion.core.logging.AppLoggerFactory;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "TxWrapper", mappedName = "TxWrapper")
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TxWrapper implements Serializable {

    public static final AppLogger log = AppLoggerFactory.core();

    private UserTransaction getJTAUserTransaction() {
        Result<Object> resObj = Jndi.lookup("java:comp/UserTransaction");
        Object obj = resObj.value(null);
        return (obj != null) ? (UserTransaction) obj : null;
    }

    /**
     * Ejecuta un bloque de ejecución arbitrario.
     *
     * @param <T> Tipo de resultado primario manejado dentro del bloque de
     * ejecución.
     * @param executable Abstracción del bloque a ejecutar.
     * @return Resultado de la operacion.
     */
    public <T> Result<T> execute(TxWrapper.Executable<T> executable) {
        // Control de seguridad.
        if (executable == null) {
            throw new IllegalArgumentException("El código de ejecución no puede ser nulo.");
        }

        UserTransaction tx = getJTAUserTransaction();
        if (tx == null) {
            throw new IllegalStateException();
        }

        Result<T> result;

        try {
            // Iniciar transaccion.
            log.debug("[TX] iniciando transacción");
            tx.begin();
            try {
                // Ejectura bloque de codigo.
                log.debug("[TX] ejecutando transacción");
                T obj = executable.execute();

                // Confirmar (commitear) cambios realizados.
                log.debug("[TX] finalizando transacción");
                tx.commit();

                result = Results.ok()
                        .value(obj)
                        .build();
            } catch (Throwable thr) {
                // Desechar (rollback) cambios realizados.
                log.error("[TX] desechando transacción");
                tx.rollback();

                result = Results.ko()
                        .cause(thr)
                        .message("Ocurrió un error durante ejecución de bloque de código")
                        .build();

                Errors.dump(System.err, thr);
            }
        } catch (Throwable thr) {
            // No se pudo iniciar la transaccion.
            log.fatal(thr, "[TX] desechando ejecución");
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error durante ejecución de transacción")
                    .build();

            Errors.dump(System.err, thr);
        }

        return result;
    }

    /**
     * Interface que define el comportamiento esperado de un bloque de código a
     * ser ejecutado.
     *
     * @param <T> Tipo de resultado primario manejado dentro del bloque de
     * ejecución.
     */
    public interface Executable<T> {

        /**
         * Metodo que implementa la lógica de ejecución del bloque.
         *
         * @return Resultado de la ejecución, si hubiere.
         * @throws Throwable Por si ocurre cualquier error.
         */
        T execute() throws Throwable;
    }

}
