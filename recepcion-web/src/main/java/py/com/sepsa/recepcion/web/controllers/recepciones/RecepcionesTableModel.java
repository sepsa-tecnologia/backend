/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.controllers.recepciones;

import fa.gs.utils.jsfags.jsf.components.impl.table.model.AbstractTableDataModel;
import fa.gs.utils.misc.DataLoader;
import py.com.sepsa.recepcion.core.data.pojos.RecepcionCabeceraInfo;
import py.com.sepsa.recepcion.core.logic.Injection;
import py.com.sepsa.recepcion.core.logic.actions.ObtenerCabecerasRecepcionesAction;

/**
 *
 * @author Fabio A. González Sosa
 */
public class RecepcionesTableModel extends AbstractTableDataModel<RecepcionCabeceraInfo> {

    private final ObtenerCabecerasRecepcionesAction loader;

    RecepcionesTableModel() {
        this.loader = Injection.bean(ObtenerCabecerasRecepcionesAction.class);
    }

    @Override
    public DataLoader<RecepcionCabeceraInfo> getItemsLoader() {
        return loader;
    }

}
