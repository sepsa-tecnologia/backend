package py.com.sepsa.recepcion.web.resources;

import fa.gs.utils.jsf.resources.ResourceMatcher;
import fa.gs.utils.jsf.resources.ResourcesResolver;
import fa.gs.utils.jsf.resources.types.CacheableResourceDecorator;
import fa.gs.utils.jsf.resources.types.EmptyResource;
import fa.gs.utils.jsf.resources.types.JarResource;
import java.util.Map;
import javax.faces.application.Resource;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Fabio A. González Sosa
 */
public class RecepcionResourcesMatcher implements ResourceMatcher {

    public static final String LIBRARY_RECEPCION = "recepcion.static";

    public static final String LIBRARY_BASE_PATH = "/py/com/sepsa/recepcion/web/resources";

    @Override
    public String getLibraryName() {
        return LIBRARY_RECEPCION;
    }

    @Override
    public Resource create(ResourcesResolver resolver, String resourceName, String libraryName, Map<String, String> params) {
        Resource resource = new JarResource(libraryName, resourceName, LIBRARY_BASE_PATH);
        resource = decorateContentType(resource);
        resource = decorateCacheable(resource);
        return resource;
    }

    private Resource decorateContentType(Resource resource) {
        String libraryName = resource.getLibraryName();
        String resourceName = resource.getResourceName();

        if (resourceName.endsWith(".js")) {
            resource.setContentType("text/javascript");
        } else if (resourceName.endsWith(".css")) {
            resource.setContentType("text/css");
        } else if (resourceName.endsWith(".ico")) {
            resource.setContentType("image/x-icon");
        } else if (resourceName.endsWith(".jpg") || resourceName.endsWith(".jpeg")) {
            resource.setContentType("image/jpeg");
        } else if (resourceName.endsWith(".png")) {
            resource.setContentType("image/png");
        } else if (resourceName.endsWith(".gif")) {
            resource.setContentType("image/gif");
        } else {
            resource = EmptyResource.instance(libraryName, resourceName);
        }

        return resource;
    }

    private Resource decorateCacheable(Resource resource) {
        return CacheableResourceDecorator.instance(resource);
    }

}
