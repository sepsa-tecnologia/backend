/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.utils;

import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.Numeric;
import java.math.BigDecimal;
import java.util.Collection;
import py.com.sepsa.recepcion.core.data.pojos.DocumentoDetalleInfo;
import py.com.sepsa.recepcion.core.data.pojos.MonedaInfo;
import py.com.sepsa.recepcion.core.data.pojos.OrdenCompraDetalleInfo;
import py.com.sepsa.recepcion.core.database.entities.recepcion.Moneda;

/**
 * NOTE: Algunas funciones existen unica y exclusivamente a manera de facilitar
 * la reutilizacion de codigo en el contexto JSF. Esto se debe principalmente a
 * que en JSF no existe el polimorfismo a la hora de llamar metodos, por lo
 * tanto deben existir algunas con firmas muy especificas para ser utilizadas en
 * las diferentes vistas de la interfaz.
 *
 * @author Fabio A. González Sosa
 */
public final class Texts {

    public static String count(Collection c) {
        int count;
        if (Assertions.isNullOrEmpty(c)) {
            count = 0;
        } else {
            count = c.size();
        }
        return String.valueOf(count);
    }

    public static String monto(Object obj) {
        if (obj instanceof OrdenCompraDetalleInfo) {
            return monto((OrdenCompraDetalleInfo) obj);
        }

        if (obj instanceof DocumentoDetalleInfo) {
            return monto((DocumentoDetalleInfo) obj);
        }

        throw new IllegalArgumentException();
    }

    public static String monto(Object a, Object b) {
        if (a instanceof BigDecimal && b instanceof Moneda) {
            return monto((BigDecimal) a, (Moneda) b);
        }

        if (a instanceof BigDecimal && b instanceof MonedaInfo) {
            return monto((BigDecimal) a, (MonedaInfo) b);
        }

        if (a instanceof BigDecimal && b instanceof String) {
            return monto((BigDecimal) a, (String) b);
        }

        throw new IllegalArgumentException();
    }

    private static String monto(OrdenCompraDetalleInfo obj) {
        return monto(obj.getPrecio(), obj.getMoneda());
    }

    private static String monto(DocumentoDetalleInfo obj) {
        return monto(obj.getPrecio(), obj.getMoneda());
    }

    private static String monto(BigDecimal monto, Moneda moneda) {
        return monto(monto, moneda.getSimbolo());
    }

    private static String monto(BigDecimal monto, MonedaInfo moneda) {
        return monto(monto, moneda.getMoneda().simbolo());
    }

    private static String monto(BigDecimal monto, String simbolo) {
        return Numeric.formatCurrency(monto, simbolo);
    }

}
