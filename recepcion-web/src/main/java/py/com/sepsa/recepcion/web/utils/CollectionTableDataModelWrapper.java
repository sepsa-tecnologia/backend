/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.utils;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.criteria.Condition;
import fa.gs.utils.database.criteria.Pagination;
import fa.gs.utils.database.criteria.Sorting;
import fa.gs.utils.jsfags.jsf.components.impl.table.model.AbstractTableDataModel;
import fa.gs.utils.misc.DataLoader;
import java.util.Collection;

/**
 *
 * @author Fabio A. González Sosa
 */
public class CollectionTableDataModelWrapper<T> extends AbstractTableDataModel<T> {

    private final Collection<T> items;

    private final DataLoader<T> loader;

    public CollectionTableDataModelWrapper() {
        this(null);
    }

    public CollectionTableDataModelWrapper(Collection<T> items) {
        this.items = Lists.empty();
        if (items != null) {
            this.items.addAll(items);
        }

        this.loader = new CollectionTableDataModelWrapper.Loader();
    }

    @Override
    public DataLoader<T> getItemsLoader() {
        return loader;
    }

    private class Loader implements DataLoader<T> {

        private Collection<T> getItems() {
            return CollectionTableDataModelWrapper.this.items;
        }

        @Override
        public Long countItems(Condition[] conditions) {
            int size = getItems().size();
            return size * 1L;
        }

        @Override
        public Collection<T> loadItems(Pagination pagination, Condition[] conditions, Sorting[] sortings) {
            return getItems();
        }

    }
}
