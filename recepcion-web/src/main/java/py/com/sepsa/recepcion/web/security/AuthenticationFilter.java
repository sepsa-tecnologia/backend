/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.recepcion.web.security;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import fa.gs.utils.authentication.tokens.jwt.JwtTokenExtractor;
import fa.gs.utils.authentication.user.AuthenticationInfo;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.text.Joiner;
import fa.gs.utils.rest.responses.ServiceResponse;
import fa.gs.utils.result.simple.Result;
import java.io.IOException;
import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import py.com.sepsa.recepcion.web.security.authentication.UserTokenAuthenticator;

/**
 *
 * @author Fabio A. González Sosa
 */
public class AuthenticationFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ;
    }

    @Override
    public void destroy() {
        ;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        // Contexto de seguridad de Spring.
        SecurityContext securityContext = getSpringSecurityContext(false);
        Authentication securityAuthentication = (securityContext != null) ? securityContext.getAuthentication() : null;

        // Intentar crear una nueva sesion autenticada.
        String token = JwtTokenExtractor.fromHttpRequest((HttpServletRequest) request);

        if (securityAuthentication == null && !Assertions.stringNullOrEmpty(token)) {
            // Autenticar token.
            UserTokenAuthenticator authenticator = new UserTokenAuthenticator();
            Result<AuthenticationInfo> resAuth = authenticator.authenticate(token);
            if (resAuth.isFailure() || resAuth.value() == null) {
                proceed(chain, request, response, resAuth);
                return;
            }

            // Contexto de autenticacion.
            AuthenticationInfo authenticationContext = resAuth.value();

            // Generar "roles" procesables por Spring.
            Collection<GrantedAuthority> authorities = authenticationContext.getPermisos()
                    .stream()
                    .map(pi -> new SimpleGrantedAuthority(pi.name()))
                    .collect(Collectors.toList());

            // Contexto de autenticacion de Spring.
            Principal userPrincipal = authenticationContext.getUserPrincipal();
            UsernamePasswordAuthenticationToken newSecurityAuthentication = new UsernamePasswordAuthenticationToken(userPrincipal, userPrincipal, authorities);
            newSecurityAuthentication.setDetails(authenticationContext);

            // Autenticar sesion.
            securityContext = getSpringSecurityContext(true);
            securityContext.setAuthentication(newSecurityAuthentication);
        }

        chain.doFilter(request, response);
    }

    private SecurityContext getSpringSecurityContext(boolean createIfEmpty) {
        SecurityContext ctx = SecurityContextHolder.getContext();
        if (ctx == null && createIfEmpty) {
            ctx = SecurityContextHolder.createEmptyContext();
        }
        return ctx;
    }

    private void proceed(FilterChain chain, ServletRequest httpRequest, ServletResponse httpResponse, Result result) throws IOException, ServletException {
        Response response = ServiceResponse.ko().cause(result).build();
        response((HttpServletResponse) httpResponse, response);
        chain.doFilter(httpRequest, httpResponse);
    }

    private void response(HttpServletResponse httpResponse, Response response) throws IOException {
        httpResponse.setStatus(response.getStatus());

        for (MultivaluedMap.Entry<String, List<String>> entry : response.getStringHeaders().entrySet()) {
            String name = entry.getKey();
            String value = Joiner.of(entry.getValue()).separator(",").join();
            httpResponse.addHeader(name, value);
        }

        httpResponse.setContentType(String.format("%s/%s", response.getMediaType().getType(), response.getMediaType().getSubtype()));
        httpResponse.setContentLength(response.getLength());

        Object entity = response.getEntity();
        if (entity != null && entity instanceof JsonElement) {
            JsonElement json = (JsonElement) entity;
            Appendable app = httpResponse.getWriter();
            new Gson().toJson(json, app);
        }
    }

}
